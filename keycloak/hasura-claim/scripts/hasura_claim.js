/**
 *  * Available variables:
 *  * user - the current user
 *  * realm - the current realm
 *  * token - the current token
 *  * userSession - the current userSession
 *  * keycloakSession - the current keycloakSession
 *  */

 var defaultRole = "reader";
 var ArrayList = Java.type("java.util.ArrayList");
 var roles = new ArrayList();
 
 user.getRealmRoleMappings().forEach(function (role) {
   roles.add(role.getName());
   role.getAttributes().forEach(function (k, v) {
     if (k === "x-hasura-default-role") {
       defaultRole = v[0];
     }
   });
 });
 
 token.getOtherClaims().put("x-hasura-user-id", user.getId());
 token.getOtherClaims().put("x-hasura-default-role", defaultRole);
 token.getOtherClaims().put("x-hasura-allowed-roles", roles);
 