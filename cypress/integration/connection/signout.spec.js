/// <reference types="cypress" />

describe("Sign Out", () => {
  beforeEach(function () {
    cy.fixture("users").then(function (data) {
      this.data = data;
      cy.login(this.data.admin.username, this.data.admin.password);
    });
  });

  it("success logout", function () {
    cy.logout();
  });
});
