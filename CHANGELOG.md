# Changelog

All notable changes to this project will be documented in this file.

This project does not adhere to [Semantic Versioning](http://semver.org/) yet.
Until 1.0 relese candidate, we use versionning based on date and deploy count.

Here is the current format: 0.{YY}{MM}.{DEPLOY_NUMBER_IN_CURRENT_MONTH_BASE100}

## [1.3.6] - 2024-05-13

### Added

- On interventions grid, i can export all data or filtered data to CSV
- Use IGN services for satellite layer and update url for ign layer
- Worksite creation: on click on map i can select or remove a location from selection
- Date range filters on interventions grid
- Use last version of material-react-table (version 1 to 2)

### Fixed

- Versions of storybook packages
- SelectionLayer for worksite creation
- Filters on all grids specially date filters
- Diagnosis filters on map filters

[PR-269](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/269)
[PR-268](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/268)
[PR-267](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/267)

## [1.3.5] - 2024-04-18

### Added

- As an admin, on account page, i can see all of users, and create/edit/delete if need
- Enhancements dashboard performances and sector filtering
- Enhancements interventions grid filters
- Intervention modal for edit/delete/declare realized intervention (remove intervention/{id} page)

### Fixed

- Map loader and worksite mode

[PR-230](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/230)
[PR-260](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/260)
[PR-263](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/263)

## [1.3.4] - 2024-04-10

### Added

- Green area table but button for adding one temporally disabled because wip

### Fixed

- Data fetching for dashboard without graphql actions (remove it), use directly api calls
- Worksite form: intervention partner id on creation
- Dashboard filter dangerous trees
- Import process for dijon data

[PR-250](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/250)
[PR-244](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/244)
[PR-243](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/243)
[PR-252](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/252)

## [1.3.3] - 2024-03-27

### Added

- Backend method to associate tree to taxon
- Modal for add diagnosis
- Remove pages intervention/add diagnosis/add => only use modal for add
- New fields on diagnosis form, location form and tree form
- Seed files for new fields
- Seed file for "Le Gosier" sectors
- Replace label by slug on database for internationalization

### Fixed

- Refactor: remove some duplicate components & code for Location and Tree form (use only LocationFields & TreeFields)
- Use satellite-v2 maptiler

[PR-219](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/219)
[PR-241](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/241)
[PR-245](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/245)
[PR-240](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/240)

## [1.3.2] - 2024-03-11

### Added

- Seed file for Alès sectors

### Fixed

- Get late interventions method
- Scheduled date on worksite form (replace datepicker)
- Style
- Retroactions on interventions validations (felling, planting and grubbing)

[PR-235](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/235)
[PR-236](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/236)
[PR-234](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/234)
[PR-233](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/233)

## [1.3.1] - 2024-02-20

### Added

- New graph on dashboard for species distribution
- New page /diagnosis to see all diagnosis
- New page /account
- Add button on tree card to see all diagnosis of this tree
- Use martin service for intervention and diagnosis modals
- Add indexes on dashboard data
- Keep dashboard filters persistent

### Fixed

- Urbasense pydantic models

[PR-223](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/223)
[PR-222](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/222)
[PR-221](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/221)
[PR-208](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/208)
[PR-207](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/207)
[PR-218](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/218)
[PR-217](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/217)
[PR-216](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/216)
[PR-215](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/215)

## [1.3.0] - 2024-01-31

### Added

- Harmonizing the visuals on interventions list page
- Only one typography "Poppins" and native typo if this one not supported
- New analytics on dashboard: well-being indice, number of trees of interest and trees to cut down
- New module on dashboard to see only late interventions
- Actualize analytics data with sector filter
- New map filter: trees equiped with urbasense captor
- Update database schema (new tables and join tables)

### Fixed

- Intervention partner field is not required anymore on intervention form
- Version of SQLAlchemy

[PR-204](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/204)
[PR-212](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/212)
[PR-211](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/211)
[PR-200](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/200)
[PR-209](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/209)
[PR-206](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/206)

## [1.2.0] - 2024-01-11

### Added

- Use map page implemented with martin service on principal map page
- Reload martin map layer when add location
- Filters logic backend and frontend
- New design pictures for locations card infos
- Add GIST index on coords table location
- Add Btree indexes on filtered data for map

### Fixed

- Import process, if coords already exists in database skip insertion but continue the process for other rows

[PR-189](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/189)
[PR-190](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/190)
[PR-194](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/194)
[PR-195](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/195)
[PR-196](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/196)

## [1.1.2] - 2023-12-21

### Added

- On location map card, add zoom controls and geolocalisation
- Update pydantic models for urbasense new response api model
- When quit edition mode without saving, open modal. If confirm, reset fields

### Fixed

- On each deploy, Meilisearch data is persistent now
- Fetch suggested address when lat/lng change
- Yup schema validation for height (type numeric now)
- When delete a location, if this is the only location concerned by a worksite, delete the worksite

[PR-181](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/181)
[PR-183](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/183)
[PR-184](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/184)
[PR-185](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/185)

## 2023-12-13

Frontend and design :

- Design on desktop and mobile headers
- Responsive design
- Add Cypress IDs to components
- Worksite page: map centered on locations

### Added

- Header design on desktop and mobile
- New mobile design for tree/location cards
- Toolbar on map page for tablet and mobile
- Map active filters information
- Cypress attributes on map page

[PR-177](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/177)
[PR-179](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/179)
[PR-175](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/175)
[PR-172](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/172)
[PR-173](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/173)

## [1.1.1] - 2023-11-30

- Integrate new backend dashboard metrics (non_allergenic, vulnerability)
- Integrate frontend dashboard metrics (non_allergenic, vulnerability)

### Added

Backend dashboard metrics:

- Non allergenic calculation
- Vulnerability calculation

  [PR-159]https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/159

  [PR-168]https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/168

### Fixed

- Fix minio image url: add DEFAULT_STORAGE_BUCKET_NAME on publicRuntimeConfig
- Fix import process error on address column

## [1.1.0] - 2023-11-24

- Definie: integrate backend dashboard metrics (air, carbon, icu, biodiversity)
- New feature: integrate frontend dashboard metrics (air, carbon, icu, biodiversity)

### Added

Backend dashboard metrics:

- Carbon metric calculation
- Air quality metric calculation
- Biodiversity metric calculation
- ICU metric calculation
- Python microservices metrics API added to GraphQL endpoint
- Add Cypress IDs to components

[PR-153](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/153)

### Changed

- Resolve react-i18next build errors, need to upgrade typescript version for working
- Upgrade typescript version from 4 to 5 - [PR-152](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/152)

### Fixed

- Set a default release version for MinIO setup.
- Mobile drawer display for worksite creationn from map
- Cardinfo hover effects

## [0.2311.02] - 2023-11-03

Mainly trying to resolve conflict on `main` branch due to missing hotfixes on `dev` branch

### Added

### Changed

### Fixed

- Syncing `main` and `dev`

## [0.2311.01] - 2023-11-03

This is the initial version release after months of active development. List of additions, changes, fixes are not fully detailled and exhaustive. Following versions will be.

### Added

#### Stack

- PostGIS **database** with Hasura **GraphQL engine**
- **Python microservices** with FastAPI
- **Search engine** with Meilisearch
- **Vector tiles service** with Martin
- **Authentification service** with Keycloak
- **Networking** with Traefik
- **Webapp** with NextJS and map visualisation GL engine

#### Features

- Add and import locations and trees
- Manage interventions on tree heritage with callback actions
- Manage tree health diagnosis
- Dashboarding
- Map visualisation and filtering
- External real-time tree monitoring (Urbasense integration)
- Manage worksites (multiple locations and interventions management)
- Geographical and administrative boundaries support
- Tree species search engine

### Changed

### Fixed
