from events.organization import router_organization
from fastapi import APIRouter

events_router = APIRouter(prefix="/events")
events_router.include_router(router_organization, prefix="/organization")
