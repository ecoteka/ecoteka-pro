import json

import boto3
from botocore.client import Config
from core.config import settings

config_dict = {
    "endpoint_url": settings.minio_entrypoint_url,
    "aws_access_key_id": settings.minio_root_user,
    "aws_secret_access_key": settings.minio_root_password,
    "config": Config(
        signature_version="s3v4",
    ),
    "region_name": "fr",
}

s3 = boto3.resource("s3", **config_dict)
s3_client = boto3.client("s3", **config_dict)

def create_bucket(bucket_name: str):
    s3_bucket = s3.Bucket(bucket_name)
    if s3_bucket not in s3.buckets.all():
        s3.create_bucket(Bucket=bucket_name)


def create_default_public_bucket():
    # create default bucket from envvars
    create_bucket(settings.minio_default_bucket)
    # set bucket policy to public read
    default_bucket_policy = {
    'Version': '2012-10-17',
    'Statement': [{
        'Sid': 'AddPerm',
        'Effect': 'Allow',
        'Principal': '*',
        'Action': ['s3:GetObject'],
        'Resource': f'arn:aws:s3:::{settings.minio_default_bucket}/*'
    }]
    }
    bucket_policy = json.dumps(default_bucket_policy)
    s3_client.put_bucket_policy(Bucket=settings.minio_default_bucket, Policy=bucket_policy)
    result = s3_client.get_bucket_policy(Bucket=settings.minio_default_bucket)
    return json.loads(result['Policy'])


def remove_bucket(bucket_name: str):
    if s3.Bucket(bucket_name) in s3.buckets.all():
        bucket = s3.Bucket(bucket_name)
        bucket.objects.all().delete()
        bucket.delete()
