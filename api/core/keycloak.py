import functools

from core.config import settings
from fastapi import FastAPI
from fastapi.security import OAuth2PasswordBearer
from fastapi_keycloak import FastAPIKeycloak


class EcotekaKeycloak(FastAPIKeycloak):
    def __init__(self, token_uri, **kwargs) -> None:
        self._token_uri = token_uri
        super().__init__(**kwargs)

    @functools.cached_property
    def user_auth_scheme(self) -> OAuth2PasswordBearer:
        """Returns the auth scheme to register the endpoints with swagger
        Returns:
            OAuth2PasswordBearer: Auth scheme for swagger
        """
        return OAuth2PasswordBearer(tokenUrl=self._token_uri)


idp = EcotekaKeycloak(
    admin_client_secret=settings.keycloak_admin_secret,
    callback_uri=settings.keycloak_callback_uri,
    client_id=settings.keycloak_client_id,
    client_secret=settings.keycloak_client_secret,
    realm=settings.keycloak_realm,
    server_url=settings.keycloak_server_url,
    timeout=settings.keycloak_timeout,
    token_uri=settings.keycloak_token_uri
)


def init_routes(app: FastAPI):
    @app.get("/callback", tags=["auth-flow"])
    def callback(session_state: str, code: str):
        return idp.exchange_authorization_code(session_state=session_state, code=code)


def init_keyclaok(app: FastAPI):
    init_routes(app)
    idp.add_swagger_config(app)
