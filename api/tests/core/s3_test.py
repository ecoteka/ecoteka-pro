import pytest
from core.s3 import create_bucket, remove_bucket, s3
from moto import mock_s3

TEST_BUCKET_NAME = "my-test-bucket"


@pytest.fixture(scope="function")
def mock_s3_resource():
    with mock_s3():
        yield s3


def test_create_bucket(mock_s3_resource):
    assert TEST_BUCKET_NAME not in [b.name for b in s3.buckets.all()]

    create_bucket(TEST_BUCKET_NAME)

    assert TEST_BUCKET_NAME in [b.name for b in s3.buckets.all()]

    remove_bucket(TEST_BUCKET_NAME)


def test_remove_bucket(mock_s3_resource):
    s3.create_bucket(Bucket=TEST_BUCKET_NAME)

    assert TEST_BUCKET_NAME in [b.name for b in s3.buckets.all()]

    remove_bucket(TEST_BUCKET_NAME)

    assert TEST_BUCKET_NAME not in [b.name for b in s3.buckets.all()]
