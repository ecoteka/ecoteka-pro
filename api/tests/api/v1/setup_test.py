import uuid


def test_protected_setup(client, access_token, faker):
    headers = {"Authorization": f"Bearer {access_token}"}
    page = "Q148775"
    domain = f"{faker.domain_name()}.test"
    default = True
    params = {"page": page, "domain": domain, "default": default}
    response = client.post("/admin/setup", headers=headers, params=params)
    assert response.status_code == 200

    data = response.json()["insert_organization_one"]
    assert data["domain"] == domain
    assert data["default"] == default
    assert isinstance(uuid.UUID(data["id"]), uuid.UUID)
    assert isinstance(data["name"], str)
    assert isinstance(data["image"], str)
    assert isinstance(data["population"], float)
    assert isinstance(data["surface"], float)
