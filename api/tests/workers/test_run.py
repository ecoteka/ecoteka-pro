from core.config import settings
from workers.run import celery_app


def test_celery_app_config():
    assert celery_app.conf.broker_url == settings.worker_broker
    assert celery_app.conf.result_backend == settings.worker_backend
