from workers.imports.tasks import toto


def test_toto_task(celery_worker, celery_app):
    task = celery_app.send_task("workers.imports.tasks.toto")
    result = task.result
    assert result == toto()
