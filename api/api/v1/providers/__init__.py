from fastapi import APIRouter

from api.v1.providers.urbasense import router as urbasense_router

router = APIRouter(prefix="/providers")
router.include_router(urbasense_router, prefix="/urbasense")
