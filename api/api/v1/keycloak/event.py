from typing import Optional

import gql
from core.gql import get_gql_client
from fastapi import APIRouter, Depends
from pydantic import BaseModel

router = APIRouter()


class Event(BaseModel):
    type: str
    userId: str
    username: Optional[str]


@router.post("")
async def event(event: Event, gql_client: gql.Client = Depends(get_gql_client)):
    if event.type in ["CREATE", "UPDATE"]:
        document = gql.gql(
            """
            mutation upsert_intervention_partner(
                $name: String = ""
                $user_id: String = ""
            ) {
                insert_intervention_partner_one(
                    object: { name: $name, user_id: $user_id }
                    on_conflict: {
                    constraint: intervention_partner_name_key
                    update_columns: name
                    }
                ) {
                    name
                    id
                    user_id
                }
            }
            """
        )

        variables = {"name": event.username, "user_id": event.userId}
        result = await gql_client.execute_async(
            document,
            variables,
        )

        return result

    if event.type == "DELETE":
        document = gql.gql(
            """
            mutation delete_intervention_partner($user_id: String = "") {
                delete_intervention_partner(where: { user_id: { _eq: $user_id } }) {
                    returning {
                        user_id
                    }
                }
            }

            """
        )

        variables = {"user_id": event.userId}
        result = await gql_client.execute_async(
            document,
            variables,
        )

        return result
