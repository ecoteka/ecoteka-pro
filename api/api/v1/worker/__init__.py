from fastapi import APIRouter

from api.v1.worker import tasks, toto, imports

router = APIRouter()
router.include_router(toto.router, prefix="/toto")
router.include_router(imports.router, prefix="/imports")
router.include_router(tasks.router, prefix="/tasks")
