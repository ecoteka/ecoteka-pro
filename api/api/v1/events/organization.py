from core.s3 import create_bucket, remove_bucket
from fastapi import APIRouter, Request
from pydantic import BaseModel


class Event(BaseModel):
    data: dict


router = APIRouter()


@router.post("")
async def organization(request: Request):
    json = await request.json()

    if json["event"]["op"] == "INSERT":
        create_bucket(json["event"]["data"]["new"]["bucket"])
    elif json["event"]["op"] == "DELETE":
        remove_bucket(json["event"]["data"]["old"]["bucket"])

    return json
