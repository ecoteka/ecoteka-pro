from api.v1.events.organization import router as organization_router
from api.v1.events.tree_location_import import router as tree_location_import_router
from fastapi import APIRouter

router = APIRouter(prefix="/events")
router.include_router(organization_router, prefix="/organization")
router.include_router(tree_location_import_router, prefix="/tree_location_import")
