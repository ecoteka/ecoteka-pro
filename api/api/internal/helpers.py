from shapely import wkb
import binascii
from core.config import settings
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, text

engine = create_engine(settings.db_postgresql_url)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

def execute_sql_query(sql_query, params=None):
    db = SessionLocal()
    try:
        if params is not None:
            return db.execute(sql_query, params).fetchall()
        else:
            return db.execute(sql_query).fetchall()
    finally:
        db.close()

def get_unique_column_values(table_name, column_name):
    sql_query = text(f"""
    SELECT DISTINCT {column_name}
    FROM {table_name}
    """)

    rows = execute_sql_query(sql_query)
    return [row[0] for row in rows]

def get_oldest_date(table_name, column_name):
    sql_query = text(f"""
    SELECT MIN(t.{column_name})
    FROM {table_name} t
    """)

    row = execute_sql_query(sql_query)
    oldest_date = row[0][0] if row else None
    return oldest_date

from sqlalchemy import text

def inclusive_filters(table_name, conditions, params=None, joins=None):
    where_clause = " AND ".join([f'"{column}" = :{column}' for column in conditions])

    sql_query = text(f"""
        SELECT * FROM {table_name}
        WHERE {where_clause}
    """)

    boundary_ids = getattr(params, 'boundary_ids', None)
    if boundary_ids and boundary_ids != []:
        boundary_ids_str = ', '.join(f"'{x}'" for x in boundary_ids)

        if joins is None:
            join_clause = f"JOIN boundaries_locations bl ON {table_name}.location_id = bl.location_id"
        else:
            join_clause = joins

        sql_query = text(f"""
            SELECT {table_name}.*
            FROM {table_name}
            {join_clause}
            WHERE {where_clause} AND bl.boundary_id IN ({boundary_ids_str})
        """)

    bound_query = sql_query.bindparams(**conditions)

    rows = execute_sql_query(bound_query)

    return rows



def exclusive_filter(table_name, column_name, excluded_value):
    sql_query = text(f"""
    SELECT * FROM {table_name}
    WHERE "{column_name}" != :excluded_value;
    """)

    rows = execute_sql_query(sql_query, {"excluded_value": excluded_value})
    return rows


def convert_coords_to_geojson(wkb_data):
    if wkb_data is None:
        return None

    binary_data = binascii.unhexlify(wkb_data)
    point = wkb.loads(binary_data)

    return {
        "type": "Point",
        "crs": {
            "type": "name",
            "properties": {
                "name": "urn:ogc:def:crs:EPSG::4326"
            }
        },
        "coordinates": [point.x, point.y]
    }

def calculate_score(column_name: str, params):
    boundary_ids_condition = ''
    if params and params.boundary_ids:
        boundary_ids_str = ','.join(f"'{x}'" for x in params.boundary_ids)
        boundary_ids_condition = f'AND boundaries_locations.boundary_id IN ({boundary_ids_str})'

    sql_query = f"""
    SELECT CAST(AVG(CAST({column_name} AS FLOAT)) AS NUMERIC(10,1)) AS mean_score
    FROM tree
    INNER JOIN taxon ON tree.taxon_id = taxon.id
    LEFT JOIN boundaries_locations ON tree.location_id = boundaries_locations.location_id
    WHERE taxon.growth_category IS NOT NULL
    {boundary_ids_condition};
    """

    sql_query_text = text(sql_query)

    result = execute_sql_query(sql_query_text)

    mean_score = result[0][0] if result else None

    result_key = f"{column_name}_score" if column_name != "allergenic_score" else f"non_{column_name}"
    return [{result_key: mean_score if mean_score is not None else 0}]
