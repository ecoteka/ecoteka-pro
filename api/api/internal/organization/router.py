from fastapi import APIRouter
from api.internal.organization.services import (
    get_default_organization,
)

router = APIRouter()

@router.get("/default")
async def fetch_default_organization():
    return get_default_organization()
