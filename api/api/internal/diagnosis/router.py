from fastapi import APIRouter, Body
from api.internal.diagnosis.services import (
    get_dangerous_tree
)
from api.internal.metrics.models import Params


router = APIRouter()

@router.post("/dangerous")
async def fetch_dangerous_tree(params: Params = Body(...)):
    return get_dangerous_tree(params)
