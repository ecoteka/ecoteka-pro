from api.internal.helpers import (
    inclusive_filters,
)

def get_dangerous_tree(params):
    conditions = {
        "tree_is_dangerous": True,
        "recommendation": "felling"
    }

    joins = """
        LEFT JOIN tree tr ON diagnosis.tree_id = tr.id
        LEFT JOIN boundaries_locations bl ON tr.location_id = bl.location_id
        """

    response = inclusive_filters("diagnosis", conditions, params, joins=joins)

    if not response:
        return {}

    # Transform the response into the desired format
    data = [
        {
            "id": row.id,
            "tree_is_dangerous": row.tree_is_dangerous,
            "__typename": "diagnosis"
        } for row in response
    ]

    return data

