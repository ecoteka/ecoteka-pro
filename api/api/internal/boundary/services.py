from api.internal.helpers import (
    exclusive_filter,
)

def get_boundaries_except_stations():
    response = exclusive_filter("boundary", "type", "station")
    if not response:
        return {}
    data = [
        {
            "id": row.id,
            "name": row.name,
            "type": row.type,
            "__typename": "boundary"
        }
        for row in response
    ]
    return data
