from fastapi import APIRouter
from api.internal.boundary.services import (
    get_boundaries_except_stations
)


router = APIRouter()

@router.get("/except/stations")
async def fetch_boundaries_except_stations():
    return get_boundaries_except_stations()
