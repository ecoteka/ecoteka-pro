from __future__ import annotations

from datetime import datetime
from enum import Enum
from typing import Any, List, Optional, Dict

from pydantic import BaseModel


class Provider(str, Enum):
    urbasense = 'urbasense'
    ecoteka = 'ecoteka'

class GroupType(str, Enum):
    site = 'site'
    group = 'group'
    sector = 'sector'
    subjects = 'subjects'

class GroupParams(BaseModel):
    provider: Provider
    id: Optional[int]
    type: GroupType
    ids: Optional[List[int]]

class Event(BaseModel):
    data: dict

class Error(BaseModel):
    error: bool
    msg: str
    status: int

class AmcMoyJour(BaseModel):
    nom: str
    valeur: Optional[float]
    badge: str
    unite: str
    couleur: str
    tooltip: str


class StressThermMoyJour(BaseModel):
    nom: str
    valeur: Optional[float]
    badge: str
    unite: str
    couleur: str
    tooltip: str


class CroissanceMoyMois(BaseModel):
    nom: str
    valeur: Optional[float]
    badge: str
    unite: str
    couleur: str
    tooltip: str


class Msg(BaseModel):
    listeParams: List[str]
    amcMoyJour: AmcMoyJour
    stressThermMoyJour: StressThermMoyJour
    croissanceMoyMois: CroissanceMoyMois


class Dendro(BaseModel):
    msg: Msg
    error: bool
    status: int


class NbSujets(BaseModel):
    nom: str
    abbrege: Optional[str]
    valeur: int
    badge: str
    unite: str
    couleur: str
    tooltip: str


class TempAirMax(BaseModel):
    nom: str
    abbrege: Optional[str]
    unite: str
    tooltip: str
    valeurs: List[int]
    valeursParSujet: Optional[Dict[str, List[Optional[int]]]]
    dates: List[str]

class HumAirMoy(BaseModel):
    nom: str
    abbrege: Optional[str]
    unite: str
    tooltip: str
    valeurs: List[int]
    valeursParSujet: Optional[Dict[str, List[Optional[int]]]]
    dates: List[str]

class VitVentMax(BaseModel):
    nom: str
    abbrege: Optional[str]
    unite: str
    tooltip: str
    valeurs: List[int]
    valeursParSujet: Optional[Dict[str, List[Optional[int]]]]
    dates: List[str]

class UTCI(BaseModel):
    nom: str
    abbrege: Optional[str]
    unite: str
    tooltip: str
    valeurs: List[int]
    valeursParSujet: Optional[Dict[str, List[Optional[int]]]]
    dates: List[str]

class Msg1(BaseModel):
    listeParams: List[str]
    nbSujets: NbSujets
    tempAirMax: TempAirMax
    humAirMoy: HumAirMoy
    vitVentMax: VitVentMax
    utci: UTCI


class Meteo(BaseModel):
    msg: Msg1
    error: bool
    status: int


class NbSujets1(BaseModel):
    nom: str
    abbrege: Optional[str]
    valeur: int
    badge: str
    unite: str
    couleur: str
    tooltip: str


class RurAmplitude(BaseModel):
    nom: str
    abbrege: Optional[str]
    valeur: Optional[int]
    badge: str
    unite: str
    couleur: str
    tooltip: str



class Arrosage(BaseModel):
    nom: str
    abbrege: Optional[str]
    valeur: Optional[datetime]
    badge: str
    unite: str
    couleur: str
    tooltip: str



class Msg2(BaseModel):
    listeParams: List[str]
    nbSujets: NbSujets1
    rurMin: RurAmplitude
    rurMoy: RurAmplitude
    rurMax: RurAmplitude
    arrosageProchain: Arrosage
    arrosageDernier: Arrosage


class Tensio(BaseModel):
    msg: Msg2
    error: bool
    status: int

class MetricsByGroupResponseModel(BaseModel):
    dendro: Dendro | Error
    meteo: Meteo | Error
    tensio: Tensio | Error


class TensioItem(BaseModel):
    Date: str
    series: str
    value: int
    series_name: str
    unite: str


class TemperatureItem(BaseModel):
    Date: str
    series: str
    value: int
    unite: str
    series_name: str


class MeteoItem(BaseModel):
    Date: str
    pp_mmjour: str
    tair_max: str


class SubjectMonitoring(BaseModel):
    tensio: List[TensioItem]
    temperature: List[TemperatureItem]
    meteo: List[MeteoItem]


class Datum(BaseModel):
    S_1: int
    S_2: int
    S_3: int
    S_t: int
    date_mes: str
    date_originale: int


class LegendeItem(BaseModel):
    coul_code: str
    coul_nom: str
    dist: str
    dist_ref: str
    id_suj: str
    marque_capteur: str
    modele_capteur: str
    nom_sonde: str
    num_sonde: str
    precision_capteur: str
    prof: str
    propriete: str
    ref: str
    ref_sond: str
    resolution_capteur: str
    unite: str


class SubjectRawMeteoItem(BaseModel):
    date_mes: str
    pp_mmjour: str
    tair_max: str


class LastPrecoItem(BaseModel):
    date_envoi_mail: str
    date_mes: str
    id_suj: str
    materiel_code: str
    picto: str
    preco: str
    preco_code: str
    preco_txt: str
    quantite: int
    risque: str
    risque_code: str


class TxpBilan(BaseModel):
    bilan: List[str]


class Sujet(BaseModel):
    annee: str
    device: str
    dispo_code: str
    dispo_img_url: str
    dispo_mode: str
    dispo_txt: str
    fin_suivi: Any
    id_suj: str
    isadmin: str
    last_preco: List[LastPrecoItem]
    lat: str
    lon: str
    nom_groupes: str
    nom_site: str
    nom_suj: str
    picto_type: str
    position: str
    ref_ext_gid: Any
    ref_ext_gtype: Any
    ref_groupes: List[str]
    ref_site: str
    ref_txp: str
    remarque: str
    tead_max: str
    txp_bilan: TxpBilan
    type_arro: Any
    type_essence: str
    type_essence_txt: str
    type_strate: str
    urlS: str


class UrbasenseSubjectMonitoringRaw(BaseModel):
    data: List[Datum]
    error: bool
    legende: List[LegendeItem]
    meteo: List[SubjectRawMeteoItem]
    status: int
    sujet: Sujet

class MonitoringDataShapeEnum(str, Enum):
    raw = "raw"
    series = "series"
    combo = "combo"
    dimensions = "dimensions"

class ComboPlotDatum(BaseModel):
    date_mes: str
    S_t: Optional[int]
    S_1: Optional[int]
    S_2: Optional[int]
    S_3: Optional[int]
    pp_mmjour: Optional[str]
    tair_max: Optional[str]


class ComboPlotDatumMetadatum(BaseModel):
    id: str
    alias: str
    unite: str


class ComboPlotModel(BaseModel):
    data: List[Datum]
    meta: List[ComboPlotDatum]
