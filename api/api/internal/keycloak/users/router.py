from fastapi import APIRouter, Body, Header

from .services import (
    create_keycloak_user,
    delete_keycloak_user,
    get_keycloak_users,
    set_password_keycloak_user,
    update_keycloak_user,
)

router = APIRouter()

@router.get("/users")
async def list_users():
    users = await get_keycloak_users()
    return users

@router.delete("/users/delete")
async def delete_user(user_id: str = Header(...)):
    deleted_user = await delete_keycloak_user(user_id)
    return deleted_user

@router.put("/users/update")
async def update_user(user_id: str = Header(...), data: dict = Body(...)):
    updated_user = await update_keycloak_user(user_id, data)
    return updated_user

@router.post("/users/create")
async def create_user(data: dict = Body(...)):
    created_user = await create_keycloak_user(data)
    return created_user

@router.put("/users/password")
async def password_user(user_id: str = Header(...), data = Body(...)):
    users = await set_password_keycloak_user(user_id, data)
    return users
