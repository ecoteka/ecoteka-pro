from fastapi import APIRouter, Body, Header

from .services import (
    get_realm_roles
)

router = APIRouter()

@router.get("/roles")
async def list_roles():
    roles = await get_realm_roles()
    return roles
