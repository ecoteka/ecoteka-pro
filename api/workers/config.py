from core.config import settings


class CeleryConfig:
    broker_url = settings.worker_broker
    result_backend = settings.worker_backend
