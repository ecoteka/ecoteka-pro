from celery import Celery
from workers.config import CeleryConfig
from celery.schedules import crontab

celery_app = Celery(__name__)
celery_app.config_from_object(CeleryConfig)
celery_app.conf.timezone = 'Europe/Paris'
celery_app.autodiscover_tasks(["workers.imports", "workers.metrics"])

celery_app.conf.beat_schedule = {
    'get_urbasense_site_metrics_everyday': {
        'task': 'metrics.tasks.get_urbasense_site_metrics',
        'schedule': crontab(minute='*/15'),
    },
}
