
import gql
from gql.transport.aiohttp import AIOHTTPTransport
from workers.core.config import logger, settings


# TODO : try to configure DSL from GraphQL Schema
# see https://gql.readthedocs.io/en/latest/advanced/dsl_module.html
def get_gql_client():
    headers = {"x-hasura-admin-secret": f"{settings.hasura_admin_secret}"}
    transport = AIOHTTPTransport(url=settings.graphql_endpoint, headers=headers)
    client = gql.Client(transport=transport)

    yield client

get_client = get_gql_client()

def insert_location(variables: dict):
    headers = {"x-hasura-admin-secret": f"{settings.hasura_admin_secret}"}
    transport = AIOHTTPTransport(url=settings.graphql_endpoint, headers=headers)
    client = gql.Client(transport=transport)
    
    mutation = gql.gql(
        """
       mutation (
        $address: String = "",
        $coords: geometry = "",
        $data_entry_user_id: String = "",
        $id_status: uuid = "bf6fcfe3-fbe9-4467-98be-a935efcf67a3",
        $organization_id: uuid = "52452b85-2efd-4427-a4fa-a31bba82fbda"
        $foot_type: String = "description"
        $plantation_type: String = ""
        $sidewalk_type: String = ""
        $urban_site_id: uuid = null
        $landscape_type: String = ""
        $is_protected: Boolean = false
        ) {
    insert_location_one(
        object: {
                    address: $address
                    coords: $coords
                    foot_type: $foot_type
                    plantation_type: $plantation_type
                    urban_site_id: $urban_site_id
                    sidewalk_type: $sidewalk_type
                    landscape_type: $landscape_type
                    is_protected: $is_protected
                    data_entry_user_id: $data_entry_user_id
                    id_status: $id_status,
                    organization_id: $organization_id
                },
                on_conflict: {
                    constraint: location_coords_key,
                }
            ) { 
                id,
                coords,
                address
            }
        }

    """
    )

    result = client.execute(
         mutation,
         variable_values=variables,
    )

    return result

def insert_location_urban_constraint(variables: dict):
    headers = {"x-hasura-admin-secret": f"{settings.hasura_admin_secret}"}
    transport = AIOHTTPTransport(url=settings.graphql_endpoint, headers=headers)
    client = gql.Client(transport=transport)
    
    mutation = gql.gql(
        """
       mutation (
          $location_id: uuid = ""
          $urban_constraint_id: uuid = ""
        ) {
    insert_location_urban_constraint(
        objects: {
                    location_id: $location_id
                    urban_constraint_id: $urban_constraint_id
                }
            ) { 
                affected_rows
            }
        }

    """
    )

    result = client.execute(
         mutation,
         variable_values=variables,
    )

    return result

def insert_location_boundary(variables: dict):
    headers = {"x-hasura-admin-secret": f"{settings.hasura_admin_secret}"}
    transport = AIOHTTPTransport(url=settings.graphql_endpoint, headers=headers)
    client = gql.Client(transport=transport)
    
    mutation = gql.gql(
        """
       mutation (
          $location_id: uuid = ""
          $boundary_id: uuid = ""
        ) {
    insert_boundaries_locations_one(
        object: { boundary_id: $boundary_id, location_id: $location_id }
            ) { 
                id
            }
        }

    """
    )

    result = client.execute(
         mutation,
         variable_values=variables,
    )

    return result

def insert_tree(variables: dict):
    headers = {"x-hasura-admin-secret": f"{settings.hasura_admin_secret}"}
    transport = AIOHTTPTransport(url=settings.graphql_endpoint, headers=headers)
    client = gql.Client(transport=transport)
    
    mutation = gql.gql(
        """
       mutation (
        $watering: Boolean = false
        $scientific_name: String = ""
        $vernacular_name: String = ""
        $plantation_date: String = ""
        $serial_number: String = ""
        $note: String = ""
        $location_id: uuid = ""
        $is_tree_of_interest: Boolean = false
        $variety: String = ""
        $habit: String = ""
        $height: numeric = null
        $estimated_date: Boolean = false
        $circumference: numeric = 10
        $data_entry_user_id: String = "c8c8b8a0-f8f6-4581-b8a7-3a812eb199cb"
        $taxon_id: Int = null
        $urbasense_subject_id: String = null
        ) {
    insert_tree_one(
        object: {
        circumference: $circumference
        variety: $variety
        habit: $habit
        height: $height
        is_tree_of_interest: $is_tree_of_interest
        location_id: $location_id
        note: $note
        plantation_date: $plantation_date
        vernacular_name: $vernacular_name
        scientific_name: $scientific_name
        serial_number: $serial_number
        estimated_date: $estimated_date
        watering: $watering
        data_entry_user_id: $data_entry_user_id
        taxon_id: $taxon_id
        urbasense_subject_id: $urbasense_subject_id
                }
            ) { 
            id
            location_id
            }
        }

    """
    )

    result = client.execute(
         mutation,
         variable_values=variables,
    )

    return result

def insert_diagnosis(variables: dict):
    headers = {"x-hasura-admin-secret": f"{settings.hasura_admin_secret}"}
    transport = AIOHTTPTransport(url=settings.graphql_endpoint, headers=headers)
    client = gql.Client(transport=transport)

    mutation = gql.gql(
        """
       mutation (
        $diagnosis_date: date = "2024-04-10"
        $note: String = ""
        $tree_id: uuid = ""
        $tree_condition: String = ""
        $crown_condition: String = ""
        $trunk_condition: String = ""
        $collar_condition: String = ""
        $data_entry_user_id: String = ""
        $recommendation: String = ""
        $diagnosis_type_id: uuid = "3c96f732-8ea1-4c44-9800-c9ea6bd0c7c9"
        $tree_is_dangerous: Boolean = false
        $tree_vigor: String = ""
        $analysis_results: String = ""
        $carpenters_condition: String = ""
        ) {
      insert_diagnosis_one(
    object: {
      tree_id: $tree_id
      note: $note
      diagnosis_date: $diagnosis_date
      tree_condition: $tree_condition
      crown_condition: $crown_condition
      trunk_condition: $trunk_condition
      collar_condition: $collar_condition
      recommendation: $recommendation
      diagnosis_type_id: $diagnosis_type_id
      data_entry_user_id: $data_entry_user_id
      tree_is_dangerous: $tree_is_dangerous
      tree_vigor: $tree_vigor
      analysis_results: $analysis_results
      carpenters_condition: $carpenters_condition
        }
        ) 
        { 
            id
            }
        }
    """
    )

    result = client.execute(
         mutation,
         variable_values=variables,
    )

    return result