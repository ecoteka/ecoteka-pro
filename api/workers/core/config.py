
import os

from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)

class Settings():
    canonical_url: str = os.getenv("CANONICAL_URL")
    db_url: str = os.getenv("DB_URL")
    hasura_admin_secret: str = os.getenv("HASURA_GRAPHQL_ADMIN_SECRET")
    minio_bucket_name: str = os.getenv("MINIO_BUCKET_NAME")

    graphql_endpoint: str = "http://graphql_engine:8080/v1/graphql"

    keycloak_admin_secret: str = "Chk2aziqwPNsszciMMlWw3w9K4NpOKlc"
    keycloak_callback_uri: str = "http://api/callback"
    keycloak_client_id: str = "ecoteka"
    keycloak_client_secret: str = "Rm414Jw6krdbjhjpmHDBxCkof7RjguS0"
    keycloak_realm: str = "ecoteka"
    keycloak_server_url: str = "http://keycloak:8080/auth"
    keycloak_timeout: int = 10

    meili_master_key: str = os.getenv("MEILI_MASTER_KEY")
    meili_endpoint: str = os.getenv("MEILI_ENDPOINT")
    meilisearch_master_key: str = "yzyzeuyee4567UYUYYIY"

    minio_entrypoint_url: str = "http://minio:8888"
    minio_root_password: str = "password"
    minio_root_user: str = "ecoteka"
    urbasense_api_access_token: str = os.getenv("URBASENSE_TOKEN")
    urbasense_monitored_site_id: int = 243
    urbasense_api_base_url: str = "https://arbre.urbasense.eu/api/v1/datahub"

    urbasense_api_preprod_access_token: str = "1TkNTw3Z4AMTREAMxTjl7Z6D0lYMZDZZNmkY5Fj"
    urbasense_api_preprod_base_url: str = "https://preprodarbre.urbasense.eu/api/v1/datahub"
    worker_broker: str = "redis://redis:6379/0"
    worker_backend: str = "db+postgresql://ecoteka:password@db/ecoteka"

    root_path: str = "/api"

    class Config:
        env_prefix = "api_"


settings = Settings()


def get_settings():
    logger.info("Loading config settings from the environment...")
    return Settings()
