from celery import shared_task
from celery.utils.log import get_task_logger
from core.s3 import s3_client
from workers.core.config import settings
from workers.utils.import_geofiles import (
    import_archive,
    import_csv,
    import_excel,
    import_geofile,
    import_geopackage,
)

logger = get_task_logger(__name__)


@shared_task
def imports(s3_object_key, crs, user_id, organization_id):
    obj = s3_client.get_object(
        Bucket=settings.minio_bucket_name, Key=s3_object_key
    )

    logger.info(f"============= {obj['ResponseMetadata']['HTTPHeaders']['content-type']} ==================")

    content_type = obj["ResponseMetadata"]['HTTPHeaders']["content-type"]


    if content_type == 'text/csv':
        result = import_csv(obj["Body"], crs, user_id, organization_id)
    elif content_type == 'application/vnd.ms-excel':
        result = import_excel(obj["Body"], crs, user_id, organization_id)
    elif content_type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
        result = import_excel(obj["Body"], crs, user_id, organization_id)
    elif content_type == 'application/geopackage+sqlite3':
        result = import_geopackage(obj["Body"], user_id, organization_id)
    elif content_type == 'application/geopackage+sqlite3':
        result = import_geofile(obj["Body"], user_id, organization_id)
    elif content_type == 'application/x-zip-compressed':
        result = import_archive(obj["Body"], user_id, organization_id)
    elif content_type == 'application/zip':
        result = import_archive(obj["Body"], user_id, organization_id)
    else:
        result = "Can't read file type"
    
    return {"import": "ok", "result": result}

