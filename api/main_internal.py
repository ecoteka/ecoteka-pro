from fastapi import FastAPI, Depends
from api.internal.keycloak import users
from api.internal import metrics

from api.internal.map import filters
from api.internal.providers import urbasense
from api.internal.interventions import widget
from api.v1 import events

app = FastAPI()

app.include_router(events.router)
app.include_router(urbasense.router, prefix="/providers/urbasense")
app.include_router(metrics.router, prefix="/metrics")
app.include_router(filters.router, prefix="/map")
app.include_router(widget.router, prefix="/widget")
app.include_router(users.router, prefix="/users")
