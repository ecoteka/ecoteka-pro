ALTER TABLE "public"."tree" DROP COLUMN "infos";
ALTER TABLE "public"."tree" DROP COLUMN "diameter";
ALTER TABLE "public"."tree" DROP COLUMN "diameter_is_estimated";
ALTER TABLE "public"."tree" DROP COLUMN "stage";
ALTER TABLE "public"."tree" DROP COLUMN "crown_diameter";
ALTER TABLE "public"."tree" DROP COLUMN "crown_diameter_is_estimated";
ALTER TABLE "public"."tree" DROP COLUMN "height_is_estimated";