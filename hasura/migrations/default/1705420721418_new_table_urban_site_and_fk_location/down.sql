
alter table "public"."location" drop constraint "location_urban_site_id_fkey";

DROP TABLE "public"."urban_site";

alter table "public"."location" add column "urban_site_type" text;
alter table "public"."location" alter column "urban_site_type" drop not null;
