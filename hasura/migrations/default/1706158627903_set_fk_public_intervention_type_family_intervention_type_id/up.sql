alter table "public"."intervention_type"
  add constraint "intervention_type_family_intervention_type_id_fkey"
  foreign key ("family_intervention_type_id")
  references "public"."family_intervention_type"
  ("id") on update restrict on delete restrict;
