
alter table "public"."diagnosis" add column "crown_condition" text
 null;

alter table "public"."diagnosis" add column "trunk_condition" text
 null;

alter table "public"."diagnosis" add column "collar_condition" text
 null;
