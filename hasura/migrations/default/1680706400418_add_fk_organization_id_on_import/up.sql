
alter table "public"."import" add column "organization_id" uuid
 not null;

alter table "public"."import"
  add constraint "import_organization_id_fkey"
  foreign key ("organization_id")
  references "public"."organization"
  ("id") on update restrict on delete restrict;
