
CREATE TABLE "public"."organization" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "name" text NOT NULL, "population" numeric NOT NULL, "surface" numeric NOT NULL, "default" boolean NOT NULL, "image" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;

DELETE FROM "public"."tree";
DELETE FROM "public"."location";
alter table "public"."location" add column "organization_id" uuid
 not null;

alter table "public"."location"
  add constraint "location_organization_id_fkey"
  foreign key ("organization_id")
  references "public"."organization"
  ("id") on update restrict on delete restrict;
