
alter table "public"."worksite_intervention" drop constraint "worksite_intervention_intervention_id_fkey",
  add constraint "worksite_intervention_intervention_id_fkey"
  foreign key ("intervention_id")
  references "public"."intervention"
  ("id") on update restrict on delete restrict;

alter table "public"."worksite_location" drop constraint "worksite_location_location_id_fkey",
  add constraint "worksite_location_location_id_fkey"
  foreign key ("location_id")
  references "public"."location"
  ("id") on update restrict on delete restrict;
