-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- create
-- or replace function get_locations_by_status(uuid [])
-- returns text language plpgsql as $$
-- begin return (
-- 	SELECT
-- 		json_build_object(
-- 			'type',
-- 			'FeatureCollection',
-- 			'features',
-- 			json_agg(ST_AsGeoJSON(t.*)::json)
-- 		)
-- 	FROM
-- 		(
-- 			select
-- 				*
-- 			from
-- 				location as l
-- 				left join location_status as s on l.id_status = s.id
-- 			where
-- 				l.id_status = any($1)
-- 		) as t
-- )::text;
--
-- end $$;
