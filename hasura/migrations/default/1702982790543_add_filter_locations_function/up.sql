CREATE OR replace FUNCTION filter_locations(
    z integer,
    x integer,
    y integer,
    query_params json
  ) RETURNS bytea LANGUAGE plpgsql IMMUTABLE STRICT PARALLEL SAFE as $$
DECLARE _scnames text [] := string_to_array(query_params->>'scientific_names', ',');
_vernacular_names text [] := string_to_array(query_params->>'vernacular_names', ',');
_date_start text := query_params->>'date_start';
_date_end text := query_params->>'date_end';
mvt bytea;
begin
SELECT INTO mvt ST_AsMVT(tile, 'filter_locations', 4096, 'coords')
FROM (
    SELECT ST_AsMVTGeom(
        ST_Transform(ST_CurveToLine(coords), 3857),
        ST_TileEnvelope(z, x, y),
        4096,
        64,
        true
      ) AS coords,
      location.id as location_id,
      location.address as location_address,
      location_status.status as status_name,
      location_status.color as status_color,
      tree.id as tree_id,
      tree.scientific_name as tree_scientific_name,
      tree.vernacular_name as tree_vernacular_name,
      tree.serial_number as tree_serial_number,
      tree.plantation_date as plantation_date
    FROM location
      LEFT JOIN tree ON location.id = tree.location_id
      RIGHT JOIN location_status ON location.id_status = location_status.id
    WHERE coords && ST_Transform(ST_TileEnvelope(z, x, y), 4326)
      AND (
        array_length(_scnames, 1) IS NULL
        OR tree.scientific_name ilike any (_scnames)
      )
      AND (
        array_length(_vernacular_names, 1) IS NULL
        OR tree.vernacular_name ilike any (_vernacular_names)
      )
      AND (
        (
          _date_start IS NULL
          AND _date_end IS NULL
        )
        OR (
          tree.plantation_date::timestamp::date = _date_start::date
          AND _date_end IS NULL
        )
        OR (
          _date_start IS NOT NULL
          AND _date_end IS NOT NULL
          AND tree.plantation_date::timestamp::date BETWEEN _date_end::date AND _date_start::date
        )
      )
  ) as tile
WHERE coords IS NOT NULL;
RETURN mvt;
end;
$$;
