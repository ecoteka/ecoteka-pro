ALTER TABLE "public"."tree"
ALTER COLUMN "height" TYPE numeric USING NULLIF(height, '')::numeric;
