CREATE OR replace FUNCTION locations_with_status_and_tree(z integer, x integer, y integer)
    RETURNS bytea
    LANGUAGE plpgsql IMMUTABLE STRICT PARALLEL SAFE
as 
$$DECLARE mvt bytea;
BEGIN
  SELECT INTO mvt ST_AsMVT(tile, 'locations_with_status_and_tree', 4096, 'coords') FROM (
    SELECT
      ST_AsMVTGeom(
          ST_Transform(ST_CurveToLine(coords), 3857),
          ST_TileEnvelope(z, x, y),
          4096, 64, true) AS coords,
      location.id as location_id,
      location.address as location_address,
      location_status.status as status_name,
      location_status.color as status_color,
      tree.id as tree_id,
      tree.scientific_name as tree_scientific_name,
      tree.vernacular_name  as tree_vernacular_name,
      tree.serial_number as tree_serial_number
    FROM location
    LEFT JOIN tree ON location.id = tree.location_id
    RIGHT JOIN location_status ON location.id_status = location_status.id
    WHERE coords && ST_Transform(ST_TileEnvelope(z, x, y), 4326)
  ) as tile WHERE coords IS NOT NULL;

  RETURN mvt;
end;$$;
