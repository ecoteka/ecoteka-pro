CREATE TABLE "public"."location" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "address" text, "description" text, PRIMARY KEY ("id") );
CREATE EXTENSION IF NOT EXISTS pgcrypto;
