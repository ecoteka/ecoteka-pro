alter table "public"."tree"
  add constraint "tree_taxon_id_fkey"
  foreign key ("taxon_id")
  references "public"."taxon"
  ("id") on update restrict on delete restrict;
