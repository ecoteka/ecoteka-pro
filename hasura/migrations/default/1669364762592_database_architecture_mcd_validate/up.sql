
alter table "public"."location" add column "deletion_date" timestamptz
 null;

alter table "public"."tree" rename column "genus" to "scientific_name";

alter table "public"."tree" rename column "species" to "variety";

ALTER TABLE "public"."tree" ALTER COLUMN "diameter" TYPE numeric;
alter table "public"."tree" rename column "diameter" to "circumference";

alter table "public"."tree" rename column "notes" to "note";

alter table "public"."tree" rename column "watering_notes" to "habit";

alter table "public"."tree" add column "serial_number" text
 null;

alter table "public"."tree" add column "estimated_date" boolean
 null default 'false';

alter table "public"."tree" alter column "location_id" set not null;

CREATE TABLE "public"."intervention" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "intervention_date" date NOT NULL, "provisional_date" date NOT NULL, "note" text, "deletion_date" timestamp with time zone, "creation_date" timestamp with time zone
 not null default now(), "edition_date" timestamp with time zone null, "cancel_date" timestamp with time zone null, "tree_id" uuid NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("tree_id") REFERENCES "public"."tree"("id") ON UPDATE restrict ON DELETE restrict, UNIQUE ("id"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;

alter table "public"."tree" add column "deletion_date" timestamp with time zone
 null;

CREATE TABLE "public"."diagnosis" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "diagnosis_date" date NOT NULL, "tree_condition" text NOT NULL, "organ" text, "recommendation" text, "note" text, "deletion_date" timestamp with time zone, "creation_date" timestamp with time zone
 not null default now(), "edition_date" timestamp with time zone null, "tree_id" uuid NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("tree_id") REFERENCES "public"."tree"("id") ON UPDATE restrict ON DELETE restrict, UNIQUE ("id"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;

alter table "public"."tree" add column "edition_date" timestamp with time zone
 null;

alter table "public"."tree" add column "creation_date" timestamp with time zone
 not null default now();

alter table "public"."location" add column "creation_date" timestamp with time zone
 not null default now();

ALTER TABLE "public"."intervention" add column "user_id" text not null;;

alter table "public"."intervention"
  add constraint "intervention_user_id_fkey"
  foreign key ("user_id")
  references "public"."user_entity"
  ("id") on update restrict on delete restrict;

alter table "public"."location" add column "user_id" text
 not null;

alter table "public"."location"
  add constraint "location_user_id_fkey"
  foreign key ("user_id")
  references "public"."user_entity"
  ("id") on update restrict on delete restrict;

alter table "public"."tree" add column "user_id" text
 not null;

alter table "public"."tree"
  add constraint "tree_user_id_fkey"
  foreign key ("user_id")
  references "public"."user_entity"
  ("id") on update restrict on delete restrict;

CREATE TABLE "public"."intervention_type" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "slug" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));

alter table "public"."intervention" add column "intervention_type_id" uuid
 not null;

alter table "public"."intervention" drop constraint "intervention_user_id_fkey",
  add constraint "intervention_user_id_fkey"
  foreign key ("user_id")
  references "public"."user_entity"
  ("id") on update restrict on delete restrict;

alter table "public"."intervention"
  add constraint "intervention_intervention_type_id_fkey"
  foreign key ("intervention_type_id")
  references "public"."intervention_type"
  ("id") on update restrict on delete restrict;

alter table "public"."diagnosis" add column "diagnosis_type_id" uuid
 not null;

CREATE TABLE "public"."diagnosis_type" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "slug" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));

alter table "public"."diagnosis"
  add constraint "diagnosis_diagnosis_type_id_fkey"
  foreign key ("diagnosis_type_id")
  references "public"."diagnosis_type"
  ("id") on update restrict on delete restrict;

CREATE TABLE "public"."intervention_partner" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "name" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE "public"."diagnosis_partner" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "name" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;

alter table "public"."intervention" add column "intervention_partner_id" uuid
 not null;

alter table "public"."intervention"
  add constraint "intervention_intervention_partner_id_fkey"
  foreign key ("intervention_partner_id")
  references "public"."intervention_partner"
  ("id") on update restrict on delete restrict;

alter table "public"."diagnosis" add column "diagnosis_partner_id" uuid
 not null;

alter table "public"."diagnosis" add column "user_id" text
 not null;

alter table "public"."diagnosis"
  add constraint "diagnosis_user_id_fkey"
  foreign key ("user_id")
  references "public"."user_entity"
  ("id") on update restrict on delete restrict;

alter table "public"."diagnosis"
  add constraint "diagnosis_diagnosis_partner_id_fkey"
  foreign key ("diagnosis_partner_id")
  references "public"."diagnosis_partner"
  ("id") on update restrict on delete restrict;

alter table "public"."diagnosis" rename column "user_id" to "data_entry_user_id";

alter table "public"."intervention" rename column "user_id" to "data_entry_user_id";

alter table "public"."location" rename column "user_id" to "data_entry_user_id";

alter table "public"."tree" rename column "user_id" to "data_entry_user_id";

CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_edition_date"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."edition_date" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_tree_edition_date"
BEFORE UPDATE ON "public"."tree"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_edition_date"();
COMMENT ON TRIGGER "set_tree_edition_date" ON "public"."tree" 
IS 'trigger to set value of column "edition_date" to current timestamp on row update';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_edition_date"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."edition_date" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_intervention_edition_date"
BEFORE UPDATE ON "public"."intervention"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_edition_date"();
COMMENT ON TRIGGER "set_intervention_edition_date" ON "public"."intervention" 
IS 'trigger to set value of column "edition_date" to current timestamp on row update';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_edition_date"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."edition_date" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_diagnosis_edition_date"
BEFORE UPDATE ON "public"."diagnosis"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_edition_date"();
COMMENT ON TRIGGER "set_diagnosis_edition_date" ON "public"."diagnosis" 
IS 'trigger to set value of column "edition_date" to current timestamp on row update';
CREATE EXTENSION IF NOT EXISTS pgcrypto;