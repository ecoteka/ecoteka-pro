
alter table "public"."tree" rename column "data_entry_user_id" to "user_id";

alter table "public"."location" rename column "data_entry_user_id" to "user_id";

alter table "public"."intervention" rename column "data_entry_user_id" to "user_id";

alter table "public"."diagnosis" rename column "data_entry_user_id" to "user_id";

alter table "public"."diagnosis" drop constraint "diagnosis_diagnosis_partner_id_fkey";

alter table "public"."diagnosis" drop constraint "diagnosis_user_id_fkey";

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."diagnosis" add column "user_id" text
--  not null;

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."diagnosis" add column "diagnosis_partner_id" uuid
--  not null;

alter table "public"."intervention" drop constraint "intervention_intervention_partner_id_fkey";

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."intervention" add column "intervention_partner_id" uuid
--  not null;

DROP TABLE "public"."diagnosis_partner";

DROP TABLE "public"."intervention_partner";

alter table "public"."diagnosis" drop constraint "diagnosis_diagnosis_type_id_fkey";

DROP TABLE "public"."diagnosis_type";

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."diagnosis" add column "diagnosis_type_id" uuid
--  not null;

alter table "public"."intervention" drop constraint "intervention_intervention_type_id_fkey";

alter table "public"."intervention" drop constraint "intervention_user_id_fkey",
  add constraint "intervention_user_id_fkey"
  foreign key ("user_id")
  references "public"."user_entity"
  ("id") on update restrict on delete restrict;

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."intervention" add column "intervention_type_id" uuid
--  not null;

DROP TABLE "public"."intervention_type";

alter table "public"."tree" drop constraint "tree_user_id_fkey";

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."tree" add column "user_id" text
--  not null;

alter table "public"."location" drop constraint "location_user_id_fkey";

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."location" add column "user_id" text
--  not null;

alter table "public"."intervention" drop constraint "intervention_user_id_fkey";

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."intervention" add column "user_id" text
--  not null;
-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."location" add column "creation_date" timestamp with time zone
--  not null default now();

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."tree" add column "creation_date" timestamp with time zone
--  not null default now();

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."tree" add column "edition_date" timestamp with time zone
--  null;

DROP TABLE "public"."diagnosis";

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."tree" add column "deleted_at" timestamp with time zone
--  null;

DROP TABLE "public"."intervention";

alter table "public"."tree" alter column "location_id" drop not null;

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."tree" add column "estimated_date" boolean
--  null default 'false';

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."tree" add column "serial_number" text
--  null;

alter table "public"."tree" rename column "habit" to "watering_notes";

alter table "public"."tree" rename column "note" to "notes";

alter table "public"."tree" rename column "circumference" to "diameter";
ALTER TABLE "public"."tree" ALTER COLUMN "diameter" TYPE integer;

alter table "public"."tree" rename column "variety" to "species";

alter table "public"."tree" rename column "scientific_name" to "genus";

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."location" add column "deleted_at" timestamptz
--  null;
