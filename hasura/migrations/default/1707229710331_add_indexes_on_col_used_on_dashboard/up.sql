
CREATE  INDEX "taxon_carbon_idx" on
  "public"."taxon" using btree ("carbon");

CREATE  INDEX "taxon_icu_idx" on
  "public"."taxon" using btree ("icu");

CREATE  INDEX "taxon_air_idx" on
  "public"."taxon" using btree ("air");

CREATE  INDEX "taxon_biodiversity_idx" on
  "public"."taxon" using btree ("biodiversity");

CREATE  INDEX "taxon_allergenic_score_idx" on
  "public"."taxon" using btree ("allergenic_score");

CREATE  INDEX "taxon_vulnerability_idx" on
  "public"."taxon" using btree ("vulnerability");

CREATE  INDEX "taxon_growth_category_idx" on
  "public"."taxon" using btree ("growth_category");

CREATE  INDEX "taxon_genus_idx" on
  "public"."taxon" using btree ("genus");

CREATE  INDEX "taxon_family_idx" on
  "public"."taxon" using btree ("family");

CREATE  INDEX "taxon_species_idx" on
  "public"."taxon" using btree ("species");

CREATE  INDEX "tree_taxon_id_idx" on
  "public"."tree" using btree ("taxon_id");

CREATE  INDEX "family_intervention_type_slug_idx" on
  "public"."family_intervention_type" using btree ("slug");




