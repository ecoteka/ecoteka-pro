
DROP INDEX IF EXISTS "public"."taxon_carbon_idx";

DROP INDEX IF EXISTS "public"."taxon_icu_idx";

DROP INDEX IF EXISTS "public"."taxon_air_idx";

DROP INDEX IF EXISTS "public"."taxon_biodiversity_idx";

DROP INDEX IF EXISTS "public"."taxon_allergenic_score_idx";

DROP INDEX IF EXISTS "public"."taxon_vulnerability_idx";

DROP INDEX IF EXISTS "public"."taxon_growth_category_idx";

DROP INDEX IF EXISTS "public"."taxon_genus_idx";

DROP INDEX IF EXISTS "public"."taxon_family_idx";

DROP INDEX IF EXISTS "public"."taxon_species_idx";

DROP INDEX IF EXISTS "public"."tree_taxon_id_idx";

DROP INDEX IF EXISTS "public"."family_intervention_type_slug_idx";
