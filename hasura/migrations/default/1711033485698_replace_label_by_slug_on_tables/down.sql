
alter table "public"."analysis_tool" rename column "slug" to "label";

alter table "public"."pathogen" rename column "slug" to "label";

alter table "public"."urban_site" rename column "slug" to "label";
