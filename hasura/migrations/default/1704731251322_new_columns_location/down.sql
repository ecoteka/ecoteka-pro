
ALTER TABLE "public"."location" DROP COLUMN "sidewalk_type";
ALTER TABLE "public"."location" DROP COLUMN "urban_site_type";
ALTER TABLE "public"."location" DROP COLUMN "landscape_type";
ALTER TABLE "public"."location" DROP COLUMN "plantation_type";
ALTER TABLE "public"."location" DROP COLUMN "is_protected";