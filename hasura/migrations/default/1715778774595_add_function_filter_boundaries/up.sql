CREATE OR REPLACE FUNCTION filter_boundaries(
    z integer,
    x integer,
    y integer,
    query_params json
) 
RETURNS bytea 
LANGUAGE plpgsql 
IMMUTABLE STRICT PARALLEL SAFE 
AS $$
DECLARE
    _geographic_boundaries_active boolean := (query_params->>'geographic_boundaries_active')::boolean;
    _station_boundaries_active boolean := (query_params->>'station_boundaries_active')::boolean;
    mvt bytea;
BEGIN
    SELECT INTO mvt ST_AsMVT(tile, 'filter_boundaries', 4096, 'geom')
    FROM (
        SELECT 
            ST_AsMVTGeom(
                ST_Transform(
                    ST_CurveToLine(ST_GeomFromGeoJSON(ST_AsGeoJSON(coords))),
                    3857
                ),
                ST_TileEnvelope(z, x, y),
                4096,
                64,
                true
            ) AS geom,  
            boundary.id AS boundary_id,
            boundary.name AS boundary_name,
            boundary.type as boundary_type
        FROM 
            boundary
        WHERE 
            ST_Intersects(coords, ST_Transform(ST_TileEnvelope(z, x, y), 4326))
            AND (
                (_geographic_boundaries_active IS TRUE AND boundary.type = 'geographic')
                OR
                (_station_boundaries_active IS TRUE AND boundary.type = 'station')
            )
    ) AS tile
    WHERE 
        geom IS NOT NULL;  
        
    RETURN mvt;
END;
$$;
