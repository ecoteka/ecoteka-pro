version: "3.9"

x-restart-policy: &restart_policy
  restart: unless-stopped

x-project-defaults: &project_defaults
  <<: *restart_policy
  networks:
    - project
  extra_hosts:
    - ${DOMAIN}:${DOMAIN_DOCKER:-host-gateway}
  env_file: ./.env

services:
  traefik:
    <<: *project_defaults
    image: ${TRAEFIK_IMAGE}
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
    healthcheck:
      test: ["CMD", "traefik", "healthcheck", "--ping"]
      interval: 10s
      timeout: 5s
      retries: 3
      start_period: 5s

  db:
    <<: *project_defaults
    image: ${DB_IMAGE:-postgres:14-alpine}
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U ${DB_USER} -h 127.0.0.1"]
      interval: 10s
      timeout: 5s
      retries: 3
      start_period: 60s
    volumes:
      - db_data:/var/lib/postgresql/data

  redis:
    <<: *project_defaults
    image: "${REDIS_IMAGE:-redis:alpine}"
    healthcheck:
      test: redis-cli ping
      interval: 5s
      timeout: 5s
      retries: 3

  keycloak:
    <<: *project_defaults
    image: ${KEYCLOAK_IMAGE}
    depends_on:
      db:
        condition: service_healthy
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8080/auth"]
      interval: 10s
      timeout: 5s
      retries: 3
      start_period: 120s
    labels:
      - traefik.enable=true
      - traefik.http.routers.keycloak.rule=PathPrefix(`${KC_HTTP_RELATIVE_PATH}`)
      - traefik.http.routers.keycloak.entrypoints=web
    command: start-dev --import-realm

  minio:
    <<: *project_defaults
    image: ${MINIO_IMAGE:-minio/minio:RELEASE.2023-07-21T21-12-44Z}
    volumes:
      - minio_data:/data
    labels:
      - traefik.enable=true
      - traefik.http.routers.minio.rule=PathPrefix(`/${MINIO_BUCKET_NAME}`)
      - traefik.http.routers.minio.entrypoints=web
      - traefik.http.services.minio.loadbalancer.server.port=8888
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8888/minio/health/live"]
      interval: 30s
      timeout: 20s
      retries: 3
    command: server --address :8888 --console-address :9001 /data

  meilisearch:
    <<: *project_defaults
    image: ${MEILI_IMAGE:-getmeili/meilisearch:v0.30}
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:7700/health"]
      interval: 30s
      timeout: 20s
      retries: 3
    depends_on:
      keycloak:
        condition: service_healthy
    labels:
      - traefik.enable=true
      - traefik.http.routers.meilisearch.entrypoints=web
      - traefik.http.routers.meilisearch.rule=PathPrefix(`${MEILI_ROOT_PATH}`)
      - traefik.http.routers.meilisearch.middlewares=meilisearch-stripprefix
      - traefik.http.middlewares.meilisearch-stripprefix.stripprefix.prefixes=${MEILI_ROOT_PATH}
    volumes:
      - meilisearch_data:/data.ms

  frontend:
    <<: *project_defaults
    image: ${FRONTEND_IMAGE:-registry.gitlab.com/natural-solutions/ecoteka-pro:frontend-main}
    build: ../frontend
    labels:
      - traefik.enable=true
      - traefik.http.routers.frontend.rule=PathPrefix(`${FRONTEND_ROOT_PATH}`)
      - traefik.http.routers.frontend.entrypoints=web
    depends_on:
      graphql_engine:
        condition: service_healthy
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:3000/"]
      interval: 30s
      timeout: 20s
      retries: 3

  graphql_engine:
    <<: *project_defaults
    image: ${HASURA_IMAGE}
    healthcheck:
      test: timeout 1s bash -c ':> /dev/tcp/127.0.0.1/8080' || exit 1
      interval: 2s
      timeout: 1s
      retries: 10
    depends_on:
      keycloak:
        condition: service_healthy
    restart: always
    expose:
      - 8080
    labels:
      - traefik.enable=true
      - traefik.http.routers.graphql_engine.entrypoints=web
      - traefik.http.routers.graphql_engine.rule=PathPrefix(`/v1`) || PathPrefix(`/v2`)

  api:
    <<: *project_defaults
    build: ../api
    image: ${API_IMAGE:-registry.gitlab.com/natural-solutions/ecoteka-pro:api-main}
    depends_on:
      keycloak:
        condition: service_healthy
    restart: always
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost/openapi.json"]
      interval: 5s
      timeout: 5s
      retries: 3
    labels:
      - traefik.enable=true
      - traefik.http.routers.api.entrypoints=web
      - traefik.http.routers.api.rule=PathPrefix(`${API_ROOT_PATH}`)
      - traefik.http.routers.api.middlewares=api-stripprefix
      - traefik.http.middlewares.api-stripprefix.stripprefix.prefixes=${API_ROOT_PATH}
    command: uvicorn main:app --host "0.0.0.0" --port 80 --proxy-headers

  api_internal:
    <<: *project_defaults
    image: ${API_IMAGE:-registry.gitlab.com/natural-solutions/ecoteka-pro:api-main}
    depends_on:
      keycloak:
        condition: service_healthy
    restart: always
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost/openapi.json"]
      interval: 5s
      timeout: 5s
      retries: 3
      start_period: 120s
    command: uvicorn main_internal:app --host "0.0.0.0" --port 80 --proxy-headers

  worker:
    <<: *project_defaults
    image: ${API_IMAGE:-registry.gitlab.com/natural-solutions/ecoteka-pro:api-main}
    depends_on:
      redis:
        condition: service_healthy
    restart: always
    healthcheck:
      test: celery --app workers.run inspect ping
      interval: 30s
      timeout: 10s
      retries: 3
    command: celery --app workers.run worker -l info -c 1

  martin:
    <<: *project_defaults
    image: ${MARTIN_IMAGE:-ghcr.io/maplibre/martin:v0.8.7}
    restart: unless-stopped
    depends_on:
      db:
        condition: service_healthy
    labels:
      - traefik.enable=true
      - traefik.http.routers.martin.entrypoints=web
      - traefik.http.routers.martin.rule=PathPrefix(`${MARTIN_ROOT_PATH}`)
      - traefik.http.routers.martin.middlewares=martin-stripprefix
      - traefik.http.middlewares.martin-stripprefix.stripprefix.prefixes=${MARTIN_ROOT_PATH}
      - traefik.http.services.martin.loadbalancer.server.port=3000
    volumes:
      - "../martin:/usr/local/etc/martin"
    command: ${DATABASE_URL} -d 4326

volumes:
  db_data:
  minio_data:
  meilisearch_data:

networks:
  project:
    name: ${PROJECT}
