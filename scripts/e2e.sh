#!/bin/bash

set -e

./scripts/docker.sh pull
./scripts/docker.sh build --no-cache
./scripts/docker.sh up -d
npm run e2e
./scripts/docker.sh down -v --remove-orphans
