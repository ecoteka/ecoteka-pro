import React, { createContext, useState, useContext, useEffect } from "react";
import { useRouter } from "next/router";

export interface FiltersWithMartin {
  location_status?: string[];
  address?: string[];
  scientific_names?: string[];
  vernacular_names?: string[];
  plantation_date_start?: string;
  plantation_date_end?: string;
  tree_condition?: string[];
  tree_is_dangerous?: boolean;
  recommendation?: string[];
  intervention_partner_id?: string[];
  intervention_type_id?: string[];
  intervention_status?: string[];
  scheduled_date_start?: string;
  scheduled_date_end?: string;
  realized_date_start?: string;
  realized_date_end?: string;
  has_urbasense?: boolean;
  has_worksite?: boolean;
  green_area_active?: boolean;
  station_boundaries_active?: boolean;
  geographic_boundaries_active?: boolean;
}

export interface TreeData {
  scientific_names: string[];
  vernacular_names: string[];
  plantation_date: string;
}

export interface LocationData {
  status: Status[];
  address: string[];
}

export interface Status {
  id: string;
  status: string;
  color: string;
}

export interface DiagnosisData {
  tree_condition: string[];
  tree_is_dangerous: boolean;
  type: string[];
  recommendation: string[];
}

export interface Partner {
  id: string;
  name: string;
}

export interface InterventionType {
  id: string;
  slug: string;
}

export interface InterventionData {
  scheduled_date: string;
  realization_date: string;
  partner: Partner[];
  type: InterventionType[];
}

export interface BoundaryData {
  id: string[];
  name: string[];
  type: string[];
}

export interface FilterData {
  tree?: TreeData;
  location?: LocationData;
  diagnosis?: DiagnosisData;
  intervention?: InterventionData;
  boundary?: BoundaryData;
}

export interface FiltersWithMartinContextType {
  filtersWithMartin: FiltersWithMartin;
  setFiltersWithMartin: React.Dispatch<
    React.SetStateAction<Partial<FiltersWithMartin>>
  >;
  filterData: FilterData;
  setFilterData: React.Dispatch<React.SetStateAction<Partial<FilterData>>>;
  shouldResetFields: boolean;
  triggerFieldReset: () => void;
  isMounted: boolean;
}

const FiltersWithMartinContext =
  createContext<FiltersWithMartinContextType | null>(null);

export default function FiltersWithMartinProvider({ children }) {
  const [filtersWithMartin, setFiltersWithMartin] = useState<
    Partial<FiltersWithMartin>
  >({});
  const [filterData, setFilterData] = useState<Partial<FilterData>>({});
  const [shouldResetFields, setShouldResetFields] = useState<boolean>(false);
  const [isMounted, setIsMounted] = useState<boolean>(false);
  const router = useRouter();

  const triggerFieldReset = () => {
    setShouldResetFields(true);
    setFiltersWithMartin({});
    router.replace(router.pathname);
  };

  useEffect(() => {
    if (shouldResetFields) {
      setShouldResetFields(false);
    }
  }, [shouldResetFields]);

  useEffect(() => {
    setIsMounted(true);
    return () => {
      setIsMounted(false);
    };
  }, []);

  return (
    <FiltersWithMartinContext.Provider
      value={{
        filtersWithMartin,
        setFiltersWithMartin,
        filterData,
        setFilterData,
        shouldResetFields,
        triggerFieldReset,
        isMounted,
      }}
    >
      {children}
    </FiltersWithMartinContext.Provider>
  );
}

export function useFiltersWithMartinContext() {
  return useContext(FiltersWithMartinContext);
}
