export interface KeycloakUser {
  id: string;
  username: string;
  enabled: boolean;
  totp: boolean;
  emailVerified: boolean;
  firstName: string;
  lastName: string;
  email: string;
  attributes: {
    role: string;
    passwordLength?: number;
    team?: string;
  };
  disableableCredentialTypes: string[];
  requiredActions: string[];
  notBefore: number;
  access: {
    manageGroupMembership: boolean;
    view: boolean;
    mapRoles: boolean;
    impersonate: boolean;
    manage: boolean;
  };
  createdTimestamp?: number
}

export interface UserData {
  id: string;
  first_name: string;
  last_name: string;
  email: string;
  team?: string;
  role: string;
  created_timestamp?: number | undefined;
}

export interface KeycloakRole {
  id: string;
  name: string;
  composite: boolean;
  clientRole: boolean;
  containerId: string;
}
