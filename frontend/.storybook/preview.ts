import type { Preview } from "@storybook/react";
import { CssBaseline, ThemeProvider, createTheme } from "@mui/material";
import { withThemeFromJSXProvider } from "@storybook/addon-styling";
import { lightTheme, darkTheme } from "../stories/themes";

import { getDesignTokens } from "../theme/theme";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import "@fontsource/material-icons";
import i18n from "./i18next";

export const decorators = [
  withThemeFromJSXProvider({
    themes: {
      light: createTheme(getDesignTokens("light")),
      dark: createTheme(getDesignTokens("dark")),
    },
    defaultTheme: "light",
    Provider: ThemeProvider,
    GlobalStyles: CssBaseline,
  }),
];

const preview: Preview = {
  globals: {
    locale: "en",
    locales: {
      en: { title: "English", left: "🇺🇸" },
      fr: { title: "Français", left: "🇫🇷" },
      es: { title: "Español", left: "🇪🇸" },
    },
  },
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
    i18n,
  },
};

export default preview;
