import { useEffect, useState, useCallback, useRef } from "react";
import { NextPage } from "next";
import { Stack } from "@mui/material";
import { FlyToInterpolator } from "deck.gl";
import { EditableGeoJsonLayer, SelectionLayer } from "@nebula.gl/layers";
import {
  createBoundariesLayer,
  createLocationsLayer,
  generateLayerDataUrl,
} from "@components/map/layers/Layers";
import { DrawPolygonMode } from "@nebula.gl/edit-modes";
import _ from "lodash";
import MapBase, { InitialViewState } from "@components/map/Base";
import TempLayer from "@components/map/layers/Temp";
import MapActions from "@components/map/MapActions";
import Panel from "@components/map/Panel";
import "maplibre-gl/dist/maplibre-gl.css";
import useStore from "@lib/store/useStore";
import { useSession } from "next-auth/react";
import { useTranslation } from "react-i18next";
import {
  LocationType,
  MapBackground,
  MapActionsBarActionType,
  useViewState,
  initialMapEditorState,
  useEditionMode,
  useMapActiveAction,
  useMapEditorActions,
  useMapBackground,
  useMapTempPoint,
  useMapUserPosition,
  useMapPanelOpen,
  useMapLocationType,
  useMapLayers,
  useMapLocations,
  useMapStyle,
  useLocationStatuses,
  useFirstLoad,
  useMapFilteredLocations,
  useMapSelectedLocations,
  useSelectionLayerActive,
  useMapActiveLocationId,
  useForceReloadLayer,
} from "@lib/store/pages/mapEditor";
import {
  CreateTreeMutationVariables,
  Urban_Constraint,
  useCreateLocationMutation,
  useCreateTreeMutation,
  useFetchAllBoundariesLazyQuery,
  useFetchLocationFormDataLazyQuery,
  useFetchLocationStatusLazyQuery,
  useFetchOneLocationLazyQuery,
  useGetLocationsInsidePolygonLazyQuery,
  useInsertLocationBoundariesMutation,
  useInsertLocationUrbanConstraintMutation,
} from "../generated/graphql";
import i18next from "../i18n";
import InitialModal from "@components/map/InitialModal";
import { useRouter } from "next/router";
import PanelLocation, { Location } from "@components/panel/Location";
import MapNavigation, { Direction } from "@components/map/buttons/Navigation";
import WorksitePanelContent from "@ecosystems/MapEditor/Panels/Worksite/WorksitePanelContent";
import CardInfos from "@components/_pages/MapEditor/CardInfos/CardInfos";
import getConfig from "next/config";
import { getPolygonArea, useDeviceSize } from "@lib/utils";
import { Feature } from "maplibre-gl";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import PanelFiltersWithMartin from "@components/panel/FiltersWithMartin";
import Button from "@mui/material/Button";
import { GlobalLoader } from "@components/layout/GlobalLoader";
import PolygonPanel from "@components/_pages/MapEditor/Panels/Polygon/PolygonPanelContent";
import {
  usePolygonFormActions,
  usePolygonFormData,
} from "@stores/forms/polygonForm";

export interface LocationFeatureProps {
  layerName: string;
  status_name: string;
  status_color: string;
  location_id: string;
  location_address: string;
  tree_id: string;
  tree_scientific_name: string;
  tree_vernacular_name: string;
  tree_serial_number: string;
  tree_plantation_date: string;
  tree_height: string;
}

const MapEditor: NextPage = () => {
  const { t } = useTranslation("components");
  const { app } = useStore((store) => store);
  const { query, push } = useRouter();
  const session = useSession();
  const currentUserId = session?.data?.user?.id ? session?.data?.user?.id : "";
  const { isDesktop } = useDeviceSize();
  const { publicRuntimeConfig } = getConfig();
  const previousMapBackground = useRef(null);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [cardPreview, setCardPreview] = useState<boolean>(false);
  const [openCardModal, setOpenCardModal] = useState<boolean>(false);
  const [infos, setInfos] = useState();
  const [selectedLocationIds, setSelectedLocationIds] = useState<Set<string>>(
    new Set()
  );
  const [queryStringLocations, setQueryStringLocations] = useState<string>("");
  const [queryStringBoundaries, setQueryStringBoundaries] = useState<string>(
    "geographic_boundaries_active=true"
  );
  const [queryStringGreenAreas, setQueryStringGreenAreas] = useState<string>(
    "green_area_active=true"
  );

  const { filtersWithMartin, setFiltersWithMartin, triggerFieldReset } =
    useFiltersWithMartinContext()!;

  const [fetchLocation, { data: activeLocationData }] =
    useFetchOneLocationLazyQuery();
  const [fetchLocationsStatuses] = useFetchLocationStatusLazyQuery();
  const [fetchBoundaries, { data: boundaries }] =
    useFetchAllBoundariesLazyQuery();
  const [fetchLocationFormData, { data: locationFormData }] =
    useFetchLocationFormDataLazyQuery();
  const [getLocationsInsidePolygon, { data: locationsInsidePolygon }] =
    useGetLocationsInsidePolygonLazyQuery();
  const [insertlocationBoundaries] = useInsertLocationBoundariesMutation();
  const [insertLocationUrbanConstraint] =
    useInsertLocationUrbanConstraintMutation();
  const [createLocationMutation] = useCreateLocationMutation();
  const [createTreeMutation] = useCreateTreeMutation();
  const [polygonFeatures, setPolygonFeatures] = useState({
    type: "FeatureCollection",
    features: [{ geometry: { coordinates: [] } }],
  });
  const [selectedFeatureIndexes] = useState([0]);

  // Fix front_build
  // here use our getters from our MapEditorStore
  const firstLoad = useFirstLoad();
  const viewState = useViewState();
  const editionMode = useEditionMode();
  const mapStyle = useMapStyle();
  const mapActiveAction = useMapActiveAction();
  const mapBackground = useMapBackground();
  const mapTempPoint = useMapTempPoint();
  const mapUserPosition = useMapUserPosition();
  const mapPanelOpen = useMapPanelOpen();
  const mapLocationType = useMapLocationType();
  const mapActiveLocationId = useMapActiveLocationId();
  const mapLayers = useMapLayers();
  const isSelectionLayerActive = useSelectionLayerActive();
  const mapLocations = useMapLocations();
  const mapFilteredLocations = useMapFilteredLocations();
  const mapSelectedLocations = useMapSelectedLocations();
  const mapStatuses = useLocationStatuses();
  const { setPolygon } = usePolygonFormActions();
  const polygon = usePolygonFormData();
  // selector for import all of our actions for the map
  const actions = useMapEditorActions();
  const {
    addLayer,
    removeLayer,
    setMapSelectedLocations,
    setForceReloadLayer,
  } = actions;
  const forceReloadLayer = useForceReloadLayer();

  useEffect(() => {
    const { object } = query;
    if (object) {
      const objectAsString = Array.isArray(object) ? object[0] : object;
      const decodedObject = JSON.parse(decodeURIComponent(objectAsString));
      setFiltersWithMartin(decodedObject);
    }
  }, [query]);
  //@ts-ignore
  const selectionLayer = new SelectionLayer({
    id: "selection",
    selectionType: "rectangle",
    onSelect: ({ pickingInfos }) => {
      if (mapActiveAction === "addWorksiteGroup") {
        const featuresProperties = pickingInfos.map(
          (picked) => picked.object.properties
        );
        actions.setMapSelectedLocations(featuresProperties);
      }
    },
    layerIds: ["locations"],
    getTentativeFillColor: () => [255, 0, 255, 100],
    getTentativeLineColor: () => [0, 0, 255, 255],
    getTentativeLineDashArray: () => [0, 0],
    lineWidthMinPixels: 3,
  });

  const polygonLayer =
    //@ts-ignore
    new EditableGeoJsonLayer({
      id: "geojson-layer",
      data: polygonFeatures,
      mode: DrawPolygonMode,
      selectedFeatureIndexes,
      onEdit: ({ updatedData }): any => {
        // Ensure that only one feature is present in updatedData
        if (polygonFeatures.features.length > 0) {
          const newFeatures = {
            ...polygonFeatures,
            features: polygonFeatures.features.slice(1), // Slice from index 1 to keep only the last polygon
          };
          setPolygonFeatures(newFeatures);
        } else {
          setPolygonFeatures(updatedData);
        }
      },
    });

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const handleOnActionClick = (action: MapActionsBarActionType) => {
    actions.setMapActiveAction(action);
    if (mapActiveAction === action) {
      actions.setMapPanelOpen(!mapPanelOpen);
    } else {
      actions.setMapPanelOpen(true);
    }
  };

  const handleOnAddLocation = (locationType: LocationType) => {
    actions.setMapActiveLocationId(undefined);
    actions.renderSelectionLayer(false);
    setOpenCardModal(false);
    setCardPreview(false);
    actions.setEditionMode(true);
    actions.setMapFilteredLocations(undefined);
    actions.setMapSelectedLocations(undefined);
    actions.setMapActiveAction("addLocation", locationType);
    actions.setMapPanelOpen(false);
    actions.setMapBackground(MapBackground.Satellite);
  };

  const handleCancelProcess = () => {
    actions.setMapActiveAction(undefined);
    actions.renderSelectionLayer(false);
    actions.setMapFilteredLocations(undefined);
    actions.setMapSelectedLocations(undefined);
    actions.setMapPanelOpen(false);
  };

  const exitEditMode = () => {
    actions.setEditionMode(false);
    actions.setMapActiveAction(undefined);
    actions.setMapPanelOpen(false);
    actions.setForceReloadLayer(true);
    actions.setMapBackground(
      previousMapBackground.current ?? MapBackground.Map
    );
    const newQuery = { ...query };
    delete newQuery.edit;
    push({ query: { ...newQuery } }, undefined, {
      shallow: true,
    });
  };

  const handleOnBaseMapChange = (map: any) => {
    previousMapBackground.current = map;
    actions.setMapBackground(map);
  };

  const handleOnGeolocate = (x: number, y: number) => {
    const newViewState = {
      longitude: x,
      latitude: y,
      zoom: viewState.zoom,
      pitch: 0,
      bearing: 0,
      transitionDuration: 3000,
      transitionInterpolator: new FlyToInterpolator(),
    };
    actions.setViewState(newViewState);
    actions.setMapUserPosition({ lon: x, lat: y });
  };

  const onSuccess = useCallback(
    (migrationType: string, element: any, newItem?: any) => {
      app.setAppState({
        snackbar: {
          ...app.snackbar,
          alerts: [
            {
              message:
                element === "location"
                  ? i18next.t(
                      `components.LocationForm.success.${migrationType}.${newItem?.status?.status}`
                    )
                  : i18next.t(
                      `components.InterventionForm.success.${migrationType}`
                    ),
              severity: "success",
            },
          ],
        },
      });
      if (element === "location") {
        fetchLocation({ variables: { _eq: newItem.id } });
        actions.setMapPanelOpen(false);
      }
    },
    [
      app.snackbar.alerts,
      fetchLocation,
      actions.fetchMapLocations,
      mapPanelOpen,
    ]
  );

  const createTree = async (
    data: CreateTreeMutationVariables,
    locationId: string
  ) => {
    createTreeMutation({
      variables: {
        ...data,
        location_id: locationId,
        data_entry_user_id: currentUserId,
      },
    });
  };

  const handleOnZoom = (direction: Direction) => {
    actions.setViewState({
      ...viewState,
      zoom:
        direction === Direction.Out ? viewState.zoom - 1 : viewState.zoom + 1,
    });
  };

  const handleCloseCardInfos = () => {
    setOpenCardModal(false);
    setCardPreview(false);
  };

  const handleCreateLocation = async (data: Location) => {
    const location = data.location;
    const coordinates = [location.longitude, location.latitude];

    createLocationMutation({
      variables: {
        coords: {
          type: "Point",
          coordinates,
        },
        address: location?.address || "",
        foot_type: location?.foot_type,
        urban_site_id: location?.urban_site_id || null,
        plantation_type: location?.plantation_type,
        sidewalk_type: location?.sidewalk_type,
        landscape_type: location?.landscape_type,
        is_protected: location?.is_protected,
        id_status: location?.status,
        organization_id: app.organization?.id,
        data_entry_user_id: currentUserId,
      },
      onCompleted: async (response) => {
        if (Object.values(location.boundaries).some((value) => value !== "")) {
          const hasBoundaries = Object.values(location.boundaries).some(
            (value) => value !== ""
          );
          if (hasBoundaries) {
            const boundaryIds = Object.values(location.boundaries).filter(
              (id) => id !== ""
            );

            Promise.all(
              boundaryIds.map(async (boundaryId) => {
                await insertlocationBoundaries({
                  variables: {
                    location_id: response?.insert_location_one?.id,
                    boundary_id: boundaryId,
                  },
                });
              })
            );
          }
        }
        const hasUrbanConstraints =
          location &&
          location.urban_constraint &&
          Object.keys(location?.urban_constraint).length > 0;
        if (hasUrbanConstraints) {
          const urbanConstraints = Object.values(
            location?.urban_constraint
          ).filter((option: Urban_Constraint) => option?.id !== "");

          Promise.all(
            urbanConstraints.map(async (urbanConstraint: Urban_Constraint) => {
              await insertLocationUrbanConstraint({
                variables: {
                  location_id: response?.insert_location_one?.id,
                  urban_constraint_id: urbanConstraint.id,
                },
              });
            })
          );
        }

        if (response?.insert_location_one?.status?.status === "alive") {
          // @ts-ignore
          createTree(data?.tree, response?.insert_location_one?.id);
        }
        setForceReloadLayer(true);
        onSuccess("create", "location", response?.insert_location_one);
      },
    });
  };

  const isFeatureSelected = (currentFeature: Feature): boolean => {
    const locationId = currentFeature.properties.location_id;
    return filtersWithMartin.has_worksite
      ? true
      : selectedLocationIds.has(locationId);
  };

  const handleOnViewStateChange = (viewState: any) => {
    const nextViewState: InitialViewState = viewState.viewState;
    actions.setViewState({
      ...nextViewState,
      longitude: nextViewState.longitude,
      latitude: nextViewState.latitude,
      zoom: nextViewState.zoom,
    });
  };
  const handleOnMapClick = async (info) => {
    if (editionMode) {
      if (mapActiveAction === "addLocation") {
        const [x, y] = info.coordinate;
        actions.setMapTempPoint({ lat: y, lon: x });
        actions.setMapPanelOpen(true);
      }
      return;
    }

    if (
      info?.layer === null &&
      mapPanelOpen &&
      !["addWorksiteGroup", "addPolygon"].includes(mapActiveAction as string)
    ) {
      actions.setMapPanelOpen(false);
      return;
    }

    if (mapActiveAction === "addWorksiteGroup" && mapSelectedLocations) {
      if (info?.object) {
        const { properties } = info.object;
        const existingIndex = mapSelectedLocations.findIndex(
          (location) => location.location_id === properties.location_id
        );

        if (existingIndex !== -1) {
          const updatedLocations = mapSelectedLocations.filter(
            (location) => location.location_id !== properties.location_id
          );
          actions.setMapSelectedLocations(updatedLocations);
        } else {
          actions.setMapSelectedLocations([
            ...mapSelectedLocations,
            properties,
          ]);
        }
      }
      return;
    }

    if (
      info?.layer?.id === "locations" &&
      info.object?.properties?.location_id &&
      !info.object?.properties?.green_area_id
    ) {
      const locationInfo = info.object?.properties;
      actions.setMapActiveLocationId(locationInfo?.location_id);
      setOpenCardModal(true);
      setCardPreview(true);
      setInfos({ ...locationInfo });
      return;
    }

    if (
      info?.layer?.id === "boundaries" &&
      info.object?.properties?.boundary_id
    ) {
      const boundaryInfo = info.object?.properties;
      setOpenCardModal(true);
      setCardPreview(false); // Assuming a different preview style for boundaries
      setInfos({ ...boundaryInfo });
      return;
    }
  };

  const debouncedHandleOnMapClick = _.debounce(handleOnMapClick, 100);

  useEffect(() => {
    if (mapActiveAction === "addLocation") {
      fetchBoundaries();
      fetchLocationFormData();
    }
  }, [mapActiveAction]);

  useEffect(() => {
    actions.setMapStyle(`/mapStyles/${mapBackground}_${app.appMode}.json`);
  }, [app.appMode, mapBackground]);

  useEffect(() => {
    if (mapLocations && mapLocations?.length > 0 && !firstLoad) {
      const initialCoords = app.organization?.coords?.coordinates;
      if (initialCoords) {
        const viewState = {
          latitude: initialCoords[1],
          longitude: initialCoords[0],
          zoom: 14,
          bearing: 0,
          pitch: 0,
        };
        actions.setViewState(viewState);
      }
    }
    if (mapLocations?.length === 0) {
      setOpenModal(true);
      actions.setMapEditorState(initialMapEditorState);
    }
    actions.fetchLocationStatuses(fetchLocationsStatuses);
  }, [mapLocations, firstLoad]);

  useEffect(() => {
    if (mapActiveAction !== "addWorksiteGroup") {
      setMapSelectedLocations([]);
    }
  }, [mapActiveAction, setMapSelectedLocations]);

  const createTempPointLayer = () => {
    if (mapTempPoint) {
      return TempLayer({
        mapTempPoint: mapTempPoint,
        position: mapUserPosition,
        setMapTempPoint: actions.setMapTempPoint,
      });
    }
    return null;
  };

  useEffect(() => {
    if (forceReloadLayer) {
      generateLayerDataUrl(
        "filter_locations",
        forceReloadLayer,
        queryStringLocations,
        ""
      );

      generateLayerDataUrl(
        "filter_boundaries",
        forceReloadLayer,
        "",
        queryStringBoundaries
      );
    }
  }, [forceReloadLayer]);

  useEffect(() => {
    const locationsLayer = createLocationsLayer(
      generateLayerDataUrl,
      forceReloadLayer,
      setIsLoading,
      handleOnMapClick,
      actions,
      queryStringLocations,
      isFeatureSelected,
      mapBackground,
      selectedLocationIds,
      mapActiveLocationId
    );

    const boundariesLayer = createBoundariesLayer(
      generateLayerDataUrl,
      forceReloadLayer,
      setIsLoading,
      handleOnMapClick,
      actions,
      queryStringBoundaries
    );
    const tempPointLayer = createTempPointLayer();
    // Combine the layers
    const updatedLayers: any = [
      locationsLayer,
      boundariesLayer,
      tempPointLayer,
    ].filter((layer) => layer !== null && layer !== undefined);
    actions.setMapLayers(updatedLayers);

    // Update selection layer
    isSelectionLayerActive && mapLayers
      ? //@ts-ignore
        addLayer(
          //@ts-ignore
          mapActiveAction === "addWorksiteGroup" ? selectionLayer : polygonLayer
        )
      : removeLayer("selection");

    if (
      mapActiveAction === "addPolygon" &&
      polygonFeatures?.features[0]?.geometry?.coordinates
    ) {
      const area = getPolygonArea(polygonFeatures?.features[0]?.geometry);

      getLocationsInsidePolygon({
        variables: {
          polygon: polygonFeatures?.features[0]?.geometry,
        },
      })
        .then((response) => {
          if (response?.data?.location.length) {
            setPolygon({
              ...polygon,
              geometry: polygonFeatures?.features[0]?.geometry,
              area: area,
              locationsInside: response?.data?.location.map((l) => l.id),
            });
          } else {
            setPolygon({
              ...polygon,
              geometry: polygonFeatures?.features[0]?.geometry,
              area: area,
              locationsInside: [],
            });
          }
        })
        .catch((error) => {
          console.error("Error getting locations:", error);
        });
    }
  }, [
    filtersWithMartin,
    queryStringLocations,
    queryStringBoundaries,
    editionMode,
    actions.setMapLayers,
    mapTempPoint,
    mapUserPosition,
    mapLocations,
    mapActiveLocationId,
    selectedLocationIds,
    mapFilteredLocations,
    activeLocationData,
    actions.setMapActiveAction,
    mapPanelOpen,
    mapActiveAction,
    fetchLocation,
    mapBackground,
    isSelectionLayerActive,
    polygonFeatures,
  ]);

  const convertToQueryString = (data) => {
    return Object.entries(data)
      .map(([key, value]) => {
        if (Array.isArray(value)) {
          return `${key}=${value.join(",")}`;
        }
        return `${key}=${value}`;
      })
      .join("&");
  };

  useEffect(() => {
    setQueryStringLocations(convertToQueryString(filtersWithMartin));

    let boundaryQueryString = "";
    if (filtersWithMartin.geographic_boundaries_active) {
      boundaryQueryString += "geographic_boundaries_active=true&";
    }
    if (filtersWithMartin.station_boundaries_active) {
      boundaryQueryString += "station_boundaries_active=true&";
    }
    if (boundaryQueryString !== "") {
      boundaryQueryString = boundaryQueryString.slice(0, -1);
      setQueryStringBoundaries(boundaryQueryString);
    } else {
      setQueryStringBoundaries("");
    }
  }, [filtersWithMartin]);

  useEffect(() => {
    if (mapSelectedLocations) {
      // Update the set of selected location IDs
      const newSelectedLocationIds = new Set(
        mapSelectedLocations.map((location) => location.location_id)
      );
      setSelectedLocationIds(newSelectedLocationIds);
    }
  }, [mapSelectedLocations]);

  return (
    <Stack sx={{ height: "100%", position: "relative" }}>
      {isLoading && <GlobalLoader />}
      <MapBase
        handleOnViewStateChange={handleOnViewStateChange}
        initialViewState={viewState}
        mapStyle={mapStyle}
        layers={mapLayers}
        editionMode={editionMode}
        onClick={debouncedHandleOnMapClick}
      >
        <>
          <MapActions
            handleOnActionClick={handleOnActionClick}
            mapBackground={mapBackground}
            handleOnBaseMapChange={handleOnBaseMapChange}
            handleOnGeolocate={handleOnGeolocate}
            cardPreview={cardPreview}
            editionMode={editionMode}
            exitEditMode={exitEditMode}
            handleOnAddLocation={handleOnAddLocation}
            locationStatusList={mapStatuses!}
          />
          {isDesktop && <MapNavigation onZoom={handleOnZoom} />}
        </>
      </MapBase>
      {openModal && (
        <InitialModal
          open={openModal}
          handleClose={handleCloseModal}
          handleOnAddLocation={handleOnAddLocation}
        />
      )}
      {openCardModal && infos && (
        <CardInfos infos={infos} handleCloseCardInfos={handleCloseCardInfos} />
      )}
      <Panel
        mapLocationType={mapLocationType!}
        mapActiveAction={mapActiveAction!}
        mapPanelOpen={mapPanelOpen}
        handleDrawerClose={() => actions.setMapPanelOpen(false)}
        handleCancelProcess={handleCancelProcess}
        mapActiveLocation={activeLocationData?.location[0]}
        handleOnOpen={() => actions.setMapPanelOpen(true)}
      >
        <>
          {mapActiveAction === "filter" && (
            <>
              {Object.keys(filtersWithMartin).length > 0 && (
                <Button onClick={triggerFieldReset}>
                  {t("common.clearFilters")}
                </Button>
              )}
              <PanelFiltersWithMartin />
            </>
          )}
          {mapActiveAction === "addLocation" &&
            boundaries &&
            locationFormData && (
              <PanelLocation
                // @ts-ignores
                activeLocation={activeLocationData?.location[0]}
                tempPoint={mapTempPoint}
                locationType={mapLocationType!}
                onCreateLocation={handleCreateLocation}
                mapActiveAction={mapActiveAction}
                locationStatusList={mapStatuses!}
                boundaries={boundaries.boundary}
                locationFormData={locationFormData}
              />
            )}
          {mapActiveAction === "addWorksiteGroup" && <WorksitePanelContent />}
          {mapActiveAction === "addPolygon" && <PolygonPanel />}
        </>
      </Panel>
    </Stack>
  );
};

export default MapEditor;
