import { NextPage } from "next";
import { useTranslation } from "react-i18next";
import { Container } from "@mui/system";
import ImportStepper from "../../components/panel/ImportStepper";
import { DetailsHeader } from "@components/_core/headers";

const InterventionAddPage: NextPage = () => {
  const { t } = useTranslation(["common"]);

  return (
    <Container maxWidth="xl" sx={{ mt: 1 }}>
      <DetailsHeader title={t("common.buttons.import")} />
      <ImportStepper />
    </Container>
  );
};

export default InterventionAddPage;
