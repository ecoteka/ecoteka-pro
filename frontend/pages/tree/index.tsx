import { useEffect, useMemo, useState } from "react";
import { Tree, useFetchAllTreesLazyQuery } from "../../generated/graphql";
import { useTranslation } from "react-i18next";
import { NextPage } from "next";
import { Container } from "@mui/material";
import { MRT_ColumnDef } from "material-react-table";
import TreesGrid from "../../components/grid/TreesGrid";
import { DetailsHeader } from "@components/_core/headers";

const TreesPage: NextPage = () => {
  const { t } = useTranslation();
  const [fetchAllTrees] = useFetchAllTreesLazyQuery();
  const [allTrees, setAllTrees] = useState<any>([]);

  const columns = useMemo<MRT_ColumnDef<Tree>[]>(
    () => [
      {
        accessorFn: (originalRow) => originalRow.serial_number,
        header: t("components.Tree.properties.serialNumber"),
      },
      {
        accessorFn: (originalRow) => {
          if (!originalRow.plantation_date) {
            return null;
          }

          return new Date(originalRow.plantation_date);
        },
        header: t("components.Tree.properties.plantationDate.exact"),
        filterVariant: "date",
        filterFn: "dateEquals",
        Cell: ({ cell }) => {
          const dateValue = cell.getValue<Date>();
          return dateValue !== null && dateValue.toLocaleDateString();
        },
      },
      {
        accessorFn: (originalRow) => originalRow.location.address || "",
        header: t("components.Tree.properties.address"),
      },
      {
        accessorFn: (originalRow) => originalRow.scientific_name,
        header: t("components.Tree.properties.scientificName"),
      },
      {
        accessorFn: (originalRow) => originalRow.vernacular_name,
        header: t("components.Tree.properties.vernacularName"),
      },
      {
        accessorFn: (originalRow) => originalRow.variety,
        header: t("components.Tree.properties.variety"),
      },
      {
        accessorFn: (originalRow) => originalRow.height,
        header: t("components.Tree.properties.height"),
      },
      {
        accessorFn: (originalRow) => originalRow.circumference,
        header: t("components.Tree.properties.circumference"),
      },
      {
        accessorFn: (originalRow) =>
          originalRow.habit !== ""
            ? t(`components.Tree.properties.habit.values.${originalRow.habit}`)
            : "",
        header: t("components.Tree.properties.habit.label"),
      },
      {
        accessorFn: (originalRow) =>
          originalRow.is_tree_of_interest ? t("common.yes") : t("common.no"),
        header: t("components.Tree.properties.isTreeOfInterest"),
      },
      {
        accessorFn: (originalRow) =>
          originalRow.watering ? t("common.yes") : t("common.no"),
        header: t("components.Tree.properties.watering"),
      },
    ],
    []
  );

  const fetchTrees = async () => {
    try {
      const result = await fetchAllTrees();
      setAllTrees(result?.data?.tree);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchTrees();
  }, []);

  return (
    <Container maxWidth="xl" sx={{ mt: 1 }}>
      <DetailsHeader
        title={t("components.Template.menuItems.plantHeritage.allTrees")}
      />
      {allTrees && <TreesGrid allTrees={allTrees} columns={columns} />}
    </Container>
  );
};

export default TreesPage;
