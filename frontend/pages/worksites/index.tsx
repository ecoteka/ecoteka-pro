import { NextPage } from "next";
import Container from "@mui/material/Container";
import { WorksitesGrid } from "@ecosystems/Worksites";
import { useFetchAllWorksitesLazyQuery } from "generated/graphql";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { DetailsHeader as GridHeader } from "@components/_core/headers";

const WorksitesIndexPage: NextPage = () => {
  const { t } = useTranslation(["components"]);
  const [fetchWorksites, { data: worksites }] = useFetchAllWorksitesLazyQuery();

  useEffect(() => {
    fetchWorksites();
  }, []);

  return (
    <Container maxWidth="xl" sx={{ mt: 1 }}>
      <GridHeader title={t("Template.menuItems.worksite.title")} />
      <WorksitesGrid worksites={worksites?.worksite ?? []} />
    </Container>
  );
};

export default WorksitesIndexPage;
