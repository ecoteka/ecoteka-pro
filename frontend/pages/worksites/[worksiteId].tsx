import { useEffect, useState } from "react";
import { NextPage } from "next";
import { useRouter } from "next/router";
import { useTranslation } from "react-i18next";
import {
  FetchOneWorksiteQuery,
  useDeleteInterventionMutation,
  useDeleteWorksiteByPkMutation,
  useFetchOneWorksiteLazyQuery,
  useFetchOneWorksiteLocationsLazyQuery,
  useUpdateInterventionMutation,
  useUpdateWorksiteMutation,
} from "@generated/graphql";
import { useForm } from "react-hook-form";
import useStore from "@lib/store/useStore";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import { Box, Button, Container, Grid, Stack } from "@mui/material";
import WorksiteCard from "@blocks/layout/WorksiteCard/WorksiteCard";
import LocationList from "@blocks/layout/LocationList";
import { DetailsHeader } from "@core/headers";
import WorkInProgress from "@core/WorkInProgress/WorkInProgress";
import ScheduledDateCard from "@components/_blocks/cards/ScheduledDateCard/ScheduledDateCard";
import _ from "lodash";
import Loading from "@components/layout/Loading";
import { exportToPDF, useDeviceSize } from "@lib/utils";
import MobileButtonsCard from "@components/_core/buttons/MobileButtonsCard";
import CloudDownloadIcon from "@mui/icons-material/CloudDownload";

// TODO:
const toNumber = (value) => _.toNumber(value);
const transformNumber = (value) => (isNaN(value) ? undefined : Number(value));

export type ConditionalSchema<T> = T extends string
  ? Yup.StringSchema
  : T extends number
  ? Yup.NumberSchema
  : T extends boolean
  ? Yup.BooleanSchema
  : T extends Record<any, any>
  ? Yup.AnyObjectSchema
  : T extends Array<any>
  ? Yup.ArraySchema<any, any>
  : Yup.AnySchema;

export type Shape<Fields> = {
  [Key in keyof Fields]: ConditionalSchema<Fields[Key]>;
};

// interface Worksite {
//   id?: string;
//   name: string;
//   location_status?: string;
//   description?: string;
// }

const worksiteSchema = Yup.object().shape<Shape<FetchOneWorksiteQuery>>({
  worksite: Yup.object().shape({
    id: Yup.string().nullable(),
    name: Yup.string().required("le champ est requis"),
    location_status: Yup.string(),
    description: Yup.string().nullable(),
    cost: Yup.number().transform(transformNumber).nullable(),
  }),
});

const WorksiteMainPage: NextPage = () => {
  const router = useRouter();
  const { worksiteId } = router.query;
  const { t } = useTranslation(["components", "common"]);
  const { app } = useStore((store) => store);
  //const session = useSession();
  const { isMobile } = useDeviceSize();
  const [
    fetchWorksite,
    {
      data: worksiteData,
      loading: worksiteLoading,
      error: worksiteError,
      refetch,
    },
  ] = useFetchOneWorksiteLazyQuery();
  const [fetchInterventions, { data: interventionsData }] =
    useFetchOneWorksiteLocationsLazyQuery();
  const [checkedInterventions, setCheckedInterventions] = useState<string[]>(
    []
  );
  const [updateWorksiteMutation] = useUpdateWorksiteMutation();
  const [deleteWorksiteByPkMutation] = useDeleteWorksiteByPkMutation();
  const [deleteInterventionByPkMutation] = useDeleteInterventionMutation();
  const [updateInterventionByPkMutation] = useUpdateInterventionMutation();

  const isWIP = false;

  const {
    handleSubmit,
    getValues,
    reset,
    setValue,
    watch,
    control,
    formState: { dirtyFields, errors },
  } = useForm<FetchOneWorksiteQuery>({
    resolver: yupResolver(worksiteSchema),
    mode: "all",
    defaultValues: {
      worksite: {
        id: "",
        name: "",
        location_status: "",
        description: "",
        cost: 0,
        creation_date: "",
        interventions: [],
        realization_date: "",
        scheduled_date: "",
      },
    },
  });

  const onSuccess = async (action: string) => {
    app.setAppState({
      snackbar: {
        ...app.snackbar,
        alerts: [
          {
            message: t(`WorksiteForm.success.${action}`),
            severity: "success",
          },
        ],
      },
    });
  };

  const watchName = watch("worksite.name");

  useEffect(() => {
    fetchWorksite({
      variables: { id: worksiteId },
    });
    fetchInterventions({
      variables: { id: worksiteId },
    });
  }, []);

  // useEffect(() => {
  //   console.log("watchAllFields :>> ", watchAllFields);
  //   console.log("errors :>> ", errors);
  // }, [watchAllFields, errors]);

  useEffect(() => {
    worksiteData && reset(worksiteData);
  }, [worksiteData]);

  const deleteWorksite = async (id: string) => {
    const deleteInterventionsPromises =
      interventionsData?.worksite?.interventions.map((worksiteIntervention) => {
        return deleteInterventionByPkMutation({
          variables: {
            id: worksiteIntervention.intervention.id,
          },
        });
      });

    if (deleteInterventionsPromises) {
      await Promise.all(deleteInterventionsPromises);
      await deleteWorksiteByPkMutation({
        variables: {
          id: id,
        },
        onCompleted: () => {
          onSuccess("delete");
        },
      });
    }
    await router.push(`/worksites`);
  };

  const updateWorksite = async () => {
    const formData = getValues();

    await updateWorksiteMutation({
      variables: {
        id: worksiteId,
        input_data: {
          scheduled_date: formData?.worksite?.scheduled_date,
          name: formData?.worksite?.name,
          cost: formData?.worksite?.cost,
          description: formData?.worksite?.description,
        },
      },
      onCompleted: () => {
        /*        if (checkedInterventions.length) {
          const now = new Date();

          const currentUserId = session?.data?.user?.id
            ? session?.data?.user?.id
            : "";
          validateInterventions({
            variables: {
              _in: checkedInterventions,
              realization_date: now.toISOString().split("T")[0],
              data_entry_user_id: currentUserId,
            },
          });
        } */

        onSuccess("update");
      },
    });
  };

  const handleCheckboxChange = (interventionId: string) => {
    setCheckedInterventions((prev) => {
      const isChecked = prev.includes(interventionId);
      if (isChecked) {
        return prev.filter((id) => id !== interventionId);
      } else {
        return [...prev, interventionId];
      }
    });
  };

  const handleScheduledDateChange = (
    id: string,
    date: Date | null | string
  ) => {
    updateWorksiteMutation({
      variables: {
        id: id,
        input_data: {
          scheduled_date: date,
        },
      },
      onCompleted: (response) => {
        interventionsData?.worksite?.interventions.map(
          (worksiteIntervention) => {
            const {
              intervention_type,
              intervention_partner,
              tree,
              location,
              scheduled_date,
              __typename,
              ...formDataWithoutObjects
            } = worksiteIntervention.intervention;
            return updateInterventionByPkMutation({
              variables: {
                scheduled_date: date,
                intervention_type_id:
                  worksiteIntervention.intervention.intervention_type.id,
                intervention_partner_id:
                  worksiteIntervention.intervention.intervention_partner?.id ||
                  null,
                ...formDataWithoutObjects,
              },
            });
          }
        );
        app.setAppState({
          snackbar: {
            ...app.snackbar,
            alerts: [
              {
                message: t(`WorksiteDetails.scheduledDateChange.success`),
                severity: "success",
              },
            ],
          },
        });
        refetch();
      },
    });
  };

  if (isWIP) {
    return <WorkInProgress type="page" />;
  }

  if (worksiteLoading) {
    return <Loading />;
  }

  return (
    <Container maxWidth="xl" sx={{ mt: 1 }}>
      {!isMobile && (
        <DetailsHeader
          title={watchName}
          withEditMode
          allowExport
          onSave={handleSubmit(updateWorksite)}
          onDelete={deleteWorksite}
          deleteMessage={t(`WorksiteForm.message.delete`)}
          reset={reset}
          dirtyFields={dirtyFields}
        />
      )}
      {isMobile && (
        <Box sx={{ textAlign: "center", mb: 1 }}>
          <Button
            variant="contained"
            color="secondary"
            onClick={exportToPDF}
            startIcon={<CloudDownloadIcon />}
          >
            {t("common.buttons.exportPdf")}
          </Button>
        </Box>
      )}
      <Grid container columns={12} spacing={2}>
        <Grid item xs={12} sm={12} md={6}>
          <Stack direction="column" spacing={{ xs: 1, sm: 2 }}>
            {worksiteData && (
              <ScheduledDateCard
                itemId={worksiteData?.worksite?.id}
                scheduledDate={worksiteData?.worksite?.scheduled_date as Date}
                onConfirmScheduledDateChange={handleScheduledDateChange}
              />
            )}
            {worksiteData && (
              <WorksiteCard
                data={worksiteData}
                control={control}
                setValue={setValue}
              />
            )}
          </Stack>
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          {interventionsData && worksiteData && (
            <LocationList
              data={interventionsData}
              onCheckboxChange={handleCheckboxChange}
            />
          )}
        </Grid>
      </Grid>
      {isMobile && (
        <MobileButtonsCard
          withEditMode
          //onSave={handleSubmit(updateLocation)}
          onDelete={deleteWorksite}
          reset={reset}
          dirtyFields={dirtyFields}
          deleteMessage={t(`WorksiteForm.message.delete`)}
        />
      )}
    </Container>
  );
};

export default WorksiteMainPage;
