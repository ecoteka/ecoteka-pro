import React, { useState, useEffect } from "react";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { DetailsHeader } from "@components/_core/headers";
import { NextPage } from "next";
import { useSession } from "next-auth/react";
import { useTranslation } from "react-i18next";
import UserInfos from "@components/_pages/Account/UserInfos/index";
import DashboardWidget from "@components/_pages/Account/DashboardWidget";
import AccountGrid from "@components/_pages/Account/AccountGrid";

const AccountPage: NextPage = () => {
  const session = useSession();
  const [isManager, setIsManager] = useState<boolean>(false);
  const [isAdmin, setIsAdmin] = useState<boolean>(false);
  const [isReader, setIsReader] = useState<boolean>(false);
  const currentUserRoles = session.data?.roles as string[];
  const adminRoleRegex = /admin/;
  const { t } = useTranslation(["components", "common"]);

  useEffect(() => {
    if (currentUserRoles.includes("team_manager")) {
      setIsManager(true);
    } else if (currentUserRoles.some((role) => adminRoleRegex.test(role))) {
      setIsAdmin(true);
    } else {
      setIsReader(true);
    }
  }, [currentUserRoles]);

  return (
    <Container
      maxWidth="xl"
      sx={{
        width: "100%",
        height: "100%",
        display: "flex",
        flexDirection: "column",
        mt: isAdmin ? "" : 2,
      }}
    >
      <DetailsHeader
        title={t("Template.menuItems.account.title")}
        type="account"
        isAdmin={isAdmin}
      />
      <Box
        sx={{
          height: "100%",
          display: "flex",
          flexDirection: "column",
          justifyContent: "flex-start",
          mt: 1,
        }}
      >
        <UserInfos />
        {isManager || isAdmin ? <DashboardWidget /> : ""}
        {isAdmin ? <AccountGrid /> : ""}
      </Box>
    </Container>
  );
};

export default AccountPage;
