import { NextPage } from "next";
import { Container } from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import dynamic from "next/dynamic";
import Loading from "@components/layout/Loading";
import _ from "lodash";
import getConfig from "next/config";

const SelectorSection = dynamic(
  () => import("@ecosystems/Index/IndexSelectorSection"),
  {
    loading: () => <Loading />,
  }
);

const LazyResilience = dynamic(
  () => import("@ecosystems/Index/IndexResilience"),
  {
    loading: () => <Loading />,
  }
);

const LazyHealth = dynamic(() => import("@ecosystems/Index/IndexHealth"), {
  loading: () => <Loading />,
});

const LazyEcosystemServices = dynamic(
  () => import("@ecosystems/Index/IndexEcosystemServices"),
  {
    loading: () => <Loading />,
  }
);

const LazySpeciesDistribution = dynamic(
  () => import("@ecosystems/Index/SpeciesDistributionWithData"),
  {
    loading: () => <Loading />,
    ssr: false,
  }
);

const LazyAnalytics = dynamic(
  () => import("@ecosystems/Index/IndexAnalytics"),
  {
    loading: () => <Loading />,
  }
);

const LazyTreeInformation = dynamic(
  () => import("@ecosystems/Index/IndexTreeInformation"),
  {
    loading: () => <Loading />,
  }
);

const Index: NextPage = () => {
  const { publicRuntimeConfig } = getConfig();

  // create custom component can (https://casl.js.org/v4/en/package/casl-react) for the dashboard with property variant 'urbasense', 'basic'
  // alternative : HOC access control (ex: can/(WidgetComponent, dashboard : ['urbasense', 'basic', 'other']))

  return (
    <Container maxWidth={false} data-cy="dashboard-container">
      <Grid xs={12} p={5} data-cy="dashboard-selector">
        {/*@ts-ignores */}
        <SelectorSection />
      </Grid>
      <Grid
        container
        data-cy="dashboard-health-and-resilience"
        flexDirection="row"
      >
        {/*@ts-ignores */}
        <Grid item xs={12} sm={12} lg={6} p={5} data-cy="dashboard-health">
          <LazyHealth />
        </Grid>
        {/*@ts-ignores */}
        <Grid item xs={12} sm={12} lg={6} p={5} data-cy="dashboard-resilience">
          <LazyResilience />
        </Grid>
      </Grid>
      <Grid xs={12} p={5} data-cy="dashboard-ecosystem-services">
        {/*@ts-ignores */}
        <LazyEcosystemServices />
      </Grid>
      <Grid xs={12} p={5} data-cy="dashboard-species">
        {/*@ts-ignores */}
        <LazySpeciesDistribution />
      </Grid>
      <Grid xs={12} p={5} data-cy="dashboard-tree-information">
        {/*@ts-ignores */}
        <LazyTreeInformation />
      </Grid>

      {/*in each component send specific metrics => dendro, tensio, meteo*/}
      {["True", "true"].includes(
        publicRuntimeConfig.URBASENSE_DASHBOARD_ACTIVE
      ) && (
        //@ts-ignores
        <LazyAnalytics />
      )}
    </Container>
  );
};

/** temporary diisable getServerSideProps while fixing api_internal webhook handler */

// export const getServerSideProps: GetServerSideProps<IndexProps> = async (
//   ctx
// ) => {
//   const { publicRuntimeConfig } = getConfig();
//   const session = await getSession(ctx);
//   const client = initializeApollo(session);

//   const { data } = await client.query<
//     GetUrbasenseMetricsByGroupQuery,
//     GetUrbasenseMetricsByGroupQueryVariables
//   >({
//     query: GetUrbasenseMetricsByGroupDocument,
//     variables: {
//       // examples variables
//       // id : groupdId
//       // provider: 'urbasense' | 'ecoteka'
//       // type: 'group', 'site'...
//     },
//   });

//   const urbasenseDashboardActive =
//     publicRuntimeConfig.URBASENSE_DASHBOARD_ACTIVE ?? true;

//   return {
//     props: {
//       metrics: data ?? null,
//       urbasenseDashboardActive,
//     },
//   };
// };

export default Index;
