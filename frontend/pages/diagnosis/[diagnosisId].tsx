import { Container } from "@mui/material";
import { NextPage } from "next";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useEffect } from "react";
import {
  Analysis_Tool,
  Diagnosis,
  Pathogen,
  useDeleteDiagnosisAnalysisToolMutation,
  useDeleteDiagnosisMutation,
  useDeleteDiagnosisPathogenMutation,
  useFetchDiagnosisFormDataLazyQuery,
  useFetchDiagnosisTypesLazyQuery,
  useFetchOneDiagnosisLazyQuery,
  useFetchOneTreeLazyQuery,
  useInsertDiagnosisAnalysisToolMutation,
  useInsertDiagnosisPathogenMutation,
  useUpdateDiagnosisMutation,
} from "../../generated/graphql";
import { useTranslation } from "react-i18next";
import useStore from "../../lib/store/useStore";
import Loading from "../../components/layout/Loading";
import DiagnosisForm, {
  IDiagnosis,
} from "../../components/forms/diagnosis/DiagnosisForm";

const DiagnosisIdPage: NextPage = () => {
  const router = useRouter();
  const { diagnosisId } = router.query;
  const { t } = useTranslation(["components", "common"]);
  const { app } = useStore((store) => store);
  const [fetchTreeData] = useFetchOneTreeLazyQuery();
  const [fetchData, { data: activeDiagnosis }] =
    useFetchOneDiagnosisLazyQuery();
  const [updateDiagnosisMutation] = useUpdateDiagnosisMutation();
  const [deleteDiagnosisMutation] = useDeleteDiagnosisMutation();
  const [insertDiagnosisPathogen] = useInsertDiagnosisPathogenMutation();
  const [insertDiagnosisAnalysisTool] =
    useInsertDiagnosisAnalysisToolMutation();
  const [deleteDiagnosisPathogen] = useDeleteDiagnosisPathogenMutation();
  const [deleteDiagnosisAnalysisTool] =
    useDeleteDiagnosisAnalysisToolMutation();
  const [fetchDiagnosisFormData, { data: diagnosisFormData }] =
    useFetchDiagnosisFormDataLazyQuery();

  const session = useSession();
  const currentUserId = session?.data?.user?.id ? session?.data?.user?.id : "";

  const onSuccess = async (action: string) => {
    app.setAppState({
      snackbar: {
        ...app.snackbar,
        alerts: [
          {
            message: t(`components.DiagnosisForm.success.${action}`),
            severity: "success",
          },
        ],
      },
    });
    const treeId = activeDiagnosis?.diagnosis[0].tree_id;
    await fetchTreeData({ variables: { _eq: treeId } });
    await router.push(`/tree/${treeId}`);
  };

  const updateDiagnosis = async (data: IDiagnosis) => {
    const {
      diagnosis_type,
      pathogens,
      analysis_tools,
      ...formDataWithoutObjects
    } = data;

    updateDiagnosisMutation({
      variables: {
        //@ts-ignores
        id: diagnosisId,
        //@ts-ignores
        diagnosis_type_id: data?.diagnosis_type.id,
        ...formDataWithoutObjects,
        data_entry_user_id: currentUserId,
      },

      onCompleted: (response) => {
        const hasPathogens =
          data && data.pathogens && Object.keys(data.pathogens).length > 0;
        const hasAnalysisTools = data.analysis_tools ? true : false;
        if (hasPathogens || !Object.keys(data.pathogens).length) {
          const pathogens = Object.values(data.pathogens).filter(
            (option: Pathogen) => option?.id !== ""
          );

          deleteDiagnosisPathogen({
            variables: { diagnosis_id: diagnosisId },
          });

          Promise.all(
            pathogens.map(async (pathogen: Pathogen) => {
              await insertDiagnosisPathogen({
                variables: {
                  diagnosis_id: diagnosisId,
                  pathogen_id: pathogen?.id,
                },
              });
            })
          );
        }
        if (hasAnalysisTools || !Object.keys(data.analysis_tools).length) {
          const analysisTools = Object.values(data.analysis_tools).filter(
            (option: Analysis_Tool) => option?.id !== ""
          );
          deleteDiagnosisAnalysisTool({
            variables: { diagnosis_id: diagnosisId },
          });
          Promise.all(
            analysisTools.map(async (analysisTool: Analysis_Tool) => {
              await insertDiagnosisAnalysisTool({
                variables: {
                  diagnosis_id: diagnosisId,
                  analysis_tool_id: analysisTool?.id,
                },
              });
            })
          );
        }

        onSuccess("update");
      },
    });
  };

  const deleteDiagnosis = async (id: string) => {
    deleteDiagnosisMutation({
      variables: {
        id: id,
      },
      onCompleted: () => {
        onSuccess("delete");
      },
    });
  };

  useEffect(() => {
    if (!activeDiagnosis) {
      fetchData({ variables: { _eq: diagnosisId } });
    }
    fetchDiagnosisFormData();
  }, [diagnosisId, activeDiagnosis]);

  if (!activeDiagnosis) {
    return <Loading />;
  }

  return (
    <Container maxWidth="xl" sx={{ mt: 1 }}>
      {activeDiagnosis && diagnosisFormData && (
        <DiagnosisForm
          activeDiagnosis={activeDiagnosis?.diagnosis[0]}
          diagnosisFormData={diagnosisFormData}
          onUpdateDiagnosis={updateDiagnosis}
          onDeleteDiagnosis={deleteDiagnosis}
        />
      )}
    </Container>
  );
};

export default DiagnosisIdPage;
