import { Box, Button, Stack } from "@mui/material";
import { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import { TreeIcon } from "@components/icons/TreeIcon";
import BigTreeIcon from "@components/icons/BigTreeIcon";
import { styled } from "@mui/material/styles";
import { useTranslation } from "react-i18next";
import BackIcon from "@mui/icons-material/ArrowBack";
import { useDeviceSize } from "@lib/utils";

const Div = styled("div")(({ theme }) => ({
  ...theme.typography.button,
  fontWeight: "bold",
  textAlign: "center",
  padding: theme.spacing(1),
}));

const Custom404: NextPage = () => {
  const router = useRouter();
  const { t } = useTranslation(["common"]);
  const { isMobile } = useDeviceSize();
  return (
    <Box sx={{ width: "100%", height: "100%" }}>
      <Head>
        <title>{t("notFound.title")}</title>
        <meta
          property="og:title"
          content={`404 - ${t("notFound.title")}`}
          key="title"
        />
      </Head>

      <Stack
        sx={{ width: "100%", height: "100%", background: "white" }}
        justifyContent="flex-end"
        alignItems="center"
      >
        <Box sx={{ width: "auto" }}>
          <Image
            src="/erreur-404@2x.png"
            width={isMobile ? 350 : 712}
            height={isMobile ? 197 : 400}
            sizes="(max-width: 768px) 100vw"
            alt="404 error image"
          />
          <Div>{t("notFound.label")}</Div>
          <Stack
            sx={{ width: "auto" }}
            direction="row"
            justifyContent="space-between"
            alignItems="center"
          >
            <Button onClick={() => router.back()} startIcon={<BackIcon />}>
              {t("notFound.back")}
            </Button>
            <Button variant="contained" href="/">
              {t("notFound.backToHomePage")}
            </Button>
          </Stack>
        </Box>
        <Box
          sx={{
            width: "100%",
            height: "25%",
            background:
              "linear-gradient(rgba(227, 227, 277, 1), rgba(227, 227, 277, 0.0) 0%), linear-gradient(-175deg, rgba(255, 255, 255, 0.9) 45%, rgba(6, 182, 174, 0.08) 0%)",
          }}
        >
          <Box sx={{ margin: "1rem" }}>
            <BigTreeIcon />
            <TreeIcon />
          </Box>
        </Box>
      </Stack>
    </Box>
  );
};

export default Custom404;
