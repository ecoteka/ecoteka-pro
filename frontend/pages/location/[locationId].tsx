import {
  Box,
  Button,
  Container,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Typography,
} from "@mui/material";
import { NextPage } from "next";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import {
  Intervention,
  Urban_Constraint,
  useCreateInterventionMutation,
  useCreateTreeMutation,
  useDeleteInterventionMutation,
  useDeleteLocationBoundariesMutation,
  useDeleteLocationMutation,
  useDeleteLocationUrbanConstraintsMutation,
  useDeleteWorksiteMutation,
  useFetchAllBoundariesLazyQuery,
  useFetchInterventionTypesAndPartnersLazyQuery,
  useFetchLocationEventsLazyQuery,
  useFetchLocationFormDataLazyQuery,
  useFetchOneInterventionLazyQuery,
  useFetchOneLocationLazyQuery,
  useInsertLocationBoundariesMutation,
  useInsertLocationUrbanConstraintMutation,
  useUpdateInterventionMutation,
  useUpdateLocationMutation,
  useUpdateLocationStatusWithLocationIdMutation,
} from "../../generated/graphql";
import * as yup from "yup";
import { useTranslation } from "react-i18next";
import useStore from "../../lib/store/useStore";
import Loading from "../../components/layout/Loading";
import { DetailsHeader } from "@components/_core/headers";
import LocationCard from "@components/_blocks/layout/LocationCard";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { ModalComponent } from "@components/forms/modal/ModalComponent";
import InterventionFields from "@components/forms/intervention/InterventionFields";
import ConfirmDialog from "@components/forms/dialog/ConfirmDialog";
import HistoricCard from "@components/_blocks/layout/HistoricCard";
import { CoreDialog } from "@components/forms/dialog/CoreDialog";
import { formatSerialNumber, useDeviceSize } from "@lib/utils";
import MobileButtonsCard from "@components/_core/buttons/MobileButtonsCard";
import { LocationType } from "@stores/pages/mapEditor";
import {
  FreeSolo,
  IFreeSoloOption,
} from "@components/forms/autocomplete/FreeSolo";

export interface Location {
  location: {
    id: string;
    latitude: number;
    longitude: number;
    address: string;
    foot_type: string;
    status?: string;
    boundaries: any;
    plantation_type: string;
    urban_site_id: string;
    sidewalk_type: string;
    landscape_type: string;
    is_protected: boolean;
    urban_constraint: Urban_Constraint[];
  };
  realization_date: string | null;
  intervention_partner: string | null;
  validation_note: string | null;
}

const locationSchema = yup.object().shape({
  location: yup.object().shape({
    id: yup.string().nullable(),
    latitude: yup.number().nullable().required(),
    longitude: yup.number().nullable().required(),
    address: yup.string().nullable(),
    description: yup.string(),
  }),
});

const LocationIdPage: NextPage = () => {
  const router = useRouter();
  const modal = router.query.interventionModal;
  const activeInterventionId = router.query.activeId;
  const { isMobile } = useDeviceSize();
  const [openConfirmModal, setOpenConfirmModal] = useState(false);
  const [action, setAction] = useState<string>("");
  const { locationId } = router.query;
  const { t } = useTranslation(["components", "common"]);
  const { app } = useStore((store) => store);
  const [fetchData, { data, refetch: refetchData }] =
    useFetchOneLocationLazyQuery();
  const [fetchBoundaries, { data: boundaries }] =
    useFetchAllBoundariesLazyQuery();
  const [fetchLocationFormData, { data: locationFormData }] =
    useFetchLocationFormDataLazyQuery();
  const [
    fetchInterventionTypesAndPartners,
    { data: interventionTypesAndPartners },
  ] = useFetchInterventionTypesAndPartnersLazyQuery();
  const [fetchOneIntervention, { data: activeIntervention }] =
    useFetchOneInterventionLazyQuery();
  const [fetchLocationEvents, { data: locationEvents, refetch }] =
    useFetchLocationEventsLazyQuery();

  const [createTreeMutation] = useCreateTreeMutation();
  const [createInterventionMutation] = useCreateInterventionMutation();
  const [updateInterventionMutation] = useUpdateInterventionMutation();
  const [deleteInterventionMutation] = useDeleteInterventionMutation();
  const [updateLocationMutation] = useUpdateLocationMutation();
  const [deleteLocationBoundaries] = useDeleteLocationBoundariesMutation();
  const [deleteLocationUrbanConstraints] =
    useDeleteLocationUrbanConstraintsMutation();
  const [insertlocationBoundaries] = useInsertLocationBoundariesMutation();
  const [insertLocationUrbanConstraint] =
    useInsertLocationUrbanConstraintMutation();
  const [deleteLocationMutation] = useDeleteLocationMutation();
  const [deleteWorksiteMutation] = useDeleteWorksiteMutation();
  const [updateLocationStatus] =
    useUpdateLocationStatusWithLocationIdMutation();

  const session = useSession();
  const now = new Date();

  const currentUserId = session?.data?.user?.id ? session?.data?.user?.id : "";

  const {
    handleSubmit,
    setValue,
    reset,
    watch,
    control,
    formState: { errors, dirtyFields },
  } = useForm<Location>({
    resolver: yupResolver(locationSchema),
  });

  const onSuccess = async (action: string) => {
    app.setAppState({
      snackbar: {
        ...app.snackbar,
        alerts: [
          {
            message: t(`components.LocationForm.success.${action}`),
            severity: "success",
          },
        ],
      },
    });
  };

  const updateLocation = async (data: Location) => {
    const locationData = data.location;

    const coordinates = [locationData.longitude, locationData.latitude];

    updateLocationMutation({
      variables: {
        locationId: locationId,
        coords: {
          type: "Point",
          coordinates,
        },
        address: locationData?.address || "",
        foot_type: locationData?.foot_type,
        plantation_type: locationData?.plantation_type,
        urban_site_id: locationData?.urban_site_id || null,
        sidewalk_type: locationData?.sidewalk_type,
        landscape_type: locationData?.landscape_type,
        is_protected: locationData?.is_protected,
        data_entry_user_id: currentUserId,
      },
      onCompleted: () => {
        const hasBoundaries = Object.values(locationData.boundaries).some(
          (value) => value !== ""
        );
        if (hasBoundaries) {
          deleteLocationBoundaries({
            variables: { location_id: locationId },
          });

          const boundaryIds = Object.values(locationData.boundaries).filter(
            (id) => id !== ""
          );

          Promise.all(
            boundaryIds.map(async (boundaryId) => {
              await insertlocationBoundaries({
                variables: {
                  location_id: locationId,
                  boundary_id: boundaryId,
                },
              });
            })
          );
        }
        const hasUrbanConstraints =
          locationData &&
          locationData?.urban_constraint &&
          Object.keys(locationData?.urban_constraint).length > 0;
        if (
          hasUrbanConstraints ||
          !Object.keys(locationData?.urban_constraint).length
        ) {
          deleteLocationUrbanConstraints({
            variables: { location_id: locationId },
          });

          const urbanConstraints = Object.values(
            locationData?.urban_constraint
          ).filter((option: Urban_Constraint) => option?.id !== "");

          Promise.all(
            urbanConstraints.map(async (urbanConstraint) => {
              await insertLocationUrbanConstraint({
                variables: {
                  location_id: locationId,
                  urban_constraint_id: urbanConstraint.id,
                },
              });
            })
          );
        }
        onSuccess("updateLocation");
      },
    });
  };

  const deleteLocation = async (id: string) => {
    // if only this location on a worksite, need to delete worksite
    if (
      data?.worksite_location &&
      data?.worksite_location.length &&
      data?.worksite_location[0]?.worksite.locations_aggregate.aggregate
        ?.count === 1
    ) {
      await deleteWorksiteMutation({
        variables: {
          location_id: id,
        },
      });
    }

    await deleteLocationMutation({
      variables: {
        id: id,
      },
      onCompleted: () => {
        onSuccess("deleteLocation");
      },
    });
    await router.push(`/map`);
  };

  const handleUpdateLocation = async (
    newStatus: string,
    interventionDate?: string
  ) => {
    ["empty", "alive"].includes(newStatus) &&
      (await updateLocationStatus({
        variables: {
          _eq: locationId,
          id_status:
            newStatus === "alive"
              ? "bf6fcfe3-fbe9-4467-98be-a935efcf67a3"
              : "b2fbee37-c6a6-4431-9c70-f361ade578b2",
        },
        onCompleted: async () => {
          if (newStatus === "empty") {
            onSuccess("updateLocation");
            setOpenConfirmModal(false);
            await router.push(`/location/${locationId}`);
            refetchData();
            refetch();
          }

          newStatus === "alive" &&
            (await createTreeMutation({
              variables: {
                serial_number: formatSerialNumber(undefined),
                location_id: locationId,
                data_entry_user_id: currentUserId,
                plantation_date: interventionDate,
              },
              onCompleted: async (response) => {
                onSuccess("updateAndCreateTree");
                await router.push(
                  `/tree/${response?.insert_tree_one?.id}?edit=active`
                );
              },
            }));
        },
      }));
  };

  const createIntervention = async (data) => {
    createInterventionMutation({
      variables: {
        tree_id: null,
        location_id: data.location?.locationId,
        realization_date: Boolean(data?.realization_date)
          ? data?.realization_date
          : null,
        scheduled_date: data?.scheduled_date,
        intervention_partner_id: data?.intervention_partner
          ? data?.intervention_partner.id
          : null,
        intervention_type_id: data?.intervention_type?.id,
        cost: data?.cost || null,
        note: data?.note,
        data_entry_user_id: currentUserId,
      },
      onCompleted: async (response) => {
        const interventionType =
          response?.insert_intervention_one?.intervention_type.slug;
        const interventionIsDone = Boolean(data?.realization_date);
        // if location is empty, intervention is planting so update status location to alive and create tree with default serial_number XXX
        if (interventionIsDone) {
          switch (interventionType) {
            case "grubbing":
              await handleUpdateLocation("empty");
              break;
            case "planting":
              await handleUpdateLocation(
                "alive",
                response?.insert_intervention_one?.realization_date
              );
              break;
            case "visual_diagnosis":
            case "detailed_diagnosis":
              router.replace(
                `/tree/${response?.insert_intervention_one?.tree?.id}?diagnosisModal=open&type=${interventionType}`
              );
              break;
            default:
              onSuccess("createInterventionAndValidate");
              router.replace(`/location/${locationId}`);
              setOpenConfirmModal(false);
              refetch();
              break;
          }
        } else {
          app.setAppState({
            snackbar: {
              ...app.snackbar,
              alerts: [
                {
                  message: t(`components.InterventionForm.success.create`),
                  severity: "success",
                },
              ],
            },
          });
          router.replace(`/location/${locationId}`);
          setOpenConfirmModal(false);
          refetch();
        }
      },
    });
  };

  const updateIntervention = async (data) => {
    updateInterventionMutation({
      variables: {
        id: activeInterventionId,
        realization_date: Boolean(data?.realization_date)
          ? data?.realization_date
          : null,
        scheduled_date: data?.scheduled_date,
        intervention_partner_id: data?.intervention_partner?.id || null,
        intervention_type_id: data?.intervention_type.id,
        cost: data?.cost,
        note: data?.note,
        data_entry_user_id: currentUserId,
      },
      onCompleted: async (response) => {
        const interventionType =
          response?.update_intervention_by_pk?.intervention_type.slug;
        const interventionIsDone = Boolean(data?.realization_date);
        // if location is empty, intervention is planting so update status location to alive and create tree with default serial_number XXX
        if (interventionIsDone) {
          switch (interventionType) {
            case "grubbing":
              await handleUpdateLocation("empty");
              break;
            case "planting":
              await handleUpdateLocation(
                "alive",
                response?.update_intervention_by_pk?.realization_date
              );
              break;
            case "visual_diagnosis":
            case "detailed_diagnosis":
              router.replace(
                `/tree/${response?.update_intervention_by_pk?.tree?.id}?diagnosisModal=open&type=${interventionType}`
              );
              break;
            default:
              onSuccess("createInterventionAndValidate");
              router.replace(`/location/${locationId}`);
              setOpenConfirmModal(false);
              refetch();
              break;
          }
        } else {
          onSuccess("updateIntervention");
          router.replace(`/location/${locationId}`);
          setOpenConfirmModal(false);
          refetch();
        }
      },
    });
  };

  const deleteIntervention = async () => {
    deleteInterventionMutation({
      variables: {
        id: activeInterventionId,
      },
      onCompleted: async () => {
        onSuccess("delete");
        router.replace(`/location/${locationId}`);
        setOpenConfirmModal(false);
        refetch();
      },
    });
  };

  const fetchOneInterventionData = async () => {
    try {
      await fetchOneIntervention({
        variables: {
          _eq: activeInterventionId,
        },
      });
    } catch (error) {
      console.log(error);
    }
  };

  const handleAbordValidationProcess = () => {
    setAction("");
    setValue("realization_date", null);
    setOpenConfirmModal(false);
  };

  useEffect(() => {
    if (!data?.location.length) {
      fetchData({ variables: { _eq: locationId } });
      fetchBoundaries();
      fetchLocationFormData();
      fetchLocationEvents({ variables: { _eq: locationId } });
      fetchInterventionTypesAndPartners();
    }
  }, [locationId, data]);

  useEffect(() => {
    if (activeInterventionId) {
      fetchOneInterventionData();
    }
  }, [activeInterventionId]);

  if (!data) {
    return <Loading />;
  }

  return (
    <Container maxWidth="xl" sx={isMobile ? styles.container : { mt: 1 }}>
      {!isMobile && (
        <DetailsHeader
          title={t("Sidebar.location")}
          locationStatus={
            data?.location[0].location_status.status as LocationType
          }
          withEditMode
          onSave={handleSubmit(updateLocation)}
          onDelete={deleteLocation}
          reset={reset}
          dirtyFields={dirtyFields}
          deleteMessage={t(`LocationForm.message.delete`)}
        />
      )}
      {data && (
        <>
          <LocationCard
            activeLocation={data?.location[0]}
            customControl={control}
            customSetValue={setValue}
            errors={errors}
            watch={watch}
            boundaries={boundaries?.boundary}
            locationFormData={locationFormData}
          />
          {!router.query.hasOwnProperty("edit") && (
            <>
              <HistoricCard
                events={locationEvents?.intervention}
                locationId={locationId}
              />

              <ModalComponent
                open={modal === "open"}
                handleClose={() => router.replace(`/location/${locationId}`)}
              >
                <>
                  <DialogTitle>
                    <Typography
                      variant="h6"
                      component="div"
                      sx={{
                        padding: "0px 0px 16px 0px",
                        textAlign: "center",
                      }}
                    >
                      {activeInterventionId
                        ? t(
                            `components.Template.menuItems.intervention.intervention`
                          )
                        : t(
                            `components.Template.menuItems.intervention.scheduleAnIntervention`
                          )}
                    </Typography>
                  </DialogTitle>
                  <DialogContent>
                    {activeInterventionId &&
                    activeIntervention?.intervention[0] ? (
                      <InterventionFields
                        customControl={control}
                        customSetValue={setValue}
                        errors={errors}
                        interventionTypes={
                          interventionTypesAndPartners
                            ? interventionTypesAndPartners?.intervention_type
                            : []
                        }
                        interventionPartners={
                          interventionTypesAndPartners
                            ? interventionTypesAndPartners?.intervention_partner
                            : []
                        }
                        activeIntervention={activeIntervention?.intervention[0]}
                        reset={reset}
                      />
                    ) : (
                      <InterventionFields
                        customControl={control}
                        customSetValue={setValue}
                        errors={errors}
                        interventionTypes={
                          interventionTypesAndPartners
                            ? interventionTypesAndPartners?.intervention_type
                            : []
                        }
                        interventionPartners={
                          interventionTypesAndPartners
                            ? interventionTypesAndPartners?.intervention_partner
                            : []
                        }
                        location={data?.location[0]}
                        reset={reset}
                      />
                    )}
                  </DialogContent>
                  <DialogActions
                    className="dialog-actions-dense"
                    sx={{ padding: "16px", justifyContent: "space-between" }}
                  >
                    <Button
                      onClick={() => {
                        setAction("");
                        setOpenConfirmModal(true);
                      }}
                      variant="contained"
                    >
                      {activeInterventionId
                        ? t(`common.buttons.save`)
                        : t(`common.buttons.schedule`)}
                    </Button>
                    {modal && activeInterventionId && (
                      <>
                        <Button
                          variant="outlined"
                          color="error"
                          onClick={() => {
                            setAction("delete");
                            setOpenConfirmModal(true);
                          }}
                        >
                          {t("common.buttons.delete")}
                        </Button>
                        {!activeIntervention?.intervention[0]
                          .realization_date && (
                          <Button
                            onClick={() => {
                              setAction("validate");
                              setValue(
                                "realization_date",
                                now.toISOString().split("T")[0]
                              );
                              setOpenConfirmModal(true);
                            }}
                          >
                            {t(
                              "components.Template.menuItems.intervention.declareRealized"
                            )}
                          </Button>
                        )}
                      </>
                    )}
                  </DialogActions>
                </>
              </ModalComponent>
              <ModalComponent
                open={openConfirmModal}
                handleClose={() => {
                  action === "validate"
                    ? handleAbordValidationProcess()
                    : setOpenConfirmModal(false);
                }}
              >
                {action === "validate" ? (
                  <ConfirmDialog
                    title={t(`components.InterventionForm.message.validate`)}
                    onConfirm={handleSubmit(updateIntervention)}
                    onAbort={handleAbordValidationProcess}
                  >
                    <>
                      {action === "validate" && (
                        <>
                          <Controller
                            name={`realization_date`}
                            control={control}
                            render={({ field: { onChange, value } }) => {
                              return (
                                <TextField
                                  label={
                                    t(
                                      `components.Intervention.properties.realizationDate`
                                    ) as string
                                  }
                                  variant="filled"
                                  type="date"
                                  sx={{ width: "100%", m: 1 }}
                                  InputLabelProps={{
                                    shrink: true,
                                  }}
                                  inputProps={{
                                    max: now.toISOString().split("T")[0],
                                  }}
                                  value={value}
                                  onChange={(e) => onChange(e.target.value)}
                                />
                              );
                            }}
                          />
                          <Controller
                            name={`intervention_partner`}
                            control={control}
                            render={() => {
                              return (
                                <Box sx={{ width: "100%", m: 1 }}>
                                  <FreeSolo
                                    defaultValue={""}
                                    control={control}
                                    setValue={setValue}
                                    name={
                                      "intervention_partner" as keyof Intervention
                                    }
                                    listOptions={
                                      interventionTypesAndPartners?.intervention_partner ??
                                      ([] as IFreeSoloOption[])
                                    }
                                    required={false}
                                    label={
                                      t(
                                        `components.Intervention.properties.interventionPartner`
                                      ) as string
                                    }
                                    object={"intervention"}
                                  />
                                </Box>
                              );
                            }}
                          />

                          <Controller
                            name={`validation_note`}
                            control={control}
                            defaultValue={""}
                            render={({ field: { onChange, ref, value } }) => (
                              <TextField
                                value={value}
                                inputRef={ref}
                                fullWidth
                                sx={{
                                  display: "flex",
                                  m: 1,
                                }}
                                multiline={true}
                                label={
                                  t(
                                    `components.Intervention.properties.note`
                                  ) as string
                                }
                                variant="filled"
                                placeholder={t(
                                  "components.Intervention.placeholder.validation_note"
                                )}
                                onChange={onChange}
                              />
                            )}
                          />
                        </>
                      )}
                    </>
                  </ConfirmDialog>
                ) : (
                  <ConfirmDialog
                    message={`${t("common.buttons.confirm")} ?`}
                    onConfirm={() => {
                      if (activeInterventionId && action !== "delete") {
                        handleSubmit(updateIntervention)();
                      } else if (activeInterventionId && action === "delete") {
                        deleteIntervention();
                      } else {
                        handleSubmit(createIntervention)();
                      }
                    }}
                    onAbort={() => setOpenConfirmModal(false)}
                  />
                )}
              </ModalComponent>
            </>
          )}
          {isMobile && (
            <MobileButtonsCard
              withEditMode
              onSave={handleSubmit(updateLocation)}
              onDelete={deleteLocation}
              reset={reset}
              dirtyFields={dirtyFields}
              deleteMessage={t(`LocationForm.message.delete`)}
            />
          )}
        </>
      )}
    </Container>
  );
};

export default LocationIdPage;

const styles = {
  container: {
    position: "relative",
    height: "calc(100% - 70px) !important",
    overflowY: "auto",
    mt: 1,
  },
};
