import create, { StateCreator } from "zustand";
import { persist } from "zustand/middleware";

type WorksiteFormData = {
  name: string;
  description?: string;
  location_status: {
    id: string;
    status: string;
  };
};

type WorksiteLocationFormData = {
  location_id: string;
  tree_id?: string | undefined;
  address: string;
};

type WorksiteInterventionFormData =
  | {
      location_id?: string; // UUID
      realization_date?: Date | string;
      scheduled_date: Date | string | null | undefined;
      note?: string | null;
      tree_id?: string;
      data_entry_user_id: string | undefined;
      intervention_partner: string;
      intervention_type: {
        slug: string;
        id: string;
      };
    }
  | undefined;

export interface IWorksiteFormState {
  worksite: WorksiteFormData;
  locations: WorksiteLocationFormData[];
  interventions: WorksiteInterventionFormData;
  actions: {
    setWorksite(worksite: WorksiteFormData): void;
    setLocations(locations: WorksiteLocationFormData[]): void;
    setInterventions(interventions: WorksiteInterventionFormData): void;
  };
}

export const initialFormDataState = {
  worksite: {
    name: "",
    description: "",
    location_status: {
      id: "",
      status: "alive",
    },
  },
  locations: [],
  interventions: undefined,
};

export const stateCreator: StateCreator<IWorksiteFormState> = (set) => ({
  ...initialFormDataState,
  actions: {
    setWorksite: (worksite: WorksiteFormData) => {
      set((state) => ({
        ...state,
        worksite,
      }));
    },
    setLocations: (locations: WorksiteLocationFormData[]) => {
      set((state) => ({
        ...state,
        locations,
      }));
    },
    setInterventions: (interventions: WorksiteInterventionFormData) => {
      set((state) => ({
        ...state,
        interventions,
      }));
    },
  },
});

export const useWorksiteFormStore = create<IWorksiteFormState>()(
  persist(stateCreator, {
    name: "worksiteFormStorage",
    partialize: (state) =>
      Object.fromEntries(
        Object.entries(state).filter(([key]) => !["actions"].includes(key))
      ),
  })
);

// getters hooks
export const useWorksiteData = () =>
  useWorksiteFormStore((state) => ({
    worksite: state.worksite,
    locations: state.locations,
    interventions: state.interventions,
  }));
export const useWorksiteFormData = () =>
  useWorksiteFormStore((state) => state.worksite);
export const useWorksiteLocationFormData = () =>
  useWorksiteFormStore((state) => state.locations);
export const useWorksiteInterventionFormData = () =>
  useWorksiteFormStore((state) => state.interventions);

// 🎉 one selector for all our actions
export const useWorksiteFormActions = () =>
  useWorksiteFormStore((state) => state.actions);
