import create, { StateCreator } from "zustand";
import { persist } from "zustand/middleware";

type PolygonFormData = {
  type: string;
  name: string;
  geometry?: any;
  area?: Number | null;
  locationsInside?: any;
  automaticAssociation?: boolean;
};

export interface IPolygonFormState {
  polygon: PolygonFormData;
  actions: {
    setPolygon(polygon: PolygonFormData): void;
  };
}

export const initialFormDataState = {
  polygon: {
    type: "",
    name: "",
    geometry: [],
    area: null,
    locationsInside: [],
    automaticAssociation: false,
  },
};

export const stateCreator: StateCreator<IPolygonFormState> = (set) => ({
  ...initialFormDataState,
  actions: {
    setPolygon: (polygon: PolygonFormData) => {
      set((state) => ({
        ...state,
        polygon,
      }));
    },
  },
});

export const usePolygonFormStore = create<IPolygonFormState>()(
  persist(stateCreator, {
    name: "polygonFormStorage",
    partialize: (state) =>
      Object.fromEntries(
        Object.entries(state).filter(([key]) => !["actions"].includes(key))
      ),
  })
);

// getters hooks

export const usePolygonFormData = () =>
  usePolygonFormStore((state) => state.polygon);

// 🎉 one selector for all our actions
export const usePolygonFormActions = () =>
  usePolygonFormStore((state) => state.actions);
