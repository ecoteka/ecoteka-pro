import { GetState, SetState } from "zustand";
import { GlobalState } from "./useStore";

export enum MapBackground {
  Map = "map",
  Satellite = "satellite",
  Ign = "ign",
}

export type MapActionsBarActionType = "filter" | "tree" | "addLocation";

export const defaultMapBackgrounds: MapBackground[] = [
  MapBackground.Map,
  MapBackground.Satellite,
  MapBackground.Ign,
];

export type LocationType = "alive" | "stump" | "empty";

export enum LocationTypeColors {
  "alive" = "#06b6ae",
  "stump" = "#966133",
  "empty" = "#f3ede8",
}

export interface MapFilters {
  locationStatus: string[];
}

export const defaultCoordinate = {
  lat: null,
  lon: null,
};

export const defaultMapFilters: MapFilters = {
  locationStatus: [],
};

export type MapZoomDirection = "in" | "out";

export interface MapState {
  editionMode: boolean;
  mapActiveAction: MapActionsBarActionType | undefined;
  mapLocationType: LocationType | undefined;
  mapBackground: MapBackground;
  mapPanelOpen: boolean;
  mapFilters: MapFilters;
  mapTempPoint: any;
  mapUserPosition: any;
  mapPanelToggle(open: boolean): void;
  mapActiveActionSet(
    action: MapActionsBarActionType | undefined,
    locationType?: LocationType
  ): void;
  mapFiltersSet(filters?: MapFilters): void;
  mapBackgroundSet(mapBackground?: MapBackground): void;
  editionModeSet(editionMode?: boolean): void;
  mapTempPointSet(mapTempPoint: any | undefined): void;
  mapUserPositionSet(mapUserPosition: any | undefined): void;
  setMapState(app: Partial<MapState>): void;
  modalDataOpen: boolean;
  modalDataOpenSet(open: boolean): void;
  zoomMax: MapZoomDirection | undefined;
  zoomMaxSet(mapZoomDirection: MapZoomDirection): void;
}

export const initialMapState = {
  editionMode: false,
  mapActiveAction: undefined,
  mapLocationType: undefined,
  mapBackground: MapBackground.Ign,
  mapPanelOpen: false,
  mapFilters: defaultMapFilters,
  mapActiveLocation: undefined,
  mapTempPoint: defaultCoordinate,
  mapUserPosition: defaultCoordinate,
  modalDataOpen: false,
  zoomMax: undefined,
};

const createMap = (set: SetState<GlobalState>, get: GetState<GlobalState>) => ({
  ...initialMapState,
  mapPanelToggle: (mapPanelOpen: boolean) => {
    set(({ map }) => ({
      map: {
        ...map,
        mapPanelOpen,
      },
    }));
  },
  mapActiveActionSet: (
    mapActiveAction?: MapActionsBarActionType | undefined,
    mapLocationType?: LocationType
  ) => {
    set(() => ({
      map: {
        ...get().map,
        mapActiveAction,
      },
    }));
    if (mapActiveAction === "addLocation") {
      set(() => ({
        map: {
          ...get().map,
          mapLocationType,
        },
      }));
    }
  },
  mapFiltersSet: (mapFilters: MapFilters) => {
    set({
      map: {
        ...get().map,
        mapFilters,
      },
    });
  },
  mapBackgroundSet: (mapBackground: MapBackground) => {
    set({
      map: {
        ...get().map,
        mapBackground,
      },
    });
  },
  editionModeSet: (editionMode: boolean) => {
    set(({ map }) => ({
      map: {
        ...map,
        editionMode,
      },
    }));
  },
  mapTempPointSet: (mapTempPoint: any | undefined) => {
    set({
      map: {
        ...get().map,
        mapTempPoint,
      },
    });
  },
  mapUserPositionSet: (mapUserPosition: any | undefined) => {
    set({
      map: {
        ...get().map,
        mapUserPosition,
      },
    });
  },
  setMapState: (map: Partial<MapState>) => {
    set({
      map: {
        ...get().map,
        ...map,
      },
    });
  },
  modalDataOpenSet: (modalDataOpen: boolean) => {
    set(() => ({
      map: {
        ...get().map,
        modalDataOpen,
      },
    }));
  },
  zoomMaxSet: (zoomMax: MapZoomDirection) => {
    set(() => ({
      map: {
        ...get().map,
        zoomMax,
      },
    }));
  },
});

export default createMap;
