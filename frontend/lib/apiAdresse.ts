export const fetchAdresse = async (lon: number, lat: number) => {
  if (!lon || !lat) {
    return;
  }
  try {
    const url = `https://api-adresse.data.gouv.fr/reverse/?lon=${lon}&lat=${lat}`;
    const response = await fetch(url);
    const json = await response.json();
    return json?.features[0]?.properties?.label;
  } catch (error) {
    return console.log(error);
  }
};

export const fetchLatLong = async (address: string) => {
  if (!address) {
    return;
  }
  try {
    const url = `https://api-adresse.data.gouv.fr/search/?q=${address}`;
    const response = await fetch(url);
    const json = await response.json();
    return {
      latitude: json?.features[0]?.geometry?.coordinates?.[1],
      longitude: json?.features[0]?.geometry?.coordinates?.[0],
    };
  } catch (error) {
    return console.log(error);
  }
};
