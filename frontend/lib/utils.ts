import { useMediaQuery, useTheme } from "@mui/material";
import { DateTime } from "luxon";
import { parse } from "date-fns";
import area from "@turf/area";

export const useDeviceSize = () => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const isTablet = useMediaQuery(theme.breakpoints.down("md"));
  const isDesktop = useMediaQuery(theme.breakpoints.up("md"));

  return { isMobile, isTablet, isDesktop };
};

export const formatDate = (str: string): string => {
  return str.split("-").reverse().join("-");
};

export const transformDateFormat = (dateString) => {
  const date = new Date(dateString);

  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  const formattedDate = `${year}-${month < 10 ? "0" + month : month}-${
    day < 10 ? "0" + day : day
  }`;

  return formattedDate;
};

export const formatSerialNumber = (
  scientificName: string | undefined
): string => {
  const scnameCode = scientificName ? scientificName.substring(0, 4) : "XXXX";
  const formatDate = DateTime.now().toFormat("yyyyLLdd-HHmmss");
  return [scnameCode, formatDate].join("_");
};

export const checkIfImageExists = (
  url: string,
  callback: (exists: boolean) => void
) => {
  const img = new Image();
  img.src = url;

  if (img.complete) {
    callback(true);
  } else {
    img.onload = () => {
      callback(true);
    };

    img.onerror = () => {
      callback(false);
    };
  }
};
type TSortObjKeysOptions = {
  order: "asc" | "desc";
};
interface ISortObjKeysParams {
  object: Record<string, number>;
  options?: TSortObjKeysOptions;
}
export const sortObjKeys = (
  obj: Record<string, number>,
  { order: value = "asc" }: TSortObjKeysOptions
): Record<string, number> =>
  Object.fromEntries(
    Object.entries(obj).sort(([, a], [, b]) =>
      value === "asc" ? a + b : a - b
    )
  );

export const pick = <T extends {}, K extends keyof T>(obj: T, ...keys: K[]) =>
  Object.fromEntries(
    keys.filter((key) => key in obj).map((key) => [key, obj[key]])
  ) as Pick<T, K>;

export const inclusivePick = <T extends {}, K extends string | number | symbol>(
  obj: T,
  ...keys: K[]
) =>
  Object.fromEntries(
    keys.map((key) => [key, obj[key as unknown as keyof T]])
  ) as { [key in K]: key extends keyof T ? T[key] : undefined };

export const omit = <T extends {}, K extends keyof T>(obj: T, ...keys: K[]) =>
  Object.fromEntries(
    Object.entries(obj).filter(([key]) => !keys.includes(key as K))
  ) as Omit<T, K>;

/**
 ** Hex color to RGBA color
 */
export const hexToRGBA = (hexCode: string, opacity: number) => {
  let hex = hexCode.replace("#", "");

  if (hex.length === 3) {
    hex = `${hex[0]}${hex[0]}${hex[1]}${hex[1]}${hex[2]}${hex[2]}`;
  }

  const r = parseInt(hex.substring(0, 2), 16);
  const g = parseInt(hex.substring(2, 4), 16);
  const b = parseInt(hex.substring(4, 6), 16);

  return `rgba(${r}, ${g}, ${b}, ${opacity})`;
};

export const calculateTreeAge = (plantationDate: string) => {
  try {
    const currentDate = new Date();
    const plantation = new Date(plantationDate);

    // Calculate the time difference in milliseconds
    const ageInMillis = currentDate.getTime() - plantation.getTime();

    // Convert milliseconds to years, months, and days
    const millisecondsPerDay = 24 * 60 * 60 * 1000;
    const years = Math.floor(ageInMillis / (365 * millisecondsPerDay));
    const remainingMillis = ageInMillis % (365 * millisecondsPerDay);
    const months = Math.floor(remainingMillis / (30 * millisecondsPerDay));
    const days = Math.floor(
      (remainingMillis % (30 * millisecondsPerDay)) / millisecondsPerDay
    );

    return { years, months, days };
  } catch (error) {
    return null;
  }
};

// !! utils to flatten nested objects, should not flatten array:  enhancement required, need typing
export const flatten = (
  objectOrArray,
  prefix = "",
  formatter = (k) => "." + k
) => {
  const nestElement = (prev, value, key) =>
    value && typeof value === "object"
      ? {
          ...prev,
          ...flatten(value, `${prefix}${formatter(key)}`, formatter),
        }
      : { ...prev, ...{ [`${prefix}${formatter(key)}`]: value } };

  return Array.isArray(objectOrArray)
    ? objectOrArray.reduce(nestElement, {})
    : Object.keys(objectOrArray).reduce(
        (prev, element) => nestElement(prev, objectOrArray[element], element),
        {}
      );
};

export const convertToPercentage = (score, maxScore) => {
  return (score / maxScore) * 100;
};

export const exportToPDF = () => {
  window.print();
};

export const convertDatetimeToDate = (datetimeString) => {
  if (!datetimeString) return null;

  const date = parse(datetimeString, "yyyy-MM-dd'T'HH:mm:ss", new Date());
  return `${date.getFullYear()}-${(date.getMonth() + 1)
    .toString()
    .padStart(2, "0")}-${date.getDate().toString().padStart(2, "0")}`;
};

export const getPolygonArea = (geometry) => {
  const coordinates = geometry.coordinates;
  return Math.round(area({ type: "Polygon", coordinates: coordinates }));
};

export const fetcher = async (url: string) => {
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error("An error occurred while fetching the data");
  }
  return response.json();
};
