import { FC, ReactNode } from "react";
import { Container, Box } from "@mui/material";
import { styled } from "@mui/material/styles";
import { SxProps, Theme } from "@mui/system";
import useStore from "../../lib/store/useStore";
import { useEffect } from "react";
import { signIn, useSession } from "next-auth/react";
import { useVh } from "@framini/use-vh";
import { useSnackbar } from "notistack";
import { useRouter } from "next/router";
import CustomHeader from "@components/_blocks/layout/Header/Header";

interface WrapperProps {
  height: number;
}

const Wrapper = styled("div")<WrapperProps>(({ theme, height }) => ({
  display: "flex",
  height: `${height}px`,
  flexDirection: "column",
  backgroundColor: theme.palette.background.default,
}));

interface LayoutBaseProps {
  children: ReactNode;
  sx?: SxProps<Theme>;
}

const LayoutBase: FC<LayoutBaseProps> = ({ children, sx }) => {
  const { snackbar, appContainer } = useStore((state) => {
    return {
      snackbar: state.app.snackbar,
      appContainer: state.app.appContainer,
    };
  });
  const { enqueueSnackbar } = useSnackbar();
  const router = useRouter();

  const { status } = useSession();
  const fullVh = useVh();

  useEffect(() => {
    if (status === "unauthenticated") {
      signIn("keycloak");
    }
  }, [status]);

  useEffect(() => {
    snackbar?.alerts?.map((alert) => {
      enqueueSnackbar(alert.message, {
        autoHideDuration: alert?.autoHideDuration ?? 4000,
        variant: alert.severity,
        persist: alert?.persist ?? false,
      });
    });
  }, [snackbar?.alerts, enqueueSnackbar]);

  return status === "authenticated" ? (
    <Wrapper height={fullVh}>
      <CustomHeader />
      {appContainer && router.route !== "/" ? (
        <Container
          data-id="container"
          sx={{
            flex: 1,
            padding: "2rem",
            ...sx,
          }}
        >
          {children}
        </Container>
      ) : (
        <Box
          data-id="box"
          sx={{
            flex: 1,
            padding: 0,
            ...sx,
          }}
        >
          {children}
        </Box>
      )}
    </Wrapper>
  ) : null;
};

export default LayoutBase;
