import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";
import Fill from "@core/Fill";

import MapEditorLayout from "./MapEditorLayout";

const meta = {
  title: "Layout/MapEditor",
  component: MapEditorLayout,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Map editor layout",
      },
    },
  },
} satisfies Meta<typeof MapEditorLayout>;

export default meta;
type Story = StoryObj<typeof MapEditorLayout>;

export const FullScreen: Story = {
  render: () => (
    <Box sx={{ with: "100vw", height: "100vh" }}>
      <MapEditorLayout
        drawerActive={false}
        closePanelCallback={() => alert("Closing panel")}
        Settings={<Fill color="blue" />}
        View={<Fill color="teal"></Fill>}
        Actions={<Fill color="red"></Fill>}
        Controls={<Fill color="orange"></Fill>}
        Navigation={<Fill color="yellow"></Fill>}
        Legend={<Fill color="green"></Fill>}
        Panel={<Fill color="beige"></Fill>}
      />
    </Box>
  ),
};

export const DrawerOpen: Story = {
  render: (args) => (
    <Box sx={{ with: "100vw", height: "100vh" }}>
      <MapEditorLayout
        View={<Fill color="teal" />}
        Actions={<Fill color="red" />}
        Controls={<Fill color="orange" />}
        Legend={<Fill color="green" />}
        Panel={<Fill color="beige" />}
        drawerActive={true}
        closePanelCallback={() => alert("Closing panel")}
      />
    </Box>
  ),
};
