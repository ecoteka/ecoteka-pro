import { Skeleton, Stack, Typography } from "@mui/material";
import { FC } from "react";
import { useTranslation } from "react-i18next";

const Loading: FC = () => {
  const { t } = useTranslation(["common"]);
  return (
    <Stack sx={{ width: "100%" }} data-cy="loading-stack">
      <Typography
        variant="h6"
        gutterBottom
        fontSize={15}
        textAlign={"center"}
        data-cy="loading-title"
      >
        {t("common.loadingPage")}
      </Typography>
      <Stack data-cy="loading-skeleton-stack">
        <Skeleton variant="rectangular" width={210} height={118} />
        <Skeleton animation="wave" />
        <Skeleton animation="wave" />
        <Skeleton animation="wave" />
        <Skeleton animation="wave" />
        <Skeleton animation="wave" />
        <Skeleton animation="wave" />
        <Skeleton animation="wave" />
        <Skeleton animation="wave" />
      </Stack>
    </Stack>
  );
};

export default Loading;
