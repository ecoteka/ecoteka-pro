import React, { FC } from "react";
import NoDataView from "./noDataView";
import DiagnosisCard from "../_blocks/layout/DiagnosisCard/DiagnosisCard";

export interface DiagnosisEventProps {
  activeTree: any;
}

const DiagnosisEvents: FC<DiagnosisEventProps> = ({ activeTree }) => {
  const lastDiagnosis = activeTree?.last_diagnoses[0];

  const LastDiagnosisBox: FC = () => {
    return <DiagnosisCard diagnosis={lastDiagnosis} />;
  };

  return (
    <>
      {!lastDiagnosis && (
        <NoDataView element="diagnosis" activeLocation={activeTree} />
      )}
      {lastDiagnosis && <LastDiagnosisBox />}
    </>
  );
};

export default DiagnosisEvents;
