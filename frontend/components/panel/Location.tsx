import { FC, useEffect, useState } from "react";
import {
  LocationType,
  MapActionsBarActionType,
} from "../../lib/store/pages/mapEditor";
import { useTranslation } from "react-i18next";
import { useForm, SubmitHandler } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { Grid, Button, CircularProgress, Typography, Box } from "@mui/material";

import { fetchAdresse } from "../../lib/apiAdresse";

import {
  CreateTreeMutationVariables,
  FetchAllBoundariesQuery,
  FetchLocationFormDataQuery,
  FetchOneLocationQuery,
} from "../../generated/graphql";
import { useMapEditorActions } from "../../lib/store/pages/mapEditor";
import { ModalComponent } from "@components/forms/modal/ModalComponent";
import ConfirmDialog from "@components/forms/dialog/ConfirmDialog";
import { formatSerialNumber } from "@lib/utils";
import LocationFields from "@components/forms/location/LocationFields";
import TreeFields from "@components/forms/tree/TreeFields";

export interface LocationProps {
  activeLocation: FetchOneLocationQuery["location"][0];
  tempPoint: any;
  locationType: LocationType;
  onCreateLocation(data: Location): void;
  mapActiveAction: MapActionsBarActionType;
  locationStatusList: { status: string; id: string }[];
  boundaries: FetchAllBoundariesQuery["boundary"];
  locationFormData: FetchLocationFormDataQuery;
}

export interface Location {
  id: string;
  location: {
    latitude: number;
    longitude: number;
    address: string;
    foot_type: string;
    plantation_type: string;
    urban_site_id: string;
    sidewalk_type: string;
    landscape_type: string;
    is_protected: boolean;
    tree?: CreateTreeMutationVariables;
    status?: string;
    note: string;
    boundaries: any;
    urban_constraint: any;
  };
  tree?: CreateTreeMutationVariables;
}

const PanelLocation: FC<LocationProps> = ({
  tempPoint,
  locationType,
  onCreateLocation,
  activeLocation,
  mapActiveAction,
  locationStatusList,
  boundaries,
  locationFormData,
}) => {
  const { t } = useTranslation(["components", "common"]);
  const actions = useMapEditorActions();

  const [openModal, setOpenModal] = useState<boolean>(false);
  const [showTreeForm, setShowTreeForm] = useState(false);
  const [loading, setLoading] = useState(false);
  const [activeStep, setActiveStep] = useState(0);

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const readOnlyForm = mapActiveAction === "addLocation" ? false : true;

  function _renderStepContent(step) {
    switch (step) {
      case 0:
        return (
          <LocationFields
            extended={true}
            activeLocation={activeLocation}
            customControl={control}
            customSetValue={setValue}
            errors={errors}
            watch={watch}
            boundaries={boundaries}
            locationFormData={locationFormData}
          />
        );
      case 1:
        return (
          <TreeFields
            activeTree={undefined}
            customControl={control}
            customSetValue={setValue}
            errors={errors}
            extended={true}
          />
        );

      default:
        return <div>Not Found</div>;
    }
  }

  const transformNumber = (value) => (isNaN(value) ? undefined : Number(value));

  const treeSchema = yup.object().shape({
    id: yup.string().nullable(),
    picture: yup.string(),
    variety: yup.string().nullable(),
    scientific_name: yup.string().nullable(),
    vernacular_name: yup.string().nullable(),
    habit: yup.string().nullable(),
    serial_number: showTreeForm ? yup.string() : yup.string(),
    height: yup.number().transform(transformNumber).nullable(),
    circumference: yup.number().transform(transformNumber).nullable(),
    plantation_date: yup.string().nullable(),
    watering: yup.boolean().nullable(),
    is_tree_of_interest: yup.boolean().nullable(),
    estimated_date: yup.boolean().nullable(),
    note: yup.string().nullable(),
    taxon_id: yup.number().nullable(),
  });

  const locationSchema = yup.object().shape({
    id: yup.string().nullable(),
    location: yup.object().shape({
      latitude: yup.number().nullable().required(),
      longitude: yup.number().nullable().required(),
      status: yup.string().nullable().required(),
      address: yup.string().nullable(),
      description: yup.string().nullable(),
    }),
    tree: treeSchema.nullable(),
  });

  const {
    handleSubmit,
    control,
    watch,
    setValue,
    getValues,
    reset,
    formState: { isDirty, dirtyFields, errors },
  } = useForm<Location>({
    resolver: yupResolver(locationSchema),
    defaultValues: {
      tree: {
        serial_number: formatSerialNumber(undefined),
      },
    },
  });

  const watchScientificName = watch("tree.scientific_name", undefined);

  const onConfirm: SubmitHandler<Location> = async (data) => {
    if (!data?.id) {
      onCreateLocation(data);
      setOpenModal(false);
    }
  };
  const mapActiveActionShowTree = ["tree", "addLocation"];

  useEffect(() => {
    locationType === "alive" &&
    mapActiveActionShowTree.includes(mapActiveAction)
      ? setShowTreeForm(true)
      : setShowTreeForm(false);
  }, [mapActiveAction, activeLocation, locationType]);

  useEffect(() => {
    if (mapActiveAction === "addLocation" && tempPoint) {
      const findAddress = async (lon, lat) => {
        if (lon && lat) {
          setLoading(true);
          const address = await fetchAdresse(lon, lat);
          return setValue("location.address", address), setLoading(false);
        }
      };
      setValue("location.latitude", tempPoint.lat);
      setValue("location.longitude", tempPoint.lon);
      setValue(
        "location.status",
        locationStatusList.find((status) => status.status === locationType)!.id
      );
      findAddress(tempPoint.lon, tempPoint.lat);
    }
  }, [
    tempPoint,
    setValue,
    mapActiveAction,
    locationStatusList,
    locationType,
    activeLocation,
  ]);

  // method to check if there is data entried on form before quit
  useEffect(() => {
    if (
      Object.keys(dirtyFields).length > 0 ||
      Object.keys(dirtyFields)["tree"]
    ) {
      actions.setModalDataOpen(true);
    } else {
      actions.setModalDataOpen(false);
    }
  }, [Object.keys(dirtyFields).length]);

  useEffect(() => {
    if (mapActiveAction === "addLocation") {
      reset({}, { keepValues: true });
    }
  }, []);

  useEffect(() => {
    setActiveStep(0);
  }, [tempPoint]);

  useEffect(() => {
    setValue(
      "tree.serial_number",
      formatSerialNumber(watchScientificName as string)
    );
  }, [watchScientificName, setValue]);

  return (
    <Grid
      container
      gap={1}
      sx={{ justifyContent: "center", mt: 0 }}
      data-cy="location-grid"
    >
      {showTreeForm && mapActiveAction === "addLocation" ? (
        <Typography sx={{ fontWeight: 600 }} data-cy="location-step">
          {t(`components.PanelTree.step`) as string} {activeStep + 1}/2
        </Typography>
      ) : (
        <Grid item data-cy="location-grid-item">
          {!showTreeForm && (
            <>
              <LocationFields
                extended={true}
                activeLocation={activeLocation}
                customControl={control}
                customSetValue={setValue}
                errors={errors}
                watch={watch}
                boundaries={boundaries}
                locationFormData={locationFormData}
              />
              <Grid
                container
                columns={12}
                justifyContent="flex-end"
                data-cy="location-form-save-button-container"
              >
                <Button
                  disabled={loading}
                  size="medium"
                  fullWidth
                  variant="contained"
                  color="primary"
                  onClick={() => setOpenModal(true)}
                  sx={{ mt: 1 }}
                >
                  {loading ? (
                    <CircularProgress size={24} />
                  ) : (
                    (t("common.buttons.save") as string)
                  )}
                </Button>
              </Grid>
            </>
          )}
        </Grid>
      )}
      {showTreeForm && (
        <Grid item>
          {mapActiveAction === "addLocation" && _renderStepContent(activeStep)}
        </Grid>
      )}
      <Grid item xs={12}>
        {showTreeForm &&
          mapActiveAction === "addLocation" &&
          activeStep === 0 && (
            <Button
              variant="contained"
              fullWidth
              color="primary"
              onClick={handleNext}
              data-cy="location-continue-step"
            >
              Continuer
            </Button>
          )}
        <Grid item xs={12} textAlign="center">
          {activeStep === 1 && (
            <>
              <Button
                variant="outlined"
                size="medium"
                onClick={handleBack}
                sx={{ m: 1 }}
                data-cy="location-back-step"
              >
                {t("common.buttons.previous")}
              </Button>
            </>
          )}
          {activeStep !== 0 && (
            <Button
              disabled={loading}
              size="medium"
              variant="contained"
              color="primary"
              onClick={() => setOpenModal(true)}
              data-cy="location-save-step"
            >
              {loading ? (
                <CircularProgress size={24} />
              ) : (
                (t("common.buttons.save") as string)
              )}
            </Button>
          )}
        </Grid>
      </Grid>
      <Box>
        <ModalComponent
          open={openModal}
          handleClose={() => setOpenModal(false)}
        >
          <ConfirmDialog
            message={t(`common.messages.save`)}
            onConfirm={handleSubmit(onConfirm)}
            onAbort={() => setOpenModal(false)}
          />
        </ModalComponent>
      </Box>
    </Grid>
  );
};

export default PanelLocation;
