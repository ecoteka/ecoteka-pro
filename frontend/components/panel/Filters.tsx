import { FC, useEffect, useState } from "react";
import {
  FormGroup,
  TextField,
  Box,
  Autocomplete,
  AutocompleteRenderInputParams,
} from "@mui/material";
import { useTranslation } from "react-i18next";
import _, { isNil, set, trim } from "lodash";
import { MapFilters } from "@lib/store/pages/mapEditor";
import {
  useFetchAllInterventionTypesLazyQuery,
  useFetchLocationsWithFiltersLazyQuery,
} from "generated/graphql";
import {
  useMapEditorActions,
  useMapFilters,
  useMapLocations,
} from "lib/store/pages/mapEditor";

import { TREE_CONDITIONS } from "@lib/constants";

export interface PanelInfoProps {
  handleLocationStatusFilterChange?: (
    id: string,
    e: React.ChangeEvent<HTMLInputElement>
  ) => void;
  mapFilters?: MapFilters;
  locationStatusList?: any;
}

export const confFilters = {
  address: { pathes: "address", operator: "_ilike" },
  interventionType: {
    pathes: [
      "tree.interventions.intervention_type.slug",
      "interventions.intervention_type.slug",
    ],
    operator: "_in",
  },
  interventionScheduledDate: {
    pathes: [
      "tree.interventions.scheduled_date",
      "interventions.scheduled_date",
    ],
    operator: "_eq",
  },
  interventionRealizationDate: {
    pathes: [
      "tree.interventions.realization_date",
      "interventions.realization_date",
    ],
    operator: "_eq",
  },
  diagnosisStatus: {
    pathes: "tree.diagnoses.tree_condition",
    operator: "_in",
  },
  locationStatus: {
    pathes: "location_status.status",
    operator: "_eq",
  },
};

export const confOperator = {
  _ilike: (val) => `%${val}%`,
  _in: (val) => val,
  _eq: (val) => val,
};

export const setWhereConfig = (mapFilters: any, confFilters: any): any => {
  const where = {
    _and: [],
  };
  for (const filterName in mapFilters) {
    let value = mapFilters[filterName];
    if (typeof value === "string") {
      value = trim(value);
    }
    if (
      isNil(value) ||
      value === "" ||
      (Array.isArray(value) && !value.length)
    ) {
      continue;
    }

    const conf = confFilters[filterName];
    if (typeof conf.pathes === "string") {
      where._and.push(
        // @ts-ignore
        set({}, conf.pathes, {
          [conf.operator]: confOperator[conf.operator](value),
        })
      );
    } else {
      // @ts-ignore
      where._and.push({
        _or: conf.pathes.map((path) => {
          return set({}, path, {
            [conf.operator]: confOperator[conf.operator](value),
          });
        }),
      });
    }
  }
  return where;
};

const PanelFilters: FC<PanelInfoProps> = ({}) => {
  const { t } = useTranslation("components");
  const mapFilters = useMapFilters();
  const actions = useMapEditorActions();
  const mapLocations = useMapLocations();
  const [fetchInterventionTypes, { data: interventionTypes }] =
    useFetchAllInterventionTypesLazyQuery();
  const [searchLocations, { data: locationsFiltered }] =
    useFetchLocationsWithFiltersLazyQuery();

  const {
    address,
    interventionType,
    diagnosisStatus,
    interventionScheduledDate,
    interventionRealizationDate,
  } = mapFilters;

  const handleInputChange = (inputName, inputValue) => {
    const updatedFilters = {
      ...mapFilters,
      [inputName]: inputValue,
    };
    handleFilterChange(updatedFilters);
  };

  const handleFilterChange = (updatedFilters) => {
    if (JSON.stringify(mapFilters) !== JSON.stringify(updatedFilters)) {
      actions.setMapFilters(updatedFilters);
    }
  };

  const optionsAddress =
    mapLocations && mapLocations.length > 0
      ? Array.from(
          new Set(
            mapLocations
              .filter((option) => option.address)
              .map(
                (option) =>
                  option.address &&
                  option.address
                    .replace(/^\d+\s*/, "")
                    .trim()
                    .replace(/\s+/g, " ")
              )
          )
        )
      : [];

  const [openAddressOptions, setOpenAddressOptions] = useState(false);

  const interventionTypeSlugs =
    interventionTypes?.intervention_type?.map((type) => type.slug) || [];
  const diagnosisTypeSlugs =
    TREE_CONDITIONS.map((condition) => condition) || [];

  const fields = [
    { type: "autocomplete", label: "address", column: "address" },
    { type: "select", label: "interventionType", column: "intervention_type" },
    {
      type: "date",
      label: "interventionScheduledDate",
      column: "intervention_scheduled_date",
    },
    {
      type: "date",
      label: "interventionRealizationDate",
      column: "intervention_realization_date",
    },
    { type: "select", label: "diagnosisStatus", column: "diagnosis" },
  ];

  const debouncedFetchSearchResults = _.debounce(() => {
    const where = setWhereConfig(mapFilters, confFilters);
    searchLocations({ variables: { where } });
  }, 350);

  useEffect(() => {
    fetchInterventionTypes();
  }, []);

  useEffect(() => {
    debouncedFetchSearchResults();

    actions.setMapFilters({
      address: address,
      interventionType: interventionType,
      diagnosisStatus: diagnosisStatus,
      interventionScheduledDate: interventionScheduledDate,
      interventionRealizationDate: interventionRealizationDate,
    });
    return () => {
      debouncedFetchSearchResults.cancel();
    };
  }, [
    address,
    interventionType,
    interventionScheduledDate,
    interventionRealizationDate,
    diagnosisStatus,
  ]);

  let areMapFiltersEmpty = true;
  areMapFiltersEmpty = Object.values(mapFilters).every((value) => {
    let withValue = false;
    withValue = !_.isEmpty(value) && true;
    return !withValue;
  });

  useEffect(() => {
    if (locationsFiltered?.locations && !areMapFiltersEmpty) {
      actions.setMapFilteredLocations(locationsFiltered.locations);
    } else {
      actions.setMapFilteredLocations(undefined);
    }
  }, [locationsFiltered, areMapFiltersEmpty]);

  return (
    <FormGroup data-cy="map-filters">
      {fields.map((field) => (
        <Box
          key={field.column}
          sx={{ m: 1 }}
          data-cy={`map-filter-${field.label}`}
        >
          {field?.label === "address" && (
            <Autocomplete
              id={`autocomplete-address`}
              autoComplete
              open={openAddressOptions}
              onOpen={() => {
                if (address) {
                  setOpenAddressOptions(true);
                }
              }}
              onClose={() => setOpenAddressOptions(false)}
              value={address}
              filterOptions={(options, { inputValue }) =>
                options.filter(
                  (option) =>
                    option &&
                    option.toLowerCase().includes(inputValue.toLowerCase())
                )
              }
              options={optionsAddress}
              noOptionsText={t("components.MapFilter.filters.noAddressFound")}
              getOptionLabel={(option) => {
                if (typeof option === "string") {
                  return option;
                }
                if (typeof option === "object") {
                  return option ?? "";
                }
                return "";
              }}
              onInputChange={(event, newInputValue, reason) => {
                handleInputChange("address", newInputValue);

                if (!newInputValue) {
                  setOpenAddressOptions(false);
                }
              }}
              renderInput={(params: AutocompleteRenderInputParams) => (
                <TextField
                  {...params}
                  label={
                    t(`components.LocationForm.properties.address`) as string
                  }
                  variant="filled"
                  InputLabelProps={{ shrink: true }}
                />
              )}
            />
          )}

          {field?.label === "interventionType" && (
            <Autocomplete
              id="autocomplete-interventionType"
              autoComplete
              multiple
              value={interventionType || []}
              options={interventionTypeSlugs || []}
              getOptionLabel={(option) =>
                t(`components.PanelTree.interventionsList.${option}`)
              }
              onChange={(event, newValues) => {
                const selectedOptions = newValues.map((option) => option); // Use the appropriate property here
                handleInputChange("interventionType", selectedOptions);
              }}
              renderInput={(params: AutocompleteRenderInputParams) => (
                <TextField
                  {...params}
                  label={
                    t(
                      `components.Intervention.properties.${field.label}`
                    ) as string
                  }
                  InputLabelProps={{ shrink: true }}
                />
              )}
            />
          )}
          {field?.label === "interventionScheduledDate" && (
            <TextField
              label={
                t(`components.Intervention.properties.scheduledDate`) as string
              }
              variant="filled"
              type="date"
              sx={{ width: "100%" }}
              InputLabelProps={{
                shrink: true,
              }}
              value={interventionScheduledDate}
              onChange={(e) =>
                handleInputChange("interventionScheduledDate", e.target.value)
              }
            />
          )}
          {field.label === "interventionRealizationDate" && (
            <TextField
              label={
                t(
                  `components.Intervention.properties.realizationDate`
                ) as string
              }
              variant="filled"
              type="date"
              sx={{ width: "100%" }}
              InputLabelProps={{
                shrink: true,
              }}
              value={interventionRealizationDate}
              onChange={(e) =>
                handleInputChange("interventionRealizationDate", e.target.value)
              }
            />
          )}

          {field?.label === "diagnosisStatus" && (
            <Autocomplete
              id="autocomplete-diagnosis"
              autoComplete
              multiple
              value={diagnosisStatus || []}
              options={diagnosisTypeSlugs || []}
              getOptionLabel={(option) =>
                t(`components.Diagnosis.conditions.${option}`) as string
              }
              onChange={(event, newValues) => {
                const selectedOptions = newValues.map((option) => option); // Use the appropriate property here
                handleInputChange("diagnosisStatus", selectedOptions);
              }}
              renderInput={(params: AutocompleteRenderInputParams) => (
                <TextField
                  {...params}
                  label={
                    t(`components.Diagnosis.labels.tree_condition`) as string
                  }
                  variant="filled"
                  InputLabelProps={{ shrink: true }}
                />
              )}
            />
          )}
        </Box>
      ))}
    </FormGroup>
  );
};

export default PanelFilters;
