import { FC } from "react";
import { Box, Stack } from "@mui/material";
import _ from "lodash";
import Geolocalisation from "./Geolocalisation";
import ExitEditButton from "./buttons/ExitEditButton";
import FilterButton from "./buttons/FilterButton";
import {
  LocationType,
  MapBackground,
  MapActionsBarActionType,
} from "../../lib/store/pages/mapEditor";
import LayerManager from "@components/_blocks/map/LayerManager/LayerManager";
import Toolbar from "@components/_blocks/map/Toolbar";
import { useDeviceSize } from "@lib/utils";

export interface MapActionsProps {
  handleOnActionClick(action: MapActionsBarActionType): void;
  handleOnBaseMapChange?(map: MapBackground): void;
  mapBackground: MapBackground;
  handleOnGeolocate?(x: number, y: number): void;
  handleOnAddLocation(type: LocationType): void;
  editionMode: boolean;
  cardPreview: boolean;
  exitEditMode(): void;
  locationStatusList: { status: string; id: string }[];
}

const MapActions: FC<MapActionsProps> = ({
  handleOnActionClick,
  handleOnBaseMapChange,
  mapBackground,
  handleOnGeolocate,
  editionMode,
  exitEditMode,
  handleOnAddLocation,
  cardPreview,
}) => {
  const { isTablet } = useDeviceSize();

  return (
    <>
      <Box
        sx={[styles.topActionsContainer, styles.nonClickable]}
        data-cy="map-actions-container-filter"
      >
        <Box
          sx={styles.clickable}
          data-cy="map-actions-container-filter-button"
        >
          <FilterButton handleOnActionClick={handleOnActionClick} />
        </Box>
      </Box>

      <Box
        sx={[styles.topActionsContainerRight, styles.nonClickable]}
        data-cy="map-actions-container-geolocalisation"
      >
        <Box
          sx={styles.clickable}
          data-cy="map-actions-container-geolocalisation-button"
        >
          <Geolocalisation onGeolocate={handleOnGeolocate} />
        </Box>
      </Box>

      <Box
        sx={[styles.topLeftActionsContainer, styles.clickable]}
        data-cy="map-actions-container-layer-manager"
      >
        <LayerManager isOpen={!isTablet} />
      </Box>
      <Stack
        sx={[styles.bottomMiddleActionsContainer]}
        spacing={1}
        direction={{ md: "row", xs: "column" }}
        alignItems={{ md: "flex-end ", xs: "center" }}
        justifyContent={{ md: "center ", xs: "left" }}
        data-cy="map-actions-container-toolbar-desktop"
      >
        {!editionMode ? (
          <Toolbar
            handleOnAddLocation={handleOnAddLocation}
            map={mapBackground}
            handleOnBaseMapChange={handleOnBaseMapChange}
          />
        ) : (
          <Box sx={styles.clickable} data-cy="map-actions-exit-edit-button">
            <ExitEditButton handleExitMode={exitEditMode} />
          </Box>
        )}
      </Stack>
    </>
  );
};

export default MapActions;

const styles = {
  nonClickable: {
    pointerEvents: "none",
  },
  clickable: {
    pointerEvents: "all",
  },
  topActionsContainer: {
    position: "absolute",
    top: 24,
    width: "100%",
    display: "flex",
    justifyContent: { md: "center ", xs: "right" },
    right: { md: 0, xs: 22 },
  },
  topActionsContainerRight: {
    position: "absolute",
    top: { md: 90, xs: 80 },
    width: "100%",
    display: "flex",
    justifyContent: { md: "right ", xs: "right" },
    right: { md: 10, xs: 22 },
  },
  bottomActionsContainerMobile: {
    position: "absolute",
    bottom: 60,
    right: 4,
    padding: 2,
    width: "100%",
    flexDirection: { md: "row", xs: "column" },
    justifyContent: { md: "space-between ", xs: "right" },
    alignItems: "end",
  },
  bottomMiddleActionsContainer: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: "50px",
  },
  staticTooltipLabel: {
    width: "150px",
  },
  topLeftActionsContainer: {
    position: "absolute",
    top: 24,
    width: "auto",
    display: "flex",
    justifyContent: { md: "left ", xs: "left" },
    left: { md: 10, xs: 22 },
  },
};
