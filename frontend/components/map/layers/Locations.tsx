import { FC } from "react";
import { GeoJsonLayer } from "@deck.gl/layers";
import { ThemeSelectorMode } from "../../header/ThemeSelector";
import { MapBackground } from "../../../lib/store/createMap";
import { Location } from "generated/graphql";
import _ from "lodash";
import { Feature } from "maplibre-gl";

export interface LocationsLayerProps {
  locations: any; // TODO(today): create type for GeoJSonFeatureCollection
  mapActiveLocation: any;
  selectedLocations: any[] | undefined; //TODO: fix types
  mapFilteredLocations: any;
  mapBackground: MapBackground;
  appMode: ThemeSelectorMode;
  mapActiveLocationId: string;
}

const isFeatureSelected = (
  currentFeature: Feature,
  selectedLocations: Location[]
): boolean => {
  if (selectedLocations) {
    return selectedLocations.some(
      (selectedLocation) => selectedLocation.id === currentFeature.properties.id
    );
  }
  return false;
};

const getFillColor = (d, selectedFeatures) => {
  if (isFeatureSelected(d, selectedFeatures)) {
    return [255, 243, 80, 255];
  }
  if (d?.properties?.location_status?.color) {
    return JSON.parse(d?.properties?.location_status?.color);
  }
  return [0, 0, 0, 255];
};

const getLineColor = (
  d,
  mapActiveLocation: number,
  selectedFeatures: Location[] | undefined,
  mapBackground: MapBackground,
  appMode: ThemeSelectorMode,
  mapActiveLocationId: string
) => {
  if (isFeatureSelected(d, selectedFeatures as Location[])) {
    return [255, 243, 80, 255];
  }
  if (
    mapActiveLocation === d.properties.id ||
    mapActiveLocationId === d.properties.id
  ) {
    return [255, 100, 0];
  }
  if (
    mapBackground === MapBackground.Satellite ||
    (appMode === ThemeSelectorMode.dark && mapBackground !== MapBackground.Ign)
  ) {
    return [255, 255, 255, 190];
  }
  return [0, 0, 0, 100];
};

const getLineWidth = (d, mapActiveLocationId: number) => {
  return mapActiveLocationId === d.properties.id ? 20 : 10;
};
const getPointRadius = (d, mapActiveLocationId: number, selectedFeatures) => {
  if (mapActiveLocationId === d.properties.id) {
    return 8;
  }
  if (isFeatureSelected(d, selectedFeatures)) {
    return 8;
  }
  return 6;
};
// @ts-ignore
const LocationsLayer: FC<LocationsLayerProps> = ({
  locations,
  mapActiveLocation,
  mapBackground,
  selectedLocations,
  mapFilteredLocations,
  appMode,
  mapActiveLocationId,
}) => {
  return new GeoJsonLayer({
    id: "locations",
    data: mapFilteredLocations ? mapFilteredLocations : locations,
    pickable: true,
    filled: true,
    extruded: true,
    pointType: "circle",
    // @ts-ignore
    lineWidthScale: (d) => getLineWidth(d, mapActiveLocation),
    // pointRadiusMinPixels: (d) =>
    //   getPointRadius(d, mapActiveLocation, selectedLocations),
    pointRadiusMinPixels: 6,
    pointRadiusScale: 1,
    minRadius: 5,
    radiusMinPixels: 0.5,
    lineWidthMinPixels: 2,
    lineWidthMaxPixels: 3,
    autoHighlight: true,
    visible: true,
    getFillColor: (d) => getFillColor(d, selectedLocations),
    // @ts-ignore
    getLineColor: (d) =>
      getLineColor(
        d,
        mapActiveLocation,
        selectedLocations,
        mapBackground,
        appMode,
        mapActiveLocationId
      ),
    updateTriggers: {
      getLineColor: [
        mapActiveLocation,
        mapBackground,
        appMode,
        selectedLocations,
        mapActiveLocationId,
      ],
    },
  });
};

export default LocationsLayer;
