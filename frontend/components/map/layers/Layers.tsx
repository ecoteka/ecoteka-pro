import { MVTLayer } from "@deck.gl/geo-layers";
import { MapBackground } from "@stores/createMap";
import getConfig from "next/config";

const generateLayerDataUrl = (
  layer: string = "",
  forceReload: boolean = false,
  queryStringLocations: string,
  queryStringBoundaries: string
) => {
  const { publicRuntimeConfig } = getConfig();
  const timestamp = forceReload ? `?t=${Date.now()}` : "";
  const baseEndpoint = `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/martin/${layer}/{z}/{x}/{y}`;

  let queryString = "";
  if (layer === "filter_locations") {
    queryString = queryStringLocations;
  } else if (layer === "filter_boundaries") {
    queryString = queryStringBoundaries;
  }

  return `${baseEndpoint}${timestamp}${
    timestamp !== "" ? `&${queryString}` : `?${queryString}`
  }`;
};

const getLineColor = (
  d,
  mapBackground: MapBackground,
  mapActiveLocationId: string | undefined,
  isFeatureSelected?: (d) => void | undefined
) => {
  if (isFeatureSelected && isFeatureSelected(d)) {
    return [255, 243, 80, 255];
  }
  if (mapActiveLocationId === d.properties.location_id) {
    return [255, 100, 0];
  }
  if (
    mapBackground === MapBackground.Satellite ||
    mapBackground !== MapBackground.Ign
  ) {
    return [139, 137, 132, 100];
  }
  return [139, 137, 132, 100];
};

const createTooltip = () => {
  const tooltip: HTMLDivElement = document.createElement("div");
  tooltip.style.position = "absolute";
  tooltip.style.pointerEvents = "none";
  document.body.append(tooltip);
  applyTooltipStyles(tooltip, tooltipBaseStyles);
  return tooltip;
};

const applyTooltipStyles = (
  element: HTMLElement,
  styles: { [key: string]: string }
) => {
  for (const [key, value] of Object.entries(styles)) {
    element.style[key] = value;
  }
};

const createLocationsLayer = (
  generateLayerDataUrl: Function,
  forceReloadLayer: boolean,
  setIsLoading: React.Dispatch<React.SetStateAction<boolean>>,
  handleOnMapClick: (d) => {},
  actions: any,
  queryStringLocations: string,
  isFeatureSelected,
  mapBackground,
  selectedLocationIds,
  mapActiveLocationId
) => {
  const getFillColor = (d) => {
    if (isFeatureSelected(d)) {
      return [255, 243, 80, 255];
    }
    if (d?.properties?.status_color) {
      return JSON.parse(d?.properties?.status_color);
    }

    return [0, 0, 0, 255];
  };
  const ecotekaLocations = new MVTLayer({
    // TODO : add to NEXT_PUBLIC_TILESERVICE_URL
    data: generateLayerDataUrl(
      "filter_locations",
      forceReloadLayer,
      queryStringLocations,
      ""
    ),
    id: "locations",
    // @ts-ignore
    getRadius: 3,
    opacity: 0.9,
    stroked: true,
    pickable: true,
    filled: true,
    extruded: true,
    pointType: "circle",
    pointRadiusMinPixels: 8,
    pointRadiusScale: 1,
    minRadius: 5,
    radiusMinPixels: 0.5,
    lineWidthMinPixels: 1,
    lineWidthMaxPixels: 3,
    autoHighlight: true,
    visible: true,
    lineWidthScale: 5,
    getFillColor: (d) => getFillColor(d),
    getLineColor: (d) => getLineColor(d, mapBackground, mapActiveLocationId),
    updateTriggers: {
      getLineColor: [mapBackground, selectedLocationIds, mapActiveLocationId],
      getFillColor: [selectedLocationIds],
    },
    onClick: (d) => handleOnMapClick(d),
    onHover: (d) => {
      // console.log("d :>> ", d);
    },
    onViewportLoad(): void {
      setTimeout(() => {
        setIsLoading(false);
      }, 5);
    },
    onTileLoad: () => {
      setIsLoading(true);
    },
  });
  actions.setMapLayers([ecotekaLocations]);
  return ecotekaLocations;
};

const createBoundariesLayer = (
  generateLayerDataUrl: Function,
  forceReloadLayer: boolean,
  setIsLoading: React.Dispatch<React.SetStateAction<boolean>>,
  handleOnMapClick: (d) => {},
  actions: any,
  queryStringBoundaries: string
) => {
  const tooltip = createTooltip();

  const updateTooltip = ({ object, x, y }: any) => {
    if (object) {
      tooltip.style.display = "block";
      tooltip.style.left = `${x}px`;
      tooltip.style.top = `${y}px`;

      let backgroundColor;
      if (object.properties.boundary_type === "geographic") {
        backgroundColor = "rgba(255, 68, 225, 0.7)";
      } else if (object.properties.boundary_type === "station") {
        backgroundColor = "rgba(144, 85, 253, 0.7)";
      }
      tooltip.style.backgroundColor = backgroundColor;
      tooltip.innerText = object.properties.boundary_name;
    } else {
      tooltip.style.display = "none";
    }
  };

  const ecotekaBoundaries = new MVTLayer({
    data: generateLayerDataUrl(
      "filter_boundaries",
      forceReloadLayer,
      "",
      queryStringBoundaries
    ),
    id: "boundaries",
    // @ts-ignore
    getRadius: 3,
    opacity: 1,
    stroked: true,
    pickable: true,
    filled: false,
    lineWidthMinPixels: 6,
    lineWidthMaxPixels: 6,
    getLineColor: (d) => {
      const boundaryType = d.properties.boundary_type;
      if (boundaryType === "geographic") {
        return [255, 68, 225, 255];
      } else if (boundaryType === "station") {
        return [144, 85, 253, 255];
      }
    },
    getFillColor: (d) => [255, 255, 255, 0],
    onClick: (d) => handleOnMapClick(d),
    onHover: updateTooltip,
    onViewportLoad: () => {
      setTimeout(() => {
        setIsLoading(false);
      }, 5);
    },
    onTileLoad: () => {
      setIsLoading(true);
    },
  });

  actions.setMapLayers([ecotekaBoundaries]);
  return ecotekaBoundaries;
};

const tooltipBaseStyles = {
  position: "absolute",
  pointerEvents: "none",
  color: "#fff",
  padding: "3px",
  borderRadius: "5px",
};

export { generateLayerDataUrl, createLocationsLayer, createBoundariesLayer };
