import { FC } from "react";
import GpsFixedIcon from "@mui/icons-material/GpsFixed";
import { Fab } from "@mui/material";
import { useGeolocated } from "react-geolocated";

interface GeolocalisationProps {
  onGeolocate?(x: number, y: number): void;
}

const Geolocalisation: FC<GeolocalisationProps> = ({ onGeolocate }) => {
  const { getPosition, isGeolocationEnabled, isGeolocationAvailable } =
    useGeolocated({
      suppressLocationOnMount: true,
      onSuccess: (position) => {
        onGeolocate &&
          onGeolocate(position.coords.longitude, position.coords.latitude);
      },
    });

  const handleOnClick = () => {
    if (isGeolocationEnabled && isGeolocationAvailable) {
      getPosition();
    }
  };

  return (
    <Fab
      color="inherit"
      size="medium"
      aria-label="geolocate"
      onClick={handleOnClick}
      data-cy="map-actions-geolocalisation-button"
    >
      <GpsFixedIcon sx={{ color: "#1D7554" }} />
    </Fab>
  );
};
export default Geolocalisation;
