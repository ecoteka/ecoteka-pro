import { FC, ReactNode, useState } from "react";
import {
  Stack,
  SwipeableDrawer,
  Typography,
  Box,
  Fab,
  Button,
  IconButton,
} from "@mui/material";
import { useTranslation } from "react-i18next";
import TuneIcon from "@mui/icons-material/Tune";
import {
  MapActionsBarActionType,
  LocationType,
} from "../../lib/store/pages/mapEditor";
import { ModalComponent } from "../forms/modal/ModalComponent";
import ConfirmDialog from "../forms/dialog/ConfirmDialog";
import BackButton from "../layout/BackButton";
import { FetchOneLocationQuery } from "../../generated/graphql";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import {
  useMapEditorActions,
  useModalDataOpen,
} from "../../lib/store/pages/mapEditor";
import MiniBar from "./MiniBar";
import { useDeviceSize } from "@lib/utils";

export interface PanelProps {
  title?: string;
  children?: ReactNode;
  mapPanelOpen: boolean;
  mapActiveAction: MapActionsBarActionType;
  handleCancelProcess(): void;
  handleDrawerClose(): void;
  mapActiveLocation: FetchOneLocationQuery["location"][0] | undefined;
  handleOnOpen(): void;
  mapLocationType: LocationType;
}

const iOS =
  typeof navigator !== "undefined" &&
  /iPad|iPhone|iPod/.test(navigator.userAgent);

const Panel: FC<PanelProps> = ({
  children,
  mapPanelOpen,
  mapActiveAction,
  handleDrawerClose,
  handleCancelProcess,
  handleOnOpen,
  mapActiveLocation,
  mapLocationType,
}) => {
  const actions = useMapEditorActions();
  const modalDataOpen = useModalDataOpen();
  const { t } = useTranslation(["components", "common"]);
  const locationStatus = mapActiveLocation?.location_status.status;
  const [openDataModal, setOpenDataModal] = useState<boolean>(false);
  const { isMobile, isDesktop } = useDeviceSize();
  const drawerWidth = isDesktop ? "33%" : 550;
  const handleMiniBarClick = () => {
    actions.setMapPanelOpen(true);
  };

  const titles = {
    tree: t(`Sidebar.${locationStatus}`, {
      returnObjects: true,
    }),
    info: t("MapActionBar.info", { returnObjects: true }),
    filter: t("Map.Filter.title", { returnObjects: true }),
    stump: t("common.addStump", { returnObjects: true }),
    empty: t("common.addLocation", { returnObjects: true }),
    alive: t("common.addTree", { returnObjects: true }),
    addIntervention: t("common.addIntervention", { returnObjects: true }),
    intervention: t("Template.menuItems.intervention.intervention", {
      returnObjects: true,
    }),
    addWorksiteGroup: t("WorksiteForm.title", {
      returnObjects: true,
    }),
  };

  const styles = {
    drawer: {
      width: drawerWidth,
      "& .MuiDrawer-paper": {
        width: drawerWidth,
        marginTop: "64px",
        height: "calc(100% - 64px)",
        boxSizing: "border-box",
      },
    },
    drawerContent: {
      flex: 1,
      padding: isMobile ? 0.5 : 2,
      overflow: "auto",
    },
    mobile: {
      "& .MuiDrawer-paper": {
        marginTop: "56px",
        maxHeight: "calc(100% - 50%)",
        boxSizing: "border-box",
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
      },
    },
  };

  const checkIfData = () => {
    if (modalDataOpen === true) {
      setOpenDataModal(true);
    } else {
      handleDrawerClose();
    }
  };

  const onConfirmModal = () => {
    handleDrawerClose();
    setOpenDataModal(false);
    actions.setModalDataOpen(false);
  };

  return (
    <>
      {
        // @ts-ignore
      }
      <SwipeableDrawer
        sx={isDesktop ? styles.drawer : styles.mobile}
        variant="persistent"
        anchor={isDesktop ? "left" : "bottom"}
        open={mapPanelOpen}
        onClose={handleDrawerClose}
        disableBackdropTransition={!iOS}
        disableDiscovery={iOS}
        onOpen={handleOnOpen}
        data-cy="map-drawer"
      >
        <Stack
          spacing={2}
          sx={styles.drawerContent}
          data-cy="map-drawer-content"
        >
          <Stack
            direction="row"
            alignItems="center"
            justifyContent={isDesktop ? "space-between" : "flex-start"}
          >
            {!["filter", "addWorksiteGroup", "addPolygon"].includes(
              mapActiveAction
            ) && <BackButton onClick={checkIfData} />}
            {["addWorksiteGroup", "addPolygon"].includes(mapActiveAction) && (
              <Button
                variant="outlined"
                onClick={handleCancelProcess}
                data-cy="worksite-cancel-button"
              >
                {t("common.buttons.cancel")}
              </Button>
            )}
            <Typography
              sx={{
                textTransform: "uppercase",
                wordBreak: "break-word",
                fontSize: isDesktop ? "24px" : "19px",
                margin: "auto",
              }}
              variant="h5"
              data-cy="map-drawer-title"
            >
              {mapActiveAction === "addLocation"
                ? (titles[mapLocationType] as unknown as string)
                : (titles[mapActiveAction] as unknown as string)}
            </Typography>
            {!isDesktop && mapActiveAction === "addWorksiteGroup" && (
              <IconButton
                onClick={handleDrawerClose}
                data-cy="map-drawer-close"
              >
                <KeyboardArrowDownIcon />
              </IconButton>
            )}
          </Stack>
          {children}
        </Stack>
        {modalDataOpen && (
          <Box data-cy="map-modal-data-box">
            <ModalComponent
              open={openDataModal}
              handleClose={() => setOpenDataModal(false)}
            >
              <ConfirmDialog
                title={t("common.quitForm")}
                message={t("common.messages.lostData")}
                onConfirm={onConfirmModal}
                onAbort={() => setOpenDataModal(false)}
              />
            </ModalComponent>
          </Box>
        )}
      </SwipeableDrawer>
      {!isDesktop &&
        !mapPanelOpen &&
        mapActiveAction === "addWorksiteGroup" && (
          <MiniBar onClick={handleMiniBarClick} />
        )}
    </>
  );
};

export default Panel;
