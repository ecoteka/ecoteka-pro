import { FC } from "react";
import { Fab, Button } from "@mui/material";
import TuneIcon from "@mui/icons-material/Tune";
import { MapActionsBarActionType } from "../../../lib/store/pages/mapEditor";
import RenderFilterBadge from "../badges/RenderFilterBadge";
import { useDeviceSize } from "@lib/utils";
import { useTranslation } from "react-i18next";

export interface FilterButtonProps {
  handleOnActionClick(action: MapActionsBarActionType): void;
}

const FilterButton: FC<FilterButtonProps> = ({ handleOnActionClick }) => {
  const { isTablet } = useDeviceSize();
  const { t } = useTranslation(["components"]);

  const handleFilterClick = async () => {
    handleOnActionClick("filter");
  };

  const button = isTablet ? (
    <Fab
      color="primary"
      size="medium"
      aria-label="filter"
      onClick={handleFilterClick}
      data-cy="map-actions-filter-button-mobile"
    >
      <TuneIcon />
    </Fab>
  ) : (
    <Button
      startIcon={<TuneIcon />}
      size="medium"
      variant="contained"
      color="primary"
      aria-label="filter"
      onClick={handleFilterClick}
      data-cy="map-actions-filter-button-desktop"
    >
      {t("components.Map.Filter.verb")}
    </Button>
  );

  return <RenderFilterBadge isTablet={isTablet}>{button}</RenderFilterBadge>;
};

export default FilterButton;
