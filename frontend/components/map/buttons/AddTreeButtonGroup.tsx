import { FC, useRef, useState } from "react";
import {
  Button,
  ButtonGroup,
  ClickAwayListener,
  Grow,
  MenuList,
  Paper,
  Popper,
} from "@mui/material";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import { LocationType } from "../../../lib/store/pages/mapEditor";
import AddLocationButton from "./AddLocationButton";
import ImportButton from "./ImportButton";
import AddWorksiteGroupButton from "./AddWorksiteGroupButton";

export interface AddTreeButtonGroupProps {
  handleOnAddLocation(type: LocationType): void;
}

const AddTreeButtonGroup: FC<AddTreeButtonGroupProps> = ({
  handleOnAddLocation,
}) => {
  const [open, setOpen] = useState(false);
  const anchorRef = useRef<HTMLDivElement>(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event: Event) => {
    if (
      anchorRef.current &&
      anchorRef.current.contains(event.target as HTMLElement)
    ) {
      return;
    }

    setOpen(false);
  };

  return (
    <>
      <ButtonGroup
        variant="contained"
        ref={anchorRef}
        aria-label="split button"
        aria-describedby="splitButton"
        style={{
          borderRadius: "50px",
          color: "#fff",
        }}
      >
        <AddLocationButton
          handleOnAddLocation={handleOnAddLocation}
          locationStatus={"alive"}
        />
        <Button
          size="small"
          aria-controls={open ? "split-button-menu" : undefined}
          aria-expanded={open ? "true" : undefined}
          aria-label="select action"
          aria-haspopup="menu"
          onClick={handleToggle}
          style={{
            backgroundColor: "#1D7554",
            borderRadius: 0,
            borderTopRightRadius: "50px",
            borderBottomRightRadius: "50px",
          }}
        >
          <ArrowDropDownIcon />
        </Button>
      </ButtonGroup>
      <Popper
        sx={{
          zIndex: 1,
        }}
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin:
                placement === "bottom" ? "center top" : "center bottom",
              color: "#000",
            }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList id="split-button-menu" autoFocusItem>
                  <AddLocationButton
                    handleOnAddLocation={handleOnAddLocation}
                    locationStatus={"stump"}
                  />
                  <AddLocationButton
                    handleOnAddLocation={handleOnAddLocation}
                    locationStatus={"empty"}
                  />
                  <ImportButton />
                  <AddWorksiteGroupButton />
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </>
  );
};
export default AddTreeButtonGroup;
