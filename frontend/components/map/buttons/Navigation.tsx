import { FC } from "react";
import { Stack, Button } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { useMapZoom } from "../../../lib/store/pages/mapEditor";

export enum Direction {
  In = 1,
  Out = -1,
}

export interface MapNavigationProps {
  onZoom(direction: Direction);
}
const MapNavigation: FC<MapNavigationProps> = ({ onZoom }) => {
  const zoom = useMapZoom();

  return (
    <Stack sx={styles.wrapper} data-cy="map-navigation-container">
      <Button
        aria-label="zoom-in"
        onClick={() => onZoom(Direction.In)}
        sx={styles.button}
        size="small"
        disabled={zoom >= 20 ? true : false}
        data-cy="map-navigation-zoom-in"
      >
        <AddIcon />
      </Button>

      <Button
        aria-label="zoom-out"
        onClick={() => onZoom(Direction.Out)}
        sx={styles.button}
        size="small"
        disabled={zoom <= 1 ? true : false}
        data-cy="map-navigation-zoom-out"
      >
        <RemoveIcon />
      </Button>
    </Stack>
  );
};

export default MapNavigation;

const styles = {
  wrapper: {
    height: "40px",
    position: "absolute",
    top: 0,
    right: 0,
    textAlign: "center",
    mr: 2,
    mt: 2,
  },
  button: {
    backgroundColor: "#fff",
    minWidth: "40px",
    maxWidth: "40px",
    borderRadius: 0,
    "&:hover": {
      backgroundColor: "rgba(255, 255, 255, 0.5)",
    },
  },
};
