import { FC } from "react";
import { Button, SvgIcon } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { useTranslation } from "react-i18next";

export interface ExitEditButtonProps {
  handleExitMode(): void;
}

const ExitEditButton: FC<ExitEditButtonProps> = ({ handleExitMode }) => {
  const { t } = useTranslation();

  return (
    <Button
      startIcon={
        <SvgIcon>
          <CloseIcon />
        </SvgIcon>
      }
      color="primary"
      size="small"
      variant="contained"
      aria-label="exit-edit"
      onClick={handleExitMode}
      data-cy="exit-edit-button"
      sx={{ zIndex: 4 }}
    >
      {t("common.exitEditMode") as string}
    </Button>
  );
};
export default ExitEditButton;
