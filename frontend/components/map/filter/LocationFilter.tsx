import React, { FC, useEffect, useState } from "react";
import {
  Typography,
  TextField,
  Box,
  Autocomplete,
  Chip,
  Stack,
} from "@mui/material";
import { useTranslation } from "react-i18next";
import IsExpanded from "../buttons/IsExpanded";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import { renderValue } from "@helpers/filter/FilterHelpers";
import { useRouter } from "next/router";

interface LocationItem {
  id: string;
  status: string;
  color: string;
}

const LocationFilter: FC = () => {
  const { t } = useTranslation("components");
  const router = useRouter();
  const [isExpanded, setIsExpanded] = useState<boolean>(true);
  const [isButtonHovered, setIsButtonHovered] = useState<boolean>(false);
  const [locationStatusData, setLocationStatusData] = useState<LocationItem[]>(
    []
  );
  const [addressData, setAddressData] = useState<Array<string>>([]);
  const [activeLocationStatus, setActiveLocationStatus] = useState<
    Record<string, boolean>
  >({});
  const [selectedAddress, setSelectedAddress] = useState<string[]>([]);
  const {
    filterData,
    setFiltersWithMartin,
    shouldResetFields,
    filtersWithMartin,
  } = useFiltersWithMartinContext()!;

  useEffect(() => {
    if (filterData && filterData.location) {
      setLocationStatusData(filterData.location.status as LocationItem[]);
      setAddressData(
        filterData.location.address.filter((item) => item !== null)
      );
    }
  }, [filterData]);

  useEffect(() => {
    const activeIds = Object.keys(activeLocationStatus).filter(
      (id) => activeLocationStatus[id]
    );
    if (activeIds.length > 0) {
      setFiltersWithMartin((prevState) => ({
        ...prevState,
        location_status: activeIds.length > 0 ? activeIds : undefined,
      }));
    } else {
      setFiltersWithMartin((prevState) => {
        const newState = { ...prevState };
        delete newState.location_status;
        return newState;
      });
    }
  }, [activeLocationStatus, setFiltersWithMartin]);

  useEffect(() => {
    if (selectedAddress.length > 0) {
      setFiltersWithMartin((prevState) => ({
        ...prevState,
        address: selectedAddress,
      }));
    } else {
      setFiltersWithMartin((prevState) => {
        const newState = { ...prevState };
        delete newState.address;
        return newState;
      });
    }
  }, [selectedAddress, setFiltersWithMartin]);

  const handleChipClick = (id: string) => {
    router.replace(router.pathname);
    setActiveLocationStatus((prevState) => ({
      ...prevState,
      [id]: !prevState[id],
    }));
  };

  useEffect(() => {
    if (shouldResetFields) {
      setSelectedAddress([]);
      setActiveLocationStatus({});
    }
  }, [shouldResetFields]);

  const handleToggleExpanded = (newState: boolean) => {
    setIsExpanded(newState);
  };

  const handleHoverChange = (isHovered: boolean) => {
    setIsButtonHovered(isHovered);
  };

  useEffect(() => {
    if (shouldResetFields) {
      setSelectedAddress([]);
      setActiveLocationStatus({});
    }
  }, [shouldResetFields]);

  const filtersList = Object.entries(filtersWithMartin).filter(
    ([key, value]) =>
      (key === "location_status" || key === "address") &&
      value &&
      value.length > 0
  );

  return (
    <>
      <Box
        sx={{
          p: 2,
          marginTop: 2,
          border: isButtonHovered
            ? "1px solid #2FA37C"
            : "1px solid rgba(67, 74, 74,.3)",
          borderRadius: "5px",
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          position: "relative",
        }}
      >
        {isExpanded ? (
          <>
            <Stack
              direction="row"
              sx={{
                width: "100%",
                marginBottom: 2,
                display: "flex",
                justifyContent: "center",
              }}
              spacing={1}
            >
              {locationStatusData.map((item, index) => (
                <Chip
                  key={index}
                  label={t(
                    `Worksite.properties.location_status_types.${item.status}`
                  )}
                  variant="outlined"
                  onClick={() => handleChipClick(item.id)}
                  sx={{
                    textTransform: "capitalize",
                    color:
                      activeLocationStatus[item.id] && index === 1
                        ? "white"
                        : "primary",
                    backgroundColor: activeLocationStatus[item.id]
                      ? `rgba(${JSON.parse(item.color).join(", ")})`
                      : undefined,
                  }}
                />
              ))}
            </Stack>
            <Autocomplete
              id="autocomplete-address"
              autoComplete
              disablePortal
              multiple
              options={addressData}
              value={selectedAddress}
              onChange={(event, newValue) => {
                router.replace(router.pathname);
                setSelectedAddress(newValue);
              }}
              getOptionLabel={(option) => option}
              sx={{ width: "100%" }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label={
                    t(`components.LocationForm.properties.address`) as string
                  }
                />
              )}
            />
          </>
        ) : filtersList.length > 0 ? (
          <>
            <Typography sx={{ pb: 1 }}>{t("Map.Filter.active")}</Typography>
            {filtersList.map(([key, value], index) => {
              const displayValue = renderValue(
                key,
                value,
                filterData,
                filtersWithMartin,
                t
              );
              return displayValue !== null ? (
                <Typography
                  key={index}
                  variant="inherit"
                  sx={{
                    mb: 0.5,
                    fontFamily: "Poppins",
                    alignSelf: "flex-start",
                  }}
                >
                  <em style={{ color: "#2FA37C", fontStyle: "normal" }}>
                    {`${t(`components.Map.Filter.${key}`)} : `}
                  </em>
                  {displayValue}
                </Typography>
              ) : null;
            })}
          </>
        ) : (
          <Typography>{t("Map.Filter.empty")}</Typography>
        )}

        <Typography
          sx={{
            position: "absolute",
            top: "-12px",
            left: "10px",
            backgroundColor: "white",
            padding: "0 4px",
            color: isButtonHovered ? "#2FA37C" : "rgba(67, 74, 74,.9)",
          }}
        >
          {t("ImportHistoryTable.headers.location")}
        </Typography>
      </Box>
      <IsExpanded
        onToggleExpanded={handleToggleExpanded}
        onHoverChange={handleHoverChange}
      />
    </>
  );
};

export default LocationFilter;
