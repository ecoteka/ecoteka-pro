import React, { FC, useState, useEffect } from "react";
import {
  Typography,
  Box,
  Stack,
  Chip,
  Divider,
  FormControlLabel,
  Switch,
} from "@mui/material";
import { useTranslation } from "react-i18next";
import IsExpanded from "../buttons/IsExpanded";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import { renderValue } from "@helpers/filter/FilterHelpers";
import { useRouter } from "next/router";

const DiagnosticFilter: FC = () => {
  const { t } = useTranslation("components");
  const router = useRouter();
  const [isExpanded, setIsExpanded] = useState<boolean>(true);
  const [isButtonHovered, setIsButtonHovered] = useState<boolean>(false);
  const [treeConditionData, setTreeConditionData] = useState<Array<string>>([]);
  const [recommendationData, setRecommendationData] = useState<Array<string>>(
    []
  );
  const [treeIsDangerousData, setTreeIsDangerousData] =
    useState<boolean>(false);
  const [activeRecommendation, setActiveRecommendation] = useState<
    Record<string, boolean>
  >({});
  const [activeTreeCondition, setActiveTreeCondition] = useState<
    Record<string, boolean>
  >({});
  const [activeTreeIsDangerous, setActiveTreeIsDangerous] = useState<
    Record<string, boolean>
  >({});
  const {
    filterData,
    setFiltersWithMartin,
    shouldResetFields,
    filtersWithMartin,
  } = useFiltersWithMartinContext()!;

  useEffect(() => {
    if (filterData && filterData.diagnosis) {
      setTreeConditionData(filterData?.diagnosis.tree_condition);
      setRecommendationData(filterData?.diagnosis.recommendation);
    }
  }, [treeConditionData, recommendationData, filterData]);

  const handleToggleExpanded = (newState: boolean) => {
    setIsExpanded(newState);
  };

  const handleHoverChange = (isHovered: boolean) => {
    setIsButtonHovered(isHovered);
  };

  const handleTreeIsDangerousChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    router.replace(router.pathname);
    setTreeIsDangerousData(event.target.checked);
  };

  const handleChipClick = (item: string, type: string) => {
    router.replace(router.pathname);
    if (type === "recommendation") {
      setActiveRecommendation((prevState) => ({
        ...prevState,
        [item]: !prevState[item],
      }));
    } else {
      setActiveTreeCondition((prevState) => ({
        ...prevState,
        [item]: !prevState[item],
      }));
    }
  };

  const updateFilters = (activeValues, filterKey, setFiltersWithMartin) => {
    const activeIds = Object.keys(activeValues).filter(
      (id) => activeValues[id]
    );

    if (activeIds.length > 0) {
      setFiltersWithMartin((prevState) => ({
        ...prevState,
        [filterKey]: activeIds,
      }));
    } else {
      setFiltersWithMartin((prevState) => {
        const newState = { ...prevState };
        delete newState[filterKey];
        return newState;
      });
    }
  };

  const activeDiagnosticFilter = (array) =>
    array.reduce((acc, curr) => {
      acc[curr] = true;
      return acc;
    }, {});

  useEffect(() => {
    const { object } = router.query;
    if (object) {
      const objectAsString = Array.isArray(object) ? object[0] : object;
      const decodedObject = JSON.parse(decodeURIComponent(objectAsString));

      const recommendationObj = activeDiagnosticFilter(
        decodedObject.recommendation || []
      );
      const treeIsDangerousObj = activeDiagnosticFilter(
        decodedObject.tree_is_dangerous || []
      );

      setActiveRecommendation(recommendationObj);
      setActiveTreeIsDangerous(treeIsDangerousObj);
      setTreeIsDangerousData(true);
    }
  }, [router.query]);

  useEffect(() => {
    updateFilters(
      activeTreeIsDangerous,
      "tree_is_dangerous",
      setFiltersWithMartin
    );
  }, [activeTreeIsDangerous, setFiltersWithMartin]);

  useEffect(() => {
    updateFilters(activeRecommendation, "recommendation", setFiltersWithMartin);
  }, [activeRecommendation, setFiltersWithMartin]);

  useEffect(() => {
    updateFilters(activeTreeCondition, "tree_condition", setFiltersWithMartin);
  }, [activeTreeCondition, setFiltersWithMartin]);

  useEffect(() => {
    if (shouldResetFields) {
      setActiveTreeCondition({});
      setActiveRecommendation({});
      setActiveTreeIsDangerous({});
    }
  }, [shouldResetFields]);

  useEffect(() => {
    setFiltersWithMartin((prevState) => ({
      ...prevState,
      tree_is_dangerous: treeIsDangerousData,
    }));
  }, [treeIsDangerousData, setFiltersWithMartin]);

  const filtersList = Object.entries(filtersWithMartin).filter(
    ([key, value]) =>
      (key === "tree_condition" && value && value.length > 0) ||
      (key === "recommendation" && value && value.length > 0) ||
      key === "tree_is_dangerous"
  );

  return (
    <>
      <Box
        sx={{
          p: 2,
          border: isButtonHovered
            ? "1px solid #2FA37C"
            : "1px solid rgba(67, 74, 74,.3)",
          borderRadius: "5px",
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          position: "relative",
        }}
      >
        {isExpanded ? (
          <>
            <Typography sx={{ alignSelf: "flex-start", fontSize: "10px" }}>
              {t(`Diagnosis.conditions.title`)}
            </Typography>
            <Stack
              direction="row"
              sx={{
                maxWSidth: "100%",
                marginTop: 1,
                display: "flex",
                justifyContent: "center",
                flexWrap: "wrap",
              }}
              spacing={1}
            >
              {treeConditionData.map((item: string, index: number) => (
                <Chip
                  key={index}
                  onClick={() => handleChipClick(item, "treeCondition")}
                  label={t(`Diagnosis.conditions.${item}`)}
                  variant="outlined"
                  sx={{
                    textTransform: "capitalize",
                    marginBottom: "10px !important",
                    backgroundColor: activeTreeCondition[item]
                      ? "#2FA37C"
                      : undefined,
                  }}
                />
              ))}
            </Stack>
            <Divider
              variant="middle"
              sx={{ height: "1px", width: "95%", my: 1 }}
            />

            <Typography sx={{ alignSelf: "flex-start", fontSize: "10px" }}>
              {t(`Diagnosis.recommendation.title`)}
            </Typography>
            <Stack
              direction="row"
              sx={{
                maxWSidth: "100%",
                marginTop: 1,
                display: "flex",
                justifyContent: "center",
                flexWrap: "wrap",
              }}
              spacing={1}
            >
              {recommendationData.map((item: string, index: number) => (
                <Chip
                  key={index}
                  onClick={() => handleChipClick(item, "recommendation")}
                  label={t(`Diagnosis.recommendation.${item}`)}
                  variant="outlined"
                  sx={{
                    textTransform: "capitalize",
                    marginBottom: "10px !important",
                    backgroundColor: activeRecommendation[item]
                      ? "#2FA37C"
                      : undefined,
                  }}
                />
              ))}
            </Stack>

            <FormControlLabel
              control={
                <Switch
                  checked={treeIsDangerousData}
                  onChange={handleTreeIsDangerousChange}
                />
              }
              label={t(`components.Diagnosis.labels.tree_is_dangerous`)}
            />
          </>
        ) : filtersList.length > 0 ? (
          <>
            <Typography sx={{ pb: 1 }}>{t("Map.Filter.active")}</Typography>
            {filtersList.map(([key, value], index) => {
              const displayValue = renderValue(
                key,
                value,
                filterData,
                filtersWithMartin,
                t
              );
              return displayValue !== null ? (
                <Typography
                  key={index}
                  variant="inherit"
                  sx={{
                    mb: 0.5,
                    fontFamily: "Poppins",
                    alignSelf: "flex-start",
                  }}
                >
                  <em style={{ color: "#2FA37C", fontStyle: "normal" }}>
                    {`${t(`Diagnosis.labels.${key}`)} : `}
                  </em>
                  {key === "tree_condition"
                    ? displayValue
                    : recommendationData
                        .map((i, index) => {
                          return t(`PanelTree.interventionsList.${i}`);
                        })
                        .join(", ")}
                  {key === "tree_is_dangerous" &&
                    t(`Diagnosis.labels.tree_is_dangerous`)}
                </Typography>
              ) : null;
            })}
          </>
        ) : (
          <Typography>{t("Map.Filter.empty")}</Typography>
        )}
        <Typography
          sx={{
            position: "absolute",
            top: "-12px",
            left: "10px",
            backgroundColor: "white",
            padding: "0 4px",
            color: isButtonHovered ? "#2FA37C" : "rgba(67, 74, 74,.9)",
          }}
        >
          {t("ImportHistoryTable.headers.diagnostic")}
        </Typography>
      </Box>
      <IsExpanded
        onToggleExpanded={handleToggleExpanded}
        onHoverChange={handleHoverChange}
      />
    </>
  );
};

export default DiagnosticFilter;
