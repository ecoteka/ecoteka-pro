import React, { useEffect, useState } from "react";
import { Typography, TextField, Box, Autocomplete, FormControlLabel, Switch } from "@mui/material";
import { useTranslation } from "react-i18next";
import DateRangeSelector from "@components/date/DatePicker";
import IsExpanded from "../buttons/IsExpanded";
import moment from "moment";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import { renderValue } from "@helpers/filter/FilterHelpers";
import Image from "next/image";
import getConfig from "next/config";
import { useRouter } from "next/router";

const TreeFilter = () => {
  const { t } = useTranslation("components");
  const router = useRouter();
  const [isExpanded, setIsExpanded] = useState<boolean>(true);
  const [hasUrbaSense, setHasUrbaSense] = useState<boolean>(false);
  const [isButtonHovered, setIsButtonHovered] = useState<boolean>(false);
  const [vernacularNameData, setVernacularNameData] = useState<Array<string>>(
    []
  );
  const [scientificNameData, setScientificNameData] = useState<Array<string>>(
    []
  );
  const [selectedVernacularNames, setSelectedVernacularNames] = useState<
    string[]
  >([]);
  const [selectedScientificNames, setSelectedScientificNames] = useState<
    string[]
  >([]);
  const {
    filterData,
    setFiltersWithMartin,
    shouldResetFields,
    filtersWithMartin,
  } = useFiltersWithMartinContext()!;

  const { publicRuntimeConfig } = getConfig();

  useEffect(() => {
    if (filterData && filterData?.tree) {
      setVernacularNameData(filterData?.tree.vernacular_names || []);
      setScientificNameData(filterData?.tree.scientific_names || []);
    }
  }, [scientificNameData, vernacularNameData, filterData]);

  const handleToggleExpanded = (newState: boolean) => {
    setIsExpanded(newState);
  };

  const handleHoverChange = (isHovered: boolean) => {
    setIsButtonHovered(isHovered);
  };

  const handleUrbaSenseChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    router.replace(router.pathname);
    setHasUrbaSense(event.target.checked);
  };

  const handlePlantationDateChange = (startDate, endDate) => {
    router.replace(router.pathname);
    const formattedStartDate = startDate
      ? moment(startDate).format("YYYY-MM-DD")
      : null;
    const formattedEndDate = endDate
      ? moment(endDate).format("YYYY-MM-DD")
      : null;

    setFiltersWithMartin((prevState) => {
      const newState = { ...prevState };
      if (formattedStartDate) {
        newState.plantation_date_start = formattedStartDate;
      } else {
        delete newState.plantation_date_start;
      }
      if (formattedEndDate) {
        newState.plantation_date_end = formattedEndDate;
      } else {
        delete newState.plantation_date_end;
      }
      return newState;
    });
  };

  useEffect(() => {
    setFiltersWithMartin((prevState) => {
      let newState = { ...prevState };
      selectedVernacularNames.length > 0
        ? (newState.vernacular_names = selectedVernacularNames)
        : delete newState.vernacular_names;
      selectedScientificNames.length > 0
        ? (newState.scientific_names = selectedScientificNames)
        : delete newState.scientific_names;
      return newState;
    });
  }, [selectedVernacularNames, selectedScientificNames, setFiltersWithMartin]);

  useEffect(() => {
    if (shouldResetFields) {
      setSelectedVernacularNames([]);
      setSelectedScientificNames([]);
      setHasUrbaSense(false);
      handlePlantationDateChange(null, null);
    }
  }, [shouldResetFields]);

  useEffect(() => {
    setFiltersWithMartin((prevState) => ({
      ...prevState,
      has_urbasense: hasUrbaSense,
    }));
  }, [hasUrbaSense, setFiltersWithMartin]);

  const filtersList = Object.entries(filtersWithMartin).filter(
    ([key, value]) =>
      key === "scientific_names" ||
      key === "vernacular_names" ||
      key === "plantation_date_start" ||
      (key === "has_urbasense" && value === true)
  );

  return (
    <>
      <Box
        sx={{
          p: 2,
          border: isButtonHovered
            ? "1px solid #2FA37C"
            : "1px solid rgba(67, 74, 74,.3)",
          borderRadius: "5px",
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          position: "relative",
        }}
      >
        {isExpanded ? (
          <>
            {["True", "true"].includes(
              publicRuntimeConfig.URBASENSE_DASHBOARD_ACTIVE
            ) && (
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  p: 1,
                }}
              >
                <FormControlLabel
                  control={
                    <Switch
                      checked={hasUrbaSense}
                      onChange={handleUrbaSenseChange}
                    />
                  }
                  label={t(`components.dashboard.urbasense.label`)}
                />
                <Image
                  src="/us_logo.png"
                  width={26}
                  height={26}
                  alt="Logo Urbasense"
                />
              </Box>
            )}

            <Autocomplete
              id={`autocomplete-scientific_name`}
              autoComplete
              disablePortal
              multiple
              options={scientificNameData}
              value={selectedScientificNames}
              onChange={(event, newValue) => {
                router.replace(router.pathname);
                setSelectedScientificNames(newValue);
              }}
              getOptionLabel={(option) => option}
              sx={{
                marginBottom: 2,
                width: "100%",
                display: isExpanded ? "block" : "none",
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label={
                    t(`components.Tree.properties.scientificName`) as string
                  }
                />
              )}
            />
            <Autocomplete
              id={`autocomplete-vernacular_name`}
              autoComplete
              disablePortal
              multiple
              options={vernacularNameData}
              value={selectedVernacularNames}
              onChange={(event, newValue) => {
                router.replace(router.pathname);
                setSelectedVernacularNames(newValue);
              }}
              getOptionLabel={(option) => option}
              sx={{ width: "100%", display: isExpanded ? "block" : "none" }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label={
                    t(`components.Tree.properties.vernacularName`) as string
                  }
                />
              )}
            />
            <DateRangeSelector
              onDateChange={handlePlantationDateChange}
              pickerType={
                t(`components.Tree.properties.plantationDate.exact`) as string
              }
              isExpanded={isExpanded}
            />
          </>
        ) : (
          ""
        )}
        {filtersList.length > 0 ? (
          <>
            <Typography sx={{ pb: 1, display: isExpanded ? "none" : "block" }}>
              {t("Map.Filter.active")}
            </Typography>
            {filtersList.map(([key, value], index) => {
              const displayValue = renderValue(
                key,
                value,
                filterData,
                filtersWithMartin,
                t
              );
              return (
                <Typography
                  key={index}
                  variant="inherit"
                  sx={{
                    mb: 0.5,
                    fontFamily: "Poppins",
                    alignSelf: "flex-start",
                    display: isExpanded ? "none" : "block",
                    color:
                      typeof displayValue === "boolean" ? "#2FA37C" : undefined,
                    fontStyle:
                      typeof displayValue === "boolean" ? "normal" : undefined,
                  }}
                >
                  <em style={{ color: "#2FA37C", fontStyle: "normal" }}>{`${t(
                    `components.Map.Filter.${key}`
                  )}${typeof displayValue !== "boolean" ? " : " : ""}`}</em>
                  {typeof displayValue !== "boolean" ? displayValue : ""}
                </Typography>
              );
            })}
          </>
        ) : (
          <Typography sx={{ display: isExpanded ? "none" : "block" }}>
            {t("Map.Filter.empty")}
          </Typography>
        )}
        <Typography
          sx={{
            position: "absolute",
            top: "-12px",
            left: "10px",
            backgroundColor: "white",
            padding: "0 4px",
            color: isButtonHovered ? "#2FA37C" : "rgba(67, 74, 74,.9)",
          }}
        >
          {t("ImportHistoryTable.headers.trees")}
        </Typography>
      </Box>

      <IsExpanded
        onToggleExpanded={handleToggleExpanded}
        onHoverChange={handleHoverChange}
      />
    </>
  );
};

export default TreeFilter;
