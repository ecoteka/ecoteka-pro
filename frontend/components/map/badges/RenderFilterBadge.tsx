import React from "react";
import { Tooltip, Badge } from "@mui/material";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import RenderTooltipContent from "@components/map/tooltips/RenderFilterBadgeTooltip";

const RenderFilterBadge = ({ isTablet, children }) => {
  const { filtersWithMartin } = useFiltersWithMartinContext()!;
  const filtersList = Object.entries(filtersWithMartin).filter(
    ([key, value]) =>
      (!key.endsWith("_end") && value.length > 0) ||
      (value === true && key === "tree_is_dangerous")
  );

  return (
    <Badge
      color="secondary"
      badgeContent={
        filtersList.length !== 0 && (
          <Tooltip title={<RenderTooltipContent />}>
            <span>{filtersList.length}</span>
          </Tooltip>
        )
      }
      sx={{
        ".MuiBadge-badge": {
          right: isTablet ? undefined : 6,
          bottom: isTablet ? 5 : 3,
          left: isTablet ? 5 : undefined,
          zIndex: "1050",
          width: "fit-content",
        },
      }}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: isTablet ? "left" : "right",
      }}
      invisible={filtersList.length === 0}
    >
      {children}
    </Badge>
  );
};

export default RenderFilterBadge;
