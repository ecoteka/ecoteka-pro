import { useRouter } from "next/router";
import { FC } from "react";
import {
  Typography,
  Paper,
  Stack,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  CardMedia,
  Button,
} from "@mui/material";
import { useTranslation } from "react-i18next";
import ZoomOutMapIcon from "@mui/icons-material/ZoomOutMap";
import SupervisedUserCircleIcon from "@mui/icons-material/SupervisedUserCircle";
import useStore from "../../lib/store/useStore";

const styles = {
  wrapper: {
    padding: 5,
    height: "100%",
  },
  image: {
    maxWidth: "700px",
    maxHeight: "300px",
    height: "100%",
    borderRadius: "10px",
  },
};

const Header: FC = () => {
  const router = useRouter();
  const { t } = useTranslation(["common", "components"]);
  const { app } = useStore((store) => store);
  return (
    <Paper sx={styles.wrapper} elevation={1}>
      <Stack>
        <Typography variant="h3">{app.organization?.name}</Typography>
        <Stack
          direction={{ md: "row", xs: "column" }}
          justifyContent="space-between"
          spacing={2}
        >
          <CardMedia
            sx={styles.image}
            component="img"
            image={app.organization?.image}
            title="ecoteka"
          />
          <Stack spacing={2} justifyContent="space-between">
            <List>
              <ListItem disableGutters>
                <ListItemIcon>
                  <SupervisedUserCircleIcon />
                </ListItemIcon>
                <ListItemText>
                  <>
                    {app.organization?.population}{" "}
                    {t("components.organization.Header.populationSize")}
                  </>
                </ListItemText>
              </ListItem>
              <ListItem disableGutters>
                <ListItemIcon>
                  <ZoomOutMapIcon />
                </ListItemIcon>
                <ListItemText>{app.organization?.surface} km2</ListItemText>
              </ListItem>
            </List>
            <Stack spacing={2}>
              <Button
                size="large"
                variant="contained"
                fullWidth
                color="primary"
                onClick={() => router.push("/map")}
              >
                {t("components.Organization.Header.mapEditor") as string}
              </Button>
              <Button
                size="large"
                variant="contained"
                fullWidth
                color="primary"
                onClick={() => (document.location.href = "/auth/admin")}
              >
                {t("common.adminConsole") as string}
              </Button>
            </Stack>
          </Stack>
        </Stack>
      </Stack>
    </Paper>
  );
};

export default Header;
