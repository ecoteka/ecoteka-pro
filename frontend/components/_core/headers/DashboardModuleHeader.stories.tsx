import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import DashboardModuleHeader from "./DashboardModuleHeader";

const meta = {
  title: "Core/Headers/DashboardModuleHeader",
  component: DashboardModuleHeader,
  parameters: {
    docs: {
      description: {
        component: "DashboardModuleHeader",
      },
    },
  },
} satisfies Meta<typeof DashboardModuleHeader>;

export default meta;
type Story = StoryObj<typeof DashboardModuleHeader>;

export const Default: Story = {
  render: () => <DashboardModuleHeader />,
};
