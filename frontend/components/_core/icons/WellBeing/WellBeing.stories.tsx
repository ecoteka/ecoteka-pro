import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import { WellBeing } from "@core/icons";

const meta = {
  title: "Core/Icons/WellBeing",
  component: WellBeing,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Layer manager",
      },
    },
  },
} satisfies Meta<typeof WellBeing>;

export default meta;
type Story = StoryObj<typeof WellBeing>;

export const WellBeingDefaults: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <WellBeing />
    </Box>
  ),
};

export const WellBeingWithControls: Story = {
  args: {
    level: 80,
  },
  render: (args) => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <WellBeing {...args} />
    </Box>
  ),
};
