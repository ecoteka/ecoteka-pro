import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import { Icu } from "@core/icons";

const meta = {
  title: "Core/Icons/Icu",
  component: Icu,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Layer manager",
      },
    },
  },
} satisfies Meta<typeof Icu>;

export default meta;
type Story = StoryObj<typeof Icu>;

export const IcuDefaults: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <Icu />
    </Box>
  ),
};

export const IcuWithControls: Story = {
  args: {
    level: 80,
  },
  render: (args) => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <Icu {...args} />
    </Box>
  ),
};
