import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import { AirQuality } from "@core/icons";

const meta = {
  title: "Core/Icons/AirQuality",
  component: AirQuality,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Layer manager",
      },
    },
  },
} satisfies Meta<typeof AirQuality>;

export default meta;
type Story = StoryObj<typeof AirQuality>;

export const AirQualityDefaults: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <AirQuality />
    </Box>
  ),
};

export const AirQualityWithControls: Story = {
  args: {
    level: 80,
  },
  render: (args) => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <AirQuality {...args} />
    </Box>
  ),
};
