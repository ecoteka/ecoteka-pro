import { AirQualityProps } from "./AirQuality.types";

const AirQuality = ({ level = 20 }: AirQualityProps) => {
  const rectHeight = (level / 100) * 79;

  return (
    <svg
      width="76px"
      height="79px"
      viewBox="0 0 76 79"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M73.33 42.25C68.83 19.95 54.43 13.35 51.53 12.15C51.13 11.95 50.73 11.85 50.33 11.85C45.43 11.65 43.43 21.05 42.73 31.25L40.03 27.55V2.84999C40.03 1.64999 39.03 0.649994 37.83 0.649994C36.63 0.649994 35.63 1.64999 35.63 2.84999V27.55L32.83 31.35C32.13 21.15 30.13 11.55 25.13 11.85C24.73 11.85 24.33 11.95 23.93 12.15C21.03 13.25 6.62999 19.95 2.12999 42.25C2.12999 42.25 -1.27001 55.95 0.529993 74.45C0.729993 76.65 2.62999 78.35 4.82999 78.25C6.22999 78.15 8.12999 77.65 10.63 76.25C10.63 76.25 20.63 72.35 27.33 65.05C30.93 61.25 32.83 56.15 33.03 50.95C33.13 48.15 33.23 44.35 33.13 40.15L37.73 33.45L42.23 40.05C42.13 44.35 42.23 48.25 42.33 51.05C42.53 56.25 44.43 61.35 48.03 65.15C54.73 72.45 64.73 76.35 64.73 76.35C67.23 77.75 69.13 78.25 70.53 78.35C72.73 78.45 74.63 76.75 74.83 74.55C76.73 56.05 73.33 42.25 73.33 42.25Z"
        fill="#FCD200"
        id="path-air"
      />
      <g
        id="air_module"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
      >
        <g id="air">
          <mask id="mask-air" fill="white">
            <use xlinkHref="#path-air"></use>
          </mask>
          <use id="mask_air" fill="#FCD200" xlinkHref="#path-air"></use>
          <rect
            id="fill"
            fill="#FFF7BE"
            mask="url(#mask-air)"
            x="1"
            y="0"
            width="76"
            height={72 - rectHeight}
          ></rect>
        </g>
      </g>
    </svg>
  );
};

export default AirQuality;
