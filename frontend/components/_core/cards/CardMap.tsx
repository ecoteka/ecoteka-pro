import { Ref, forwardRef } from "react";
import Map from "react-map-gl";
import type { MapRef, MapProps } from "react-map-gl";
import maplibregl from "maplibre-gl";

const CardMapLibre = forwardRef(
  (
    {
      mapStyle = "/mapStyles/map_light.json",
      mapLib,
      style,
      ...props
    }: MapProps,
    ref: Ref<MapRef>
  ) => (
    // @ts-ignore: MapProps type error on fog props not used here...
    <Map
      ref={ref}
      mapLib={maplibregl as any}
      mapStyle={mapStyle}
      {...props}
      style={{ width: "100%", height: 320 }}
    >
      {props.children}
    </Map>
  )
);

export default CardMapLibre;
