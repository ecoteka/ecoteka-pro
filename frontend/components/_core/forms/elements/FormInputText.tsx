import { Controller } from "react-hook-form";
import type { Control } from "react-hook-form";
import TextField, { TextFieldProps } from "@mui/material/TextField";

export interface IControlledFormProps {
  name: string;
  /** control method from __react-hook-form__ `useForm()` */
  control: Control;
  label: string;
}

type IFormInputTextProps = TextFieldProps & IControlledFormProps;

export const FormInputText = ({
  name,
  control,
  label,
  ...muiTextFieldProps
}: IFormInputTextProps) => {
  return (
    <Controller
      name={name}
      control={control}
      render={({
        field: { onChange, value },
        fieldState: { error },
        formState,
      }) => (
        <TextField
          helperText={error ? error.message : null}
          error={!!error}
          onChange={onChange}
          value={value}
          label={label}
          {...muiTextFieldProps}
        />
      )}
    />
  );
};

export default FormInputText;
