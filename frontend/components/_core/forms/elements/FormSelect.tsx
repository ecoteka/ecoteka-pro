import {
  FormControl,
  FormControlProps,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import { Controller } from "react-hook-form";
import { Options, IFormInput } from "./types";

const defaultOptions = [
  {
    label: "--",
    value: undefined,
  },
  {
    label: "Dropdown Option 1",
    value: "1",
  },
  {
    label: "Dropdown Option 2",
    value: "2",
  },
];

export interface IFormSelect extends IFormInput {
  options: Options;
  formControls;
}

type IFormSelectProps = FormControlProps & IFormSelect;

export const FormInputDropdown: React.FC<IFormSelectProps> = ({
  name,
  control,
  label,
  options = defaultOptions,
  ...formControlProps
}) => {
  const generateSingleOptions = () => {
    return options.map((option: any) => {
      return (
        <MenuItem key={option.value} value={option.value}>
          {option.label}
        </MenuItem>
      );
    });
  };
  return (
    <FormControl {...formControlProps}>
      <InputLabel>{label}</InputLabel>
      <Controller
        render={({ field: { onChange, value } }) => (
          <Select onChange={onChange} value={value}>
            {generateSingleOptions()}
          </Select>
        )}
        control={control}
        name={name}
      />
    </FormControl>
  );
};
