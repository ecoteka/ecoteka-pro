import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";

import Fill from "./Fill";

const meta = {
  title: "Core/Display/Fill",
  component: Fill,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component:
          "This utility component fills a given block with a custom CSS color",
      },
    },
  },
} satisfies Meta<typeof Fill>;

export default meta;
type Story = StoryObj<typeof Fill>;

export const Playground: Story = {
  args: {
    color: "red",
  },
  render: (args) => (
    <Box sx={{ with: "300px", height: "125px" }}>
      <Fill color={args.color} />
    </Box>
  ),
};

export const Colors: Story = {
  render: () => (
    <Stack spacing={2} maxWidth={300}>
      <Box sx={{ with: "300px", height: "125px" }}>
        <Fill color="teal" />
      </Box>
      <Box sx={{ with: "300px", height: "125px" }}>
        <Fill color="orange" />
      </Box>
      <Box sx={{ with: "300px", height: "125px" }}>
        <Fill color="#E1CC00" />
      </Box>
    </Stack>
  ),
};
