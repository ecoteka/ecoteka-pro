import React, { FC, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Control, Controller, UseFormSetValue } from "react-hook-form";
import { MVTLayer } from "@deck.gl/geo-layers";
import { MVTLayerProps } from "@deck.gl/geo-layers/typed";
import { debounce } from "lodash";
import {
  Grid,
  TextField,
  Autocomplete,
  AutocompleteRenderInputParams,
  Box,
  Switch,
  InputAdornment,
  Typography,
} from "@mui/material";

import { FreeSolo, IFreeSoloOption } from "../autocomplete/FreeSolo";
import { FormInputError } from "../error/FormInputError";
import {
  FetchInterventionPartnersQuery,
  FetchInterventionTypesQuery,
  FetchOneInterventionQuery,
  Intervention_Type,
} from "../../../generated/graphql";
import {
  useMapActiveAction,
  useMapStyle,
} from "../../../lib/store/pages/mapEditor";
import { InterventionCardMap } from "@components/_core/cards";
import useStore from "@stores/useStore";
import getConfig from "next/config";
import { GlobalLoader } from "@components/layout/GlobalLoader";
import { InitialViewState } from "@components/map/Base";
import { Feature } from "maplibre-gl";
import { useRouter } from "next/router";
import Link from "next/link";

export interface Intervention {
  id?: string;
  realization_date?: string;
  scheduled_date?: string;
  location: {
    id: string;
    address: string;
  };
  intervention_type?: any;
  intervention_partner?: any;
  note: string;
  validation_note?: string;
  cost?: Number;
}

export interface InterventionFieldsProps {
  interventionTypes: FetchInterventionTypesQuery["intervention_type"];
  interventionPartners: FetchInterventionPartnersQuery["intervention_partner"];
  activeIntervention?: FetchOneInterventionQuery["intervention"][0];
  customControl: Control<any>;
  customSetValue: UseFormSetValue<any>;
  errors: any;
  onUpdateIntervention?(data: Intervention): void;
  onCreateIntervention?(data: Intervention): void;
  onDeleteIntervention?(id: string): void;
  onValidateIntervention?(
    id: string,
    interventionDate: string,
    interventionPartner: string,
    interventionNote: string
  ): void;
  hasTree?: boolean;
  goToNextStep?: () => void;
  location?: any;
  reset?: any;
}

export interface SelectedLocationProps {
  status_name: string;
  location_id: string;
  location_address: string;
  tree_serial_number: string | undefined | null;
  tree_id: string;
}

const InterventionFields: FC<InterventionFieldsProps> = ({
  interventionTypes,
  interventionPartners,
  activeIntervention,
  customControl,
  customSetValue,
  errors,
  hasTree,
  location,
  reset,
}) => {
  const { t } = useTranslation(["components", "common"]);
  const { app } = useStore((store) => store);
  const router = useRouter();
  const activeInterventionId = router.query.activeId;
  const { publicRuntimeConfig } = getConfig();
  const [selectedLocation, setSelectedLocation] =
    useState<SelectedLocationProps>();

  const [viewState, setViewState] = useState<InitialViewState>({
    longitude: app?.organization?.coords.coordinates[0],
    latitude: app?.organization?.coords.coordinates[1],
    zoom: 13,
    bearing: 0,
    pitch: 0,
  });
  const mapStyle = useMapStyle();
  const [layers, setLayers] = useState<any[]>();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [interventionTypesFiltered, setInterventionTypesFiltered] =
    useState(interventionTypes);
  const [interventionIsDone, setInterventionIsDone] = useState<boolean>(false);
  const [readOnly, setReadOnly] = useState(false);
  // get default organization
  const mapActiveAction = useMapActiveAction();
  const now = new Date();
  const fields = [
    {
      type: "textReadOnly",
      label: "location",
      column: hasTree ? "tree_id" : "location_id",
    },
    {
      type: "autocomplete",
      label: "interventionType",
      column: "intervention_type",
    },
    {
      type: "autocomplete",
      label: "interventionPartner",
      column: "intervention_partner",
    },
    { type: "date", label: "realizationDate", column: "realization_date" },
    { type: "date", label: "scheduledDate", column: "scheduled_date" },
    { type: "number", label: "cost", column: "cost" },
    { type: "textArea", label: "note", column: "note" },
  ];

  const status = hasTree
    ? "alive"
    : activeIntervention?.location?.location_status?.status;
  const statusReadOnlyType = ["stump", "empty"];

  const handleOnViewStateChange = (viewState: any) => {
    const nextViewState: InitialViewState = viewState.viewState;
    setViewState({
      ...nextViewState,
      longitude: nextViewState.longitude,
      latitude: nextViewState.latitude,
      zoom: nextViewState.zoom,
    });
  };

  const handleOnMapClick = async (info) => {
    setSelectedLocation(info?.object?.properties);
  };

  const debouncedHandleOnMapClick = debounce(handleOnMapClick, 100);

  const isFeatureSelected = (currentFeature: Feature): boolean => {
    const locationId = currentFeature.properties.location_id;
    return selectedLocation?.location_id === locationId;
  };

  const getFillColor = (d) => {
    if (isFeatureSelected(d)) {
      return [255, 243, 80, 255];
    }
    if (d?.properties?.status_color) {
      return JSON.parse(d?.properties?.status_color);
    }
    return [0, 0, 0, 255];
  };

  const createLocationsLayer = () => {
    const ecotekaLocations: MVTLayerProps = new MVTLayer({
      // TODO : add to NEXT_PUBLIC_TILESERVICE_URL
      data: location
        ? `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/martin/filter_locations/{z}/{x}/{y}?location_id=${location.id}`
        : `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/martin/filter_locations/{z}/{x}/{y}`,
      id: "locations",
      // @ts-ignore
      getRadius: 3,
      opacity: 0.9,
      stroked: true,
      pickable: true,
      filled: true,
      extruded: true,
      pointType: "circle",
      pointRadiusMinPixels: 8,
      pointRadiusScale: 1,
      minRadius: 5,
      radiusMinPixels: 0.5,
      lineWidthMinPixels: 1,
      lineWidthMaxPixels: 3,
      autoHighlight: true,
      visible: true,
      lineWidthScale: 5,
      getFillColor: (d) => getFillColor(d),
      updateTriggers: {
        getFillColor: [selectedLocation],
      },
      onClick: (d) => handleOnMapClick(d),
      onViewportLoad(): void {
        setIsLoading(false);
      },
      onTileLoad: () => {
        setIsLoading(true);
      },
    });
    setLayers([ecotekaLocations]);
    return ecotekaLocations;
  };
  useEffect(() => {
    createLocationsLayer();
  }, [selectedLocation]);

  useEffect(() => {
    if (selectedLocation && !activeIntervention) {
      const status = selectedLocation?.status_name;
      customSetValue("location.locationId", selectedLocation?.location_id);
      customSetValue("location.treeId", selectedLocation?.tree_id || null);
      customSetValue("location.address", selectedLocation?.location_address);
      customSetValue("intervention_type", "");
      if (status === "stump") {
        setInterventionTypesFiltered(
          interventionTypes?.filter((type) => type.slug === "grubbing")
        );
      } else if (status === "empty") {
        setInterventionTypesFiltered(
          interventionTypes?.filter((type) => type.slug === "planting")
        );
      } else {
        setInterventionTypesFiltered(
          interventionTypes?.filter(
            (type) => !["planting", "grubbing"].includes(type.slug)
          )
        );
      }
    }
  }, [selectedLocation]);

  useEffect(() => {
    const handleLocationAndIntervention = () => {
      let transformedLocation;
      let initialValues = {
        intervention_partner: "",
        intervention_type: "",
        cost: "",
        note: "",
        location: "",
        scheduled_date: "",
      };

      if (activeIntervention) {
        const { location, tree } = activeIntervention;
        transformedLocation = {
          status_name: location?.location_status?.status || "",
          location_id: tree ? tree.location_id : "",
          location_address: location?.address || "",
          tree_serial_number: tree ? tree.serial_number : "",
          tree_id: tree ? tree?.id : "",
        };
        let coordinates = location?.coords;

        if (tree && tree.location?.coords) {
          coordinates = tree.location.coords;
        }
        setViewState((prevViewState) => ({
          ...prevViewState,
          longitude: coordinates?.coordinates[0] || 0,
          latitude: coordinates?.coordinates[1] || 0,
        }));

        reset(activeIntervention);
        if (Boolean(activeIntervention?.realization_date)) {
          setInterventionIsDone(true);
          setReadOnly(true);
        }
        if (
          activeIntervention?.intervention_worksite_interventions.length > 0
        ) {
          setReadOnly(true);
        }
      } else {
        if (location) {
          transformedLocation = {
            status_name: location?.location_status?.status || "",
            location_id: location?.id,
            location_address: location?.address,
            tree_serial_number: location?.tree?.[0]?.serial_number || "",
            tree_id: location?.tree?.[0]?.id || location?.tree?.id || "",
          };
          setViewState((prevViewState) => ({
            ...prevViewState,
            longitude: location?.coords?.coordinates[0] || 0,
            latitude: location?.coords?.coordinates[1] || 0,
          }));
        }

        const interventionSelected = interventionTypes.find(
          (type) => type.slug === router.query.type
        );
        if (router.query.type && interventionSelected) {
          initialValues.intervention_type = interventionSelected.slug;
          customSetValue(
            "intervention_type",
            interventionSelected as Intervention_Type
          );
        }

        reset(initialValues);
      }

      setSelectedLocation(transformedLocation);
    };

    handleLocationAndIntervention();
  }, [location, activeIntervention, interventionTypes, reset, customSetValue]);

  return (
    <Grid container spacing={0}>
      {isLoading && <GlobalLoader />}
      <InterventionCardMap
        handleOnViewStateChange={handleOnViewStateChange}
        initialViewState={viewState}
        mapStyle={mapStyle}
        layers={layers ? layers : []}
        onClick={debouncedHandleOnMapClick}
      />
      {mapActiveAction !== "addWorksiteGroup" && !activeInterventionId && (
        <Box>
          {t(`components.InterventionForm.message.alreadyDone`) as string}
          <Switch
            checked={interventionIsDone}
            color="primary"
            disabled={Boolean(activeIntervention?.realization_date)}
            onChange={(e) => {
              setInterventionIsDone(!interventionIsDone);
              customSetValue("scheduled_date", now.toISOString().split("T")[0]);
              customSetValue(
                "realization_date",
                now.toISOString().split("T")[0]
              );
            }}
          />
        </Box>
      )}
      {activeIntervention?.intervention_worksite_interventions &&
        activeIntervention?.intervention_worksite_interventions.length > 0 && (
          <Typography>
            Cette intervention appartient à un{" "}
            <Link
              href={`/worksites/${activeIntervention?.intervention_worksite_interventions[0].worksite.id}`}
              style={{ textDecoration: "underline" }}
            >
              chantier
            </Link>
            , vous pouvez seulement la déclarer réalisée ou non
          </Typography>
        )}
      <Grid container spacing={1}>
        {fields.map((fieldName, i) => (
          <Grid item xs={12} key={i}>
            {fieldName?.label === "interventionType" && (
              <>
                <Controller
                  name={`${fieldName.column as keyof Intervention}`}
                  rules={{ required: true }}
                  control={customControl}
                  render={({ field: { onChange, value } }) => {
                    value = value ?? null;
                    return (
                      <Autocomplete
                        id={`autocomplete-${name}`}
                        options={interventionTypesFiltered}
                        value={value}
                        renderInput={(
                          params: AutocompleteRenderInputParams
                        ) => (
                          <TextField
                            {...params}
                            required
                            label={
                              t(
                                `components.Intervention.properties.${fieldName?.label}`
                              ) as string
                            }
                            variant="filled"
                            InputLabelProps={{ shrink: true }}
                          />
                        )}
                        getOptionLabel={(option) => {
                          if (option.slug !== undefined) {
                            return t(
                              `components.PanelTree.interventionsList.${option.slug}`
                            );
                          } else {
                            return "";
                          }
                        }}
                        onChange={(e, newValue) => {
                          onChange(newValue);
                        }}
                        isOptionEqualToValue={(option, value) =>
                          option.slug === value.slug
                        }
                        disabled={
                          readOnly ||
                          statusReadOnlyType.includes(status as string)
                            ? true
                            : false
                        }
                      />
                    );
                  }}
                />
                <FormInputError name={fieldName.column!} errors={errors} />
              </>
            )}

            {fieldName?.label === "interventionPartner" && (
              <FreeSolo
                control={customControl}
                setValue={customSetValue}
                name={`${fieldName.column as keyof Intervention}`}
                listOptions={interventionPartners ?? ([] as IFreeSoloOption[])}
                required={false}
                label={
                  t(
                    `components.Intervention.properties.${fieldName?.label}`
                  ) as string
                }
                readOnly={readOnly}
                object={"intervention"}
              />
            )}
            {fieldName.column === "scheduled_date" && (
              <>
                <Controller
                  name={`${fieldName?.column as keyof Intervention}`}
                  control={customControl}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <TextField
                        required
                        InputProps={{
                          inputProps: {
                            min: now.toISOString().split("T")[0],
                          },
                        }}
                        label={
                          t(
                            `components.Intervention.properties.${fieldName?.label}`
                          ) as string
                        }
                        variant="filled"
                        type="date"
                        disabled={readOnly}
                        sx={{
                          width: "100%",
                          display: interventionIsDone ? "none" : "auto",
                        }}
                        InputLabelProps={{
                          shrink: true,
                        }}
                        value={value}
                        onChange={(e) => onChange(e.target.value)}
                      />
                    );
                  }}
                />
                <FormInputError name={fieldName.column} errors={errors} />
              </>
            )}

            {fieldName?.label === "location" && (
              <Controller
                name={"location.address"}
                control={customControl}
                rules={{ required: true }}
                render={({ field: { onChange, value } }) => (
                  <TextField
                    fullWidth
                    disabled={true}
                    required
                    value={
                      selectedLocation?.location_address !== ""
                        ? selectedLocation?.location_address
                        : selectedLocation?.tree_serial_number
                    }
                    sx={{
                      display: "flex",
                    }}
                    label={
                      t(
                        `components.Intervention.properties.${fieldName?.label}`
                      ) as string
                    }
                    variant="filled"
                    InputLabelProps={{ shrink: true }}
                    error={!!errors[fieldName?.label]}
                    helperText={errors[fieldName?.label]?.message}
                    type={fieldName?.type}
                  />
                )}
              />
            )}

            {(fieldName?.type === "text" ||
              fieldName?.type === "textArea" ||
              fieldName?.type === "number") && (
              <Controller
                name={`${fieldName?.column as keyof Intervention}`}
                control={customControl}
                render={({ field: { onChange, ref, value } }) => (
                  <TextField
                    value={value}
                    inputRef={ref}
                    fullWidth
                    disabled={readOnly}
                    sx={{
                      display: "flex",
                    }}
                    multiline={fieldName?.type === "textArea" ? true : false}
                    label={
                      t(
                        `components.Intervention.properties.${fieldName?.label}`
                      ) as string
                    }
                    variant="filled"
                    placeholder={t(
                      `components.Intervention.placeholder.${fieldName?.label}`
                    )}
                    error={!!errors[fieldName?.label]}
                    helperText={errors[fieldName?.label]?.message}
                    onChange={onChange}
                    type={fieldName?.type}
                    inputProps={fieldName?.type === "number" ? { min: 0 } : {}}
                    InputProps={{
                      endAdornment: fieldName?.label === "cost" && (
                        <InputAdornment position="end">€</InputAdornment>
                      ),
                    }}
                  />
                )}
              />
            )}

            {fieldName.column === "realization_date" && interventionIsDone && (
              <>
                <Controller
                  name={`${fieldName?.column as keyof Intervention}`}
                  control={customControl}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <TextField
                        InputProps={{
                          inputProps: {
                            max: now.toISOString().split("T")[0],
                          },
                        }}
                        label={
                          t(
                            `components.Intervention.properties.${fieldName?.label}`
                          ) as string
                        }
                        variant="filled"
                        type="date"
                        disabled={readOnly}
                        sx={{ width: "100%" }}
                        InputLabelProps={{
                          shrink: true,
                        }}
                        value={value}
                        onChange={(e) => onChange(e.target.value)}
                      />
                    );
                  }}
                />
                <FormInputError name={fieldName.column} errors={errors} />
              </>
            )}
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
};

export default InterventionFields;
