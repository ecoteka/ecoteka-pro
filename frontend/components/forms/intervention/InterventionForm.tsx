import React, { FC, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import {
  Grid,
  TextField,
  Autocomplete,
  AutocompleteRenderInputParams,
  Stack,
  Typography,
  Button,
  Box,
  SpeedDial,
  Backdrop,
  SpeedDialAction,
  Fab,
  Switch,
} from "@mui/material";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import DeleteIcon from "@mui/icons-material/Delete";
import CheckIcon from "@mui/icons-material/Check";
import EditIcon from "@mui/icons-material/Edit";
import { useSession } from "next-auth/react";
import { FreeSolo, IFreeSoloOption } from "../autocomplete/FreeSolo";
import { FormInputError } from "../error/FormInputError";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import BackButton from "../../layout/BackButton";
import { useRouter } from "next/router";
import { ModalComponent } from "../modal/ModalComponent";
import ConfirmDialog from "../dialog/ConfirmDialog";
import { FetchOneInterventionQuery } from "../../../generated/graphql";
import {
  useMapActiveAction,
  useMapEditorActions,
  useModalDataOpen,
} from "../../../lib/store/pages/mapEditor";
import { useWorksiteFormActions } from "@stores/forms/worksiteForm";
import { useDeviceSize } from "@lib/utils";
import Link from "next/link";
import { size } from "lodash";

export interface Intervention {
  id?: string;
  realization_date?: string;
  scheduled_date?: string;
  treeId?: string;
  locationId?: string;
  intervention_type?: any;
  intervention_partner?: any;
  note: string;
  validation_note?: string;
}

export interface InterventionFormProps {
  interventionTypes?: any;
  interventionPartners?: any;
  activeIntervention?: FetchOneInterventionQuery["intervention"][0];
  activeLocation?: any;
  onUpdateIntervention?(data: Intervention): void;
  onCreateIntervention?(data: Intervention): void;
  onDeleteIntervention?(id: string): void;
  onValidateIntervention?(
    id: string,
    interventionDate: string,
    interventionPartner: string,
    interventionNote: string
  ): void;
  hasTree?: boolean;
  goToNextStep?: () => void;
}

const validationSchema = Yup.object({
  realization_date: Yup.string().nullable(),
  scheduled_date: Yup.string().nullable().required(),
  intervention_type: Yup.object({
    id: Yup.string().required(),
  }).required(),
});

const InterventionForm: FC<InterventionFormProps> = ({
  interventionTypes,
  interventionPartners,
  activeIntervention,
  activeLocation,
  onUpdateIntervention,
  onCreateIntervention,
  onValidateIntervention,
  onDeleteIntervention,
  hasTree,
}) => {
  const { t } = useTranslation(["components", "common"]);
  const session = useSession();
  const router = useRouter();
  const actions = useMapEditorActions();
  const modalDataOpen = useModalDataOpen();
  const [interventionIsDone, setInterventionIsDone] = useState<boolean>(false);
  const [openDataModal, setOpenDataModal] = useState<boolean>(false);
  const [openDeleteModal, setOpenDeleteModal] = useState<boolean>(false);
  const [readOnly, setReadOnly] = useState(true);
  const [open, setOpen] = useState(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [message, setMessage] = useState("");
  const [action, setAction] = useState<string>("");
  const [actionsIntervention, setActionsIntervention] = useState<string[]>([
    "validate",
  ]);
  const mapActiveAction = useMapActiveAction();
  const { setInterventions } = useWorksiteFormActions();
  const { isDesktop } = useDeviceSize();

  const now = new Date();
  const fields = [
    {
      type: "textReadOnly",
      label: "location",
      column: hasTree ? "tree_id" : "location_id",
    },
    { type: "date", label: "realizationDate", column: "realization_date" },
    { type: "date", label: "scheduledDate", column: "scheduled_date" },
    {
      type: "textReadOnly",
      label: "dataEntryUsername",
    },
    {
      type: "autocomplete",
      label: "interventionType",
      column: "intervention_type",
    },
    {
      type: "autocomplete",
      label: "interventionPartner",
      column: "intervention_partner",
    },
    { type: "textArea", label: "note", column: "note" },
  ];

  const currentUsername = session?.data?.user?.name
    ? session?.data?.user?.name
    : "";

  const status = hasTree
    ? "alive"
    : activeIntervention?.location?.location_status?.status;
  const statusReadOnlyType = ["stump", "empty"];

  const handleOpen = (event) => {
    setOpen(!open);
  };

  const {
    handleSubmit,
    control,
    setValue,
    getValues,
    watch,
    formState: { errors, dirtyFields },
  } = useForm<Intervention>({
    resolver: yupResolver(validationSchema),
  });

  Yup.setLocale({
    mixed: {
      required: t("components.errors.required"),
    },
  });

  const onSubmitIntervention: SubmitHandler<Intervention> = async (data) => {
    setReadOnly(true);

    activeIntervention
      ? onUpdateIntervention && onUpdateIntervention(data)
      : onCreateIntervention && onCreateIntervention(data);

    actions.setModalDataOpen(false);
  };

  const checkIfData = () => {
    if (modalDataOpen === true) {
      setOpenDataModal(true);
    } else {
      router.back();
    }
  };

  const onConfirm: SubmitHandler<Intervention> = async () => {
    const id = activeIntervention?.id;
    const interventionDate = getValues("realization_date");
    const interventionPartner = getValues("intervention_partner.id");
    const validationNote = getValues("validation_note");
    switch (action) {
      case "validate":
        onValidateIntervention &&
          onValidateIntervention(
            id,
            interventionDate as string,
            interventionPartner,
            validationNote as string
          );
        break;
      case "delete":
        setOpenDeleteModal(true);
        break;
    }

    setOpenModal(false);
  };

  const onConfirmModal = () => {
    setOpenDataModal(false);
    actions.setModalDataOpen(false);
    hasTree
      ? router.push(
          `/tree/${
            activeLocation?.tree
              ? activeLocation?.tree[0]?.id
              : activeLocation?.id
          }`
        )
      : router.push(`/location/${activeLocation?.id}`);
  };

  const onConfirmDeletionModal: SubmitHandler<Intervention> = async () => {
    const id = activeIntervention?.id;
    setOpenDeleteModal(false);
    onDeleteIntervention && onDeleteIntervention(id);
    hasTree
      ? router.push(
          `/tree/${
            activeLocation?.tree
              ? activeLocation?.tree[0]?.id
              : activeLocation?.id
          }`
        )
      : router.push(`/location/${activeLocation?.id}`);
  };

  // method to check if there is data entried on form before quit
  useEffect(() => {
    if (Object.keys(dirtyFields).length > 0) {
      actions.setModalDataOpen(true);
    } else {
      actions.setModalDataOpen(false);
    }
  }, [Object.keys(dirtyFields).length]);

  useEffect(() => {
    if (!activeIntervention) {
      setReadOnly(false);
    } else {
      setReadOnly(true);
      if (Boolean(activeIntervention?.realization_date)) {
        setActionsIntervention([]);
        setInterventionIsDone(true);
      }
    }
  }, [activeIntervention]);

  useEffect(() => {
    const shouldSetDefaultValue = ["empty", "stump"].includes(
      activeLocation?.location_status?.status
    );
    if (shouldSetDefaultValue && interventionTypes.length > 0) {
      setValue("intervention_type", interventionTypes[0]);
    }
  }, [activeLocation, interventionTypes, setValue]);

  useEffect(() => {
    if (action === "validate") {
      setValue("realization_date", now.toISOString().split("T")[0]);
    }
  }, [action]);

  useEffect(() => {
    const realizationDate = interventionIsDone
      ? now.toISOString().split("T")[0]
      : "";
    action !== "validate" && setValue("realization_date", realizationDate);
  }, [interventionIsDone]);

  useEffect(() => {
    const realizationDate = interventionIsDone
      ? now.toISOString().split("T")[0]
      : "";
    action !== "validate" && setValue("realization_date", realizationDate);
  }, [interventionIsDone]);

  useEffect(() => {
    if (!activeIntervention) {
      const interventionSelected = interventionTypes.find(
        (interventionType) => interventionType.slug === router.query.type
      );
      setValue("intervention_type", interventionSelected);
    }
  }, [interventionTypes]);

  useEffect(() => {
    if (mapActiveAction === "addWorksiteGroup") {
      const watchFields = [
        "scheduled_date",
        "intervention_type",
        "intervention_partner.id",
        "note",
      ];
      const data = {
        location: activeLocation?.address || null,
        scheduled_date: watchFields.includes("scheduled_date")
          ? watch("scheduled_date")
          : null,
        intervention_type: watchFields.includes("intervention_type")
          ? watch("intervention_type")
          : null,
        intervention_partner: watchFields.includes("intervention_partner.id")
          ? watch("intervention_partner.id")
          : null,
        note: watchFields.includes("note") ? watch("note") : null,
        data_entry_user_id: session?.data?.user?.id,
        // ... other properties ...
      };

      setInterventions(data);
    }
  }, [
    watch("scheduled_date"),
    watch("intervention_type"),
    watch("intervention_partner.id"),
    watch("note"),
  ]);

  if (
    !activeIntervention &&
    !router.query.locationId &&
    mapActiveAction !== "addWorksiteGroup"
  ) {
    return null;
  }

  return (
    <>
      {mapActiveAction !== "addWorksiteGroup" && (
        <Stack
          flexDirection="row"
          justifyContent="space-between"
          sx={{ mb: 3 }}
        >
          <BackButton title={t("common.back")} onClick={checkIfData} />
          <Typography
            component="h1"
            variant="h5"
            fontWeight={400}
            textAlign={"center"}
            textTransform={"uppercase"}
          >
            {activeIntervention
              ? t("components.Template.menuItems.intervention.intervention")
              : t("common.addIntervention")}
          </Typography>
          <Stack flexDirection="row" gap={1}>
            {readOnly && isDesktop && (
              <Button
                size="large"
                variant="contained"
                color="primary"
                onClick={() => setReadOnly(false)}
                disabled={
                  size(
                    activeIntervention?.intervention_worksite_interventions
                  ) > 0
                    ? true
                    : false
                }
              >
                {t("common.edit") as string}
              </Button>
            )}
            {!readOnly && isDesktop && (
              <Button
                size="large"
                variant="contained"
                color="primary"
                onClick={handleSubmit(onSubmitIntervention)}
              >
                {t("common.save") as string}
              </Button>
            )}
            {isDesktop && activeIntervention ? (
              readOnly ? (
                actionsIntervention.map((action) => (
                  <Button
                    key={action}
                    size="medium"
                    variant="outlined"
                    color={action === "delete" ? "error" : "primary"}
                    onClick={() => {
                      setAction(action);
                      setMessage(
                        t(`components.InterventionForm.message.${action}`)
                      );
                      setOpenModal(true);
                    }}
                  >
                    {t("components.InterventionForm.message.validate")}
                  </Button>
                ))
              ) : (
                <Button
                  variant="outlined"
                  color="error"
                  onClick={() => {
                    setAction("delete");
                    setMessage(t("components.InterventionForm.message.delete"));
                    setOpenModal(true);
                  }}
                >
                  {t("common.buttons.delete")}
                </Button>
              )
            ) : (
              <Box
                sx={{
                  position: "absolute",
                  bottom: 16,
                  right: 16,
                  pointerEvents: "all",
                }}
              >
                {activeIntervention && (
                  <SpeedDial
                    open={open}
                    onClick={handleOpen}
                    ariaLabel="SpeedDial"
                    icon={
                      <Fab color="primary" size="medium" aria-label="filter">
                        <MoreVertIcon />
                      </Fab>
                    }
                  >
                    <Backdrop open={true} />
                    {readOnly && (
                      <SpeedDialAction
                        tooltipTitle={"Editer"}
                        tooltipOpen
                        icon={<EditIcon color="primary" />}
                        onClick={() => {
                          setReadOnly(false);
                        }}
                      />
                    )}

                    {!Boolean(activeIntervention?.realization_date) && (
                      <SpeedDialAction
                        tooltipTitle={"Déclarer réalisée"}
                        tooltipOpen
                        icon={<CheckIcon color="primary" />}
                        onClick={() => {
                          setAction("validate");
                          setMessage(
                            t("components.InterventionForm.message.validate")
                          );
                          setOpenModal(true);
                        }}
                      />
                    )}
                    {!readOnly && (
                      <SpeedDialAction
                        tooltipTitle={"Supprimer l'intervention"}
                        tooltipOpen
                        icon={<DeleteIcon color="error" />}
                        onClick={() => {
                          setAction("delete");
                          setMessage(
                            t("components.InterventionForm.message.delete")
                          );
                          setOpenModal(true);
                        }}
                      />
                    )}
                  </SpeedDial>
                )}
              </Box>
            )}
          </Stack>
        </Stack>
      )}

      <Grid container gap={1.5}>
        {!activeIntervention &&
          mapActiveAction !== "addWorksiteGroup" &&
          !interventionIsDone && (
            <Box>
              {t(`components.InterventionForm.message.alreadyDone`) as string}
              <Switch
                checked={interventionIsDone}
                color="primary"
                onChange={(e) => {
                  setInterventionIsDone(!interventionIsDone);
                  setValue("scheduled_date", now.toISOString().split("T")[0]);
                  setValue("realization_date", now.toISOString().split("T")[0]);
                }}
              />
            </Box>
          )}

        {size(activeIntervention?.intervention_worksite_interventions) > 0 && (
          <Typography>
            Cette intervention appartient à un{" "}
            <Link
              href={`/worksites/${activeIntervention?.intervention_worksite_interventions[0].worksite.id}`}
              style={{ textDecoration: "underline" }}
            >
              chantier
            </Link>
            , vous ne pouvez pas l'éditer
          </Typography>
        )}

        {fields.map((fieldName) => (
          <Grid key={fieldName?.label} item md={12} xs={12}>
            {fieldName?.label === "interventionType" && (
              <>
                <Controller
                  defaultValue={activeIntervention?.[fieldName.column!] || ""}
                  name={`${fieldName.column as keyof Intervention}`}
                  rules={{ required: true }}
                  control={control}
                  render={({ field: { onChange, value } }) => {
                    value = value ?? null;
                    return (
                      <Autocomplete
                        id={`autocomplete-${name}`}
                        options={interventionTypes}
                        value={value}
                        renderInput={(
                          params: AutocompleteRenderInputParams
                        ) => (
                          <TextField
                            {...params}
                            required
                            label={
                              t(
                                `components.Intervention.properties.${fieldName?.label}`
                              ) as string
                            }
                            variant="filled"
                            InputLabelProps={{ shrink: true }}
                          />
                        )}
                        getOptionLabel={(option) => {
                          if (option.slug !== undefined) {
                            return t(
                              `components.PanelTree.interventionsList.${option.slug}`
                            );
                          } else {
                            return "";
                          }
                        }}
                        onChange={(e, newValue) => {
                          onChange(newValue);
                        }}
                        disabled={
                          readOnly ||
                          statusReadOnlyType.includes(status as string)
                            ? true
                            : false
                        }
                      />
                    );
                  }}
                />
                <FormInputError name={fieldName.column!} errors={errors} />
              </>
            )}
            {fieldName?.label === "interventionPartner" && (
              <FreeSolo
                defaultValue={activeIntervention?.[fieldName.column!] || ""}
                control={control}
                setValue={setValue}
                name={`${fieldName.column as keyof Intervention}`}
                listOptions={interventionPartners ?? ([] as IFreeSoloOption[])}
                required={false}
                label={
                  t(
                    `components.Intervention.properties.${fieldName?.label}`
                  ) as string
                }
                readOnly={readOnly}
                object={"intervention"}
              />
            )}
            {((fieldName?.label === "location" &&
              mapActiveAction !== "addWorksiteGroup") ||
              fieldName?.label === "dataEntryUsername") && (
              <TextField
                fullWidth
                required
                disabled
                sx={{
                  display: "flex",
                }}
                label={
                  t(
                    `components.Intervention.properties.${fieldName?.label}`
                  ) as string
                }
                variant="filled"
                placeholder={t(
                  `components.Intervention.placeholder.${fieldName?.label}`
                )}
                error={!!errors[fieldName?.label]}
                helperText={errors[fieldName?.label]?.message}
                type={fieldName?.type}
                defaultValue={
                  fieldName?.label === "location" &&
                  mapActiveAction !== "addWorksiteGroup"
                    ? hasTree
                      ? activeIntervention?.tree?.location?.address
                      : activeIntervention?.location?.address
                    : fieldName?.label === "dataEntryUsername"
                    ? currentUsername
                    : undefined
                }
              />
            )}

            {(fieldName?.type === "text" ||
              fieldName?.type === "textArea" ||
              fieldName?.type === "number") && (
              <Controller
                name={`${fieldName?.column as keyof Intervention}`}
                control={control}
                defaultValue={activeIntervention?.[fieldName.column!] || ""}
                render={({ field: { onChange, ref, value } }) => (
                  <TextField
                    value={value}
                    inputRef={ref}
                    fullWidth
                    disabled={readOnly}
                    sx={{
                      display: "flex",
                    }}
                    multiline={fieldName?.type === "textArea" ? true : false}
                    label={
                      t(
                        `components.Intervention.properties.${fieldName?.label}`
                      ) as string
                    }
                    variant="filled"
                    placeholder={t(
                      `components.Intervention.placeholder.${fieldName?.label}`
                    )}
                    error={!!errors[fieldName?.label]}
                    helperText={errors[fieldName?.label]?.message}
                    onChange={onChange}
                    type={fieldName?.type}
                    inputProps={fieldName?.type === "number" ? { min: 0 } : {}}
                  />
                )}
              />
            )}
            {fieldName.column === "realization_date" && interventionIsDone && (
              <>
                <Controller
                  name={`${fieldName?.column as keyof Intervention}`}
                  control={control}
                  defaultValue={activeIntervention?.[fieldName.column!] || ""}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <TextField
                        InputProps={{
                          inputProps: {
                            max: now.toISOString().split("T")[0],
                          },
                        }}
                        label={
                          t(
                            `components.Intervention.properties.${fieldName?.label}`
                          ) as string
                        }
                        variant="filled"
                        type="date"
                        disabled={readOnly}
                        sx={{ width: "100%" }}
                        InputLabelProps={{
                          shrink: true,
                        }}
                        value={value}
                        onChange={(e) => onChange(e.target.value)}
                      />
                    );
                  }}
                />
                <FormInputError name={fieldName.column} errors={errors} />
              </>
            )}
            {fieldName.column === "scheduled_date" && (
              <>
                <Controller
                  name={`${fieldName?.column as keyof Intervention}`}
                  control={control}
                  defaultValue={activeIntervention?.[fieldName.column!] || ""}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <TextField
                        required
                        InputProps={{
                          inputProps: {
                            min: now.toISOString().split("T")[0],
                          },
                        }}
                        label={
                          t(
                            `components.Intervention.properties.${fieldName?.label}`
                          ) as string
                        }
                        variant="filled"
                        type="date"
                        disabled={readOnly}
                        sx={{
                          width: "100%",
                          display: interventionIsDone ? "none" : "auto",
                        }}
                        InputLabelProps={{
                          shrink: true,
                        }}
                        value={value}
                        onChange={(e) => onChange(e.target.value)}
                      />
                    );
                  }}
                />
                <FormInputError name={fieldName.column} errors={errors} />
              </>
            )}
          </Grid>
        ))}
        {!readOnly && !isDesktop && (
          <Button
            size="large"
            variant="contained"
            color="primary"
            onClick={handleSubmit(onSubmitIntervention)}
            sx={{ m: "auto" }}
          >
            {t("common.save") as string}
          </Button>
        )}
      </Grid>

      <Box>
        <ModalComponent
          open={openModal}
          handleClose={() => setOpenModal(false)}
        >
          <ConfirmDialog
            message={message}
            onConfirm={onConfirm}
            onAbort={() => setOpenModal(false)}
          >
            <>
              {action === "validate" && (
                <>
                  <Controller
                    name={`realization_date`}
                    control={control}
                    defaultValue={now.toISOString().split("T")[0]}
                    render={({ field: { onChange, value } }) => {
                      return (
                        <TextField
                          label={
                            t(
                              `components.Intervention.properties.realizationDate`
                            ) as string
                          }
                          variant="filled"
                          type="date"
                          sx={{ width: "100%", mb: 2, mt: 1 }}
                          InputLabelProps={{
                            shrink: true,
                          }}
                          inputProps={{
                            max: now.toISOString().split("T")[0],
                          }}
                          value={value}
                          onChange={(e) => onChange(e.target.value)}
                        />
                      );
                    }}
                  />
                  <Controller
                    name={`intervention_partner`}
                    control={control}
                    render={() => {
                      return (
                        <FreeSolo
                          defaultValue={
                            activeIntervention?.intervention_partner?.name || ""
                          }
                          control={control}
                          setValue={setValue}
                          name={"intervention_partner" as keyof Intervention}
                          listOptions={
                            interventionPartners ?? ([] as IFreeSoloOption[])
                          }
                          required={false}
                          label={
                            t(
                              `components.Intervention.properties.interventionPartner`
                            ) as string
                          }
                          object={"intervention"}
                        />
                      );
                    }}
                  />
                  <Controller
                    name={`validation_note`}
                    control={control}
                    defaultValue={""}
                    render={({ field: { onChange, ref, value } }) => (
                      <TextField
                        value={value}
                        inputRef={ref}
                        fullWidth
                        sx={{
                          display: "flex",
                          m: 1,
                        }}
                        multiline={true}
                        label={
                          t(`components.Intervention.properties.note`) as string
                        }
                        placeholder={t(
                          `components.Intervention.placeholder.validation_note
                      `
                        )}
                        onChange={onChange}
                      />
                    )}
                  />
                </>
              )}
            </>
          </ConfirmDialog>
        </ModalComponent>

        <ModalComponent
          open={openDeleteModal}
          handleClose={() => setOpenDeleteModal(false)}
        >
          <ConfirmDialog
            message={t("common.messages.lostData")}
            onConfirm={onConfirmDeletionModal}
            onAbort={() => setOpenDeleteModal(false)}
            showConfirmButton={true}
          />
        </ModalComponent>
      </Box>

      {modalDataOpen && (
        <Box>
          <ModalComponent
            open={openDataModal}
            handleClose={() => setOpenDataModal(false)}
          >
            <ConfirmDialog
              title={t("common.quitForm")}
              message={t("common.messages.lostData")}
              onConfirm={onConfirmModal}
              onAbort={() => setOpenDataModal(false)}
            />
          </ModalComponent>
        </Box>
      )}
    </>
  );
};

export default InterventionForm;
