import React, { FC } from "react";
import { useTranslation } from "react-i18next";
import { Controller, useForm } from "react-hook-form";
import { TextField, Button } from "@mui/material";
import { IFreeSoloOption } from "../autocomplete/FreeSolo";
import { useCreateInterventionPartnerMutation } from "../../../generated/graphql";
import { ModalComponent } from "../modal/ModalComponent";

export interface PartnerFormProps {
  name: string;
  open: boolean;
  handleClose: () => void;
  onSubmit: (newOptions: IFreeSoloOption) => void;
  object: string | undefined;
}

const PartnerForm: FC<PartnerFormProps> = ({
  name,
  open,
  handleClose,
  onSubmit,
  object,
}) => {
  const { t } = useTranslation(["common"]);
  const form = useForm();
  const [insertInterventionPartner] = useCreateInterventionPartnerMutation();

  const onSubmitForm = async () => {
    let newValue = form.getValues();
    let variables = {
      name: newValue.name,
    };
    try {
      let result;
      if (object === "intervention") {
        result = await insertInterventionPartner({
          variables,
        });
        newValue = result.data
          ?.insert_intervention_partner_one as IFreeSoloOption;
      }

      onSubmit(newValue as IFreeSoloOption);
    } catch {
      // TODO si erreur
      console.log(`error in save`);
    }
  };
  return (
    <ModalComponent open={open} handleClose={handleClose}>
      <>
        <form
          onSubmit={form.handleSubmit(onSubmit)}
          style={{ textAlign: "center" }}
        >
          <Controller
            name={"name"}
            control={form.control}
            defaultValue={name}
            render={({ field: { onChange, ref, value } }) => (
              <TextField
                value={value}
                inputRef={ref}
                variant="filled"
                fullWidth
                label={
                  t(
                    `components.Intervention.properties.interventionPartner`
                  ) as string
                }
                placeholder={t(`components.Intervention.placeholder.name`)}
                onChange={onChange}
                sx={{ mb: 1 }}
              />
            )}
          />
          <Button variant="contained" onClick={form.handleSubmit(onSubmitForm)}>
            {t(`common.buttons.save`) as string}
          </Button>
        </form>
      </>
    </ModalComponent>
  );
};

export default PartnerForm;
