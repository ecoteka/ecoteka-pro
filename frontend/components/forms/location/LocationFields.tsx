import React, { FC, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Control, Controller, UseFormSetValue } from "react-hook-form";
import {
  Grid,
  TextField,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Typography,
  InputAdornment,
  IconButton,
  SelectChangeEvent,
  Chip,
  ListItemText,
  Box,
  Switch,
  FormControlLabel,
} from "@mui/material";
import CancelIcon from "@mui/icons-material/Cancel";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import _ from "lodash";
import { FormInputError } from "../error/FormInputError";
import { useRouter } from "next/router";
import {
  CreateTreeMutationVariables,
  FetchAllBoundariesQuery,
  FetchLocationFormDataQuery,
  FetchOneLocationQuery,
  FetchOneLocationWithTreeIdQuery,
  Urban_Constraint,
  useGetBoundariesWithCoordsLazyQuery,
} from "../../../generated/graphql";
import { fetchAdresse } from "@lib/apiAdresse";
import { without as _without } from "lodash";
import { LANDSCAPE_TYPE, PLANTATION_TYPE, SIDEWALK_TYPE } from "@lib/constants";
import { useMapActiveAction } from "@stores/pages/mapEditor";

export interface LocationFieldsProps {
  extended?: boolean;
  activeLocation:
    | FetchOneLocationQuery["location"][0]
    | FetchOneLocationWithTreeIdQuery["location"][0];
  statusOptions?: string[];
  loading?: boolean;
  customControl: Control<any>;
  customSetValue: UseFormSetValue<any>;
  watch: any;
  errors: any;
  boundaries: FetchAllBoundariesQuery["boundary"];
  locationFormData: FetchLocationFormDataQuery;
}

export interface Location {
  id: string;
  latitude: number;
  longitude: number;
  address: string;
  foot_type: string;
  plantation_type: string;
  urban_site_id: string;
  sidewalk_type: string;
  tree?: CreateTreeMutationVariables;
  status?: string;
  note: string;
  boundaries: {
    boundary: {
      id: string;
    };
  };
}

const LocationFields: FC<LocationFieldsProps> = ({
  activeLocation,
  customControl,
  customSetValue,
  errors,
  watch,
  boundaries,
  locationFormData,
  extended,
}) => {
  const [suggestedAddress, setSuggestedAddress] = useState("");
  const { t } = useTranslation(["components", "common"]);
  const { query, push } = useRouter();
  const mapActiveAction = useMapActiveAction();
  const [getBoundariesWithCoords, { data: boundariesWithCoords }] =
    useGetBoundariesWithCoordsLazyQuery();

  const [selectedUrbanConstraint, setSelectedUrbanConstraint] = React.useState<
    Urban_Constraint[]
  >([]);

  const handleChange = (
    event: SelectChangeEvent<typeof selectedUrbanConstraint>
  ) => {
    const {
      target: { value },
    } = event;

    setSelectedUrbanConstraint(value as Urban_Constraint[]);
    customSetValue("location.urban_constraint", value as Urban_Constraint[]);
  };
  const handleDelete = (event: React.MouseEvent, value: Urban_Constraint) => {
    event.preventDefault();

    setSelectedUrbanConstraint((current) => {
      const updatedConstraints = _without(current, value as Urban_Constraint);
      customSetValue("location.urban_constraint", updatedConstraints);
      return updatedConstraints;
    });
  };

  const latitude = watch("location.latitude");
  const longitude = watch("location.longitude");

  const isReadOnly = () =>
    !query.hasOwnProperty("edit") && mapActiveAction !== "addLocation";

  const fetchAddressAndUpdate = async () => {
    const fetchedAddress = await fetchAdresse(longitude, latitude);
    setSuggestedAddress(
      fetchedAddress || t("addressNotFound", { ns: "common" })
    );
  };

  useEffect(() => {
    const debouncedFetch = _.debounce(fetchAddressAndUpdate, 500);

    debouncedFetch();
    if (latitude && longitude) {
      getBoundariesWithCoords({
        variables: {
          coords: {
            type: "Point",
            coordinates: [longitude, latitude],
          },
        },
      });
    }

    return () => debouncedFetch.cancel();
  }, [latitude, longitude]);

  useEffect(() => {
    if (boundariesWithCoords) {
      const geographicBoundary =
        boundariesWithCoords.boundary?.find((b) => b?.type === "geographic")
          ?.id || "";

      const stationBoundary =
        boundariesWithCoords.boundary?.find((b) => b?.type === "station")?.id ||
        "";

      customSetValue(
        "location.boundaries.geographic_boundary",
        geographicBoundary
      );
      customSetValue("location.boundaries.station_boundary", stationBoundary);
    }
  }, [boundariesWithCoords, customSetValue]);

  useEffect(() => {
    const urbanConstraints = activeLocation?.location_urban_constraints.map(
      (o) => o.urban_constraint
    );
    setSelectedUrbanConstraint(urbanConstraints);
    customSetValue(
      "location.urban_constraint",
      urbanConstraints as Urban_Constraint[]
    );
  }, [activeLocation]);

  return (
    <>
      <Grid container columns={12} spacing={1}>
        <Grid item sm={extended ? 12 : 6} xs={12}>
          <Controller
            name="location.latitude"
            control={customControl}
            defaultValue={
              activeLocation?.coords?.coordinates[1].toFixed(8) || ""
            }
            render={({ field: { onChange, ref, value } }) => (
              <TextField
                disabled={isReadOnly()}
                value={value}
                inputRef={ref}
                fullWidth
                label={t(`components.Tree.latitude`) as string}
                variant="filled"
                placeholder={t(`components.Tree.latitude`)}
                InputLabelProps={{ shrink: true }}
                error={!!errors?.latitude}
                helperText={errors?.latitude?.message}
                onChange={(e) => {
                  onChange(e);
                }}
                type="text"
              />
            )}
          />
        </Grid>
        <Grid item sm={extended ? 12 : 6} xs={12}>
          <Controller
            name="location.longitude"
            control={customControl}
            defaultValue={
              activeLocation?.coords?.coordinates[0].toFixed(8) || ""
            }
            render={({ field: { onChange, ref, value } }) => (
              <TextField
                disabled={isReadOnly()}
                value={value}
                inputRef={ref}
                fullWidth
                label={t(`components.Tree.longitude`) as string}
                variant="filled"
                placeholder={t(`components.Tree.longitude`)}
                InputLabelProps={{ shrink: true }}
                error={!!errors?.latitude}
                helperText={errors?.latitude?.message}
                onChange={(e) => {
                  onChange(e);
                }}
                type="text"
              />
            )}
          />
        </Grid>
        <Grid item sm={extended ? 12 : 6} xs={12}>
          <>
            <Controller
              name="location.address"
              control={customControl}
              defaultValue={activeLocation?.address || ""}
              render={({ field: { onChange, ref, value } }) => (
                <TextField
                  inputRef={ref}
                  fullWidth
                  label={
                    t(`components.LocationForm.properties.address`) as string
                  }
                  variant="filled"
                  placeholder={t(`components.LocationForm.placeholder.address`)}
                  InputLabelProps={{ shrink: true }}
                  error={!!errors["address"]}
                  onChange={(e) => {
                    onChange(e);
                  }}
                  type="text"
                  value={value}
                  disabled={isReadOnly()}
                  InputProps={{
                    endAdornment: (
                      <>
                        {!isReadOnly() &&
                          suggestedAddress !== "" &&
                          suggestedAddress !== value && (
                            <InputAdornment position="end">
                              <Typography color="textSecondary" variant="body2">
                                {suggestedAddress}
                              </Typography>
                            </InputAdornment>
                          )}
                        {suggestedAddress !== "" &&
                          suggestedAddress !== value && (
                            <IconButton
                              onClick={() => {
                                customSetValue(
                                  "location.address",
                                  suggestedAddress
                                );
                              }}
                              edge="end"
                              disabled={isReadOnly()}
                            >
                              <CheckCircleOutlineIcon />
                            </IconButton>
                          )}
                      </>
                    ),
                  }}
                />
              )}
            />
            <FormInputError name="address" errors={errors} />
          </>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>
              {t(`components.LocationForm.properties.administrative_boundary`)}
            </InputLabel>
            <Controller
              name="location.boundaries.administrative_boundary"
              defaultValue={
                activeLocation?.location_boundaries.find(
                  (boundary) => boundary.boundary.type === "administrative"
                )?.boundary.id || ""
              }
              control={customControl}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={value}
                  variant="filled"
                  label={t(
                    `components.LocationForm.properties.administrative_boundary`
                  )}
                  onChange={onChange}
                  disabled={isReadOnly()}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {boundaries &&
                    boundaries
                      .filter((b) => b.type === "administrative")
                      .map((option, i) => {
                        return (
                          <MenuItem key={i} value={option.id}>
                            {option.name}
                          </MenuItem>
                        );
                      })}
                </Select>
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>
              {t(`components.LocationForm.properties.geographic`)}
            </InputLabel>
            <Controller
              name="location.boundaries.geographic_boundary"
              defaultValue={
                activeLocation?.location_boundaries.find(
                  (boundary) => boundary.boundary.type === "geographic"
                )?.boundary.id || ""
              }
              control={customControl}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={value}
                  variant="filled"
                  label={t(`components.LocationForm.properties.geographic`)}
                  onChange={onChange}
                  disabled={isReadOnly()}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {boundaries &&
                    boundaries
                      .filter((b) => b.type === "geographic")
                      .map((option, i) => {
                        return (
                          <MenuItem key={i} value={option.id}>
                            {option.name}
                          </MenuItem>
                        );
                      })}
                </Select>
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>
              {t(`components.LocationForm.properties.station_boundary`)}
            </InputLabel>
            <Controller
              name="location.boundaries.station_boundary"
              control={customControl}
              defaultValue={
                activeLocation?.location_boundaries.find(
                  (boundary) => boundary.boundary.type === "station"
                )?.boundary.id || ""
              }
              render={({ field: { onChange, value } }) => (
                <Select
                  value={value}
                  variant="filled"
                  label={t(
                    `components.LocationForm.properties.station_boundary`
                  )}
                  onChange={onChange}
                  disabled={isReadOnly()}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {boundaries &&
                    boundaries
                      .filter((b) => b.type === "station")
                      .map((option, i) => {
                        return (
                          <MenuItem key={i} value={option.id}>
                            {option.name}
                          </MenuItem>
                        );
                      })}
                </Select>
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>
              {t("components.LocationForm.labels.landscape_type")}
            </InputLabel>
            <Controller
              name="location.landscape_type"
              control={customControl}
              defaultValue={activeLocation?.landscape_type || ""}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={value}
                  variant="filled"
                  label={t("components.LocationForm.labels.landscape_type")}
                  onChange={onChange}
                  disabled={isReadOnly()}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {LANDSCAPE_TYPE.map((option, i) => {
                    return (
                      <MenuItem key={i} value={option}>
                        {
                          t(
                            `components.LocationForm.properties.landscape_type.${option}`
                          ) as string
                        }
                      </MenuItem>
                    );
                  })}
                </Select>
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>Environnement urbain</InputLabel>
            <Controller
              name="location.urban_site_id"
              control={customControl}
              defaultValue={activeLocation?.urban_site?.id || ""}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={value}
                  variant="filled"
                  label="Environnement urbain"
                  onChange={onChange}
                  disabled={isReadOnly()}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {locationFormData?.urban_site.map((option, i) => {
                    return (
                      <MenuItem key={i} value={option.id}>
                        {
                          t(
                            `components.LocationForm.properties.urban_site.${option.slug}`
                          ) as string
                        }
                      </MenuItem>
                    );
                  })}
                </Select>
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>Trottoir</InputLabel>
            <Controller
              name="location.sidewalk_type"
              control={customControl}
              defaultValue={activeLocation?.sidewalk_type || ""}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={value}
                  variant="filled"
                  label={t(`components.LocationForm.properties.sidewalk_type`)}
                  onChange={onChange}
                  disabled={isReadOnly()}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {SIDEWALK_TYPE.map((option, i) => {
                    return (
                      <MenuItem key={i} value={option}>
                        {
                          t(
                            `components.LocationForm.properties.sidewalk_type.${option}`
                          ) as string
                        }
                      </MenuItem>
                    );
                  })}
                </Select>
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>
              {t("components.LocationForm.labels.urban_constraint")}
            </InputLabel>
            <Controller
              name="location.urban_constraint"
              control={customControl}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={selectedUrbanConstraint || []}
                  variant="filled"
                  label={t("components.LocationForm.labels.urban_constraint")}
                  onChange={handleChange}
                  multiple
                  disabled={isReadOnly()}
                  renderValue={(selectedUrbanConstraint) => (
                    <Box sx={{ display: "flex", flexWrap: "wrap" }}>
                      {selectedUrbanConstraint.map((option) => (
                        <Chip
                          key={option.id}
                          label={t(
                            `components.LocationForm.properties.urban_constraint.${option.slug}`
                          )}
                          disabled={isReadOnly()}
                          clickable
                          deleteIcon={
                            <CancelIcon
                              onMouseDown={(event) => event.stopPropagation()}
                            />
                          }
                          onDelete={(e) => handleDelete(e, option)}
                        />
                      ))}
                    </Box>
                  )}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {locationFormData?.urban_constraint.map((option: any) => (
                    <MenuItem
                      key={option.id}
                      value={option}
                      selected={selectedUrbanConstraint?.includes(option)}
                    >
                      <ListItemText
                        primary={t(
                          `components.LocationForm.properties.urban_constraint.${option.slug}`
                        )}
                      />
                    </MenuItem>
                  ))}
                </Select>
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>
              {t(`components.LocationForm.labels.foot_type`) as string}
            </InputLabel>
            <Controller
              defaultValue={activeLocation?.foot_type || ""}
              name="location.foot_type"
              control={customControl}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={value}
                  error={!!errors["foot_type"]}
                  variant="filled"
                  label={
                    t(`components.LocationForm.labels.foot_type`) as string
                  }
                  onChange={onChange}
                  disabled={isReadOnly()}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {locationFormData?.foot_type.map((option, i) => {
                    return (
                      <MenuItem key={i} value={option.slug}>
                        {
                          t(
                            `components.LocationForm.properties.foot_type.${option.slug}`
                          ) as string
                        }
                      </MenuItem>
                    );
                  })}
                </Select>
              )}
            />
          </FormControl>
        </Grid>
        <Grid item sm={extended ? 12 : 6} xs={12}>
          <Controller
            name={`location.is_protected`}
            control={customControl}
            defaultValue={activeLocation?.is_protected || false}
            render={({ field: { onChange, ref, value } }) => (
              <FormControlLabel
                label={
                  t(`components.LocationForm.labels.is_protected`) as string
                }
                control={
                  <Switch
                    disabled={isReadOnly()}
                    ref={ref}
                    checked={value as boolean}
                    color="primary"
                    onChange={(e) => onChange(e.target.checked)}
                  />
                }
                labelPlacement="end"
              />
            )}
          />
        </Grid>
        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>
              {t(`components.LocationForm.labels.plantation_type`) as string}
            </InputLabel>
            <Controller
              defaultValue={activeLocation?.plantation_type || ""}
              name="location.plantation_type"
              control={customControl}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={value}
                  error={!!errors["plantation_type"]}
                  variant="filled"
                  label={
                    t(
                      `components.LocationForm.labels.plantation_type`
                    ) as string
                  }
                  onChange={onChange}
                  disabled={isReadOnly()}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {PLANTATION_TYPE.map((option, i) => {
                    return (
                      <MenuItem key={i} value={option}>
                        {
                          t(
                            `components.LocationForm.properties.plantation_type.${option}`
                          ) as string
                        }
                      </MenuItem>
                    );
                  })}
                </Select>
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <Controller
            name="location.note"
            control={customControl}
            defaultValue={activeLocation?.note || ""}
            render={({ field: { onChange, ref, value } }) => (
              <TextField
                disabled={isReadOnly()}
                value={value}
                inputRef={ref}
                fullWidth
                label="Remarques"
                variant="filled"
                InputLabelProps={{ shrink: true }}
                onChange={(e) => {
                  onChange(e);
                }}
                type="text"
              />
            )}
          />
        </Grid>
      </Grid>
    </>
  );
};
export default LocationFields;
