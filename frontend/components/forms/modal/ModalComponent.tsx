import { useDeviceSize } from "@lib/utils";
import { Box, IconButton, Modal, SxProps } from "@mui/material";
import Backdrop from "@mui/material/Backdrop";
import { FC, ReactElement } from "react";
import CloseIcon from "@mui/icons-material/Close";

interface ModalProps {
  open: boolean;
  handleClose: () => void;
  children: ReactElement | null;
  sx?: SxProps;
}

const ModalComponent: FC<ModalProps> = (props) => {
  const { open, handleClose, ...modalProps } = props;
  const { isMobile } = useDeviceSize();
  return (
    <Modal
      open={open}
      onClose={handleClose}
      {...modalProps}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
      slots={{ backdrop: Backdrop }}
      slotProps={{
        backdrop: {
          sx: {
            backdropFilter: "blur(0.75px)",
          },
        },
      }}
      data-cy="map-modal-data-component"
    >
      <Box
        sx={
          props.sx
            ? props.sx
            : {
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                width: isMobile ? "95%" : "auto",
                bgcolor: "background.paper",
                borderRadius: "0.250rem",
                boxShadow: 24,
                p: 2,
                overflow: "auto",
                maxHeight: "85%",
              }
        }
      >
        <IconButton
          onClick={handleClose}
          sx={{
            position: "absolute",
            top: "0.5rem",
            right: "0.5rem",
          }}
        >
          <CloseIcon />
        </IconButton>
        {props.children}
      </Box>
    </Modal>
  );
};

export { ModalComponent };
