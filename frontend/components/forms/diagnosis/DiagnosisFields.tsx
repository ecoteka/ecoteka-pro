import React, { FC, useEffect, useState } from "react";
import { MVTLayer } from "@deck.gl/geo-layers";
import { MVTLayerProps } from "@deck.gl/geo-layers/typed";
import { useTranslation } from "react-i18next";
import {
  Control,
  Controller,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";
import CancelIcon from "@mui/icons-material/Cancel";
import ErrorIcon from "@mui/icons-material/Error";
import {
  Grid,
  TextField,
  Autocomplete,
  AutocompleteRenderInputParams,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  SelectChangeEvent,
  FormControlLabel,
  ListItemText,
  Chip,
  Switch,
  Box,
} from "@mui/material";
import { useSession } from "next-auth/react";
import { FormInputError } from "../error/FormInputError";
import * as Yup from "yup";
import { useRouter } from "next/router";
import {
  Analysis_Tool,
  Diagnosis,
  Diagnosis_Type,
  FetchDiagnosisFormDataQuery,
  FetchOneDiagnosisQuery,
  FetchOneTreeQuery,
  Pathogen,
} from "../../../generated/graphql";
import { debounce, without as _without } from "lodash";
import { useMapStyle } from "../../../lib/store/pages/mapEditor";
import {
  TREE_CONDITIONS,
  RECOMMENDATIONS,
  COLLAR_TRUNK_CONDITIONS,
  VIGOR_CONDITIONS,
  CROWN_CARPENTERS_CONDITIONS,
} from "@lib/constants";
import { GlobalLoader } from '@components/layout/GlobalLoader';
import { InterventionCardMap } from "@components/_core/cards";
import getConfig from "next/config";
import useStore from "@stores/useStore";
import { InitialViewState } from "@components/map/Base";
import { Feature } from "maplibre-gl";

export interface SelectedLocationProps {
  status_name: string;
  location_id: string;
  location_address: string;
  tree_serial_number: string;
  tree_id: string;
}
export interface DiagnosisFieldsProps {
  customControl: Control<any>;
  customSetValue: UseFormSetValue<any>;
  customWatch: UseFormWatch<any>;
  activeDiagnosis?: FetchOneDiagnosisQuery["diagnosis"][0];
  errors: any;
  diagnosisFormData: FetchDiagnosisFormDataQuery;
  activeTree?: FetchOneTreeQuery["tree"][0];
}

const fields = [
  { type: "date", label: "diagnosis_date", column: "diagnosis_date", size: 6 },
  {
    type: "autocomplete",
    label: "diagnosisType",
    column: "diagnosis_type",
    size: 6,
  },
  {
    type: "textReadOnly",
    label: "dataEntryUsername",
    size: 6,
  },
  {
    type: "select",
    label: "tree_condition",
    column: "tree_condition",
    size: 6,
  },
  { type: "select", label: "tree_vigor", column: "tree_vigor", size: 12 },

  {
    type: "select",
    label: "collar_condition",
    column: "collar_condition",
    size: 6,
  },
  {
    type: "select",
    label: "trunk_condition",
    column: "trunk_condition",
    size: 6,
  },
  {
    type: "select",
    label: "carpenters_condition",
    column: "carpenters_condition",
    size: 6,
  },
  {
    type: "select",
    label: "crown_condition",
    column: "crown_condition",
    size: 6,
  },
  {
    type: "boolean",
    label: "tree_is_dangerous",
    column: "tree_is_dangerous",
    size: 12,
  },
  { type: "multiselect", label: "pathogen", column: "pathogens", size: 12 },

  {
    type: "multiselect",
    label: "analysis_tool",
    column: "analysis_tools",
    size: 12,
  },
  {
    type: "text",
    label: "analysis_results",
    column: "analysis_results",
    size: 12,
  },
  { type: "textArea", label: "note", column: "note", size: 12 },
  {
    type: "select",
    label: "recommendation",
    column: "recommendation",
    size: 6,
  },
  {
    type: "textReadOnly",
    label: "location",
    column: "tree_id",
    size: 6,
  },
];

const selectOptions = {
  tree_condition: TREE_CONDITIONS,
  tree_vigor: VIGOR_CONDITIONS,
  crown_condition: CROWN_CARPENTERS_CONDITIONS,
  trunk_condition: COLLAR_TRUNK_CONDITIONS,
  collar_condition: COLLAR_TRUNK_CONDITIONS,
  carpenters_condition: CROWN_CARPENTERS_CONDITIONS,
  recommendation: RECOMMENDATIONS,
};

const DiagnosisFields: FC<DiagnosisFieldsProps> = ({
  activeDiagnosis,
  diagnosisFormData,
  activeTree,
  customSetValue,
  customControl,
  customWatch,
  errors,
}) => {
  const { t } = useTranslation(["components", "common"]);
  const { publicRuntimeConfig } = getConfig();
  const { app } = useStore((store) => store);
  const session = useSession();
  const router = useRouter();
  const [showDetailedForm, setShowDetailedForm] = useState<boolean>(false);
  const [selectedPathogen, setSelectedPathogen] = React.useState<Pathogen[]>(
    []
  );
  const [selectedAnalysisTool, setSelectedAnalysisTool] = React.useState<
    Analysis_Tool[]
  >([]);

  const diagnosisType = customWatch("diagnosis_type");

  const [selectedLocation, setSelectedLocation] =
    useState<SelectedLocationProps>();
  const [viewState, setViewState] = useState<InitialViewState>({
    longitude: activeTree
      ? activeTree.location.coords.coordinates[0]
      : app?.organization?.coords.coordinates[0],
    latitude: activeTree
      ? activeTree.location.coords.coordinates[1]
      : app?.organization?.coords.coordinates[1],
    zoom: 13,
    bearing: 0,
    pitch: 0,
  });
  const mapStyle = useMapStyle();
  const [layers, setLayers] = useState<any[]>();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const [readOnly, setReadOnly] = useState(false);

  const now = new Date();

  const currentUsername = session?.data?.user?.name
    ? session?.data?.user?.name
    : "";

  Yup.setLocale({
    mixed: {
      required: t("components.errors.required"),
    },
  });

  const handleOnViewStateChange = (viewState: any) => {
    const nextViewState: InitialViewState = viewState.viewState;
    setViewState({
      ...nextViewState,
      longitude: nextViewState.longitude,
      latitude: nextViewState.latitude,
      zoom: nextViewState.zoom,
    });
  };

  const handleOnMapClick = async (info) => {
    setSelectedLocation(info?.object?.properties);
  };

  const debouncedHandleOnMapClick = debounce(handleOnMapClick, 100);

  const isFeatureSelected = (currentFeature: Feature): boolean => {
    const locationId = currentFeature.properties.location_id;
    return selectedLocation?.location_id === locationId;
  };

  const getFillColor = (d) => {
    if (isFeatureSelected(d)) {
      return [255, 243, 80, 255];
    }
    if (d?.properties?.status_color) {
      return JSON.parse(d?.properties?.status_color);
    }
    return [0, 0, 0, 255];
  };

  const createLocationsLayer = () => {
    const ecotekaLocations: MVTLayerProps = new MVTLayer({
      // TODO : add to NEXT_PUBLIC_TILESERVICE_URL
      data: activeTree
        ? `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/martin/filter_locations/{z}/{x}/{y}?tree_id=${activeTree.id}&location_status=bf6fcfe3-fbe9-4467-98be-a935efcf67a3`
        : `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/martin/filter_locations/{z}/{x}/{y}?location_status=bf6fcfe3-fbe9-4467-98be-a935efcf67a3`,
      id: "trees",
      // @ts-ignore
      getRadius: 3,
      opacity: 0.9,
      stroked: true,
      pickable: true,
      filled: true,
      extruded: true,
      pointType: "circle",
      pointRadiusMinPixels: 8,
      pointRadiusScale: 1,
      minRadius: 5,
      radiusMinPixels: 0.5,
      lineWidthMinPixels: 1,
      lineWidthMaxPixels: 3,
      autoHighlight: true,
      visible: true,
      lineWidthScale: 5,
      getFillColor: (d) => getFillColor(d),
      updateTriggers: {
        getFillColor: [selectedLocation],
      },
      onClick: (d) => handleOnMapClick(d),
      onViewportLoad(): void {
        setIsLoading(false);
      },
      onTileLoad: () => {
        setIsLoading(true);
      },
    });
    setLayers([ecotekaLocations]);
    return ecotekaLocations;
  };

  const handleChange = (
    event: SelectChangeEvent<
      typeof selectedPathogen | typeof selectedAnalysisTool
    >,
    type: string
  ) => {
    const {
      target: { value },
    } = event;
    if (type === "pathogen") {
      setSelectedPathogen(value as Pathogen[]);
      customSetValue("pathogens", value as Pathogen[]);
    } else if (type === "analysis_tool") {
      setSelectedAnalysisTool(value as Analysis_Tool[]);
      customSetValue("analysis_tools", value as Analysis_Tool[]);
    }
  };

  const handleDelete = (
    event: React.MouseEvent,
    type: string,
    value: Pathogen | Analysis_Tool
  ) => {
    event.preventDefault();

    if (type === "pathogen") {
      setSelectedPathogen((current) => {
        const updatedPathogens = _without(current, value as Pathogen);
        customSetValue("pathogens", updatedPathogens);
        return updatedPathogens;
      });
    } else if (type === "analysis_tool") {
      setSelectedAnalysisTool((current) => {
        const updatedAnalysisTools = _without(current, value as Analysis_Tool);
        customSetValue("analysis_tools", updatedAnalysisTools);
        return updatedAnalysisTools;
      });
    }
  };

  useEffect(() => {
    createLocationsLayer();
  }, [selectedLocation]);

  useEffect(() => {
    if (selectedLocation) {
      customSetValue("tree.id", selectedLocation?.tree_id || null);
      customSetValue("location.address", selectedLocation?.location_address);
    }
  }, [selectedLocation]);

  useEffect(() => {
    if (activeTree) {
      const transformedLocation = {
        status_name: "alive",
        location_id: activeTree.location.id,
        location_address: activeTree.location.address || "",
        tree_id: activeTree.id,
        tree_serial_number: activeTree.serial_number || "",
      };

      setSelectedLocation(transformedLocation);
    }
  }, [location]);

  useEffect(() => {
    if (!activeDiagnosis) {
      const diagnosisSelected = diagnosisFormData?.diagnosis_type.find(
        (diag) => diag.slug === router.query.type
      );
      customSetValue("diagnosis_type", diagnosisSelected as Diagnosis_Type);
    }
  }, [diagnosisFormData]);

  useEffect(() => {
    if (diagnosisType && diagnosisType?.slug === "detailed_diagnosis") {
      setShowDetailedForm(true);
    } else {
      setShowDetailedForm(false);
    }
  }, [diagnosisType]);

  return (
    <Grid
      container
      flexDirection="row"
      justifyContent="space-between"
      alignContent={"center"}
      sx={{ mt: 1 }}
    >
      {isLoading && <GlobalLoader />}
      <InterventionCardMap
        handleOnViewStateChange={handleOnViewStateChange}
        initialViewState={viewState}
        mapStyle={mapStyle}
        layers={layers ? layers : []}
        onClick={debouncedHandleOnMapClick}
      />

      <Grid item xs={12} mt={1}>
        <Grid container spacing={1.5}>
          {fields.map((fieldName, index) => (
            <Grid item xs={12} sm={6} lg={3} xl={fieldName.size} key={index}>
              {fieldName?.type === "select" && (
                <FormControl fullWidth>
                  <InputLabel
                    required={
                      ["recommendation", "tree_condition"].includes(
                        fieldName?.label
                      ) && true
                    }
                  >
                    {
                      t(
                        `components.Diagnosis.labels.${fieldName.label}`
                      ) as string
                    }
                  </InputLabel>
                  <Controller
                    defaultValue={
                      (activeDiagnosis &&
                        activeDiagnosis?.[fieldName.column!]) ||
                      ""
                    }
                    name={`${fieldName.column as keyof Diagnosis}`}
                    control={customControl}
                    render={({ field: { onChange, value } }) => (
                      <Select
                        value={value}
                        onChange={onChange}
                        disabled={readOnly}
                        label={
                          t(
                            `components.Diagnosis.labels.${fieldName.label}`
                          ) as string
                        }
                      >
                        {selectOptions[fieldName?.label]?.map(
                          (option: string, i: React.Key) => {
                            return (
                              <MenuItem key={i} value={option}>
                                {
                                  t(
                                    `components.Diagnosis.${
                                      fieldName?.label === "recommendation"
                                        ? "recommendation"
                                        : "conditions"
                                    }.${option}`
                                  ) as string
                                }
                              </MenuItem>
                            );
                          }
                        )}
                      </Select>
                    )}
                  />
                  <FormInputError
                    name={fieldName.column || ""}
                    errors={errors}
                  />
                </FormControl>
              )}

              {fieldName?.label === "pathogen" && (
                <FormControl fullWidth>
                  <InputLabel>
                    {
                      t(
                        `components.Diagnosis.labels.${fieldName.label}`
                      ) as string
                    }
                  </InputLabel>
                  <Controller
                    name={`pathogens`}
                    control={customControl}
                    render={() => (
                      <Select
                        value={selectedPathogen}
                        onChange={(event) =>
                          handleChange(event, fieldName?.label)
                        }
                        disabled={readOnly ? true : false}
                        multiple
                        renderValue={(selectedPathogen) => (
                          <Box sx={{ display: "flex", flexWrap: "wrap" }}>
                            {selectedPathogen.map((option) => (
                              <Chip
                                key={option.id}
                                label={t(
                                  `components.Diagnosis.pathogen.${option.slug}`
                                )}
                                disabled={readOnly}
                                clickable
                                deleteIcon={
                                  <CancelIcon
                                    onMouseDown={(event) =>
                                      event.stopPropagation()
                                    }
                                  />
                                }
                                onDelete={(e) =>
                                  handleDelete(e, fieldName?.label, option)
                                }
                              />
                            ))}
                          </Box>
                        )}
                        label={
                          t(
                            `components.Diagnosis.labels.${fieldName.label}`
                          ) as string
                        }
                      >
                        {diagnosisFormData["pathogen"]?.map((option: any) => (
                          <MenuItem
                            key={option.id}
                            value={option}
                            selected={selectedPathogen.includes(option)}
                          >
                            <ListItemText
                              primary={t(
                                `components.Diagnosis.pathogen.${option.slug}`
                              )}
                            />
                          </MenuItem>
                        ))}
                      </Select>
                    )}
                  />
                  <FormInputError
                    name={fieldName.column || ""}
                    errors={errors}
                  />
                </FormControl>
              )}

              {fieldName?.label === "analysis_tool" && showDetailedForm && (
                <FormControl fullWidth>
                  <InputLabel>
                    {
                      t(
                        `components.Diagnosis.labels.${fieldName.label}`
                      ) as string
                    }
                  </InputLabel>

                  <Controller
                    name={`analysis_tools`}
                    control={customControl}
                    render={() => (
                      <Select
                        value={selectedAnalysisTool}
                        onChange={(event) =>
                          handleChange(event, fieldName?.label)
                        }
                        renderValue={(selectedAnalysisTool) => (
                          <Box sx={{ display: "flex", flexWrap: "wrap" }}>
                            {selectedAnalysisTool.map((option) => (
                              <Chip
                                key={option.id}
                                label={t(
                                  `components.Diagnosis.analysis_tool.${option.slug}`
                                )}
                                disabled={readOnly}
                                clickable
                                deleteIcon={
                                  <CancelIcon
                                    onMouseDown={(event) =>
                                      event.stopPropagation()
                                    }
                                  />
                                }
                                onDelete={(e) =>
                                  handleDelete(e, fieldName?.label, option)
                                }
                              />
                            ))}
                          </Box>
                        )}
                        disabled={readOnly ? true : false}
                        multiple
                        label={
                          t(
                            `components.Diagnosis.labels.${fieldName.label}`
                          ) as string
                        }
                      >
                        {diagnosisFormData["analysis_tool"]?.map(
                          (option: any) => (
                            <MenuItem
                              key={option.id}
                              value={option}
                              selected={selectedAnalysisTool.includes(option)}
                            >
                              <ListItemText
                                primary={t(
                                  `components.Diagnosis.analysis_tool.${option.slug}`
                                )}
                              />
                            </MenuItem>
                          )
                        )}
                      </Select>
                    )}
                  />
                  <FormInputError
                    name={fieldName.column || ""}
                    errors={errors}
                  />
                </FormControl>
              )}
              {fieldName?.label === "analysis_results" && showDetailedForm && (
                <Controller
                  name={`${fieldName?.column as keyof Diagnosis}`}
                  control={customControl}
                  defaultValue={activeDiagnosis?.[fieldName.column!] || ""}
                  render={({ field: { onChange, ref, value } }) => (
                    <TextField
                      value={value}
                      inputRef={ref}
                      fullWidth
                      disabled={readOnly}
                      sx={{
                        display: "flex",
                      }}
                      multiline
                      label={
                        t(
                          `components.Diagnosis.labels.${fieldName?.label}`
                        ) as string
                      }
                      variant="filled"
                      placeholder={t(
                        `components.Diagnosis.labels.${fieldName?.label}`
                      )}
                      error={!!errors[fieldName?.label]}
                      helperText={errors[fieldName?.label]?.message}
                      onChange={onChange}
                      type={fieldName?.type}
                    />
                  )}
                />
              )}

              {fieldName?.label === "diagnosisType" && (
                <>
                  <Controller
                    defaultValue={activeDiagnosis?.[fieldName.column!] || ""}
                    name={`${fieldName.column as keyof Diagnosis}`}
                    rules={{ required: true }}
                    control={customControl}
                    render={({ field: { onChange, value } }) => {
                      value = value ?? null;
                      return (
                        <Autocomplete
                          id={`autocomplete-${name}`}
                          options={diagnosisFormData?.diagnosis_type}
                          value={value}
                          renderInput={(
                            params: AutocompleteRenderInputParams
                          ) => (
                            <TextField
                              {...params}
                              required
                              label={
                                t(
                                  `components.Diagnosis.labels.${fieldName?.label}`
                                ) as string
                              }
                              variant="filled"
                              InputLabelProps={{ shrink: true }}
                            />
                          )}
                          getOptionLabel={(option) => {
                            if (option.slug !== undefined) {
                              return t(
                                `components.PanelTree.interventionsList.${option.slug}`
                              );
                            } else {
                              return "";
                            }
                          }}
                          onChange={(e, newValue) => {
                            onChange(newValue);
                          }}
                          disabled={readOnly ? true : false}
                        />
                      );
                    }}
                  />
                  <FormInputError name={fieldName.column!} errors={errors} />
                </>
              )}
              {fieldName?.type === "boolean" && (
                <Controller
                  defaultValue={activeDiagnosis?.[fieldName.column!] || false}
                  name={`${fieldName.column as keyof Diagnosis}`}
                  control={customControl}
                  render={({ field: { onChange, ref, value } }) => (
                    <FormControlLabel
                      label={
                        <div style={{ display: "flex", alignItems: "center" }}>
                          <ErrorIcon color="error" sx={{ mr: 1 }} />
                          <span>
                            {
                              t(
                                `components.Diagnosis.labels.${fieldName?.label}`
                              ) as string
                            }
                          </span>
                        </div>
                      }
                      control={
                        <Switch
                          disabled={readOnly}
                          ref={ref}
                          checked={value as boolean}
                          color="primary"
                          onChange={(e) => onChange(e.target.checked)}
                        />
                      }
                      labelPlacement="end"
                    />
                  )}
                />
              )}
              {fieldName?.type === "textReadOnly" &&
                fieldName?.label !== "location" && (
                  <TextField
                    defaultValue={
                      activeDiagnosis?.[fieldName.column!] || currentUsername
                    }
                    fullWidth
                    disabled={true}
                    sx={{
                      display: "flex",
                    }}
                    label={
                      t(
                        `components.Intervention.properties.${fieldName?.label}`
                      ) as string
                    }
                    variant="filled"
                    placeholder={t(
                      `components.Intervention.placeholder.${fieldName?.label}`
                    )}
                    error={!!errors[fieldName?.label]}
                    helperText={errors[fieldName?.label]?.message}
                    type={fieldName?.type}
                  />
                )}
              {(fieldName?.type === "textArea" ||
                fieldName?.type === "number") && (
                <Controller
                  name={`${fieldName?.column as keyof Diagnosis}`}
                  control={customControl}
                  defaultValue={activeDiagnosis?.[fieldName.column!] || ""}
                  render={({ field: { onChange, ref, value } }) => (
                    <TextField
                      value={value}
                      inputRef={ref}
                      fullWidth
                      disabled={readOnly}
                      sx={{
                        display: "flex",
                      }}
                      multiline={fieldName?.type === "textArea" ? true : false}
                      label={
                        t(
                          `components.Intervention.properties.${fieldName?.label}`
                        ) as string
                      }
                      variant="filled"
                      placeholder={t(
                        `components.Intervention.placeholder.${fieldName?.label}`
                      )}
                      error={!!errors[fieldName?.label]}
                      helperText={errors[fieldName?.label]?.message}
                      onChange={onChange}
                      type={fieldName?.type}
                      inputProps={
                        fieldName?.type === "number" ? { min: 0 } : {}
                      }
                    />
                  )}
                />
              )}
              {fieldName.type === "date" && (
                <>
                  <Controller
                    name={`${fieldName?.column as keyof Diagnosis}`}
                    control={customControl}
                    defaultValue={
                      activeDiagnosis?.[fieldName.column!] ||
                      now.toISOString().split("T")[0]
                    }
                    render={({ field: { onChange, value } }) => {
                      return (
                        <TextField
                          required
                          label={
                            t(
                              `components.Diagnosis.labels.${fieldName?.label}`
                            ) as string
                          }
                          variant="filled"
                          type="date"
                          disabled={readOnly}
                          sx={{ width: "100%" }}
                          inputProps={{
                            max: now.toISOString().split("T")[0],
                          }}
                          InputLabelProps={{
                            shrink: true,
                          }}
                          value={value}
                          onChange={(e) => onChange(e.target.value)}
                        />
                      );
                    }}
                  />
                  <FormInputError name={fieldName.column!} errors={errors} />
                </>
              )}
              {fieldName?.label === "location" && (
                <Controller
                  name={"location.address"}
                  control={customControl}
                  defaultValue={activeDiagnosis?.[fieldName.column!] || ""}
                  rules={{ required: true }}
                  render={() => (
                    <TextField
                      fullWidth
                      disabled={true}
                      required
                      value={
                        selectedLocation?.location_address ||
                        selectedLocation?.tree_serial_number ||
                        ""
                      }
                      sx={{
                        display: "flex",
                      }}
                      label={
                        t(
                          `components.Intervention.properties.${fieldName?.label}`
                        ) as string
                      }
                      variant="filled"
                      InputLabelProps={{ shrink: true }}
                      error={!!errors[fieldName?.label]}
                      helperText={errors[fieldName?.label]?.message}
                      type={fieldName?.type}
                    />
                  )}
                />
              )}
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default DiagnosisFields;
