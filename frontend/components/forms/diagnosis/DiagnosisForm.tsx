import React, { FC, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import CancelIcon from "@mui/icons-material/Cancel";
import _without from "lodash/without";
import {
  Grid,
  TextField,
  Autocomplete,
  AutocompleteRenderInputParams,
  Stack,
  Typography,
  Button,
  Box,
  SpeedDial,
  Backdrop,
  SpeedDialAction,
  Fab,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  FormControlLabel,
  Switch,
  SelectChangeEvent,
  Chip,
  Checkbox,
  ListItemText,
} from "@mui/material";
import ErrorIcon from "@mui/icons-material/Error";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import { useSession } from "next-auth/react";
import { FormInputError } from "../error/FormInputError";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import BackButton from "../../layout/BackButton";
import { useRouter } from "next/router";
import { ModalComponent } from "../modal/ModalComponent";
import ConfirmDialog from "../dialog/ConfirmDialog";
import {
  Analysis_Tool,
  Diagnosis,
  Diagnosis_Type,
  FetchDiagnosisFormDataQuery,
  FetchOneDiagnosisQuery,
  FetchOneTreeQuery,
  Pathogen,
} from "../../../generated/graphql";
import { TreeDiagnosisIcon } from "../../icons/TreeDiagnosisIcon";
import _ from "lodash";
import {
  useMapEditorActions,
  useModalDataOpen,
} from "../../../lib/store/pages/mapEditor";
import {
  TREE_CONDITIONS,
  RECOMMENDATIONS,
  COLLAR_TRUNK_CONDITIONS,
  CROWN_CARPENTERS_CONDITIONS,
  VIGOR_CONDITIONS,
} from "@lib/constants";
import { useDeviceSize } from "@lib/utils";

export interface IDiagnosis extends Diagnosis {
  pathogens: Pathogen[];
  analysis_tools: Analysis_Tool[];
}

export interface DiagnosisFormProps {
  diagnosisFormData: FetchDiagnosisFormDataQuery;
  activeDiagnosis?: FetchOneDiagnosisQuery["diagnosis"][0];
  activeTree?: FetchOneTreeQuery["tree"][0];
  onCreateDiagnosis?(data: IDiagnosis): void;
  onUpdateDiagnosis?(data: IDiagnosis): void;
  onDeleteDiagnosis?(id: string): void;
}

const fields = [
  { type: "date", label: "diagnosis_date", column: "diagnosis_date", size: 6 },
  {
    type: "autocomplete",
    label: "diagnosisType",
    column: "diagnosis_type",
    size: 6,
  },
  {
    type: "textReadOnly",
    label: "dataEntryUsername",
    size: 6,
  },
  {
    type: "select",
    label: "tree_condition",
    column: "tree_condition",
    size: 6,
  },
  { type: "select", label: "tree_vigor", column: "tree_vigor", size: 12 },

  {
    type: "select",
    label: "collar_condition",
    column: "collar_condition",
    size: 6,
  },
  {
    type: "select",
    label: "trunk_condition",
    column: "trunk_condition",
    size: 6,
  },
  {
    type: "select",
    label: "carpenters_condition",
    column: "carpenters_condition",
    size: 6,
  },
  {
    type: "select",
    label: "crown_condition",
    column: "crown_condition",
    size: 6,
  },
  {
    type: "boolean",
    label: "tree_is_dangerous",
    column: "tree_is_dangerous",
    size: 12,
  },
  { type: "multiselect", label: "pathogen", column: "pathogens", size: 12 },

  {
    type: "multiselect",
    label: "analysis_tool",
    column: "analysis_tools",
    size: 12,
  },
  {
    type: "text",
    label: "analysis_results",
    column: "analysis_results",
    size: 12,
  },
  { type: "textArea", label: "note", column: "note", size: 12 },
  {
    type: "select",
    label: "recommendation",
    column: "recommendation",
    size: 6,
  },
];

const selectOptions = {
  tree_condition: TREE_CONDITIONS,
  tree_vigor: VIGOR_CONDITIONS,
  crown_condition: CROWN_CARPENTERS_CONDITIONS,
  trunk_condition: COLLAR_TRUNK_CONDITIONS,
  collar_condition: COLLAR_TRUNK_CONDITIONS,
  carpenters_condition: CROWN_CARPENTERS_CONDITIONS,
  recommendation: RECOMMENDATIONS,
};

const validationSchema = Yup.object({
  diagnosis_date: Yup.string().required(),
  tree_condition: Yup.string().required(),
  crown_condition: Yup.string().nullable(),
  trunk_condition: Yup.string().nullable(),
  collar_condition: Yup.string().nullable(),
  carpenters_condition: Yup.string().nullable(),
  recommendation: Yup.string().required(),
  diagnosis_type: Yup.object({
    id: Yup.string().required(),
  }).required(),
});

const DiagnosisForm: FC<DiagnosisFormProps> = ({
  diagnosisFormData,
  activeDiagnosis,
  activeTree,
  onCreateDiagnosis,
  onUpdateDiagnosis,
  onDeleteDiagnosis,
}) => {
  const { t } = useTranslation(["components", "common"]);
  const session = useSession();
  const router = useRouter();
  const actions = useMapEditorActions();
  const modalDataOpen = useModalDataOpen();
  const [action, setAction] = useState<string>("");

  const [openDataModal, setOpenDataModal] = useState<boolean>(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openDeleteModal, setOpenDeleteModal] = useState<boolean>(false);
  const [showDetailedForm, setShowDetailedForm] = useState<boolean>(false);
  const [selectedPathogen, setSelectedPathogen] = React.useState<Pathogen[]>(
    []
  );
  const [selectedAnalysisTool, setSelectedAnalysisTool] = React.useState<
    Analysis_Tool[]
  >([]);

  const [readOnly, setReadOnly] = useState(true);
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");
  const { isDesktop } = useDeviceSize();
  const now = new Date();

  const currentUsername = session?.data?.user?.name
    ? session?.data?.user?.name
    : "";

  const handleOpen = (event: any) => {
    setOpen(!open);
  };

  Yup.setLocale({
    mixed: {
      required: t("components.errors.required"),
    },
  });

  const {
    handleSubmit,
    control,
    formState: { errors, dirtyFields },
    setValue,

    watch,
  } = useForm<IDiagnosis>({
    resolver: yupResolver(validationSchema),
  });

  const diagnosisType = watch("diagnosis_type");

  const onSubmitDiagnosis: SubmitHandler<IDiagnosis> = async (data) => {
    setReadOnly(true);
    activeDiagnosis
      ? onUpdateDiagnosis && onUpdateDiagnosis(data)
      : onCreateDiagnosis && onCreateDiagnosis(data);

    actions.setModalDataOpen(false);
  };

  const checkIfData = () => {
    if (modalDataOpen === true) {
      setOpenDataModal(true);
    } else {
      router.back();
    }
  };

  const onConfirm: SubmitHandler<IDiagnosis> = async (data) => {
    if (action === "delete") {
      setOpenModal(false);
      setOpenDeleteModal(true);
    } else {
      onSubmitDiagnosis(data);
    }
  };

  const onConfirmDeletionModal: SubmitHandler<IDiagnosis> = async () => {
    const id = activeDiagnosis?.id;
    setOpenDeleteModal(false);
    onDeleteDiagnosis && onDeleteDiagnosis(id);
    activeTree
      ? router.push(`/tree/${activeTree?.id}`)
      : router.push(`/tree/${activeDiagnosis?.tree_id}`);
  };

  const onConfirmModal = () => {
    setOpenDataModal(false);
    actions.setModalDataOpen(false);
    activeTree
      ? router.push(`/tree/${activeTree?.id}`)
      : router.push(`/tree/${activeDiagnosis?.tree_id}`);
  };

  const handleChange = (
    event: SelectChangeEvent<
      typeof selectedPathogen | typeof selectedAnalysisTool
    >,
    type: string
  ) => {
    const {
      target: { value },
    } = event;
    if (type === "pathogen") {
      setSelectedPathogen(value as Pathogen[]);
      setValue("pathogens", value as Pathogen[]);
    } else if (type === "analysis_tool") {
      setSelectedAnalysisTool(value as Analysis_Tool[]);
      setValue("analysis_tools", value as Analysis_Tool[]);
    }
  };

  const handleDelete = (
    event: React.MouseEvent,
    type: string,
    value: Pathogen | Analysis_Tool
  ) => {
    event.preventDefault();

    if (type === "pathogen") {
      setSelectedPathogen((current) => {
        const updatedPathogens = _without(current, value as Pathogen);
        setValue("pathogens", updatedPathogens);
        return updatedPathogens;
      });
    } else if (type === "analysis_tool") {
      setSelectedAnalysisTool((current) => {
        const updatedAnalysisTools = _without(current, value as Analysis_Tool);
        setValue("analysis_tools", updatedAnalysisTools);
        return updatedAnalysisTools;
      });
    }
  };

  // method to check if there is data entried on form before quit
  useEffect(() => {
    if (Object.keys(dirtyFields).length > 0) {
      actions.setModalDataOpen(true);
    } else {
      actions.setModalDataOpen(false);
    }
  }, [Object.keys(dirtyFields).length]);

  useEffect(() => {
    if (!activeDiagnosis) {
      setReadOnly(false);
    } else {
      setReadOnly(true);
      const analysisTools = activeDiagnosis?.analysis_tools.map(
        (o) => o.analysis_tool
      );
      const pathogens = activeDiagnosis?.pathogens.map((o) => o.pathogen);
      setSelectedAnalysisTool(analysisTools);
      setValue("analysis_tools", analysisTools as Analysis_Tool[]);
      setSelectedPathogen(pathogens);
      setValue("pathogens", pathogens as Pathogen[]);
    }
  }, [activeDiagnosis]);

  useEffect(() => {
    if (!activeDiagnosis) {
      const diagnosisSelected = diagnosisFormData?.diagnosis_type.find(
        (diag) => diag.slug === router.query.type
      );
      setValue("diagnosis_type", diagnosisSelected as Diagnosis_Type);
    }
  }, [diagnosisFormData]);

  useEffect(() => {
    if (diagnosisType && diagnosisType?.slug === "detailed_diagnosis") {
      setShowDetailedForm(true);
    } else {
      setShowDetailedForm(false);
    }
  }, [diagnosisType]);

  if (!activeDiagnosis && !router.query.treeId) {
    return null;
  }

  return (
    <>
      <Stack flexDirection="row" justifyContent="space-between" sx={{ mb: 3 }}>
        <BackButton title={t("common.back")} onClick={checkIfData} />
        <Typography
          component="h1"
          variant="h5"
          fontWeight={400}
          textAlign={"center"}
          textTransform={"uppercase"}
        >
          {activeDiagnosis
            ? t(`components.PanelTree.diagnosis`)
            : t("common.addDiagnosis")}
        </Typography>
        <Stack flexDirection="row" gap={1}>
          {readOnly && isDesktop && (
            <Button
              size="large"
              variant="contained"
              color="primary"
              onClick={() => setReadOnly(false)}
            >
              {t("common.edit") as string}
            </Button>
          )}
          {!readOnly && isDesktop && (
            <Button
              size="large"
              variant="contained"
              color="primary"
              onClick={() => {
                setAction("save");
                setMessage(t(`common.messages.save`));
                setOpenModal(true);
              }}
            >
              {t("common.save") as string}
            </Button>
          )}
          {isDesktop && activeDiagnosis ? (
            !readOnly && (
              <Button
                variant="outlined"
                color="error"
                onClick={() => {
                  setAction("delete");
                  setMessage(t("components.DiagnosisForm.message.delete"));
                  setOpenModal(true);
                }}
              >
                {t("common.buttons.delete")}
              </Button>
            )
          ) : (
            <Box
              sx={{
                position: "absolute",
                bottom: 16,
                right: 16,
                pointerEvents: "all",
              }}
            >
              {activeDiagnosis && (
                <SpeedDial
                  open={open}
                  onClick={handleOpen}
                  ariaLabel="SpeedDial"
                  icon={
                    <Fab color="primary" size="medium" aria-label="filter">
                      <MoreVertIcon />
                    </Fab>
                  }
                >
                  <Backdrop open={true} />
                  {readOnly && (
                    <SpeedDialAction
                      tooltipTitle={"Editer"}
                      tooltipOpen
                      icon={<EditIcon color="primary" />}
                      onClick={() => {
                        setReadOnly(false);
                      }}
                    />
                  )}

                  {!readOnly && (
                    <SpeedDialAction
                      tooltipTitle={"Supprimer le diagnostic"}
                      tooltipOpen
                      icon={<DeleteIcon color="error" />}
                      onClick={() => {
                        setMessage(
                          t("components.DiagnosisForm.message.delete")
                        );
                        setOpenModal(true);
                      }}
                    />
                  )}
                </SpeedDial>
              )}
            </Box>
          )}
        </Stack>
      </Stack>

      <Grid container spacing={2}>
        {isDesktop && (
          <Grid item md={2} lg={2} xl={2}>
            <TreeDiagnosisIcon />
          </Grid>
        )}
        <Grid item xs={12} md={10} lg={10} xl={10}>
          <Grid container spacing={2}>
            {fields.map((fieldName) => (
              <Grid
                item
                xs={12}
                sm={fieldName.size}
                lg={fieldName.size}
                xl={fieldName.size}
                key={fieldName.label}
              >
                {fieldName?.type === "select" && (
                  <FormControl fullWidth>
                    <InputLabel
                      required={
                        ["recommendation", "tree_condition"].includes(
                          fieldName?.label
                        ) && true
                      }
                    >
                      {
                        t(
                          `components.Diagnosis.labels.${fieldName.label}`
                        ) as string
                      }
                    </InputLabel>
                    <Controller
                      defaultValue={
                        (activeDiagnosis &&
                          activeDiagnosis?.[fieldName.column!]) ||
                        ""
                      }
                      name={`${fieldName.column as keyof Diagnosis}`}
                      control={control}
                      render={({ field: { onChange, value } }) => (
                        <Select
                          value={value}
                          onChange={onChange}
                          disabled={readOnly}
                          label={
                            t(
                              `components.Diagnosis.labels.${fieldName.label}`
                            ) as string
                          }
                        >
                          {selectOptions[fieldName?.label]?.map(
                            (option: string, i: React.Key) => {
                              return (
                                <MenuItem key={i} value={option}>
                                  {
                                    t(
                                      `components.Diagnosis.${
                                        fieldName?.label === "recommendation"
                                          ? "recommendation"
                                          : "conditions"
                                      }.${option}`
                                    ) as string
                                  }
                                </MenuItem>
                              );
                            }
                          )}
                        </Select>
                      )}
                    />
                    <FormInputError
                      name={fieldName.column || ""}
                      errors={errors}
                    />
                  </FormControl>
                )}

                {fieldName?.label === "pathogen" && (
                  <FormControl fullWidth>
                    <InputLabel>
                      {
                        t(
                          `components.Diagnosis.labels.${fieldName.label}`
                        ) as string
                      }
                    </InputLabel>
                    <Controller
                      name={`pathogens`}
                      control={control}
                      render={() => (
                        <Select
                          value={selectedPathogen}
                          onChange={(event) =>
                            handleChange(event, fieldName?.label)
                          }
                          disabled={readOnly ? true : false}
                          multiple
                          renderValue={(selectedPathogen) => (
                            <Box sx={{ display: "flex", flexWrap: "wrap" }}>
                              {selectedPathogen.map((option) => (
                                <Chip
                                  key={option.id}
                                  label={t(
                                    `components.Diagnosis.pathogen.${option.slug}`
                                  )}
                                  disabled={readOnly}
                                  clickable
                                  deleteIcon={
                                    <CancelIcon
                                      onMouseDown={(event) =>
                                        event.stopPropagation()
                                      }
                                    />
                                  }
                                  onDelete={(e) =>
                                    handleDelete(e, fieldName?.label, option)
                                  }
                                />
                              ))}
                            </Box>
                          )}
                          label={
                            t(
                              `components.Diagnosis.${fieldName.label}`
                            ) as string
                          }
                        >
                          {diagnosisFormData[fieldName?.label]?.map(
                            (option: any) => (
                              <MenuItem
                                key={option.id}
                                value={option}
                                selected={selectedPathogen.includes(option)}
                              >
                                <ListItemText
                                  primary={t(
                                    `components.Diagnosis.pathogen.${option.slug}`
                                  )}
                                />
                              </MenuItem>
                            )
                          )}
                        </Select>
                      )}
                    />
                    <FormInputError
                      name={fieldName.column || ""}
                      errors={errors}
                    />
                  </FormControl>
                )}

                {fieldName?.label === "analysis_tool" && showDetailedForm && (
                  <FormControl fullWidth>
                    <InputLabel>
                      {
                        t(
                          `components.Diagnosis.labels.${fieldName.label}`
                        ) as string
                      }
                    </InputLabel>

                    <Controller
                      name={`analysis_tools`}
                      control={control}
                      render={() => (
                        <Select
                          value={selectedAnalysisTool}
                          onChange={(event) =>
                            handleChange(event, fieldName?.label)
                          }
                          renderValue={(selectedAnalysisTool) => (
                            <Box sx={{ display: "flex", flexWrap: "wrap" }}>
                              {selectedAnalysisTool.map((option) => (
                                <Chip
                                  key={option.id}
                                  label={t(
                                    `components.Diagnosis.analysis_tool.${option.slug}`
                                  )}
                                  disabled={readOnly}
                                  clickable
                                  deleteIcon={
                                    <CancelIcon
                                      onMouseDown={(event) =>
                                        event.stopPropagation()
                                      }
                                    />
                                  }
                                  onDelete={(e) =>
                                    handleDelete(e, fieldName?.label, option)
                                  }
                                />
                              ))}
                            </Box>
                          )}
                          disabled={readOnly ? true : false}
                          multiple
                          label={
                            t(
                              `components.Diagnosis.labels.${fieldName.label}`
                            ) as string
                          }
                        >
                          {diagnosisFormData[fieldName?.label]?.map(
                            (option: any) => (
                              <MenuItem
                                key={option.id}
                                value={option}
                                selected={selectedAnalysisTool.includes(option)}
                              >
                                <ListItemText
                                  primary={t(
                                    `components.Diagnosis.analysis_tool.${option.slug}`
                                  )}
                                />
                              </MenuItem>
                            )
                          )}
                        </Select>
                      )}
                    />
                    <FormInputError
                      name={fieldName.column || ""}
                      errors={errors}
                    />
                  </FormControl>
                )}
                {fieldName?.label === "analysis_results" &&
                  showDetailedForm && (
                    <Controller
                      name={`${fieldName?.column as keyof Diagnosis}`}
                      control={control}
                      defaultValue={activeDiagnosis?.[fieldName.column!] || ""}
                      render={({ field: { onChange, ref, value } }) => (
                        <TextField
                          value={value}
                          inputRef={ref}
                          fullWidth
                          disabled={readOnly}
                          sx={{
                            display: "flex",
                          }}
                          multiline
                          label={
                            t(
                              `components.Diagnosis.labels.${fieldName?.label}`
                            ) as string
                          }
                          variant="filled"
                          placeholder={t(
                            `components.Diagnosis.labels.${fieldName?.label}`
                          )}
                          error={!!errors[fieldName?.label]}
                          helperText={errors[fieldName?.label]?.message}
                          onChange={onChange}
                          type={fieldName?.type}
                        />
                      )}
                    />
                  )}

                {fieldName?.label === "diagnosisType" && (
                  <>
                    <Controller
                      defaultValue={activeDiagnosis?.[fieldName.column!] || ""}
                      name={`${fieldName.column as keyof Diagnosis}`}
                      rules={{ required: true }}
                      control={control}
                      render={({ field: { onChange, value } }) => {
                        value = value ?? null;
                        return (
                          <Autocomplete
                            id={`autocomplete-${name}`}
                            options={diagnosisFormData?.diagnosis_type}
                            value={value}
                            renderInput={(
                              params: AutocompleteRenderInputParams
                            ) => (
                              <TextField
                                {...params}
                                required
                                label={
                                  t(
                                    `components.Diagnosis.labels.${fieldName?.label}`
                                  ) as string
                                }
                                variant="filled"
                                InputLabelProps={{ shrink: true }}
                              />
                            )}
                            getOptionLabel={(option) => {
                              if (option.slug !== undefined) {
                                return t(
                                  `components.PanelTree.interventionsList.${option.slug}`
                                );
                              } else {
                                return "";
                              }
                            }}
                            onChange={(e, newValue) => {
                              onChange(newValue);
                            }}
                            disabled={readOnly ? true : false}
                          />
                        );
                      }}
                    />
                    <FormInputError name={fieldName.column!} errors={errors} />
                  </>
                )}
                {fieldName?.type === "boolean" && (
                  <Controller
                    defaultValue={activeDiagnosis?.[fieldName.column!] || false}
                    name={`${fieldName.column as keyof Diagnosis}`}
                    control={control}
                    render={({ field: { onChange, ref, value } }) => (
                      <FormControlLabel
                        label={
                          <div
                            style={{ display: "flex", alignItems: "center" }}
                          >
                            <ErrorIcon color="error" sx={{ mr: 1 }} />
                            <span>
                              {
                                t(
                                  `components.Diagnosis.labels.${fieldName?.label}`
                                ) as string
                              }
                            </span>
                          </div>
                        }
                        control={
                          <Switch
                            disabled={readOnly}
                            ref={ref}
                            checked={value as boolean}
                            color="primary"
                            onChange={(e) => onChange(e.target.checked)}
                          />
                        }
                        labelPlacement="end"
                      />
                    )}
                  />
                )}
                {fieldName?.type === "textReadOnly" && (
                  <TextField
                    defaultValue={
                      activeDiagnosis?.[fieldName.column!] || currentUsername
                    }
                    fullWidth
                    disabled={true}
                    sx={{
                      display: "flex",
                    }}
                    label={
                      t(
                        `components.Intervention.properties.${fieldName?.label}`
                      ) as string
                    }
                    variant="filled"
                    placeholder={t(
                      `components.Intervention.placeholder.${fieldName?.label}`
                    )}
                    error={!!errors[fieldName?.label]}
                    helperText={errors[fieldName?.label]?.message}
                    type={fieldName?.type}
                  />
                )}
                {(fieldName?.type === "textArea" ||
                  fieldName?.type === "number") && (
                  <Controller
                    name={`${fieldName?.column as keyof Diagnosis}`}
                    control={control}
                    defaultValue={activeDiagnosis?.[fieldName.column!] || ""}
                    render={({ field: { onChange, ref, value } }) => (
                      <TextField
                        value={value}
                        inputRef={ref}
                        fullWidth
                        disabled={readOnly}
                        sx={{
                          display: "flex",
                        }}
                        multiline={
                          fieldName?.type === "textArea" ? true : false
                        }
                        label={
                          t(
                            `components.Intervention.properties.${fieldName?.label}`
                          ) as string
                        }
                        variant="filled"
                        placeholder={t(
                          `components.Intervention.placeholder.${fieldName?.label}`
                        )}
                        error={!!errors[fieldName?.label]}
                        helperText={errors[fieldName?.label]?.message}
                        onChange={onChange}
                        type={fieldName?.type}
                        inputProps={
                          fieldName?.type === "number" ? { min: 0 } : {}
                        }
                      />
                    )}
                  />
                )}
                {fieldName.type === "date" && (
                  <>
                    <Controller
                      name={`${fieldName?.column as keyof Diagnosis}`}
                      control={control}
                      defaultValue={
                        activeDiagnosis?.[fieldName.column!] ||
                        now.toISOString().split("T")[0]
                      }
                      render={({ field: { onChange, value } }) => {
                        return (
                          <TextField
                            required
                            label={
                              t(
                                `components.Diagnosis.labels.${fieldName?.label}`
                              ) as string
                            }
                            variant="filled"
                            type="date"
                            disabled={readOnly}
                            sx={{ width: "100%" }}
                            inputProps={{
                              max: now.toISOString().split("T")[0],
                            }}
                            InputLabelProps={{
                              shrink: true,
                            }}
                            value={value}
                            onChange={(e) => onChange(e.target.value)}
                          />
                        );
                      }}
                    />
                    <FormInputError name={fieldName.column!} errors={errors} />
                  </>
                )}
              </Grid>
            ))}
            <Grid item xs={12} sm={6} lg={6} xl={6}>
              <TextField
                value={
                  (activeTree
                    ? activeTree.location?.address
                    : activeDiagnosis?.tree?.location?.address) ||
                  activeDiagnosis?.tree?.serial_number ||
                  ""
                }
                fullWidth
                disabled={true}
                sx={{
                  display: "flex",
                }}
                label={t(`components.Diagnosis.labels.linkedTree`) as string}
                variant="filled"
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      {!readOnly && !isDesktop && (
        <Button
          size="large"
          variant="contained"
          color="primary"
          onClick={() => {
            setAction("save");
            setMessage(t(`common.messages.save`));
            setOpenModal(true);
          }}
          fullWidth
          sx={{ mt: 1 }}
        >
          {t("common.save") as string}
        </Button>
      )}

      <Box>
        <ModalComponent
          open={openModal}
          handleClose={() => setOpenModal(false)}
        >
          <ConfirmDialog
            message={message}
            onConfirm={handleSubmit(onConfirm)}
            onAbort={() => setOpenModal(false)}
          />
        </ModalComponent>

        <ModalComponent
          open={openDeleteModal}
          handleClose={() => setOpenDeleteModal(false)}
        >
          <ConfirmDialog
            message={t("common.messages.lostData")}
            onConfirm={onConfirmDeletionModal}
            onAbort={() => setOpenDeleteModal(false)}
            showConfirmButton={true}
          />
        </ModalComponent>
      </Box>

      {modalDataOpen && (
        <Box>
          <ModalComponent
            open={openDataModal}
            handleClose={() => setOpenDataModal(false)}
          >
            <ConfirmDialog
              title={t("common.quitForm")}
              message={t("common.messages.lostData")}
              onConfirm={onConfirmModal}
              onAbort={() => setOpenDataModal(false)}
            />
          </ModalComponent>
        </Box>
      )}
    </>
  );
};

export default DiagnosisForm;
