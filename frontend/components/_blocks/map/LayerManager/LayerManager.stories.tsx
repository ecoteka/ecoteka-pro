import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";
import LayerManager from "@blocks/map/LayerManager";

const meta = {
  title: "Blocks/Map/LayerManager",
  component: LayerManager,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Layer manager",
      },
    },
  },
} satisfies Meta<typeof LayerManager>;

export default meta;
type Story = StoryObj<typeof LayerManager>;

export const LayerManagerIsOpen: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <LayerManager isOpen={true} />
    </Box>
  ),
};

export const LayerManagerDefaults: Story = {
  render: () => (
    <Box
      sx={{
        with: "100px",
        height: "300px",
      }}
    >
      <LayerManager isOpen={false} />
    </Box>
  ),
};

export const LayerManagerWithControls: Story = {
  render: () => (
    <Box
      sx={{
        with: "100px",
        height: "300px",
      }}
    >
      <LayerManager isOpen={false} showControls enableControls />
    </Box>
  ),
};
