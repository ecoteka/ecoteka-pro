import * as React from "react";
import { styled } from "@mui/material/styles";
// import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import MuiAccordion, { AccordionProps } from "@mui/material/Accordion";
// import MuiAccordionSummary, {
//   AccordionSummaryProps,
// } from "@mui/material/AccordionSummary";
// import MuiAccordionDetails, {
//   AccordionDetailsProps,
// } from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { useTranslation } from "react-i18next";
import { Circle } from "@core/icons";
import List from "@mui/material/List";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import ListItem from "@mui/material/ListItem";
import Switch from "@mui/material/Switch";
import OpacityIcon from "@mui/icons-material/Brightness6";
import { ILayerConfig, LayerManagerProps } from "./LayerManager.types";

const Accordion = styled((props: AccordionProps) => (
  <MuiAccordion {...props} />
))(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === "dark"
      ? "rgb(0 0 0 / 80%)"
      : "rgb(255 255 255 / 80%)",
}));

// const AccordionSummary = styled((props: AccordionSummaryProps) => (
//   <MuiAccordionSummary {...props} />
// ))(({ theme }) => ({
//   backgroundColor:
//     theme.palette.mode === "dark"
//       ? "rgba(255, 255, 255, .05)"
//       : "rgba(0, 0, 0, .03)",
// }));

// const AccordionDetails = styled((props: AccordionDetailsProps) => (
//   <MuiAccordionDetails {...props} />
// ))(({ theme }) => ({
//   backgroundColor:
//     theme.palette.mode === "dark"
//       ? "rgb(0 0 0 / 0.50%)"
//       : "rgb(255 255 255 / 0.50%))",
// }));

const layersDefaults: ILayerConfig[] = [
  {
    id: "locations",
    label: "LayerManager.layers.Locations",
    legendItems: [
      {
        label: "LayerManager.layers.LocationsItems.Tree",
        symbol: <Circle fill="#06B6AE" stroke="grey" />,
      },
      {
        label: "LayerManager.layers.LocationsItems.Empty",
        symbol: <Circle fill="white" stroke="grey" />,
      },
      {
        label: "LayerManager.layers.LocationsItems.Stump",
        symbol: <Circle fill="#966133" stroke="grey" />,
      },
      {
        label: "LayerManager.layers.LocationsItems.Position",
        symbol: <Circle fill="#FFF350" stroke="#FEFBDB" />,
      },
    ],
  },
];

const LayerManager = ({
  isOpen = false,
  layers = layersDefaults,
  showControls = false,
  enableControls = false,
}: LayerManagerProps) => {
  const { t } = useTranslation(["components"]);
  const [expanded, setExpanded] = React.useState<boolean>(isOpen);
  const [checked, setChecked] = React.useState<string[]>([]);

  const formatLabel = (value: string): string => {
    const localesKeyRegex = /[.:]/;
    return localesKeyRegex.test(value) ? t(value) : value;
  };

  const handleToggle = (value: string) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const handleAccordionChange =
    () => (event: React.SyntheticEvent, isExpanded: boolean) => {
      setExpanded(isExpanded);
    };

  return (
    <div data-cy="layer-manager">
      <Accordion
        expanded={expanded}
        onChange={handleAccordionChange()}
        disableGutters
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="layer-manager-content"
          id="layer-manager-header"
          data-cy="layer-manager-header"
        >
          <Typography
            sx={{
              width: "33%",
              flexShrink: 0,
              color: "text.secondary",
              fontWeight: "bold",
            }}
          >
            {t("blocks.LayerManager.title")}
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <List
            component="nav"
            aria-labelledby="nested-list-subheader"
            disablePadding
            data-cy="layer-manager-list"
          >
            {layers.map((layer, i) => {
              return (
                <div key={i}>
                  <ListItem key={i} data-cy="layer-manager-list-item">
                    {showControls && (
                      <Switch
                        edge="start"
                        disabled={!enableControls}
                        onChange={handleToggle(layer.label)}
                        checked={checked.indexOf(layer.label) !== -1}
                        inputProps={{
                          "aria-labelledby": `switch-layer-label-${layer.label}`,
                        }}
                      />
                    )}
                    <ListItemText
                      id={`switch-layer-label-wifi-${
                        formatLabel(layer.label).toLowerCase
                      }`}
                      primary={formatLabel(layer.label)}
                    />
                    {showControls && (
                      <ListItemIcon sx={{ minWidth: "36px" }}>
                        <OpacityIcon />
                      </ListItemIcon>
                    )}
                  </ListItem>
                  <List
                    dense
                    component="div"
                    disablePadding
                    data-cy="layer-manager-locations-list"
                  >
                    {layer.legendItems.map((item, i) => {
                      return (
                        <ListItem
                          key={i}
                          sx={{ pl: 4 }}
                          data-cy="layer-manager-locations-item"
                        >
                          <ListItemIcon sx={{ minWidth: "36px" }}>
                            {item.symbol}
                          </ListItemIcon>
                          <ListItemText primary={formatLabel(item.label)} />
                        </ListItem>
                      );
                    })}
                  </List>
                </div>
              );
            })}
          </List>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

export default LayerManager;
