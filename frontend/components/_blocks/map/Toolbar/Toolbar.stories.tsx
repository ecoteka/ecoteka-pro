import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Toolbar from "@components/_blocks/map/Toolbar";

const meta = {
  title: "Blocks/Map/Toolbar",
  component: Toolbar,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Analytics dendro section",
      },
    },
  },
} satisfies Meta<typeof Toolbar>;

export default meta;
type Story = StoryObj<typeof Toolbar>;

export const ToolbarComponent: Story = {
  render: () => (
    <Toolbar handleOnAddLocation={() => console.log("handleOnAddLocation")} />
  ),
};
