import { FC, useState } from "react";
import { useTranslation } from "react-i18next";
import { IconButton, Menu, Stack, Tooltip } from "@mui/material";
import TreeToolbarIcon from "@components/_core/icons/Tree/TreeToolbarIcon";
import BackupIcon from "@mui/icons-material/Backup";
import { AddLocationIcon } from "@components/_core/icons/Location";
import AddLocationButton from "@components/map/buttons/AddLocationButton";
import {
  LocationType,
  MapBackground,
  useMapEditorActions,
} from "@stores/pages/mapEditor";
import { useRouter } from "next/router";
import BaseMapSelector from "@components/map/BaseMapSelector";
import { useDeviceSize } from "@lib/utils";
import { AddPolygonIcon } from "@components/_core/icons/Polygon";

interface IToolbarProps {
  handleOnAddLocation(type: LocationType): void;
  map?: MapBackground;
  handleOnBaseMapChange?(map: MapBackground): void;
}

const Toolbar: FC<IToolbarProps> = ({
  handleOnAddLocation,
  map,
  handleOnBaseMapChange,
}) => {
  const { t } = useTranslation(["components"]);
  const [anchorEl, setAnchorEl] = useState(null);
  const { isTablet } = useDeviceSize();
  const { setMapActiveAction, setMapPanelOpen } = useMapEditorActions();
  const router = useRouter();
  const handleClick = (e) => {
    setAnchorEl(e.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOnAddWorksiteGroup = () => {
    setMapActiveAction("addWorksiteGroup");
    setMapPanelOpen(true);
  };

  const handleOnAddPolygon = () => {
    setMapActiveAction("addPolygon");
    setMapPanelOpen(true);
  };

  const menuItems = [
    {
      status: "alive",
    },
    {
      status: "empty",
    },
    {
      status: "stump",
    },
  ];
  const tools = [
    {
      icon: (
        <BaseMapSelector
          map={map}
          handleOnBaseMapChange={handleOnBaseMapChange}
        />
      ),
      tooltip: t("components.Map.Toolbar.layer"),
    },
    {
      icon: <AddLocationIcon />,
      tooltip: t("components.Map.Toolbar.add"),
      onClick: handleClick,
    },
    {
      icon: <AddPolygonIcon />,
      tooltip: t("components.Map.Toolbar.polygon"),
      onClick: handleOnAddPolygon,
    },
    {
      icon: <TreeToolbarIcon />,
      tooltip: t("components.Map.Toolbar.worksite"),
      onClick: handleOnAddWorksiteGroup,
    },
    {
      icon: <BackupIcon sx={{ color: "#7e8989" }} />,
      tooltip: t("components.Map.Toolbar.import"),
      onClick: () => router.push("/import/add"),
    },
  ];

  const toolsInMenu = isTablet ? tools.slice(0, -1) : tools;

  return (
    <Stack
      direction={"row"}
      sx={styles.stack}
      gap={2}
      data-cy="toolbar-container"
    >
      {toolsInMenu.map((tool, index) => (
        <Tooltip title={tool.tooltip} data-cy={`toolbar-item-${index}`}>
          <IconButton
            onClick={(e) => (tool.onClick && tool.onClick(e)) || undefined}
            key={index}
          >
            {tool.icon}
          </IconButton>
        </Tooltip>
      ))}
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        data-cy="toolbar-menu-add-locations"
      >
        {menuItems.map((item) => (
          <AddLocationButton
            key={item.status}
            handleOnAddLocation={handleOnAddLocation}
            locationStatus={item.status as LocationType}
          />
        ))}
        {/*} <MenuItem
          onClick={handleOnAddGreenArea}
          sx={{
            zIndex: 4,
            bgcolor: "#ffff",
            "&:hover": {
              backgroundColor: "#EEEEEE",
            },
            borderRadius: "50px 0 0 50px",
            width: "auto",
            justifyContent: "flex-start",
            color: "primary",
          }}
        >
          <SvgIcon>
            <AddGreenAreaIcon fillColor={"black"} fillOpacity="0.54" />
          </SvgIcon>
          <Typography color="fff">
            {t("common.addGreenArea") as string}
          </Typography>
        </MenuItem>*/}
      </Menu>
    </Stack>
  );
};

export default Toolbar;

const styles = {
  stack: {
    backgroundColor: "#fff",
    borderTopLeftRadius: "6px",
    borderTopRightRadius: "6px",
    padding: 1,
  },
};
