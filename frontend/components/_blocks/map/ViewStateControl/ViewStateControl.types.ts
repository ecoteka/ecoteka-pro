export type ViewStateControlMode = "zoom" | "bearing";

export interface ViewControlOwnProps {
  mode: ViewStateControlMode;
  initialValue?: number;
  value: number;
  incrementValue?: number;
  decrementValue?: number;
  setValueCallback?: (value: number) => void | undefined;
  maxValue?: number;
  minValue?: number;
}
