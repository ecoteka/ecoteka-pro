import React, { useState } from "react";
import { ButtonGroup, Button } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import Plus from "@mui/icons-material/Add";
import Minus from "@mui/icons-material/Remove";
import ArrowDown from "@mui/icons-material/ArrowDropDown";
import ArrowUp from "@mui/icons-material/ArrowDropUp";
import { ViewControlOwnProps } from "./ViewStateControl.types";

const defaultParams = {
  zoom: {
    maxValue: 17,
    minValue: 5,
    incrementValue: 1,
    decrementValue: 1,
  },
  bearing: {
    maxValue: 45,
    minValue: 0,
    incrementValue: 3,
    decrementValue: 3,
  },
};

const ViewStateControl = ({
  maxValue,
  minValue,
  value,
  setValueCallback,
  incrementValue,
  decrementValue,
  mode,
}: ViewControlOwnProps) => {
  const [currentValue, setCurrentValue] = useState(value);
  const theme = useTheme();

  const isEventDisabled = (
    event: "increment" | "decrement"
  ): boolean | undefined => {
    if (event == "increment") {
      return maxValue
        ? currentValue >= maxValue
        : currentValue >= defaultParams[mode].maxValue;
    }
    if (event == "decrement") {
      return minValue
        ? currentValue <= minValue
        : currentValue <= defaultParams[mode].minValue;
    }
  };

  const handleIncrement = () => {
    const newValue = incrementValue
      ? currentValue + incrementValue
      : currentValue + defaultParams[mode].incrementValue;
    setCurrentValue(newValue);
    if (setValueCallback) setValueCallback(newValue);
  };

  const handleDecrement = () => {
    const newValue = decrementValue
      ? currentValue - decrementValue
      : currentValue - defaultParams[mode].decrementValue;
    setCurrentValue(newValue);
    if (setValueCallback) setValueCallback(newValue);
  };

  return (
    <ButtonGroup
      orientation="vertical"
      aria-label="map zoom button group"
      variant="outlined"
      size="small"
    >
      <Button
        variant="contained"
        onClick={() => handleIncrement()}
        sx={{
          mx: 0,
          px: 0,
          minWidth: 0,
          maxWidth: "36px",
          borderRadius: "25%",
        }}
        style={{ backgroundColor: theme.palette.background.default }}
        disabled={isEventDisabled("increment")}
      >
        {mode == "zoom" ? (
          <Plus color={isEventDisabled("increment") ? "disabled" : "primary"} />
        ) : (
          <ArrowUp
            color={isEventDisabled("increment") ? "disabled" : "primary"}
          />
        )}
      </Button>
      <Button
        variant="contained"
        disabled={isEventDisabled("decrement")}
        onClick={() => handleDecrement()}
        sx={{
          mx: 0,
          px: 0,
          minWidth: 0,
          maxWidth: "36px",
          borderRadius: "33%",
        }}
        style={{ backgroundColor: theme.palette.background.default }}
      >
        {mode == "zoom" ? (
          <Minus
            color={isEventDisabled("decrement") ? "disabled" : "primary"}
          />
        ) : (
          <ArrowDown
            color={isEventDisabled("decrement") ? "disabled" : "primary"}
          />
        )}
      </Button>
    </ButtonGroup>
  );
};

export default ViewStateControl;
