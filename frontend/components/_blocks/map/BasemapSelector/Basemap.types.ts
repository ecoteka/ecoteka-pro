export enum MapBackground {
  Map = "map",
  Satellite = "satellite",
  Ign = "ign",
}

export interface BaseMapSelectorProps {
  map?: MapBackground;
  handleOnBaseMapChange(map: MapBackground): void;
  basemapOptions?: MapBackground[];
}

export interface BaseMapPopoverProps {
  handleOnBaseMapChange(map: MapBackground): void;
  visible?: boolean;
  mapBackground: MapBackground;
  setVisible(visible: boolean): void;
  isTablet?: boolean;
  basemapOptions?: MapBackground[];
}
