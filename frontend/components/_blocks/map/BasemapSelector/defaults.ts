import { MapBackground } from "./Basemap.types";

export const defaultMapBackgrounds: MapBackground[] = [
  MapBackground.Map,
  MapBackground.Satellite,
  MapBackground.Ign,
];
