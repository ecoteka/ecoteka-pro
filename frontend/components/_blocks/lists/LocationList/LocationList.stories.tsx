import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import LocationListItem from "./ListItem";

const meta = {
  title: "Blocks/Lists/LocationListItem",
  component: LocationListItem,
  parameters: {
    docs: {
      description: {
        component: "LocationListItem",
      },
    },
  },
} satisfies Meta<typeof LocationListItem>;

export default meta;
type Story = StoryObj<typeof LocationListItem>;

const defaultData = {
  interventionId: "1233",
  locationId: "1335",
  treeId: "1235",
  active: true,
  isCompleted: true,
  showWarning: true,
  address: "rue Sainte, Marseillle",
  tree_species: "Quercus ilex",
};

export const Default: Story = {
  render: () => (
    <LocationListItem
      {...defaultData}
      onCheckboxChange={() => console.log("on change")}
    />
  ),
};

// export const WithWarning: Story = {
//   render: () => <LocationListItem />,
// };
