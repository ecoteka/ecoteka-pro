import {
  ListItemIcon,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Stack,
  Typography,
  Link,
  Checkbox,
} from "@mui/material";
import ListItem, { ListItemProps } from "@mui/material/ListItem";
import CheckIcon from "@mui/icons-material/Check";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ClearIcon from "@mui/icons-material/ClearOutlined";
import { green } from "@mui/material/colors";
import ErrorIcon from "@mui/icons-material/Error";
import NextLink from "next/link";
import router, { useRouter } from "next/router";

export interface IListLocationItemProps {
  interventionId: string;
  locationId: string;
  treeId?: string;
  active?: boolean;
  isCompleted?: boolean;
  showWarning?: boolean;
  address?: string | null;
  tree_species?: string | null;
  onCheckboxChange(interventionId: string): void;
}

const ListLocationItem = ({
  interventionId,
  locationId,
  treeId,
  active = false,
  isCompleted = false,
  showWarning = false,
  address,
  tree_species,
  onCheckboxChange,
  ...props
}: IListLocationItemProps & ListItemProps) => {
  const { query } = useRouter();
  const isReadOnly = () => !query.hasOwnProperty("edit");
  const getLocationURI = () => {
    return treeId
      ? `/tree/${treeId}?interventionModal=open&activeId=${interventionId}`
      : `/location/${locationId}?interventionModal=open&activeId=${interventionId}`;
  };
  return (
    <ListItem
      sx={{
        padding: "0 1.5rem",
        background: isCompleted ? green[100] : "none",
      }}
    >
      <ListItemIcon>
        {isCompleted && <CheckIcon sx={{ color: green[400] }} />}
        {/* : (
          <Checkbox
            disabled={isReadOnly()}
            onChange={() => onCheckboxChange(interventionId)}
          />
        ) */}
      </ListItemIcon>
      <ListItemText
        primary={
          <Stack
            direction="row"
            alignItems="start"
            spacing={1}
            sx={{ width: "100%" }}
          >
            {showWarning && <ErrorIcon fontSize="small" color="error" />}
            <Typography component="span" variant="body2" color="text.primary">
              {address}
            </Typography>
          </Stack>
        }
        secondary={tree_species}
      />
      <ListItemSecondaryAction>
        <IconButton edge="end" onClick={() => router.push(getLocationURI())}>
          <ChevronRightIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
};
export default ListLocationItem;
