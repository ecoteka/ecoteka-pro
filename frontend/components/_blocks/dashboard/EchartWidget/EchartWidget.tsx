import ReactECharts from "echarts-for-react";
import { EChartsOption } from "echarts-for-react/lib/types";
import { Alert } from "@mui/material";

export interface IEchartWidgetProps {
  config: EChartsOption;
  loading?: boolean;
  error: boolean;
  customStyle?: {};
}

const EchartWidget = ({ config, error, customStyle }: IEchartWidgetProps) => {
  if (error) {
    return <Alert severity="info">Aucune donnée</Alert>;
  }

  if (config) {
    return (
      <ReactECharts option={config} style={customStyle ? customStyle : {}} />
    );
  } else {
    return <Alert severity="error">Erreur inattendue!</Alert>;
  }
};
export default EchartWidget;
