import { Paper, Typography, Box } from "@mui/material";
import { ISpeciesDistributionProps } from "./SpeciesDistribution.types";
import { useTranslation } from "react-i18next";
import _ from "lodash";
import DashboardModuleHeader from "@components/_core/headers/DashboardModuleHeader";
import EchartWidget from "../EchartWidget";

const SpeciesDistribution = ({ data }: ISpeciesDistributionProps) => {
  const { t } = useTranslation(["components"]);

  const chartData =
    data &&
    data.map((genus) => ({
      name: genus.name,
      color: genus.color,
      size: genus.size,
      percentage: genus.percentage,
      children: genus.children ? genus.children : [],
      value: [genus.size, genus.children ? genus.children?.length : 0],
    }));

  const option = {
    title: {
      show: false,
    },
    tooltip: {
      formatter: (params) => {
        if (params.data.children) {
          const children = params.data.children;
          const childrenList = children
            .map((c) => `${c.count} ${c.name}`)
            .join("<br/>");
          return `
          ${params.data.percentage}% ${params.name}<br>
          ${childrenList}
        `;
        }
      },
    },
    xAxis: {
      show: false,
    },
    yAxis: {
      show: false,
    },
    series: [
      {
        data: chartData,
        type: "scatter",
        symbol: "circle",
        symbolSize: (data) => Math.sqrt(data[0]) * 8,
        itemStyle: {
          color: (params) => params.data.color,
        },
        label: {
          show: true,
          formatter: (params) =>
            params.data.size > 60 ? params.data.name : "",
          color: "#fff",
          fontSize: 13,
        },
      },
    ],
  };
  return (
    <>
      <DashboardModuleHeader title={t("blocks.SpeciesDistribution.title")} />
      <Paper elevation={1} data-cy="dashboard-species-paper">
        <Box sx={{ p: 3 }} data-cy="dashboard-species-box">
          <Typography
            data-cy="dashboard-species-subtitle"
            sx={{
              wordBreak: "break-word",
              mt: 1,
              fontWeight: 400,
              color: "#7E8989",
            }}
            fontSize="14px"
            variant="h6"
            textAlign="center"
          >{`${t("blocks.SpeciesDistribution.subtitle")}: ${
            chartData ? chartData.length : 0
          }`}</Typography>
          <Box
            sx={{ with: "100px", height: "300px" }}
            data-cy="dashboard-species-bar-box"
          >
            {data && <EchartWidget config={option} error={false} />}
          </Box>
        </Box>
      </Paper>
    </>
  );
};

export default SpeciesDistribution;
