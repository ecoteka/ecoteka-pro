import { IconButton, Paper, Stack, Tooltip, Typography } from "@mui/material";
import DashboardModuleHeader from "@components/_core/headers/DashboardModuleHeader";
import { useTranslation } from "react-i18next";
import InfoIcon from "@mui/icons-material/Info";

export interface IHealthProps {
  data: any;
}

const Health = ({ data }: IHealthProps) => {
  const { t } = useTranslation(["components"]);
  return (
    <>
      <DashboardModuleHeader title={t("blocks.Health.title")} />
      <Paper elevation={1} data-cy="dashboard-health-paper">
        <Stack
          direction={"row"}
          justifyContent={"center"}
          p={2}
          flexWrap={"wrap"}
          data-cy="dashboard-health-stack"
          style={{ height: "350px" }}
        >
          {data.map((block, i) => (
            <Stack
              key={i}
              flexDirection="column"
              data-cy={`dashboard-health-stack-${i}`}
            >
              <Typography
                variant="h6"
                data-cy={`dashboard-health-title-${i}`}
                textAlign={"center"}
              >
                {block.title}
                {block.title ===
                  t("blocks.Health.analyticsModules.wellBeingScore") && (
                  <Tooltip
                    title={t("blocks.Health.description")}
                    placement="right"
                  >
                    <IconButton aria-label="info">
                      <InfoIcon fontSize="medium" />
                    </IconButton>
                  </Tooltip>
                )}
              </Typography>

              <Stack
                flexDirection="column"
                alignItems="center"
                data-cy={`dashboard-health-component-${i}`}
              >
                {block.component}
                <Typography
                  variant="caption"
                  fontSize={16}
                  color="#89868D"
                  data-cy={`dashboard-health-value-${i}`}
                >
                  {block.value}
                  {block.unit}
                </Typography>
              </Stack>
            </Stack>
          ))}
        </Stack>
      </Paper>
    </>
  );
};
export default Health;
