import { Box, Paper, Stack, Typography } from "@mui/material";
import DashboardModuleHeader from "@components/_core/headers/DashboardModuleHeader";
import { useTranslation } from "react-i18next";

export interface IResilienceProps {
  data: any;
}

const Resilience = ({ data }: IResilienceProps) => {
  const { t } = useTranslation(["components"]);
  return (
    <Box sx={{ textAlign: "center" }}>
      <DashboardModuleHeader title={t("blocks.Resilience.title")} />
      <Paper elevation={1} data-cy="dashboard-resilience-paper">
        <Stack
          direction={"row"}
          justifyContent={"center"}
          p={2}
          flexWrap={"wrap"}
          data-cy="dashboard-resilience-stack"
          style={{ height: "350px" }}
        >
          {data.map((block, i) => (
            <Stack
              key={i}
              flexDirection="column"
              data-cy={`dashboard-resilience-stack-${i}`}
            >
              <Typography
                variant="h6"
                data-cy={`dashboard-resilience-title-${i}`}
              >
                {block.title}
              </Typography>
              <Stack
                flexDirection="column"
                alignItems="center"
                data-cy={`dashboard-resilience-component-${i}`}
              >
                {block.component}
                <Typography
                  variant="caption"
                  fontSize={16}
                  color="#89868D"
                  data-cy={`dashboard-resilience-value-${i}`}
                >
                  {block.value}
                  {block.unit}
                </Typography>
              </Stack>
            </Stack>
          ))}
        </Stack>
      </Paper>
    </Box>
  );
};
export default Resilience;
