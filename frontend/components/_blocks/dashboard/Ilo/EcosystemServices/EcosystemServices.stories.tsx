import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import EcosystemServices from "./EcosystemServices";
import { Rating } from "@mui/material";

const meta = {
  title: "Blocks/Dashboard/Ilo/EcosystemServices",
  component: EcosystemServices,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "EcosystemServices",
      },
    },
  },
} satisfies Meta<typeof EcosystemServices>;
export default meta;
type Story = StoryObj<typeof EcosystemServices>;

const dataFixture = [
  {
    component: <Rating name="read-only" value={3} readOnly />,
    value: 1568,
    unit: "tonnes",
    title: "C02",
  },
  {
    component: <Rating name="read-only" value={3} readOnly />,
    value: 3,
    unit: "/5",
    title: "Qualité de l'air",
  },
  {
    component: <Rating name="read-only" value={3} readOnly />,
    value: 50,
    unit: "%",
    title: "Ilots de fraîcheur",
  },
  {
    component: <Rating name="read-only" value={3} readOnly />,
    value: 3,
    title: "Biodiversité animale",
  },
  {
    component: <Rating name="read-only" value={3} readOnly />,
    value: 20,
    unit: "%",
    title: "Infiltration",
  },
];
export const Default: Story = {
  render: () => <EcosystemServices data={dataFixture} />,
};
