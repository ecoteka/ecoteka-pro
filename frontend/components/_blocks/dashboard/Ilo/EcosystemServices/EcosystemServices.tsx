import { Paper, Stack, Typography } from "@mui/material";
import DashboardModuleHeader from "@components/_core/headers/DashboardModuleHeader";
import { useTranslation } from "react-i18next";

export interface IEcosystemServicesProps {
  data: any;
}

const EcosystemServices = ({ data }: IEcosystemServicesProps) => {
  const { t } = useTranslation(["components"]);
  return (
    <>
      <DashboardModuleHeader title={t("blocks.EcosystemServices.title")} />
      <Paper elevation={1} data-cy="dashboard-ecosystem-paper">
        <Stack
          direction={"row"}
          spacing={2}
          justifyContent={"space-around"}
          p={2}
          flexWrap={"wrap"}
          data-cy="dashboard-ecosystem-stack"
        >
          {data.map((block, i) => (
            <Stack
              key={i}
              flexDirection="column"
              textAlign="center"
              data-cy={`dashboard-ecosystem-stack-${i}`}
            >
              <Typography
                variant="h6"
                sx={{ mb: 2 }}
                data-cy={`dashboard-ecosystem-title-${i}`}
              >
                {block.title}
              </Typography>
              <Stack
                flexDirection="column"
                alignItems="center"
                spacing={3}
                data-cy={`dashboard-ecosystem-component-${i}`}
              >
                {block.component}
                <Typography
                  variant="caption"
                  fontSize={16}
                  color="#89868D"
                  data-cy={`dashboard-ecosystem-value-${i}`}
                >
                  {block.value}
                  {block.unit}
                </Typography>
              </Stack>
            </Stack>
          ))}
        </Stack>
      </Paper>
    </>
  );
};
export default EcosystemServices;
