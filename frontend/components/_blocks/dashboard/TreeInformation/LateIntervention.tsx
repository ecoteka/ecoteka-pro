/** Components Types */
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useElementSize } from "usehooks-ts";
import Alert from "@mui/material/Alert";
import DashboardModuleHeader from "@components/_core/headers/DashboardModuleHeader";
import { Paper, Box, Tooltip, Typography } from "@mui/material";
import InfoIcon from "@mui/icons-material/Info";
import dynamic from "next/dynamic";
import {
  getAllFamiliesInterventionType,
  getLateInterventionByFamily,
  LateInterventionsByFamilyId,
} from "@services/AnalyticsService";
import { useRouter } from "next/router";
import { useDeviceSize } from "@lib/utils";
import { useDashboardFilters } from "@stores/pages/dashboard";

export interface PieChartData {
  id: string;
  label: string;
  value: number;
  color: string;
}

export interface LegendData {
  family_id: string;
  family_slug: string;
  family_color: number;
}

const LateIntervention = ({ data }) => {
  const { t } = useTranslation(["components", "common"]);
  const [widgetRef, { width }] = useElementSize();
  const { isMobile } = useDeviceSize();
  const [pieData, setPieData] = useState<PieChartData[]>();
  const [legendData, setLegendData] = useState<LegendData[]>();
  const router = useRouter();
  const { boundaries } = useDashboardFilters();

  const ResponsivePie = dynamic(
    () => import("@nivo/pie").then((m) => m.ResponsivePie),
    {
      ssr: false,
    }
  ) as any;

  const handleSliceClick = (itemId: string, itemLabel: string) => {
    if (itemId && itemLabel) {
      router.push({
        pathname: "/intervention",
        query: { object: "all", type: itemId, state: itemLabel },
      });
    }
  };

  useEffect(() => {
    const fetchFilterData = async (params) => {
      try {
        const response: LateInterventionsByFamilyId =
          await getLateInterventionByFamily(params);

        const pieChartData = Object.entries(response).map(
          ([familyId, interventions]) => {
            return {
              id: t(
                `Dashboard.interventions.family.${interventions[0].family_name}`
              ),
              label: t(
                `Dashboard.interventions.family.${interventions[0].family_name}`
              ),
              value: interventions.length,
              color: interventions[0].family_color,
              family_slug: interventions[0].family_name,
            };
          }
        );
        setPieData(pieChartData);
      } catch (error) {
        console.error(
          "Erreur lors de la récupération des données de filtre",
          error
        );
      }
    };

    const fetchDataForLegend = async () => {
      try {
        const response = await getAllFamiliesInterventionType();
        setLegendData(response);
      } catch (error) {
        console.error(
          "Erreur lors de la récupération des données de filtre",
          error
        );
      }
    };

    fetchFilterData(boundaries);
    fetchDataForLegend();
  }, [boundaries]);

  if (data) {
    return (
      <>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            px: 1,
          }}
        >
          <DashboardModuleHeader
            title={t("Template.menuItems.dashboard.lateIntervention")}
          />
          <Tooltip title={t("Template.menuItems.dashboard.lateIntervention")}>
            <InfoIcon />
          </Tooltip>
        </Box>
        <Paper
          elevation={1}
          ref={widgetRef}
          data-cy="dashboard-late-intervention"
          sx={{ height: "20vh" }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              width: "100%",
              height: "100%",
              position: "relative",
              cursor: "pointer",
            }}
          >
            {pieData && pieData.length > 0 && legendData ? (
              <>
                <Typography
                  sx={{
                    position: "absolute",
                    top: isMobile ? "0px" : "20px",
                    right: isMobile ? "10px" : "58px",
                  }}
                >
                  {t("Dashboard.interventions.family.name")}
                </Typography>
                <ResponsivePie
                  data={pieData}
                  margin={{
                    top: isMobile ? 30 : 20,
                    right: isMobile ? 50 : 130,
                    bottom: isMobile ? 10 : 20,
                    left: isMobile ? -50 : -100,
                  }}
                  pixelRatio={2}
                  startAngle={-180}
                  sortByValue={true}
                  innerRadius={0.9}
                  padAngle={5}
                  cornerRadius={5}
                  activeOuterRadiusOffset={8}
                  colors={(d) => d.data.color}
                  borderWidth={1}
                  borderColor={{ from: "color", modifiers: [["darker", 0.2]] }}
                  enableArcLinkLabels={false}
                  arcLinkLabelsTextColor="#333333"
                  arcLinkLabelsThickness={2}
                  arcLinkLabelsColor={{ from: "color" }}
                  enableArcLabels={false}
                  arcLabelsRadiusOffset={0}
                  arcLabelsTextColor="#333333"
                  onClick={(e) =>
                    handleSliceClick(e.data.family_slug, t("common.late"))
                  }
                  legends={[
                    {
                      anchor: "right",
                      direction: "column",
                      justify: false,
                      translateX: isMobile ? 50 : 30,
                      translateY: isMobile ? 8 : 17,
                      itemsSpacing: 0,
                      itemWidth: 100,
                      itemHeight: 18,
                      itemTextColor: "#999",
                      itemDirection: "left-to-right",
                      itemOpacity: 1,
                      symbolSize: 10,
                      symbolShape: "circle",
                      data: legendData.map((family) => ({
                        label: t(
                          `Dashboard.interventions.family.${family.family_slug}`
                        ),
                        color: family.family_color,
                      })),
                    },
                  ]}
                />
              </>
            ) : (
              <Typography>{t("Dashboard.interventions.notLate")}</Typography>
            )}
          </Box>
        </Paper>
      </>
    );
  } else {
    return (
      <Alert severity="error" data-cy="alert">
        {t("Core.Error.message.unexpected")}
      </Alert>
    );
  }
};

export default LateIntervention;
