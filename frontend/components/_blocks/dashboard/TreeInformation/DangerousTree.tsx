/** MUI Import */
import { Box, Tooltip, Button, Typography } from "@mui/material";
import InfoIcon from "@mui/icons-material/Info";

/** Components Types */
import { useTranslation } from "react-i18next";
import { useElementSize } from "usehooks-ts";
import Alert from "@mui/material/Alert";
import DashboardModuleHeader from "@components/_core/headers/DashboardModuleHeader";
import { Paper } from "@mui/material";
import DangerousTreeSVG from "@components/_core/icons/Tree/DangerousTree";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";

const DangerousTree = ({ data }) => {
  const { t } = useTranslation(["components"]);
  const [widgetRef, { width }] = useElementSize();
  const [dangerousTreeNumbers, setDangerousTreeNumbers] = useState<number>(0);

  const router = useRouter();

  useEffect(() => {
    if (data && data.length > 0) {
      const queryData = data.length;
      const formattedNumber = parseInt(
        queryData.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "),
        10
      );
      setDangerousTreeNumbers(formattedNumber);
    } else {
      setDangerousTreeNumbers(0)
    }
  }, [data, dangerousTreeNumbers]);

  const handleClick = () => {
    const dangerousTreeFilter = {
      tree_is_dangerous: [true],
      recommendation: ["felling"],
    };
    const encodedObject = encodeURIComponent(
      JSON.stringify(dangerousTreeFilter)
    );
    router.push(`/map?object=${encodedObject}`);
  };

  if (data) {
    return (
      <>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            px: 1,
          }}
        >
          <DashboardModuleHeader
            title={t("Template.menuItems.dashboard.dangerousTree")}
          />
          <Tooltip title={t("Template.menuItems.dashboard.dangerousTree")}>
            <InfoIcon />
          </Tooltip>
        </Box>
        <Paper
          elevation={1}
          id="soilwater"
          ref={widgetRef}
          data-cy="dashboard-dangerous-tree-component"
          sx={{
            p: 3,
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            alignItems: "center",
            px: 1,
            pl: 2,
            height: "20vh",
          }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-around",
              alignItems: "flex-end",
              width: "100%",
            }}
          >
            <DangerousTreeSVG />
            <Box
              sx={{
                maxWidth: "55%",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                pb: 1,
              }}
            >
              <Typography
                sx={{
                  color: "#FF4C51",
                  fontWeight: 500,
                  fontSize: 24,
                  px: 1.5,
                  overflowWrap: "break-word",
                  lineHeight: 1,
                }}
              >
                {dangerousTreeNumbers}
              </Typography>
              <Typography sx={{ fontSize: 15 }}>
                {t("Diagnosis.recommendation.fellingFull")}
              </Typography>
            </Box>
          </Box>
          {dangerousTreeNumbers != 0 && (
            <Button onClick={handleClick} sx={{ width: "50%", mt: 2, mb: 1 }}>
              {t("Diagnosis.recommendation.felling_plural")}
            </Button>
          )}
        </Paper>
      </>
    );
  } else {
    return (
      <Alert severity="error" data-cy="alert">
        Erreur inatendue!
      </Alert>
    );
  }
};

export default DangerousTree;
