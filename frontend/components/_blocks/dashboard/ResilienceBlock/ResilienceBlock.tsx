import {
  WidgetBlockContainer,
  WidgetContainer,
} from "@components/_core/dashboard";
import { Grid, Stack } from "@mui/material";
import ReactECharts from "echarts-for-react";
import VulnerabilityWidget from "./VulnerabilityWidget";
import SustainabilityWidget from "./SustainabilityWidget";
import DevelopmentStagesWidget from "./DevelopmentStagesWidget";
import { useTranslation } from "react-i18next";

const { t } = useTranslation(["components"]);
const vulnerabilityOption = {
  tooltip: {
    trigger: "item",
  },
  legend: {
    top: "bottom",
  },
  series: [
    {
      name: t("blocks.Resilience.analyticsModules.durabilityScore"),
      type: "pie",
      radius: ["80%", "100%"],
      center: ["50%", "30%"],
      avoidLabelOverlap: false,
      itemStyle: {
        borderRadius: 5,
        borderColor: "#fff",
        borderWidth: 2,
      },
      color: ["steelblue", "whitesmoke"],
      data: [
        { value: 1048, name: t("blocks.Resilience.endangered") },
        { value: 735, name: t("blocks.Resilience.noStatus") },
      ],
    },
  ],
};

export interface IResilienceBlockProps {}
const ResilienceBlock = ({}: IResilienceBlockProps) => {
  return (
    <WidgetBlockContainer title={t("blocks.Resilience.title")}>
      <Grid container spacing={6}>
        <Grid item xs={12} sm={6} md={4}>
          <WidgetContainer
            title={t("blocks.Resilience.development")}
            tooltipText={t("blocks.Resilience.development")}
          >
            <DevelopmentStagesWidget />
          </WidgetContainer>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <WidgetContainer title={t("blocks.Resilience.analyticsModules.durability")} tooltipText={t("blocks.Resilience.analyticsModules.durability")} >
            <SustainabilityWidget />
          </WidgetContainer>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <WidgetContainer title={t("blocks.Resilience.analyticsModules.vulnerabilityScore")} tooltipText={t("blocks.Resilience.analyticsModules.vulnerabilityScore")}>
            <VulnerabilityWidget />
          </WidgetContainer>
        </Grid>
      </Grid>
    </WidgetBlockContainer>
  );
};
export default ResilienceBlock;
