import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import {
  Box,
  Card,
  CardActions,
  CardHeader,
  CardContent,
  Button,
  Typography,
  IconButton,
  Menu,
  MenuItem,
  ChipProps,
  Chip,
  Alert,
} from "@mui/material";
import {
  CheckCircleOutline,
  WarningAmber as WarningAmberIcon,
  AccessTime as AccessTimeIcon,
  ArrowForward as ArrowForwardIcon,
  Tune as TuneIcon,
  ChevronRight as ChevronRightIcon,
  Add as AddIcon,
} from "@mui/icons-material";
import { styled } from "@mui/material/styles";
import {
  Timeline,
  TimelineDot,
  TimelineItem,
  TimelineContent,
  TimelineSeparator,
  TimelineConnector,
} from "@mui/lab";
import { useRouter } from "next/router";
import { formatDate, transformDateFormat, useDeviceSize } from "@lib/utils";

// Styled Timeline component
const StyledTimeline = styled(Timeline)({
  paddingLeft: 0,
  paddingRight: 0,
  "& .MuiTimelineItem-root": {
    width: "100%",
    "&:before": {
      display: "none",
    },
  },
});

export default function BasicCard({ events, locationId }) {
  const { t } = useTranslation(["components, common"]);
  const now = new Date();
  const today = now.toISOString();
  const oneWeekLater = new Date();
  oneWeekLater.setDate(now.getDate() + 7);
  const router = useRouter();
  const { isMobile } = useDeviceSize();

  // State variables for selected status filter and filter menu
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedStatus, setSelectedStatus] = useState(null);

  const statusColors = {
    late: "#FF9800",
    toCome: "#3AB4D0",
    realized: "#38b24b",
  };

  const renderIconForStatus = (status) => {
    switch (status) {
      case "toCome":
        return <AccessTimeIcon sx={{ color: "#3AB4D0 !important" }} />;
      case "late":
        return <WarningAmberIcon color="warning" />;
      case "realized":
        return <CheckCircleOutline color="success" />;
      default:
        return null;
    }
  };

  const getEventStatus = (e, now) => {
    const currentDate = transformDateFormat(now);
    if (e.realization_date) {
      return "realized";
    } else if (e.scheduled_date < currentDate) {
      return "late";
    } else {
      return "toCome";
    }
  };

  const countLateInterventions = (events) => {
    return events?.reduce((count, e) => {
      const status = getEventStatus(e, now);
      return status === "late" ? count + 1 : count;
    }, 0);
  };
  const lateInterventionsCount = countLateInterventions(events);

  const handleFilterButtonClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleStatusFilterChange = (status) => {
    setSelectedStatus(status);
    setAnchorEl(null); // Close the filter menu
  };

  const clearStatusFilter = () => {
    setSelectedStatus(null);
    setAnchorEl(null); // Close the filter menu
  };

  const renderStatusFilterMenu = () => {
    return (
      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={() => setAnchorEl(null)}
      >
        {["late", "toCome", "realized"].map((status) => (
          <MenuItem
            key={status}
            onClick={() => handleStatusFilterChange(status)}
          >
            {t(`common.${status}`)}
          </MenuItem>
        ))}
        {selectedStatus && (
          <MenuItem onClick={clearStatusFilter}>
            {t("common.clearFilters")}
          </MenuItem>
        )}
      </Menu>
    );
  };

  interface CustomChipProps extends ChipProps {
    status: string;
    statusColors: { [key: string]: string };
  }

  // Define your custom styles for the Chip component
  const CustomChip = styled(
    ({ status, statusColors, ...rest }: CustomChipProps) => <Chip {...rest} />
  )(({ status, statusColors }) => ({
    color: statusColors[status],
    borderColor: statusColors[status],
    borderStyle: "solid", // Specify the border style
    borderWidth: 1, // Adjust the border width as needed
    backgroundColor: "#fff",
    borderRadius: "5px",
    marginLeft: 7,
  }));

  const filteredEvents = selectedStatus
    ? events.filter((e) => getEventStatus(e, now) === selectedStatus)
    : events;

  return (
    <Card sx={{ minWidth: "275px" }}>
      <CardHeader
        title={locationId ? t("components.TreeForm.history") : t("components.TreeForm.treeHistory")}
        titleTypographyProps={{ variant: "h5" }}
        action={
          <div>
            <Button
              aria-label="settings"
              onClick={handleFilterButtonClick}
              sx={{
                backgroundColor: "#fff",
                "&:hover": {
                  background: "#fff",
                },
              }}
            >
              <TuneIcon sx={{ mr: "8px", fontSize: "1rem" }} />
              {t("components.Interventions.Card.filter")}
            </Button>
            {renderStatusFilterMenu()}
          </div>
        }
      />
      <CardContent
        sx={{ display: "flex", flexDirection: "column", gap: "16px", py: "0" }}
      >
        <Alert
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() =>
                router.query.treeId
                  ? router.push(
                      `/intervention?object=tree&id=${router.query.treeId}&state=En+retard`
                    )
                  : router.push(
                      `/intervention?object=location&id=${locationId}&state=En+retard`
                    )
              }
            >
              <ArrowForwardIcon fontSize="inherit" />
            </IconButton>
          }
          icon={<AccessTimeIcon />}
          severity="warning"
        >
          {lateInterventionsCount} interventions(s){" "}
          {t("common.late").toLowerCase()}
        </Alert>
        <StyledTimeline sx={{ margin: "0", padding: "0" }}>
          {filteredEvents?.slice(0, 5).map((e) => (
            <TimelineItem key={e.id}>
              <TimelineSeparator>
                <TimelineDot color="error" />
                <TimelineConnector />
              </TimelineSeparator>
              <TimelineContent
                sx={{ "& svg": { verticalAlign: "bottom", mx: 4 } }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexWrap: "wrap",
                    alignItems: "center",
                    justifyContent: "space-between",
                    flexDirection: "row",
                  }}
                >
                  <Box sx={{ flexDirection: "column" }}>
                    <Typography
                      variant="body2"
                      sx={{
                        mb: 1,
                        fontWeight: 600,
                        color: "text.primary",
                      }}
                    >
                      {formatDate(e.scheduled_date)}
                      <CustomChip
                        label={t(`common.${getEventStatus(e, now)}`)}
                        status={getEventStatus(e, now)}
                        statusColors={statusColors}
                        icon={
                          renderIconForStatus(
                            getEventStatus(e, now)
                          ) as React.ReactElement
                        }
                      />
                    </Typography>
                    <Typography variant="caption">
                      {e.intervention_worksite_interventions.length > 0 &&
                        `${t("common.worksite")} (${
                          e.intervention_worksite_interventions[0].worksite
                            ?.name
                        }): `}
                      {t(
                        `components.PanelTree.interventionsList.${e.intervention_type.slug}`
                      )}
                    </Typography>
                    <IconButton
                      aria-label="details"
                      sx={{
                        height: "fit-content",
                        "&:hover": {
                          backgroundColor: "#fff",
                        },
                      }}
                      onClick={() =>
                        router.query.treeId
                          ? router.replace(
                              `/tree/${router.query.treeId}?interventionModal=open&activeId=${e.id}`
                            )
                          : router.replace(
                              `/location/${locationId}?interventionModal=open&activeId=${e.id}`
                            )
                      }
                    >
                      <ChevronRightIcon />
                    </IconButton>
                  </Box>
                </Box>
              </TimelineContent>
            </TimelineItem>
          ))}
        </StyledTimeline>
      </CardContent>
      <CardActions
        sx={{
          p: "16px",
          gap: "16px",
          flexWrap: "wrap",
          justifyContent: "space-between",
        }}
      >
        <Button
          variant="text"
          color="primary"
          sx={{ width: isMobile ? "100%" : "auto" }}
          onClick={() => {
            router.query.treeId
              ? router.push(
                  `/intervention?object=tree${`&id=`}${router.query.treeId}`
                )
              : router.push(
                  `/intervention?object=location${`&id=`}${locationId}`
                );
          }}
        >
          {t("components.Interventions.Card.seeAll")}
        </Button>
        <Button
          variant="contained"
          startIcon={<AddIcon />}
          sx={{ width: isMobile ? "100%" : "auto" }}
          onClick={() =>
            router.query.treeId
              ? router.replace(
                  `/tree/${router.query.treeId}?interventionModal=open`
                )
              : router.replace(`/location/${locationId}?interventionModal=open`)
          }
        >
          {t("components.Interventions.Card.addNew")}
        </Button>
      </CardActions>
    </Card>
  );
}
