import * as React from "react";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import AddIcon from "@mui/icons-material/Add";
import WarningAmberIcon from "@mui/icons-material/WarningAmber";

import Alert from "@mui/material/Alert";
import AlertTitle from "@mui/material/AlertTitle";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { useTranslation } from "react-i18next";
import { useRouter } from "next/router";
import { useDeviceSize } from "@lib/utils";

export default function DiagnosisCard({ diagnosis }) {
  const { t } = useTranslation(["components"]);
  const { isMobile } = useDeviceSize();
  const router = useRouter();

  const colors = {
    good: "success",
    stressed: "warning",
    resilient: "warning",
    wilting: "error",
  };
  return (
    <Card sx={{ minWidth: "275px" }}>
      <CardHeader
        title={t("PanelTree.noData.diagnosis.title")}
        titleTypographyProps={{ variant: "h5" }}
      />
      <CardContent
        sx={{ display: "flex", flexDirection: "column", gap: "16px", py: "0" }}
      >
        <Alert
          severity={colors[diagnosis.tree_condition]}
          icon={<FavoriteIcon />}
        >
          <AlertTitle>
            {t("LayerManager.layers.label.Tree") + " " + t(`Diagnosis.conditions.${diagnosis.tree_condition}`)}
          </AlertTitle>
        </Alert>
        {diagnosis.tree_is_dangerous && (
          <Alert severity={"error"} icon={<WarningAmberIcon />}>
            <AlertTitle>
              {t(`components.Diagnosis.labels.tree_is_dangerous`)}
            </AlertTitle>
          </Alert>
        )}

        <Box
          sx={{
            display: "flex",
            gap: "16px",
          }}
        >
          <TextField
            id="filled-read-only-input"
            label={t(`Diagnosis.labels.tree_condition`)}
            defaultValue={t(
              `Diagnosis.conditions.${diagnosis.tree_condition}`
            )}
            InputProps={{
              readOnly: true,
            }}
            variant="filled"
            sx={{
              flex: "1",
            }}
          />
          <TextField
            id="filled-read-only-input"
            label={t(`Diagnosis.labels.id`)}
            defaultValue={diagnosis.id}
            InputProps={{
              readOnly: true,
            }}
            variant="filled"
            sx={{
              flex: "1",
            }}
          />
        </Box>
        <Box
          sx={{
            display: "flex",
            gap: "16px",
          }}
        >
          <TextField
            id="filled-read-only-input"
            label={t(`Diagnosis.labels.crown_condition`)}
            defaultValue={
              diagnosis.crown_condition
                ? t(
                    `Diagnosis.conditions.${diagnosis.crown_condition}`
                  )
                : ""
            }
            InputProps={{
              readOnly: true,
            }}
            variant="filled"
            sx={{
              flex: "1",
            }}
          />
          <TextField
            id="filled-read-only-input"
            label={t(`Diagnosis.labels.trunk_condition`)}
            defaultValue={
              diagnosis.trunk_condition
                ? t(
                    `Diagnosis.conditions.${diagnosis.trunk_condition}`
                  )
                : ""
            }
            InputProps={{
              readOnly: true,
            }}
            variant="filled"
            sx={{
              flex: "1",
            }}
          />
          <TextField
            id="filled-read-only-input"
            label={t(`Diagnosis.labels.collar_condition`)}
            defaultValue={
              diagnosis.collar_condition
                ? t(
                    `Diagnosis.conditions.${diagnosis.collar_condition}`
                  )
                : ""
            }
            InputProps={{
              readOnly: true,
            }}
            variant="filled"
            sx={{
              flex: "1",
            }}
          />
        </Box>
      </CardContent>
      <CardActions
        sx={{
          p: "16px",
          gap: "16px",
          flexWrap: "wrap",
          justifyContent: "space-between",
        }}
      >
        <Button
          variant="text"
          color="primary"
          sx={{ width: isMobile ? "100%" : "auto" }}
          onClick={() => router.push(`/diagnosis/${diagnosis.id}`)}
        >
          {t(`Diagnosis.labels.diagnosis_complete`)}
        </Button>
        <Button
          variant="text"
          color="primary"
          sx={{ width: isMobile ? "100%" : "auto" }}
          onClick={() =>
            router.push(`/diagnosis?object=tree&id=${diagnosis.tree.id}`)
          }
        >
          {t(`components.Diagnosis.labels.diagnosis_all`)}
        </Button>
        <Button
          variant="contained"
          sx={{ width: isMobile ? "100%" : "auto" }}
          startIcon={<AddIcon />}
          onClick={() =>
            router.replace(`/tree/${diagnosis.tree.id}?diagnosisModal=open`)
          }
        >
          {t(`Diagnosis.labels.add_diagnosis`)}
        </Button>
      </CardActions>
    </Card>
  );
}
