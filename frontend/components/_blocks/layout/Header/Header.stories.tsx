import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Header from "./Header";

const meta = {
  title: "Blocks/Layout/Header",
  component: Header,
  parameters: {
    docs: {
      description: {
        component: "Header",
      },
    },
  },
} satisfies Meta<typeof Header>;

export default meta;
type Story = StoryObj<typeof Header>;

export const Test: Story = {
  render: () => <Header />,
};
