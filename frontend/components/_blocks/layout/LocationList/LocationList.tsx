import * as React from "react";
import { useTranslation } from "react-i18next";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { List, Stack } from "@mui/material";
import Progress from "@components/_core/metrics/Progress";
import ListLocationItem from "@components/_blocks/lists/LocationList/ListItem";
import Count from "@components/_core/metrics/Count";
import { FetchOneWorksiteLocationsQuery } from "@generated/graphql";
import { CardMap } from "@components/_core/cards";
import { type MapRef, type LngLatBoundsLike, Marker } from "react-map-gl";
import { useEffect } from "react";

type LatLngCoords = [number, number];

// latitude: activeLocation?.coords?.coordinates[1],
// longitude: activeLocation?.coords?.coordinates[0],

const getBbox = (coords: LatLngCoords[] | undefined): LngLatBoundsLike => {
  if (coords && coords.map.length > 0) {
    const latitudes = coords.map((coord) => coord[0]);
    const longitudes = coords.map((coord) => coord[1]);
    return [
      [Math.min(...latitudes), Math.min(...longitudes)],
      [Math.max(...latitudes), Math.max(...longitudes)],
    ];
  }
  return [
    [-180, -180],
    [180, 180],
  ];
};
export interface ILocationListProps {
  data: FetchOneWorksiteLocationsQuery | undefined | null;
  onCheckboxChange(interventionId): void;
}
export interface WorksiteProps {
  interventions?: Array<{
    __typename?: "worksite_intervention";
    id: any;
    intervention: {
      __typename?: "intervention";
      id: any;
      realization_date?: any | null;
      scheduled_date: any;
      intervention_type: { __typename?: "intervention_type"; slug: string };
      intervention_partner?: {
        __typename?: "intervention_partner";
        name: string;
      } | null;
      tree?: {
        __typename?: "tree";
        id: any;
        scientific_name?: string | null;
        location?: {
          __typename?: "location";
          address?: string | null;
          coords: any;
          id: any;
          location_status: { __typename?: "location_status"; status: string };
        } | null;
      } | null;

      location?: {
        __typename?: "location";
        address?: string | null;
        coords: any;
        id: any;
        location_status: { __typename?: "location_status"; status: string };
      } | null;
    };
  }>;
}

const LocationList = ({ data, onCheckboxChange }: ILocationListProps) => {
  const { t } = useTranslation(["components, common"]);
  const mapRef = React.useRef<MapRef>(null);
  // const [data, setData] =
  //   useState<FetchOneWorksiteLocationsQuery | null | undefined>(data);
  const isTree = data?.worksite?.interventions[0].intervention?.tree;

  const defaultViewState = {
    longitude: isTree
      ? data?.worksite?.interventions[0].intervention?.tree?.location.coords
          .coordinates[0]
      : data?.worksite?.interventions[0].intervention?.location?.coords
          .coordinates[0],
    latitude: isTree
      ? data?.worksite?.interventions[0].intervention?.tree?.location.coords
          .coordinates[1]
      : data?.worksite?.interventions[0].intervention?.location?.coords
          .coordinates[1],
    zoom: 12,
  };

  const getWorksiteLocationsCoords = (
    worksite: WorksiteProps
  ): LatLngCoords[] | undefined => {
    return worksite?.interventions?.map((item) => {
      return isTree
        ? (item?.intervention?.tree?.location?.coords
            ?.coordinates as LatLngCoords)
        : (item?.intervention?.location?.coords?.coordinates as LatLngCoords);
    });
  };

  const getnterventionsProgress = (
    worksite: WorksiteProps | null | undefined
  ): number => {
    if (worksite?.interventions && Array.isArray(worksite?.interventions)) {
      const totalInterventions = worksite.interventions.length;
      const doneInterventions = worksite.interventions.filter(
        ({ intervention }, i) => Boolean(intervention.realization_date)
      ).length;
      return Math.round((doneInterventions / totalInterventions) * 100);
    } else {
      return 0;
    }
  };

  useEffect(() => {
    if (mapRef.current === null) return;
  });

  useEffect(() => {
    if (mapRef.current === null) return;
    if (data?.worksite) {
      mapRef?.current.fitBounds(
        getBbox(getWorksiteLocationsCoords(data?.worksite)),
        {
          padding: 40,
          duration: 1000,
        }
      );
    }
  });

  return (
    <Card sx={{ width: "100%", minWidth: "275px" }}>
      <CardMap
        ref={mapRef}
        initialViewState={{ ...defaultViewState }}
        mapStyle="/mapStyles/satellite_light.json"
      >
        {data?.worksite?.interventions &&
          data?.worksite?.interventions.map(({ intervention }, i) => {
            return (
              <Marker
                key={`location-${i}`}
                longitude={
                  isTree
                    ? intervention.tree?.location?.coords?.coordinates[0]
                    : intervention.location?.coords?.coordinates[0]
                }
                latitude={
                  isTree
                    ? intervention.tree?.location?.coords?.coordinates[1]
                    : intervention.location?.coords?.coordinates[1]
                }
                color="teal"
              />
            );
          })}
      </CardMap>
      <CardContent sx={{ width: "100%", minWidth: "275px" }}>
        <Stack
          direction="column"
          alignItems="start"
          spacing={2}
          sx={{ width: "100%" }}
        >
          <Stack
            direction="column"
            alignItems="start"
            spacing={1}
            sx={{ width: "100%" }}
          >
            <Typography
              variant="h6"
              component="div"
              sx={{
                padding: 0,
              }}
            >
              {t("components.LocationList.title")}
            </Typography>
            <Count
              name="emplacements"
              data={data?.worksite?.interventions}
            ></Count>
          </Stack>
          <Progress
            value={getnterventionsProgress(data?.worksite)}
            unit="% réalisé"
          ></Progress>
          <List sx={{ width: "100%", overflow: "auto", maxHeight: 434 }}>
            {data?.worksite?.interventions &&
              data.worksite.interventions.map(({ intervention }, i) => (
                <ListLocationItem
                  key={`location-item-${i}`}
                  locationId={
                    isTree
                      ? intervention.tree?.location?.id
                      : intervention.location?.id
                  }
                  interventionId={intervention.id}
                  treeId={isTree && intervention.tree?.id}
                  isCompleted={Boolean(intervention.realization_date)}
                  showWarning={false}
                  address={
                    isTree
                      ? intervention.tree?.location?.address
                      : intervention.location?.address
                  }
                  tree_species={isTree && intervention.tree?.scientific_name}
                  onCheckboxChange={onCheckboxChange}
                ></ListLocationItem>
              ))}
          </List>
        </Stack>
      </CardContent>
      {/* <CardActions
        sx={{
          p: "16px",
          gap: "16px",
          flexWrap: "wrap",
          justifyContent: "space-around",
        }}
      >
        <Button
          variant="contained"
          size="medium"
          disabled
          sx={{ mb: { xs: 4, md: 0 } }}
        >
          {t("components.LocationList.add")}
        </Button>
      </CardActions> */}
    </Card>
  );
};

export default LocationList;
