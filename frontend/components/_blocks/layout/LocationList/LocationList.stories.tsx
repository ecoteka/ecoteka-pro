import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import LocationList from "./LocationList";
import { FetchOneWorksiteLocationsQuery } from "@generated/graphql";

const meta = {
  title: "Blocks/Layout/LocationList",
  component: LocationList,
  parameters: {
    docs: {
      description: {
        component: "LocationList",
      },
    },
  },
} satisfies Meta<typeof LocationList>;

const data = {
  worksite: {
    interventions: [
      {
        id: "e1a66ca9-dc50-4688-ac0c-ae4c0e03d41e",
        intervention: {
          id: "4d34a353-e91a-4c30-acc3-b1437434908d",
          realization_date: "2023-10-17",
          scheduled_date: "2023-10-19",
          intervention_type: {
            slug: "watering",
            __typename: "intervention_type",
          },
          intervention_partner: {
            name: "Arbres et techniques ",
            __typename: "intervention_partner",
          },
          tree: {
            id: "ee805c25-ba8b-4cc1-91c2-3dab8a1e548a",
            scientific_name: "Acer campestre",
            location: {
              address: "38 Rue senac de meilhan 13001 Marseille",
              coords: {
                type: "Point",
                crs: {
                  type: "name",
                  properties: {
                    name: "urn:ogc:def:crs:EPSG::4326",
                  },
                },
                coordinates: [5.38359196, 43.29691557],
              },
              id: "29cfd682-704e-47bc-96cb-ce8fd6f1bc36",
              location_status: {
                status: "alive",
                __typename: "location_status",
              },
              __typename: "location",
            },
            __typename: "tree",
          },
          __typename: "intervention",
        },
        __typename: "worksite_intervention",
      },
      {
        id: "4caefc23-47c1-48f8-9547-a41506e36450",
        intervention: {
          id: "0249c47e-1ba2-4ec5-b296-2142aaa0f027",
          realization_date: null,
          scheduled_date: "2023-10-19",
          intervention_type: {
            slug: "watering",
            __typename: "intervention_type",
          },
          intervention_partner: null,
          tree: {
            id: "9f7c9c2f-115c-4e56-ae8b-0987f64c9e4b",
            scientific_name: "Quercus robur",
            location: {
              address: "35 Rue terrusse 13005 Marseille",
              coords: {
                type: "Point",
                crs: {
                  type: "name",
                  properties: {
                    name: "urn:ogc:def:crs:EPSG::4326",
                  },
                },
                coordinates: [5.38901723, 43.29643404],
              },
              id: "208ed0e4-9f34-42c7-ac56-5f2a93510bd4",
              location_status: {
                status: "alive",
                __typename: "location_status",
              },
              __typename: "location",
            },
            __typename: "tree",
          },
          __typename: "intervention",
        },
        __typename: "worksite_intervention",
      },
    ],
    __typename: "worksite",
  },
} as FetchOneWorksiteLocationsQuery;

export default meta;
type Story = StoryObj<typeof LocationList>;

export const Test: Story = {
  render: () => (
    <LocationList
      data={data}
      onCheckboxChange={() => console.log("on change")}
    />
  ),
};
