import WorkInProgress from "@core/WorkInProgress/WorkInProgress";
import FormInputText from "@core/forms/elements/FormInputText";
import { FetchOneWorksiteQuery } from "@generated/graphql";
import {
  Autocomplete,
  Box,
  Grid,
  InputAdornment,
  TextField,
} from "@mui/material";
import type { Control, FieldValues, UseFormSetValue } from "react-hook-form";
import { useTranslation } from "react-i18next";

export interface IWorksiteFormFieldsProps {
  data: FetchOneWorksiteQuery | null | undefined;
  control: Control;
  setValue: UseFormSetValue<FieldValues>;
  isEditable: boolean;
}
const WorksiteFormFields = ({
  data,
  control,
  setValue,
  isEditable,
}: IWorksiteFormFieldsProps) => {
  const { t } = useTranslation(["components"]);

  return (
    // JSX here
    <Grid container columns={12} spacing={2}>
      <Grid item xs={6}>
        <FormInputText
          control={control}
          required
          fullWidth
          disabled={!isEditable}
          name="worksite.name"
          label={t(`Worksite.properties.name`)}
          variant="filled"
        />
      </Grid>
      <Grid item xs={6}>
        <FormInputText
          control={control}
          defaultValue={"--"}
          fullWidth
          disabled
          InputLabelProps={{
            shrink: true,
          }}
          name="worksite.station"
          label={t(`Worksite.properties.station`)}
          variant="filled"
        />
      </Grid>
      <Grid item xs={6}>
        <TextField
          defaultValue={
            data?.worksite?.location_status
              ? t(
                  `Worksite.properties.location_status_types.${data?.worksite?.location_status}`
                )
              : ""
          }
          fullWidth
          disabled
          InputLabelProps={{
            shrink: true,
          }}
          label={t(`Worksite.properties.location_status`)}
          variant="filled"
        ></TextField>
      </Grid>
      <Grid item xs={6}>
        <TextField
          defaultValue={
            data?.worksite?.interventions?.[0].intervention?.intervention_type
              ?.slug
              ? t(
                  `PanelTree.interventionsList.${data?.worksite?.interventions?.[0].intervention?.intervention_type?.slug}`
                )
              : ""
          }
          fullWidth
          disabled
          InputLabelProps={{
            shrink: true,
          }}
          label={t(`Worksite.properties.intervention_type`)}
          variant="filled"
        ></TextField>
      </Grid>
      <Grid item xs={6}>
        <TextField
          defaultValue={
            data?.worksite?.interventions?.[0].intervention?.tree
              ?.scientific_name + "..."
          }
          fullWidth
          disabled
          InputLabelProps={{
            shrink: true,
          }}
          label={t(`Worksite.properties.tree_genuses`)}
          variant="filled"
        ></TextField>
      </Grid>
      <Grid item xs={6}>
        <TextField
          defaultValue="--"
          fullWidth
          disabled
          InputLabelProps={{
            shrink: true,
          }}
          label={t(`Worksite.properties.intervention_details`)}
          variant="filled"
        ></TextField>
      </Grid>
      <Grid item xs={6}>
        <TextField
          defaultValue={
            data?.worksite?.interventions?.[0].intervention
              ?.intervention_partner?.name
              ? data?.worksite?.interventions?.[0].intervention
                  ?.intervention_partner?.name
              : ""
          }
          disabled
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          label={t(`Worksite.properties.partner`)}
          variant="filled"
          helperText={
            true ? t(`Partner.types.internal`) : t(`Partner.types.external`)
          }
        ></TextField>
      </Grid>
      <Grid item xs={6}>
        <FormInputText
          control={control}
          fullWidth
          type="number"
          disabled={!isEditable}
          name="worksite.cost"
          label={t(`Worksite.properties.cost`)}
          aria-describedby="worksite-cost-input"
          InputLabelProps={{
            shrink: true,
          }}
          InputProps={{
            endAdornment: <InputAdornment position="start">€</InputAdornment>,
            "aria-label": "cost",
          }}
          variant="filled"
        />
      </Grid>
      <Grid item xs={12}>
        <FormInputText
          control={control}
          fullWidth
          multiline
          rows={3}
          maxRows={6}
          disabled={!isEditable}
          name="worksite.description"
          label={t(`Worksite.properties.description`)}
          variant="filled"
        />
      </Grid>
    </Grid>
  );
};
export default WorksiteFormFields;
