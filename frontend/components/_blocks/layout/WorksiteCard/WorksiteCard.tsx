import * as React from "react";
import { Ref, createRef, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";

import type { FeatureCollection } from "geojson";

import { useRouter } from "next/router";
import { FetchOneWorksiteQuery } from "@generated/graphql";
import WorksiteFormFields from "./WorksiteFormFields";
import { Control, FieldValues, UseFormSetValue } from "react-hook-form";

export interface IWorksiteCardProps {
  data: FetchOneWorksiteQuery | null | undefined;
  control: Control;
  setValue: UseFormSetValue<FieldValues>;
}

const WorksiteCard = ({ data, control, setValue }: IWorksiteCardProps) => {
  const router = useRouter();
  const { query } = router;

  const isEditable = query.hasOwnProperty("edit");

  const { t } = useTranslation(["components"]);

  return (
    <Card>
      <CardContent
        sx={{
          display: "flex",
          flexDirection: "column",
          padding: "16px",
        }}
      >
        <Typography
          variant="h6"
          sx={{
            padding: "0px 0px 16px 0px",
          }}
        >
          {t("WorksiteCard.title")}
        </Typography>
        <WorksiteFormFields
          data={data}
          control={control}
          setValue={setValue}
          isEditable={isEditable}
        />
      </CardContent>
      <CardActions
        sx={{
          p: "16px",
          gap: "16px",
          flexWrap: "wrap",
          justifyContent: "space-between",
        }}
      >
        <Button variant="outlined" size="medium" color="error" disabled>
          {t("WorksiteCard.worksiteCanceled")}
        </Button>
        <Button variant="contained" size="medium" disabled>
          {t("WorksiteCard.worksiteRealised")}
        </Button>
      </CardActions>
    </Card>
  );
};

export default WorksiteCard;
