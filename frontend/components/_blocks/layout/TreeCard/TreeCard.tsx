import React, { useEffect, useState } from "react";
import Image from "next/image";
import { useTranslation } from "react-i18next";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import TreeFields from "@components/forms/tree/TreeFields";
import getConfig from "next/config";
import { checkIfImageExists } from "@lib/utils";
import { useRouter } from "next/router";
import { Box, Button, CircularProgress, Grid, Stack } from "@mui/material";
import UploadComponent from "@components/Upload";

export default function BasicCard({
  activeTree,
  customControl,
  customSetValue,
  errors,
}) {
  const { t } = useTranslation(["components"]);
  const { query, push } = useRouter();
  const [imageUrl, setImageUrl] = useState<string>("");
  const [hasImage, setHasImage] = useState<boolean>(false);
  const [imageIsLoading, setImageIsLoading] = useState<boolean>(false);
  const { publicRuntimeConfig } = getConfig();

  const isReadOnly = () => !query.hasOwnProperty("edit");

  useEffect(() => {
    if (activeTree) {
      activeTree.id &&
        setImageUrl(
          `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/${
            publicRuntimeConfig.DEFAULT_STORAGE_BUCKET_NAME
          }/trees/${activeTree.id}/latest.png?t=${Date.now()}`
        );
    }
  }, [activeTree]);

  useEffect(() => {
    checkIfImageExists(imageUrl, (exists) => {
      if (exists) {
        setHasImage(true);
        setImageIsLoading(false);
      } else {
        setHasImage(false);
        setImageIsLoading(false);
      }
    });
  }, [imageUrl]);

  return (
    <Card sx={{ minWidth: "275px" }}>
      <Grid item xs={12}>
        <Box sx={styles.box}>
          <CardMedia
            sx={{
              height: !hasImage ? 150 : "320px",
              backgroundSize: "contain",
              m: !hasImage ? 1 : 0,
            }}
            image={!hasImage ? `/components/map/location/tree.png` : imageUrl}
            key={hasImage ? "hasImage" : "noImage"}
            title={t("Template.menuItems.map")}
          />

          {!isReadOnly() && (
            <UploadComponent
              path={`trees/${activeTree?.id}/latest.png`}
              hasImage={hasImage}
              onUploaded={(url) => {
                setImageIsLoading(true);
                setImageUrl(`${url}?data=${Date.now()}`);
              }}
              contextUploadPhoto={true}
            />
          )}
          {imageIsLoading && <CircularProgress sx={styles.loader} />}
        </Box>
      </Grid>

      <CardContent
        sx={{
          display: "flex",
          flexDirection: "column",
          padding: "16px",
        }}
      >
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
          spacing={2}
        >
          <Grid item>
            <Typography
              variant="h5"
              component="div"
              sx={{
                padding: "0px 0px 16px 0px",
              }}
            >
              {t("TreeCard.title")}
            </Typography>
          </Grid>
          {activeTree.urbasense_subject_id && (
            <Grid item>
              <Stack direction="row" spacing={1} alignItems="center">
                <Image
                  src="/us_logo.png"
                  width={26}
                  height={26}
                  alt="Logo Urbasense"
                />
                <Typography variant="overline" display="block" component="span">
                  équipé par Urbasense
                </Typography>
              </Stack>
            </Grid>
          )}
        </Grid>

        <TreeFields
          activeTree={activeTree}
          customControl={customControl}
          customSetValue={customSetValue}
          errors={errors}
          extended={false}
        />
      </CardContent>
    </Card>
  );
}

const styles = {
  box: {
    width: "100%",
    position: "relative",
    textAlign: "center",
  },
  text: {
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    textTransform: "uppercase",
    position: "absolute",
    color: "#696B6D",
    opacity: "0.8",
  },
  loader: {
    top: "50%",
    left: "50%",
    position: "absolute",
    color: "#FFC107",
  },
};
