import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import TreeCard from "./TreeCard";

const meta = {
  title: "Blocks/Layout/TreeCard",
  component: TreeCard,
  parameters: {
    docs: {
      description: {
        component: "TreeCard",
      },
    },
  },
} satisfies Meta<typeof TreeCard>;

export default meta;
type Story = StoryObj<typeof TreeCard>;

export const Test: Story = {
  render: () => (
    <TreeCard
      activeTree={[]}
      customControl={[]}
      customSetValue={[]}
      errors={[]}
    />
  ),
};
