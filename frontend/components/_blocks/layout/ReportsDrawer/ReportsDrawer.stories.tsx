import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import ReportsDrawer from "./ReportsDrawer";

const meta = {
  title: "Blocks/Layout/ReportsDrawer",
  component: ReportsDrawer,
  parameters: {
    docs: {
      description: {
        component: "ReportsDrawer",
      },
    },
  },
} satisfies Meta<typeof ReportsDrawer>;

export default meta;
type Story = StoryObj<typeof ReportsDrawer>;

export const Test: Story = {
  render: () => <ReportsDrawer />,
};
