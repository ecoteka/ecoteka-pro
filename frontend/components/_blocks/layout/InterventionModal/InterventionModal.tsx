import { useTranslation } from "react-i18next";
// ** React Imports
import { Fragment, useState } from "react";

// ** MUI Imports
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import DialogContentText from "@mui/material/DialogContentText";
import Box from "@mui/material/Box";
import { Grid, TextField, Typography } from "@mui/material";
import { Controller } from "react-hook-form";
/* import InterventionForm from "@components/forms/intervention/InterventionForm"; */

const DialogConfirmation = () => {
  // ** State
  const [open, setOpen] = useState<boolean>(false);

  const handleClickOpen = () => setOpen(true);

  const handleClose = () => setOpen(false);

  const { t } = useTranslation(["components, common"]);
  return (
    <Fragment>
      <Button variant="outlined" onClick={handleClickOpen}>
        Open dialog
      </Button>
      <Dialog
        open={open}
        disableEscapeKeyDown
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        onClose={(event, reason) => {
          if (reason !== "backdropClick") {
            handleClose();
          }
        }}
      >
        <DialogTitle id="alert-dialog-title" sx={{ textAlign: "center" }}>
          {t(`components.Interventions.Card.addNew`)}
        </DialogTitle>
        <DialogContent>
          <Box sx={{ height: "300px", overflow: "hidden" }}>
            <img src="https://www.wired.com/wp-content/uploads/2016/11/GoogleMapTA.jpg" />
          </Box>
          <Grid container spacing={3} columns={12}>
            <Grid item xs={12} md={12} lg={12} xl={12}>
              <Grid container spacing={1}>
                <Grid item xs={6}>
                  <TextField
                    id="filled-read-only-input"
                    label={t(`components.Diagnosis.labels.tree_condition`)}
                    defaultValue="Bon état"
                    InputProps={{
                      readOnly: true,
                    }}
                    variant="filled"
                    sx={{
                      flex: "1",
                    }}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          {/* <InterventionForm /> */}
        </DialogContent>
        <DialogActions
          className="dialog-actions-dense"
          sx={{ padding: "16px", justifyContent: "space-between" }}
        >
          <Button onClick={handleClose}>{t("common.buttons.cancel")}</Button>
          <Button onClick={handleClose} variant="contained">
            {t("common.buttons.confirm")}
          </Button>
        </DialogActions>
      </Dialog>
    </Fragment>
  );
};

export default DialogConfirmation;
