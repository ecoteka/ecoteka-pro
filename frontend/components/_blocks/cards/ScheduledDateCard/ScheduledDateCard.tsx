import { CardTitle } from "@core/typography";
import {
  Button,
  Card,
  CardContent,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { differenceInDays, format, isBefore } from "date-fns";
import { fr } from "date-fns/locale";
import { ScheduleIcon } from "@components/icons";
import type { IScheduleIconProps } from "@components/icons";

export interface IScheduledDateCardProps {
  itemId: string;
  scheduledDate: Date;
  onChangeScheduledDate?: (date: Date | null | string) => void;
  onConfirmScheduledDateChange?: (
    id: string,
    date: Date | null | string
  ) => void;
}

const ScheduledDateCard = ({
  itemId,
  scheduledDate,
  onChangeScheduledDate = (date) => console.log(date),
  onConfirmScheduledDateChange = (id, date) => console.log(id, date),
}: IScheduledDateCardProps) => {
  const { t } = useTranslation(["components"]);

  const dateToString = (date: Date | null): string => {
    if (!date) {
      return "--";
    } else {
      return format(date, "d LLLL y", { locale: fr });
    }
  };

  const [date, setDate] = useState<Date | null | string>(scheduledDate);
  const [showDatepicker, setDatePickerDisplay] = useState<boolean>(false);

  const getSchedulingStatus = (
    scheduledDate: Date | null | string
  ): IScheduleIconProps["variant"] => {
    if (scheduledDate) {
      const diffInDays = differenceInDays(new Date(scheduledDate), Date.now());
      if (isBefore(new Date(scheduledDate), Date.now())) {
        return "late";
      }
      if (
        !isBefore(new Date(scheduledDate), Date.now()) &&
        diffInDays >= 0 &&
        diffInDays < 7
      ) {
        return "soon";
      }
      if (!isBefore(new Date(scheduledDate), Date.now()) && diffInDays >= 7) {
        return "planned";
      }
      return "unknown";
    } else {
      return "unknown";
    }
  };

  const handleConfirm = () => {
    console.log("itemId :>> ", itemId);
    console.log("date :>> ", date);
    onConfirmScheduledDateChange(itemId, date);
    setDatePickerDisplay(false);
  };

  const handleCancel = () => {
    setDatePickerDisplay(false);
    setDate(scheduledDate);
  };

  return (
    <>
      <Card>
        <CardContent
          sx={{
            padding: "16px",
          }}
        >
          <Stack
            direction="row"
            spacing={2}
            alignContent="center"
            justifyContent="space-between"
          >
            <Stack
              direction="column"
              justifyContent="center"
              alignItems="flex-start"
              spacing={0}
            >
              <CardTitle title={`${t("ScheduledDateCard.title")}`} />
              <Stack
                direction="row"
                spacing={0.5}
                alignContent="center"
                justifyContent="space-between"
              >
                <ScheduleIcon variant={getSchedulingStatus(date)} />
                <Typography variant="subtitle2" gutterBottom>
                  {dateToString(date ? new Date(date) : null)}
                </Typography>
              </Stack>
            </Stack>
            <Stack
              direction="column"
              justifyContent="center"
              alignItems="flex-start"
              spacing={0}
            >
              {!showDatepicker && (
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => setDatePickerDisplay(true)}
                >{`${t("ScheduledDateCard.replan")}`}</Button>
              )}
              {showDatepicker && (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => handleConfirm()}
                >{`${t("ScheduledDateCard.confirm")}`}</Button>
              )}
            </Stack>
          </Stack>
          {showDatepicker && date && (
            <>
              <TextField
                required
                variant="filled"
                type="date"
                sx={{ width: "100%" }}
                InputLabelProps={{
                  shrink: true,
                }}
                value={date}
                onChange={(e) => setDate(e.target.value)}
              />

              <Button
                sx={{ margin: 1 }}
                variant="outlined"
                onClick={() => handleCancel()}
              >
                {`${t("ScheduledDateCard.cancel")}`}
              </Button>
            </>
          )}
        </CardContent>
      </Card>
    </>
  );
};
export default ScheduledDateCard;
