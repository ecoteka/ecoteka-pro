import React, { useState, useEffect, ChangeEvent } from "react";
import "react-dates/initialize";
import {
  DateRangePicker,
  SingleDatePicker,
  FocusedInputShape,
} from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import moment, { Moment } from "moment";
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import "moment/locale/fr";
import { useTranslation } from "react-i18next";
import {
  Typography,
  Checkbox,
  FormControlLabel,
  Box,
  Tooltip,
} from "@mui/material";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";

interface DateRangeSelectorProps {
  pickerType: string;
  isExpanded: boolean;
  onDateChange: (startDate: Moment | null, endDate: Moment | null) => void; // Ajout de cette prop
}

const DateRangeSelector: React.FC<DateRangeSelectorProps> = ({
  pickerType,
  onDateChange,
  isExpanded,
}) => {
  const { t } = useTranslation("components");
  const [startDate, setStartDate] = useState<Moment | null>(null);
  const [endDate, setEndDate] = useState<Moment | null>(null);
  const [minDate, setMinDate] = useState<Moment | null>(null);
  const [maxDate, setMaxDate] = useState<Moment | null>(null);
  const [focusedInput, setFocusedInput] = useState<"startDate" | null>(null);
  const [isSingleDayPicker, setIsSingleDayPicker] = useState<boolean>(false);
  const { filterData, filtersWithMartin, shouldResetFields } =
    useFiltersWithMartinContext()!;

  useEffect(() => {
    const language = localStorage.getItem("i18nextLng") || "fr";
    moment.locale(language === "en" ? "en" : "fr");
  }, []);

  const handleDatesChange = (dates: {
    startDate: Moment | null;
    endDate: Moment | null;
  }) => {
    setStartDate(dates.startDate);
    setEndDate(dates.endDate);
    onDateChange(dates.startDate, dates.endDate);
  };

  const getMinDates = (pickerType: string) => {
    if (filtersWithMartin) {
      switch (pickerType) {
        case t(`components.Intervention.properties.scheduledDate`):
          setMinDate(moment(filterData?.intervention?.scheduled_date));
          break;
        case t(`components.Intervention.properties.realizationDate`):
          setMinDate(moment(filterData?.intervention?.realization_date));
          break;
        case t(`components.Tree.properties.plantationDate.exact`):
          setMinDate(
            moment(filterData?.tree?.plantation_date.substring(0, 10))
          );
          break;
        default:
          setMinDate(moment().subtract(1, "years"));
          break;
      }
    }
  };

  const handleFocusChange = (focusedInput) => {
    if (focusedInput) {
      getMinDates(pickerType);
      setMaxDate(moment().add(1, "years"));
    }
    setFocusedInput(focusedInput);
  };

  useEffect(() => {
    if (shouldResetFields) {
      setStartDate(null);
      setEndDate(null);
    }
  }, [shouldResetFields]);

  return (
    <Box
      sx={{
        width: "100%",
        alignItems: "flex-end",
        flexDirection: "column",
        display: isExpanded ? "flex" : "none",
      }}
    >
      <FormControlLabel
        label={t("Map.Filter.uniqueDate")}
        control={
          <Tooltip title={t("Map.Filter.uniqueDate")}>
            <Checkbox
              checked={isSingleDayPicker}
              onChange={(
                event: ChangeEvent<HTMLInputElement>,
                checked: boolean
              ) => setIsSingleDayPicker(checked)}
            />
          </Tooltip>
        }
      />
      <Box sx={{ position: "relative", width: "100%" }}>
        {isSingleDayPicker ? (
          <SingleDatePicker
            isOutsideRange={(day: Moment | null) =>
              day ? day.isBefore(minDate) || day.isAfter(maxDate) : false
            }
            date={startDate}
            onDateChange={(date: Moment | null) =>
              handleDatesChange({ startDate: date, endDate: null })
            }
            focused={focusedInput === "startDate"}
            onFocusChange={({ focused }) =>
              setFocusedInput(focused ? "startDate" : null)
            }
            numberOfMonths={1}
            id="your_unique_date_id"
          />
        ) : (
          <DateRangePicker
            startDate={startDate}
            startDateId="your_unique_start_date_id"
            endDate={endDate}
            isOutsideRange={(day) =>
              day.isBefore(minDate) || day.isAfter(maxDate)
            }
            endDateId="your_unique_end_date_id"
            onDatesChange={handleDatesChange}
            focusedInput={focusedInput}
            onFocusChange={handleFocusChange}
            openDirection="down"
            showClearDates={true}
            minDate={minDate ?? undefined}
            maxDate={maxDate ?? undefined}
            numberOfMonths={1}
            startDatePlaceholderText={t("Map.Filter.startDate")}
            endDatePlaceholderText={t("Map.Filter.endDate")}
          />
        )}
        <Box
          sx={{
            position: "absolute",
            right: "15px",
            top: "37%",
            display: focusedInput || endDate || startDate ? "none" : "flex",
          }}
        >
          <CalendarMonthIcon />
        </Box>
        <Typography
          className={`labelDatePicker ${focusedInput ? "focused" : ""}`}
        >
          {pickerType}
        </Typography>
      </Box>
    </Box>
  );
};

export default DateRangeSelector;
