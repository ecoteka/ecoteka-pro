import React, { FC, useState } from "react";
import {
  Button,
  Menu,
  MenuItem,
  IconButton,
  Avatar,
  Divider,
} from "@mui/material";
import { useTranslation } from "react-i18next";
import PersonIcon from "@mui/icons-material/Person";
import router, { Router } from "next/router";
import { useSession } from "next-auth/react";
import type { Profile } from "next-auth";

export enum UserMainMenuAction {
  logout,
  account,
}

export interface UserMainMenuButtonProps {
  onAction?(action?: UserMainMenuAction);
  isDesktop: boolean;
}


const UserMainMenuButton: FC<UserMainMenuButtonProps> = ({
  onAction = () => {},
  isDesktop,
}) => {
  const { t } = useTranslation(["component", "common"]);
  const session = useSession();
  const currentUser = session?.data?.user as Profile | undefined;
  const user = {
    given_name: currentUser?.given_name,
    family_name: currentUser?.family_name,
    email: currentUser?.email,
  };
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOnAction = (action: UserMainMenuAction) => {
    handleClose();
    if (action === UserMainMenuAction.account) {
      router.push('/account');
    } else if (action === UserMainMenuAction.logout) {
      onAction(action);
    }
  };


  function stringAvatar(name: string) {
    return {
      sx: {
        bgcolor: "primary",
      },
      children: `${name.split(" ")[0][0]}${name.split(" ")[1][0]}`,
    };
  }

  return (
    <div>
      {isDesktop ? (
        <Button
          data-test="user-menu"
          variant="text"
          color="inherit"
          sx={{
            whiteSpace: "no-wrap",
          }}
          aria-haspopup="true"
          aria-expanded={open ? "true" : undefined}
          onClick={handleClick}
        >
          <Avatar
            {...stringAvatar(`${user.given_name} ${user.family_name}`)}
            sx={{ fontFamily: "Poppins", mr: 1, backgroundColor: "#2FA37C" }}
          />
          {`${user.given_name} ${user.family_name}`}
        </Button>
      ) : (
        <IconButton color="primary" onClick={handleClick}>
          <PersonIcon fontSize="small" />
        </IconButton>
      )}

      <Menu anchorEl={anchorEl} open={open} onClose={handleClose}>
        <MenuItem disabled>{user.email}</MenuItem>
        <MenuItem onClick={() => {
            handleOnAction(UserMainMenuAction.account);
          }}>
          {t("common.account") as string}
        </MenuItem>
        {/*         {Number(publicRuntimeConfig.ALLOW_ALPHA_MODE) === 1 && [
          <Divider key="divider1" />,
          <MenuItem key="item1" onClick={() => router.push("/experimental")}>
            Fonctionnalités expérimentales
          </MenuItem>,
          <Divider key="divider2" />,
        ]} */}

        <MenuItem
          onClick={() => {
            handleOnAction(UserMainMenuAction.logout);
          }}
        >
          {t("components.Logout.logout") as string}
        </MenuItem>
      </Menu>
    </div>
  );
};

export default UserMainMenuButton;
