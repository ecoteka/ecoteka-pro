import { FC } from "react";
import { styled } from "@mui/material/styles";
import { Stack, Box, Typography } from "@mui/material";
import { ThemeSelectorMode } from "../header/ThemeSelector";

export interface LogoProps {
  mode?: ThemeSelectorMode;
}

const Logo = styled(Box)<LogoProps>(({ mode }) => ({
  height: "32px",
  width: "32px",
  paddingTop: ".3rem",

  backgroundSize: "contain",
  backgroundPosition: "center center",
  backgroundRepeat: "no-repeat",
  backgroundImage: `url(../logo-${mode}.svg)`,
}));

export interface HeaderLogoProps {
  mode?: ThemeSelectorMode;
  onClick(): void;
  isDesktop: boolean;
}

const HeaderLogo: FC<HeaderLogoProps> = ({
  mode = ThemeSelectorMode.light,
  onClick,
  isDesktop,
}) => {
  return (
    <Stack
      spacing={1}
      onClick={onClick}
      direction="row"
      sx={{ cursor: "pointer" }}
    >
      <Logo mode={mode} />
      {isDesktop && (
        <Typography color="primary" variant="h5">
          Ecoteka
        </Typography>
      )}
    </Stack>
  );
};

export default HeaderLogo;
