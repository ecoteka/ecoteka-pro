import { FC } from "react";
import { usePresignedUpload } from "next-s3-upload";
import { Button } from "@mui/material";
import { useTranslation } from "react-i18next";
import { TreePhotoIcon } from "./icons/TreePhotoIcon";

export interface UploadComponentProps {
  hasImage?: boolean;
  path: string;
  onUploaded(url: string, file: any): void;
  contextUploadPhoto?: boolean;
}

const UploadComponent: FC<UploadComponentProps> = ({
  path,
  hasImage,
  onUploaded,
  contextUploadPhoto = false,
}) => {
  const { t } = useTranslation(["components"]);
  let { FileInput, openFileDialog, uploadToS3 } = usePresignedUpload();

  let handleFileChange = async (file) => {
    try {
      let { url } = await uploadToS3(file, {
        endpoint: {
          request: {
            body: {
              path,
            },
          },
        },
      });
      onUploaded(contextUploadPhoto ? url : path, file);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <FileInput onChange={handleFileChange} />

      <Button
        onClick={openFileDialog}
        sx={contextUploadPhoto ? styles.button : {}}
        startIcon={contextUploadPhoto && <TreePhotoIcon />}
      >
        {hasImage && contextUploadPhoto
          ? t(`TreeForm.imagesUpload.edit`)
          : !contextUploadPhoto
          ? "Joindre un fichier"
          : t(`TreeForm.imagesUpload.upload`)}
      </Button>
    </>
  );
};

export default UploadComponent;

const styles = {
  button: {
    bottom: "50%",
    right: "50%",
    top: "25%",
    width: "150px",
    height: "100px",
    margin: "0px -100px -15px 0px",
    position: "absolute",
    color: "#696B6D",
    display: "flex",
    flexDirection: "column",
    opacity: "0.8",
    backgroundColor: "#EBECF0",
    "&:hover": {
      backgroundColor: "#EBECF0",
    },
  },
};
