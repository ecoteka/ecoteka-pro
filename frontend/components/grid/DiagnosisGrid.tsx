import { Box, IconButton } from "@mui/material";
import { FC } from "react";
import { Diagnosis, FetchAllDiagnosesQuery } from "../../generated/graphql";
import useTableLocale from "../../lib/useTableLocale";
import {
  MRT_ColumnDef,
  MaterialReactTable,
  useMaterialReactTable,
} from "material-react-table";
import { useRouter } from "next/router";
import EditIcon from "@mui/icons-material/Edit";
import { useGridContext } from "@context/grid/grid.context";

export interface DiagnosisGridProps {
  allDiagnoses: FetchAllDiagnosesQuery["diagnosis"];
  columns: MRT_ColumnDef<Diagnosis>[];
}

export const DiagnosisGrid: FC<DiagnosisGridProps> = ({
  allDiagnoses,
  columns,
}) => {
  const router = useRouter();
  const localization = useTableLocale();
  const { rowSelected, setRowSelected } = useGridContext() ?? {};

  const table = useMaterialReactTable({
    columns: columns as any,
    data: allDiagnoses,
    localization: localization,
    enableGlobalFilter: false,
    enableTopToolbar: true,
    enablePagination: true,
    enableRowSelection: true,
    enableColumnResizing: true,
    onRowSelectionChange: setRowSelected,
    getRowId: (originalRow) => originalRow.id,
    filterFns: {
      dateEquals: (row, id, filterValue) => {
        const rowDate = new Date(row.original.diagnosis_date);
        const filterDate = filterValue ? filterValue : null;

        if (filterDate && rowDate) {
          rowDate.setTime(
            rowDate.getTime() + rowDate.getTimezoneOffset() * 60 * 1000
          );
        }

        return (
          !filterDate || (rowDate && filterDate.getTime() === rowDate.getTime())
        );
      },
    },
    state: {
      rowSelection: rowSelected,
    },
    initialState: {
      density: "compact",
      pagination: { pageIndex: 0, pageSize: 30 },
    },
    positionActionsColumn: "first",
    enableRowActions: true,
    renderRowActions: ({ row }) => (
      <Box>
        <IconButton
          onClick={() => router.push(`/diagnosis/${row.original.id}`)}
        >
          <EditIcon />
        </IconButton>
      </Box>
    ),
  });

  return (
    <Box>
      <MaterialReactTable table={table} />
    </Box>
  );
};

export default DiagnosisGrid;
