import { Box, Button, Stack, Typography } from "@mui/material";
import { FC, useState } from "react";
import { useTranslation } from "react-i18next";
import { FetchAllImportsQuery, Import } from "../../generated/graphql";
import useTableLocale from "../../lib/useTableLocale";
import {
  MRT_ColumnDef,
  MaterialReactTable,
  useMaterialReactTable,
} from "material-react-table";
import BackButton from "../layout/BackButton";
import { useRouter } from "next/router";
import AddIcon from "@mui/icons-material/Add";
import { ModalComponent } from "@components/forms/modal/ModalComponent";
import ConfirmDialog from "@components/forms/dialog/ConfirmDialog";
import { SubmitHandler } from "react-hook-form";

export interface ImportsGridProps {
  deleteImports: (ids: string[]) => void;
  allImports: FetchAllImportsQuery["import"];
  title: string;
  columns: MRT_ColumnDef<Import>[];
}

export const ImportsGrid: FC<ImportsGridProps> = ({
  deleteImports,
  allImports,
  columns,
  title,
}) => {
  const { t } = useTranslation(["common"]);
  const localization = useTableLocale();
  const router = useRouter();
  const [rowSelection, setRowSelection] = useState({});
  const [openDeleteModal, setOpenDeleteModal] = useState<boolean>(false);

  const onConfirmDeletionModal: SubmitHandler<Import> = async () => {
    const ids = Object.keys(rowSelection);
    setOpenDeleteModal(false);
    deleteImports(ids);
  };

  const handleOnBackToLocation = () => {
    router.push(`/map`);
  };

  const table = useMaterialReactTable({
    columns: columns as any,
    data: allImports,
    localization: localization,
    enableGlobalFilter: false,
    enableTopToolbar: true,
    enablePagination: true,
    enableRowSelection: true,
    enableColumnResizing: true,
    onRowSelectionChange: setRowSelection,
    getRowId: (originalRow) => originalRow.id,
    initialState: {
      density: "compact",
      pagination: { pageIndex: 0, pageSize: 30 },
    },
    state: {
      rowSelection,
    },
    positionActionsColumn: "first",
    renderTopToolbarCustomActions: () =>
      Object.keys(rowSelection).length > 0 && (
        <Button
          size="large"
          variant="outlined"
          color="error"
          onClick={() => {
            setOpenDeleteModal(true);
          }}
        >
          {t("delete")}
        </Button>
      ),
  });

  return (
    <Box>
      <Stack direction="row" alignItems="center" sx={{ mb: 2 }}>
        <BackButton title={t("back")} onClick={handleOnBackToLocation} />
        <Typography
          variant="h6"
          textAlign="center"
          sx={{
            fontSize: "20px",
            textTransform: "uppercase",
            flexGrow: 1,
          }}
        >
          {title}
        </Typography>

        <Button
          variant="contained"
          startIcon={<AddIcon />}
          onClick={() => router.push(`/import/add`)}
        >
          {t(`buttons.import`) as string}
        </Button>
      </Stack>

      <MaterialReactTable table={table} />
      {openDeleteModal && (
        <ModalComponent
          open={openDeleteModal}
          handleClose={() => setOpenDeleteModal(false)}
        >
          <ConfirmDialog
            message={t("messages.lostData")}
            onConfirm={onConfirmDeletionModal}
            onAbort={() => setOpenDeleteModal(false)}
            showConfirmButton={true}
          />
        </ModalComponent>
      )}
    </Box>
  );
};

export default ImportsGrid;
