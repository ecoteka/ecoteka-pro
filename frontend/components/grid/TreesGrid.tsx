import { Box, IconButton } from "@mui/material";
import { FC } from "react";
import { FetchAllTreesQuery, Tree } from "../../generated/graphql";
import useTableLocale from "../../lib/useTableLocale";
import {
  MRT_ColumnDef,
  MaterialReactTable,
  useMaterialReactTable,
} from "material-react-table";
import EditIcon from "@mui/icons-material/Edit";
import { useRouter } from "next/router";

export interface TreesGridProps {
  allTrees: FetchAllTreesQuery["tree"];
  columns: MRT_ColumnDef<Tree>[];
}
export const TreesGrid: FC<TreesGridProps> = ({ allTrees, columns }) => {
  const localization = useTableLocale();
  const router = useRouter();

  const table = useMaterialReactTable({
    columns: columns as any,
    data: allTrees,
    localization: localization,
    enableGlobalFilter: false,
    enableTopToolbar: true,
    enablePagination: true,
    enableRowActions: true,
    enableColumnResizing: true,
    getRowId: (originalRow) => originalRow.id,
    filterFns: {
      dateEquals: (row, id, filterValue) => {
        const rowDate = new Date(row.original.plantation_date);
        const filterDate = filterValue ? filterValue : null;

        if (filterDate && rowDate) {
          rowDate.setTime(
            rowDate.getTime() + rowDate.getTimezoneOffset() * 60 * 1000
          );
        }

        return (
          !filterDate || (rowDate && filterDate.getTime() === rowDate.getTime())
        );
      },
    },
    initialState: {
      density: "compact",
      pagination: { pageIndex: 0, pageSize: 30 },
    },
    positionActionsColumn: "first",
    renderRowActions: ({ row }) => (
      <Box>
        <IconButton onClick={() => router.push(`/tree/${row.original.id}`)}>
          <EditIcon />
        </IconButton>
      </Box>
    ),
  });

  return (
    <Box>
      <MaterialReactTable table={table} />
    </Box>
  );
};

export default TreesGrid;
