import { MRT_TableInstance } from "material-react-table";
import { Box, Button } from "@mui/material";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { mkConfig } from "export-to-csv";
import { FC } from "react";
import { useTranslation } from "react-i18next";

export const csvConfig = mkConfig({
  fieldSeparator: ",",
  decimalSeparator: ".",
  useKeysAsHeaders: true,
});

interface IExportButtonsProps {
  table: MRT_TableInstance<any>;
  data: any;
  exportAllData: (rows: any[]) => void;
  exportFilteredData: (filteredData: any[]) => void;
}

const ExportButtons: FC<IExportButtonsProps> = ({
  table,
  data,
  exportFilteredData,
  exportAllData,
}) => {
  const { t } = useTranslation("common");
  const handleExportAll = () => {
    exportAllData(data);
  };

  const handleExportFiltered = () => {
    const filteredData = table.getPrePaginationRowModel().rows;
    exportFilteredData(filteredData);
  };
  return (
    <Box
      sx={{
        display: "flex",
        gap: "16px",
        padding: "8px",
        flexWrap: "wrap",
      }}
    >
      <Button onClick={handleExportAll} startIcon={<FileDownloadIcon />}>
        {t("buttons.exportAllData")}
      </Button>
      <Button
        disabled={table.getPrePaginationRowModel().rows.length === 0}
        onClick={handleExportFiltered}
        startIcon={<FileDownloadIcon />}
      >
        {t("buttons.exportFilteredData")}
      </Button>
    </Box>
  );
};

export default ExportButtons;
