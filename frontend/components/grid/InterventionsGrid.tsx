import { Box } from "@mui/material";
import { FC, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import {
  FetchAllInterventionsOfALocationQuery,
  FetchAllInterventionsOfATreeQuery,
  Intervention,
} from "../../generated/graphql";
import useTableLocale from "../../lib/useTableLocale";
import {
  MRT_ColumnDef,
  MRT_RowData,
  MaterialReactTable,
  useMaterialReactTable,
} from "material-react-table";
import { useRouter } from "next/router";
import { useGridContext } from "@context/grid/grid.context";
import ExportButtons, { csvConfig } from "./buttons/ExportButtons";
import { download, generateCsv } from "export-to-csv";

export interface InterventionsGridProps {
  allInterventions:
    | FetchAllInterventionsOfALocationQuery["intervention"]
    | FetchAllInterventionsOfATreeQuery["intervention"];

  columns: MRT_ColumnDef<Intervention>[];
  initialFilters: MRT_ColumnFiltersState[];
}

interface MRT_ColumnFiltersState {
  id: string;
  value: string;
}

export const InterventionsGrid: FC<InterventionsGridProps> = ({
  allInterventions,
  columns,
  initialFilters,
}) => {
  const router = useRouter();
  const { t } = useTranslation();
  const localization = useTableLocale();
  const [colFilters, setColsFilters] = useState<MRT_ColumnFiltersState[]>([]);
  const {
    rowSelected,
    setRowSelected,
    selectedLocationIds,
    setSelectedLocationIds,
  } = useGridContext() ?? {};

  const containsUndefined = initialFilters.some((obj: MRT_ColumnFiltersState) =>
    obj.value.includes("undefined")
  );

  useEffect(() => {
    if (!containsUndefined) {
      setColsFilters(initialFilters);
    }
    if (router.query.state && !router.query.type) {
      setColsFilters([
        { id: "state", value: t(`common.${router.query.state}`) },
      ]);
    }
  }, [initialFilters]);

  useEffect(() => {
    const selectedInterventionIds = rowSelected && Object.keys(rowSelected);
    const selectedLocationIds = (allInterventions as Intervention[])
      .filter((intervention: Intervention) =>
        selectedInterventionIds?.includes(intervention.id)
      )
      .map((intervention: Intervention) =>
        intervention.location_id !== null
          ? intervention.location_id
          : intervention.tree?.location_id
      );

    setSelectedLocationIds && setSelectedLocationIds(selectedLocationIds);
  }, [rowSelected]);

  const transformInterventionData = (item: any) => {
    let originalItem = item;
    if (item.original) {
      originalItem = item.original;
    }
    const {
      id,
      intervention_type,
      tree,
      location,
      scheduled_date,
      realization_date,
      intervention_partner,
      intervention_worksite_interventions,
      cost,
    } = originalItem;

    const hasWorksiteInterventions =
      intervention_worksite_interventions.length > 0;

    let type_intervention = t(
      `components.PanelTree.interventionsList.${intervention_type?.slug}`
    );
    if (hasWorksiteInterventions) {
      type_intervention = `Chantier: ${type_intervention}`;
    }

    let genre_espece = "";
    if (tree?.scientific_name) {
      genre_espece = tree.scientific_name;
    } else if (location?.location_status?.status) {
      const statusKey = location.location_status.status;
      genre_espece = t(
        `components.LocationForm.properties.status.${statusKey}`
      );
    } else {
      genre_espece = t(`components.LocationForm.properties.status.alive`);
    }
    const emplacement = tree ? tree.location.address : location?.address;
    const matricule = tree ? tree.serial_number : "";

    return {
      id,
      type_intervention,
      date_planifiee: scheduled_date,
      date_de_realisation: realization_date,
      intervenant: intervention_partner?.name,
      emplacement,
      genre_espece,
      matricule,
      cout: cost,
    };
  };

  const handleExport = (
    data:
      | FetchAllInterventionsOfALocationQuery["intervention"]
      | FetchAllInterventionsOfATreeQuery["intervention"]
      | MRT_RowData[]
  ) => {
    const rowData = data.map((item) => transformInterventionData(item));

    const csv = generateCsv(csvConfig)(rowData);
    download(csvConfig)(csv);
  };

  const table = useMaterialReactTable({
    columns: columns as any,
    data: allInterventions,
    localization: localization,
    enableGlobalFilter: false,
    enableTopToolbar: true,
    enablePagination: true,
    enableColumnResizing: true,
    filterFns: {
      dateRangeBetweenInclusive: (row, id, filterValues) => {
        const rowDate = new Date(row.original?.scheduled_date);

        const filterStartDate = filterValues[0] ? filterValues[0] : null;
        const filterEndDate = filterValues[1] ? filterValues[1] : null;

        if (filterStartDate || filterEndDate) {
          rowDate.setTime(
            rowDate.getTime() + rowDate.getTimezoneOffset() * 60 * 1000
          );
        }
        return (
          (!filterStartDate || rowDate >= filterStartDate) &&
          (!filterEndDate || rowDate <= filterEndDate)
        );
      },
    },
    renderTopToolbarCustomActions: (table) => (
      <ExportButtons
        table={table?.table}
        data={allInterventions}
        exportFilteredData={handleExport}
        exportAllData={handleExport}
      />
    ),
    enableRowSelection: (row) => {
      const selectedInterventionType = row.original.intervention_type.slug;
      const selectedInterventionIds = rowSelected && Object.keys(rowSelected);

      if (selectedInterventionIds?.length === 0) {
        return !row.original.realization_date;
      }

      const isSameInterventionType = allInterventions.some(
        (intervention) =>
          intervention.intervention_type.slug === selectedInterventionType &&
          selectedInterventionIds?.includes(intervention.id)
      );

      return isSameInterventionType && !row.original.realization_date;
    },
    onRowSelectionChange: setRowSelected,
    getRowId: (originalRow) => originalRow.id,
    state: {
      columnFilters: colFilters,
      rowSelection: rowSelected,
    },
    initialState: {
      showColumnFilters: colFilters.length > 0 && !containsUndefined,
      density: "compact",
      pagination: { pageIndex: 0, pageSize: 30 },
    },
    onColumnFiltersChange: setColsFilters,
    muiTableBodyRowProps: (e) => ({
      onClick: () =>
        router.replace(
          `/intervention?object=all&interventionModal=open&activeId=${e.row.id}`
        ),
      sx: {
        cursor: "pointer",
      },
    }),
  });

  return (
    <Box>
      <MaterialReactTable table={table} />
    </Box>
  );
};

export default InterventionsGrid;
