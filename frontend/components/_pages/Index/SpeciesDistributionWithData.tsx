import SpeciesDistribution from "@blocks/dashboard/SpeciesDistribution";
import Loading from "@components/layout/Loading";
import { getTaxonomy } from "@services/AnalyticsService";
import _ from "lodash";
import { useEffect, useState } from "react";
import { useDashboardFilters } from "@stores/pages/dashboard";
import { Alert } from "@mui/material";
import { TaxonomyData } from "types/analytics/analytics.types";

export const SpeciesDistributionWithData = () => {
  const [taxonomy, setTaxonomy] = useState();

  const dashboardFilters = useDashboardFilters();

  useEffect(() => {
    const fetchTaxonomy = async () => {
      try {
        const data = await getTaxonomy(dashboardFilters.boundaries);
        setTaxonomy(data);
      } catch (err) {
        console.error("Error fetching taxon data:", err);
      }
    };

    fetchTaxonomy();
  }, [dashboardFilters]);

  // if (loading) {
  //   return <Loading />;
  // }

  if (taxonomy) {
    return <SpeciesDistribution data={taxonomy} />;
  }

  return <Alert severity="info">Aucune donnée</Alert>;
};

export default SpeciesDistributionWithData;
