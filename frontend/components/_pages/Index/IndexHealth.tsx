import React, { useState, useEffect } from "react";
import { Alert, Box } from "@mui/material";
import { NonAllergenic, WellBeing } from "@components/_core/icons";
import Loading from "@components/layout/Loading";
import Health from "@components/_blocks/dashboard/Ilo/Health";
import { useDashboardFilters } from "@stores/pages/dashboard";
import { getHealthScore } from "@services/AnalyticsService";
import { convertToPercentage, useDeviceSize } from "@lib/utils";
import { useTranslation } from "react-i18next";
import { HealthData } from "types/analytics/analytics.types";

const IndexHealth = () => {
  const { t } = useTranslation(["components"]);
  const [healthData, setHealthData] = useState<HealthData | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const { isMobile } = useDeviceSize();
  const dashboardFilters = useDashboardFilters();

  useEffect(() => {
    const fetchHealthData = async () => {
      try {
        const data = await getHealthScore(dashboardFilters.boundaries);
        setHealthData(data);
        setLoading(false);
      } catch (err) {
        console.error("Error fetching health data:", err);
        setError(err);
        setLoading(false);
      }
    };

    fetchHealthData();
  }, [dashboardFilters.boundaries]);

  const customStyle = {
    height: isMobile ? "100%" : "250px",
    width: "200px",
  };

  const customBox = {
    ...customStyle,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  };

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <Alert severity="info">{t("Dashboard.noData")}</Alert>;
  }

  if (!healthData) {
    return <Alert severity="info">Aucune donnée</Alert>;
  }

  const healthDataComponents = [
    {
      component: (
        <Box sx={customBox} data-cy="dashboard-ecosystem-well-being-score">
          <WellBeing
            level={convertToPercentage(
              healthData.health_score.num_trees_no_felling_date,
              healthData.health_score.population
            )}
          />
        </Box>
      ),
      title: t("blocks.Health.analyticsModules.wellBeingScore"),
      unit:
        healthData.health_score.population &&
        healthData.health_score.num_trees_no_felling_date &&
        `1 arbre pour ${Number(
          (
            healthData.health_score.population /
            healthData.health_score.num_trees_no_felling_date
          ).toFixed(1)
        )} habitants`,
    },
    {
      component: (
        <Box sx={customBox} data-cy="dashboard-ecosystem-air-quality">
          {healthData.non_allergenic_score && (
            <NonAllergenic
              level={convertToPercentage(healthData.non_allergenic_score, 5)}
            />
          )}
        </Box>
      ),
      value: healthData.non_allergenic_score,
      unit: "/5",
      title: t("blocks.Health.analyticsModules.nonAllergenicScore"),
    },
  ];

  return <Health data={healthDataComponents} />;
};

export default IndexHealth;
