import React, { FC, useState, useEffect } from "react";
import { Box, Typography, Button, BoxProps } from "@mui/material";
import { useTranslation } from "react-i18next";

// ** MUI Imports
import Step from "@mui/material/Step";
import Stepper from "@mui/material/Stepper";
import StepLabel from "@mui/material/StepLabel";
import StepContent from "@mui/material/StepContent";
import { styled } from "@mui/material/styles";
import {
  useMapEditorActions,
} from "@stores/pages/mapEditor";

// ** Hooks Imports
import useBgColor, { UseBgColorType } from "@core/hooks/useBgColor";
// ** Third Party Imports
import clsx from "clsx";
// ** Custom Components Imports
import StepperCustomDot from "../../../_core/subcomponents/StepperCustomDot";

export interface Step {
  title: string;
  subtitle?: string | undefined;
  description: JSX.Element;
  validate: () => void;
}

export interface StepperSectionProps {
  onFinish: () => void;
  steps: Step[];
}

const StepperWrapper = styled(Box)<BoxProps>(({ theme }) => {
  // ** Hook
  const bgColors: UseBgColorType = useBgColor();

  return {
    [theme.breakpoints.down("md")]: {
      "& .MuiStepper-horizontal:not(.MuiStepper-alternativeLabel)": {
        flexDirection: "column",
        alignItems: "flex-start",
      },
    },
    "& .MuiStep-root": {
      "& .step-label": {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      },
      "& .step-number": {
        fontWeight: "bold",
        fontSize: "2.125rem",
        marginRight: theme.spacing(2.5),
        color: theme.palette.text.primary,
      },
      "& .step-title": {
        fontWeight: 600,
        fontSize: "0.875rem",
        color: theme.palette.text.primary,
      },
      "& .step-subtitle": {
        fontSize: "0.75rem",
        color: theme.palette.text.secondary,
      },
      "& .MuiStepLabel-root.Mui-disabled": {
        "& .step-number": {
          color: theme.palette.text.disabled,
        },
      },
      "& .Mui-error": {
        "& .MuiStepLabel-labelContainer, & .step-number, & .step-title, & .step-subtitle":
          {
            color: theme.palette.error.main,
          },
      },
    },
    "& .MuiStepConnector-root": {
      "& .MuiStepConnector-line": {
        borderWidth: 3,
        borderRadius: 3,
      },
      "&.Mui-active, &.Mui-completed": {
        "& .MuiStepConnector-line": {
          borderColor: theme.palette.primary.main,
        },
      },
      "&.Mui-disabled .MuiStepConnector-line": {
        borderColor: bgColors.primaryLight.backgroundColor,
      },
    },
    "& .MuiStepper-alternativeLabel": {
      "& .MuiStepConnector-root": {
        top: 10,
      },
      "& .MuiStepLabel-labelContainer": {
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        "& > * + *": {
          marginTop: theme.spacing(1),
        },
      },
    },
    "& .MuiStepper-vertical": {
      "& .MuiStep-root": {
        "& .step-label": {
          justifyContent: "flex-start",
        },
        "& .MuiStepContent-root": {
          borderWidth: 3,
          marginLeft: theme.spacing(1.25),
          borderColor: theme.palette.primary.main,
        },
        "& .button-wrapper": {
          marginTop: theme.spacing(4),
        },
        "&.active + .MuiStepConnector-root .MuiStepConnector-line": {
          borderColor: theme.palette.primary.main,
        },
      },
      "& .MuiStepConnector-root": {
        marginLeft: theme.spacing(1.25),
        "& .MuiStepConnector-line": {
          borderRadius: 0,
        },
      },
    },
  };
});

const StepperSection: FC<StepperSectionProps> = ({ onFinish, steps }) => {
  const { t } = useTranslation(["components", "common"]);

  // ** States
  const [activeStep, setActiveStep] = useState<number>(0);
  const [failedStep, setFailedStep] = useState<number | undefined>(undefined);
  const { renderSelectionLayer } =
  useMapEditorActions();

  // Handle Stepper
  const handleBack = () =>
    setActiveStep((prevActiveStep) => prevActiveStep - 1);

  const handleNext = () => {
    const currentStep = steps[activeStep];
    const isValid = currentStep.validate();
    //@ts-ignore
    if (!isValid) {
      setFailedStep(activeStep);
    } else {
      setFailedStep(undefined);
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
      if (activeStep === steps.length - 1) {
        onFinish();
      }
    }
  };

  useEffect(() => {
    if (activeStep != 1) {
      renderSelectionLayer(false);
    }
  }, [renderSelectionLayer, activeStep]);

  const isStepFailed = (step: number) => failedStep === step;
  return (
    <StepperWrapper data-cy="worksite-stepper-wrapper">
      <Stepper
        activeStep={activeStep}
        orientation="vertical"
        data-cy="worksite-stepper"
      >
        {steps.map((step, index) => {
          const labelProps: {
            optional?: React.ReactNode;
            error?: boolean;
          } = {};
          if (isStepFailed(index)) {
            labelProps.optional = (
              <Typography variant="caption" color="error">
                {t("common.errors.fillInRequiredFields")}
              </Typography>
            );
            labelProps.error = true;
          }

          return (
            <Step
              key={index}
              className={clsx({ active: activeStep === index })}
              data-cy={`worksite-stepper-step-${index}`}
            >
              <StepLabel
                StepIconComponent={StepperCustomDot}
                {...labelProps}
                data-cy={`worksite-stepper-step-${index}-label`}
              >
                <div className="step-label">
                  <Typography className="step-number">{`0${
                    index + 1
                  }`}</Typography>
                  <div>
                    <Typography className="step-title">{step.title}</Typography>
                    <Typography className="step-subtitle">
                      {step.subtitle}
                    </Typography>
                  </div>
                </div>
              </StepLabel>
              <StepContent data-cy={`worksite-stepper-step-${index}-content`}>
                <Typography>{step.description}</Typography>
                <div className="button-wrapper">
                  <Button
                    size="small"
                    color="secondary"
                    variant="outlined"
                    onClick={handleBack}
                    disabled={activeStep === 0}
                    data-cy={`worksite-stepper-step-${index}-back-button`}
                  >
                    {t("common.back")}
                  </Button>
                  <Button
                    size="small"
                    variant="contained"
                    onClick={handleNext}
                    sx={{ ml: 4 }}
                    data-cy={`worksite-stepper-step-${index}-next-save-button`}
                  >
                    {activeStep === steps.length - 1
                      ? t("common.buttons.finish")
                      : t("common.buttons.next")}
                  </Button>
                </div>
              </StepContent>
            </Step>
          );
        })}
      </Stepper>
    </StepperWrapper>
  );
};

export default StepperSection;
