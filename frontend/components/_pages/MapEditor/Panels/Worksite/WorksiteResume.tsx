import { useTranslation } from "react-i18next";
import {
  useWorksiteFormData,
  useWorksiteInterventionFormData,
  useWorksiteLocationFormData,
} from "@stores/forms/worksiteForm";
import { Box, Stack, Typography } from "@mui/material";
import { formatDate } from "@lib/utils";

const WorksiteResume = () => {
  /** HOOKS **/
  const { t } = useTranslation(["components"]);
  const worksite = useWorksiteFormData();
  const intervention = useWorksiteInterventionFormData();
  const locations = useWorksiteLocationFormData();

  return (
    <Stack direction="column">
      <Box>
        {t(`WorksiteForm.name`)} : {worksite.name}
      </Box>
      <Box>
        {t(`Intervention.properties.interventionType`)} :{" "}
        {t(
          `components.PanelTree.interventionsList.${intervention?.intervention_type.slug}`
        )}
      </Box>
      <Box>
        {t(`Intervention.properties.scheduledDate`)} :{" "}
        {intervention && formatDate(intervention.scheduled_date!.toString())}
      </Box>
      <Box>
        {t(`LayerManager.layers.Locations`)} :{" "}
        <ul>
          {locations.map((location) => (
            <li key={location.location_id}>{location.address}</li>
          ))}
        </ul>
      </Box>
    </Stack>
  );
};

export default WorksiteResume;
