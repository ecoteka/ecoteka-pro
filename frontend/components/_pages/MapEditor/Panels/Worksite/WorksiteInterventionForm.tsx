import InterventionForm from "@components/forms/intervention/InterventionForm";
import { Button } from "@mui/material";
import {
  useWorksiteLocationFormData,
  useWorksiteFormData,
} from "@stores/forms/worksiteForm";
import {
  useFetchInterventionTypesLazyQuery,
  useFetchInterventionPartnersLazyQuery,
} from "generated/graphql";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";

export interface IWorksiteInterventionFormProps {
  goToPreviousStep: () => void;
  goToNextStep: () => void;
}

const WorksiteInterventionForm = ({}) => {
  /** Hooks */
  const { t } = useTranslation(["common"]);
  const [fetchInterventionsTypes, { data: interventionsTypes }] =
    useFetchInterventionTypesLazyQuery();
  const [fetchInterventionPartners, { data: interventionPartners }] =
    useFetchInterventionPartnersLazyQuery();
  const worksiteLocationsFormData = useWorksiteLocationFormData();
  const worksiteFormData = useWorksiteFormData();

  useEffect(() => {
    const status = worksiteFormData?.location_status;
    fetchInterventionPartners();
    fetchInterventionsTypes({
      variables: {
        _has_key:
          status.status === "alive"
            ? "tree"
            : status.status === "empty"
            ? "empty_location"
            : "stump",
      },
    });
  }, []);

  return (
    <>
      <InterventionForm
        hasTree={worksiteFormData?.location_status.status === "alive"}
        activeLocation={worksiteLocationsFormData[0]}
        interventionTypes={
          interventionsTypes ? interventionsTypes?.intervention_type : []
        }
        interventionPartners={
          interventionPartners ? interventionPartners?.intervention_partner : []
        }
      />
    </>
  );
};

export default WorksiteInterventionForm;
