import React, { FC, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Stack } from "@mui/material";
import _ from "lodash";

import {
  useMapEditorActions,
  useMapFilters,
  useMapLocations,
  useMapSelectedLocations,
} from "@stores/pages/mapEditor";
import { confFilters, setWhereConfig } from "@components/panel/Filters";
import {
  useCreateOneWorksiteWithRelationshipsMutation,
  useFetchLocationsWithFiltersLazyQuery,
} from "generated/graphql";
import {
  useWorksiteData,
  useWorksiteFormActions,
  useWorksiteFormData,
  useWorksiteInterventionFormData,
  useWorksiteLocationFormData,
} from "@stores/forms/worksiteForm";

import { useSession } from "next-auth/react";
import useStore from "@stores/useStore";
import { useRouter } from "next/router";

import StepperSection from "@ecosystems/MapEditor/Stepper/StepperSection";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import WorkSiteForm from "./WorksiteForm";
import WorksiteLocationSelector from "./LocationSelector";
import WorksiteInterventionForm from "./WorksiteInterventionForm";

interface InterventionDataItem {
  intervention: {
    data: {
      // Define properties and their types here
      location_id: string | null;
      realization_date: null;
      scheduled_date: string | Date | null | undefined;
      note: string | undefined;
      tree_id: any;
      data_entry_user_id: string;
      intervention_partner_id: string | null;
      intervention_type_id: string;
    };
  };
}

const WorksitePanel: FC = () => {
  /** HOOKS */
  const { t } = useTranslation(["components", "common"]);
  const router = useRouter();
  const [searchLocations, { data: locationsFiltered }] =
    useFetchLocationsWithFiltersLazyQuery();
  const [createWorksite] = useCreateOneWorksiteWithRelationshipsMutation();
  const mapFilters = useMapFilters();
  const mapLocations = useMapLocations();
  const mapSelectedLocations = useMapSelectedLocations();
  const worksite = useWorksiteFormData();
  const locations = useWorksiteLocationFormData();
  const intervention = useWorksiteInterventionFormData();
  const {
    setMapFilteredLocations,
    setMapPanelOpen,
    setMapActiveAction,
  } = useMapEditorActions();
  const { setLocations, setInterventions } = useWorksiteFormActions();
  const { setFiltersWithMartin } = useFiltersWithMartinContext()!;
  const { renderSelectionLayer, setMapSelectedLocations } =
    useMapEditorActions();
  const { app } = useStore((store) => store);
  const session = useSession();
  const worksiteData = useWorksiteData();
  const currentUserId = session?.data?.user?.id ? session?.data?.user?.id : "";
  const validateWorksiteForm = () => {
    if (
      !worksiteData.worksite.name ||
      !worksiteData.worksite.location_status.id
    ) {
      return false;
    }
    return true;
  };

  const validateLocationsForm = () => {
    if (worksiteData.locations.length === 0) {
      return false;
    }
    return true;
  };

  const validateInterventionsForm = () => {
    if (
      !worksiteData.interventions?.intervention_type ||
      !worksiteData.interventions?.scheduled_date
    ) {
      return false;
    }
    return true;
  };
  const steps = [
    {
      title: t("WorksiteStep.idLabels.worksite"),
      subtitle: t("WorksiteForm.name"),
      description: <WorkSiteForm />,
      validate: validateWorksiteForm,
    },
    {
      title: t("WorksiteStep.idLabels.locations"),
      subtitle: t("WorksiteStep.idLabels.selectedLocations"),
      description: <WorksiteLocationSelector />,
      validate: validateLocationsForm,
    },
    {
      title: t("WorksiteStep.idLabels.intervention"),
      description: <WorksiteInterventionForm />,
      validate: validateInterventionsForm,
    },
  ];

  const debouncedFetchSearchResults = _.debounce(() => {
    const where = setWhereConfig(mapFilters, confFilters);
    searchLocations({ variables: { where } });
  }, 350);

  const onComplete = async (result: string) => {
    app.setAppState({
      snackbar: {
        ...app.snackbar,
        alerts: [
          {
            message:
              result === "success"
                ? t(`components.WorksiteForm.success.create`)
                : "Une erreur est survenue",
            severity: result === "success" ? "success" : "error",
          },
        ],
      },
    });
    setInterventions(undefined);
    setMapSelectedLocations(undefined);
    setMapFilteredLocations(undefined);
    setMapActiveAction(undefined);
    renderSelectionLayer(false);
    setFiltersWithMartin({});
    setMapPanelOpen(false);
    result === "success" && router.push("/worksites");
  };

  // reminders for final steps; not used here
  const onSubmitFinalStep = async () => {
    try {
      const locationData = locations.map((location) => ({
        location_id: location.location_id,
      }));
      const interventionData: InterventionDataItem[] = [];

      locations.forEach((location) => {
        interventionData.push({
          intervention: {
            data: {
              location_id:
                worksite.location_status.status !== "alive"
                  ? location.location_id
                  : null,
              realization_date: null,
              scheduled_date: intervention ? intervention.scheduled_date : null,
              note: intervention?.note || "",
              tree_id:
                worksite.location_status.status === "alive"
                  ? location.tree_id
                  : null,
              data_entry_user_id: currentUserId,
              intervention_partner_id:
                intervention && intervention.intervention_partner
                  ? intervention.intervention_partner
                  : null,
              intervention_type_id: intervention
                ? intervention.intervention_type.id
                : "",
            },
          },
        });
      });

      const variables = {
        object: {
          name: worksite.name,
          location_status: worksite.location_status.status,
          description: worksite.description,
          scheduled_date: interventionData[0].intervention.data.scheduled_date,
          locations: { data: locationData },
          interventions: { data: interventionData },
        },
      };

      createWorksite({ variables, onCompleted: () => onComplete("success") });
    } catch (err) {
      console.log(`foo = `, err);
      onComplete("failure");
    }
  };

  /** USE EFFECTS */

  /*** handling filters on location_status change */
  // todo: business logic should be refact and move to store
  useEffect(() => {
    debouncedFetchSearchResults();
    return () => {
      debouncedFetchSearchResults.cancel();
    };
  }, [mapFilters.locationStatus]);

  useEffect(() => {
    const hasFilterValues = mapFilters.locationStatus !== "";

    if (locationsFiltered?.locations && hasFilterValues) {
      setMapFilteredLocations(locationsFiltered.locations);
    }
  }, [locationsFiltered]);

  useEffect(() => {
    const hasNoFilterValues = mapFilters.locationStatus === "";

    if (hasNoFilterValues) {
      setMapFilteredLocations(mapLocations);
    }
  }, [mapFilters.locationStatus]);
  /*** end of handling filters on location_status change */

  useEffect(() => {
    mapSelectedLocations &&
      setLocations(
        mapSelectedLocations.map((location) => {
          return {
            location_id: location?.location_id,
            tree_id: location?.tree_id,
            address: location.location_address || "",
          };
        })
      );
    !mapSelectedLocations && setLocations([]);
  }, [mapSelectedLocations]);

  return (
    <Stack data-cy="worksite-form-container">
      <StepperSection steps={steps} onFinish={onSubmitFinalStep} />
    </Stack>
  );
};

export default WorksitePanel;
