import {
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";
import { Controller, useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useTranslation } from "react-i18next";
import { useEffect } from "react";
import {
  useWorksiteFormActions,
  useWorksiteFormData,
} from "@stores/forms/worksiteForm";
import { FormInputError } from "@components/forms/error/FormInputError";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import { useFetchLocationStatusLazyQuery } from "@generated/graphql";

const worksiteSchema = yup.object().shape({
  name: yup.string().required(),
  description: yup.string().nullable(),
  location_status: yup.string().required(),
});

const WorkSiteForm = ({}) => {
  /** HOOKS **/
  const { t } = useTranslation(["components", "common"]);
  const [fetchLocationStatuses, { data: locationStatuses }] =
    useFetchLocationStatusLazyQuery();
  const { setFiltersWithMartin } = useFiltersWithMartinContext()!;
  const { setWorksite } = useWorksiteFormActions();
  const formData = useWorksiteFormData();

  const {
    handleSubmit,
    control,
    watch,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(worksiteSchema),
    defaultValues: {
      name: formData.name,
      description: formData.description,
      location_status: "",
    },
  });

  /** private methods */
  const location_status = watch("location_status");
  const name = watch("name");
  const description = watch("description");

  const onSubmit = (data) => {
    setWorksite(data);
  };

  useEffect(() => {
    fetchLocationStatuses();
  }, []);

  useEffect(() => {
    setFiltersWithMartin((prevState) => ({
      ...prevState,
      location_status: [location_status],
    }));
  }, [location_status]);

  useEffect(() => {
    const worksiteData = {
      name: name,
      description: description,
      location_status: {
        id: location_status,
        status:
          locationStatuses?.status.find((item) => item.id === location_status)
            ?.status || "",
      },
    };
    setWorksite(worksiteData);
  }, [location_status, name, description]);

  return (
    <form onSubmit={handleSubmit(onSubmit)} noValidate>
      <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <TextField
            {...field}
            required
            fullWidth
            label={t(`WorksiteForm.name`) as string}
            variant="filled"
            placeholder={t(`WorksiteForm.name`)}
            sx={{ mb: 1 }}
          />
        )}
      />
      <FormInputError name={"name"} errors={errors} />
      <FormControl fullWidth>
        <InputLabel id="label_location_status" required>
          {t(`WorksiteForm.location_status`) as string}
        </InputLabel>
        <Controller
          name="location_status"
          control={control}
          render={({ field }) => (
            <Select
              required={true}
              {...field}
              labelId="label_location_status"
              label={t(`WorksiteForm.location_status`) as string}
              variant="filled"
              error={!!errors?.location_status}
            >
              <MenuItem value="">
                <em>---</em>
              </MenuItem>
              {locationStatuses?.status?.map((option: any, i: React.Key) => {
                return (
                  <MenuItem key={i} value={option.id}>
                    {
                      t(
                        `LocationForm.properties.status.${option.status}`
                      ) as string
                    }
                  </MenuItem>
                );
              })}
            </Select>
          )}
        />
      </FormControl>
      <FormInputError name={"location_status"} errors={errors} />
      <Controller
        name={"description"}
        control={control}
        render={({ field }) => (
          <TextField
            {...field}
            fullWidth
            label={t(`WorksiteForm.description`) as string}
            variant="filled"
            placeholder={t(`WorksiteForm.description`)}
            sx={{ mt: 1 }}
          />
        )}
      />
    </form>
  );
};

export default WorkSiteForm;
