import React, { FC, useEffect } from "react";
import {
  Alert,
  AlertTitle,
  IconButton,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
} from "@mui/material";
import { useWorksiteFormActions } from "@stores/forms/worksiteForm";
import {
  useMapEditorActions,
  useMapSelectedLocations,
} from "@stores/pages/mapEditor";
import { useTranslation } from "react-i18next";
import CloseIcon from "@mui/icons-material/Close";

export interface IWorksiteLocationSelectorProps {
  goToPreviousStep?: () => void;
  goToNextStep?: () => void;
}

const WorksiteLocationSelector: FC<IWorksiteLocationSelectorProps> = ({}) => {
  /** hooks */
  const { t } = useTranslation(["components"]);
  const { setLocations } = useWorksiteFormActions();
  const { renderSelectionLayer, removeFromMapSelectedLocations } =
    useMapEditorActions();
  const mapSelectedLocations = useMapSelectedLocations();

  useEffect(() => {
    renderSelectionLayer(true);
  }, []);

  useEffect(() => {
    if (mapSelectedLocations) {
      setLocations(
        mapSelectedLocations.map((location) => {
          return {
            location_id: location.location_id,
            tree_id: location.tree_id,

            address: location.location_address || "",
          };
        })
      );
    }
  }, [mapSelectedLocations]);

  const handleRemoveLocation = (locationId) => {
    removeFromMapSelectedLocations(locationId);
  };

  return (
    <>
      {!mapSelectedLocations?.length && (
        <Alert severity="info">
          <AlertTitle>{t(`WorksiteForm.locations.title`)}</AlertTitle>
          {t(`WorksiteForm.locations.text`)}
        </Alert>
      )}
      <List sx={{ p: 0, maxHeight: "300px", overflowY: "auto" }}>
        <Alert severity="info">
          <AlertTitle>{t(`WorksiteForm.locations.infos`)}</AlertTitle>
        </Alert>
        {mapSelectedLocations?.map((feature) => (
          <ListItem sx={{ p: 0 }}>
            <ListItemText
              primary={feature.location_address}
              secondary={feature.tree_serial_number || ""}
            />
            <ListItemSecondaryAction>
              <IconButton
                edge="end"
                onClick={() => handleRemoveLocation(feature.location_id)}
              >
                <CloseIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}{" "}
      </List>
    </>
  );
};

export default WorksiteLocationSelector;
