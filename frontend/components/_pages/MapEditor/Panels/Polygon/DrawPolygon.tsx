import React, { FC, useEffect, useState } from "react";
import { Alert, AlertTitle, FormControlLabel, Switch } from "@mui/material";

import { useMapEditorActions } from "@stores/pages/mapEditor";
import { useTranslation } from "react-i18next";
import {
  usePolygonFormActions,
  usePolygonFormData,
} from "@stores/forms/polygonForm";

export interface IDrawPolygonProps {
  goToPreviousStep?: () => void;
  goToNextStep?: () => void;
}

const DrawPolygon: FC<IDrawPolygonProps> = ({}) => {
  /** hooks */
  const { t } = useTranslation(["components", "common"]);
  const polygonData = usePolygonFormData();
  const { renderSelectionLayer } = useMapEditorActions();
  const { setPolygon } = usePolygonFormActions();
  const [openDialog, setOpenDialog] = useState(false);

  useEffect(() => {
    renderSelectionLayer(true);
  }, []);

  const handleRemovePolygon = (locationId) => {
    console.log("remove");
  };

  const handleAssociationChange = () => {
    setOpenDialog(false);
    setPolygon({
      ...polygonData,
      automaticAssociation: !polygonData.automaticAssociation,
    });
  };

  useEffect(() => {
    if (polygonData?.locationsInside?.length > 0) {
      setOpenDialog(true);
    } else {
      setOpenDialog(false);
    }
  }, [polygonData?.locationsInside]);

  return (
    <>
      <Alert severity="info">
        <AlertTitle>{t(`GreenAreaForm.drawPolygon.title`)}</AlertTitle>
        {t(`PolygonForm.draw`)}
      </Alert>
      <FormControlLabel
        control={
          <Switch
            checked={polygonData.automaticAssociation}
            onChange={handleAssociationChange}
            name="automaticAssociation"
            color="primary"
          />
        }
        label={t(`components.PolygonForm.associationMessage`)}
      />
    </>
  );
};

export default DrawPolygon;
