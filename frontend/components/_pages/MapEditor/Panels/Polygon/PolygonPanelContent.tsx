import React, { FC, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { InputLabel, MenuItem, Select, Stack, TextField } from "@mui/material";
import _ from "lodash";
import { useSession } from "next-auth/react";
import useStore from "@stores/useStore";
import { useRouter } from "next/router";

import StepperSection from "@ecosystems/MapEditor/Stepper/StepperSection";
import DrawPolygon from "./DrawPolygon";
import {
  useCreateBoundaryAndDeleteExistingAssociationsMutation,
  useCreateBoundaryMutation,
  useInsertLocationBoundariesMutation,
} from "@generated/graphql";
import { useMapEditorActions } from "@stores/pages/mapEditor";
import {
  usePolygonFormActions,
  usePolygonFormData,
} from "@stores/forms/polygonForm";

interface PolygonPanelProps {}

const PolygonPanel: FC<PolygonPanelProps> = ({}) => {
  /** HOOKS */
  const { t } = useTranslation(["components", "common"]);
  const router = useRouter();
  const [polygonType, setPolygonType] = useState<string>("");
  const [polygonName, setPolygonName] = useState<string>("");

  const { app } = useStore((store) => store);
  const session = useSession();
  const [createBoundary] = useCreateBoundaryMutation();
  const [createBoundaryAndDeleteExistingAssociations] =
    useCreateBoundaryAndDeleteExistingAssociationsMutation();
  const [insertLocationBoundaries] = useInsertLocationBoundariesMutation();
  const currentUserId = session?.data?.user?.id ? session?.data?.user?.id : "";

  const polygonData = usePolygonFormData();
  const {
    renderSelectionLayer,
    setMapSelectedLocations,
    setMapPanelOpen,
    setForceReloadLayer,
  } = useMapEditorActions();
  const { setPolygon } = usePolygonFormActions();

  const validatePolygonForm = () => {
    if (!polygonData.geometry) {
      return false;
    }
    return true;
  };

  const steps = [
    {
      title: t(`components.PolygonForm.label`), // Title for the step
      description: (
        <>
          <InputLabel required>
            {t(`components.PolygonForm.polygonTypes.label`)}
          </InputLabel>
          <Select
            value={polygonType}
            onChange={(e) => setPolygonType(e.target.value)}
            fullWidth
            variant="filled"
            required
          >
            <MenuItem value="geographic">
              {t(`components.PolygonForm.polygonTypes.geographic`)}
            </MenuItem>
            <MenuItem value="station">
              {t(`components.PolygonForm.polygonTypes.station`)}
            </MenuItem>
          </Select>

          <TextField
            required
            fullWidth
            onChange={(e) => setPolygonName(e.target.value)}
            label={t(`PolygonForm.name`) as string}
            variant="filled"
            placeholder={t(`GreenAreaForm.name`)}
            sx={{ mb: 1 }}
            value={polygonName}
          />
        </>
      ),
      validate: () => (polygonType !== "" ? true : false),
    },
    {
      title: t("GreenAreaStep.idLabels.drawPolygon"),
      description: <DrawPolygon />,
      validate: validatePolygonForm,
    },
  ];

  const onComplete = async (result: string) => {
    app.setAppState({
      snackbar: {
        ...app.snackbar,
        alerts: [
          {
            message:
              result === "success"
                ? t(`components.PolygonForm.success.create.${polygonType}`)
                : "Une erreur est survenue",
            severity: result === "success" ? "success" : "error",
          },
        ],
      },
    });
    setMapSelectedLocations(undefined);
    renderSelectionLayer(false);
    setMapPanelOpen(false);
    setPolygon({
      type: "",
      name: "",
      geometry: {},
      area: null,
      locationsInside: [],
    });
    setForceReloadLayer(true);
  };

  const onSubmitFinalStep = async () => {
    try {
      if (polygonType === "green_area") {
        /* // create green_area
        createGreenArea({
          variables: {
            coords: {
              type: "Polygon",
              coordinates: greenAreaData?.geometry?.coordinates,
            },
            name: greenAreaData.name,
            description: greenAreaData.description,
          },
          onCompleted: () => onComplete("success"),
        }); */
      } else if (["geographic", "station"].includes(polygonType)) {
        const automaticAssociation = polygonData?.automaticAssociation;
        if (automaticAssociation) {
          createBoundaryAndDeleteExistingAssociations({
            variables: {
              coords: {
                type: "Polygon",
                coordinates: polygonData?.geometry?.coordinates,
              },
              name: polygonData.name,
              type: polygonType,
              locationsIds: polygonData.locationsInside,
            },
            onCompleted: (response) => {
              Promise.all(
                polygonData.locationsInside.map(async (id) => {
                  await insertLocationBoundaries({
                    variables: {
                      location_id: id,
                      boundary_id: response?.insert_boundary_one?.id,
                    },
                    onCompleted: () => onComplete("success"),
                  });
                })
              );
            },
          });
        } else {
          createBoundary({
            variables: {
              coords: {
                type: "Polygon",
                coordinates: polygonData?.geometry?.coordinates,
              },
              name: polygonData.name,
              type: polygonType,
            },
            onCompleted: () => onComplete("success"),
          });
        }
        // here get automaticAssociation from the store
        // if true create boundary and then delete loc_boundaries and insert loc_boundaries for each locationsIds
        // if false just createBoundary
        // create boundary with geom
      }
    } catch (err) {
      console.log(`foo = `, err);
      onComplete("failure");
    }
  };

  useEffect(() => {
    const polygon = {
      name: polygonName,
      type: polygonType,
    };
    setPolygon(polygon);
  }, [polygonName]);

  return (
    <Stack data-cy="worksite-form-container">
      <StepperSection onFinish={onSubmitFinalStep} steps={steps} />
    </Stack>
  );
};

export default PolygonPanel;
