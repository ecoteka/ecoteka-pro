import {
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";
import { Controller, useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useTranslation } from "react-i18next";
import { useEffect } from "react";
import {
  useWorksiteFormActions,
  useWorksiteFormData,
} from "@stores/forms/worksiteForm";
import { FormInputError } from "@components/forms/error/FormInputError";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import { useFetchLocationStatusLazyQuery } from "@generated/graphql";
import {
  useGreenAreaFormActions,
  useGreenAreaFormData,
} from "@stores/forms/greenAreaForm";

const greenAreaSchema = yup.object().shape({
  name: yup.string().required(),
  description: yup.string().nullable(),
});

const GreenAreaForm = ({}) => {
  /** HOOKS **/
  const { t } = useTranslation(["components", "common"]);
  const { setGreenArea } = useGreenAreaFormActions();
  const formData = useGreenAreaFormData();

  const {
    handleSubmit,
    control,
    watch,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(greenAreaSchema),
    defaultValues: {
      name: "",
      description: "",
      geometry: null,
      area: null,
    },
  });

  /** private methods */
  const name = watch("name");
  const description = watch("description");

  const onSubmit = (data) => {
    setGreenArea(data);
  };

  useEffect(() => {
    const greenAreaData = {
      name: name,
      description: description,
    };
    setGreenArea(greenAreaData);
  }, [name, description]);

  return (
    <form onSubmit={handleSubmit(onSubmit)} noValidate>
      <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <TextField
            {...field}
            required
            fullWidth
            label={t(`GreenAreaForm.name`) as string}
            variant="filled"
            placeholder={t(`GreenAreaForm.name`)}
            sx={{ mb: 1 }}
          />
        )}
      />
      <FormInputError name={"name"} errors={errors} />

      <Controller
        name={"description"}
        control={control}
        render={({ field }) => (
          <TextField
            {...field}
            fullWidth
            label={t(`GreenAreaForm.description`) as string}
            variant="filled"
            placeholder={t(`GreenAreaForm.description`)}
            sx={{ mt: 1 }}
          />
        )}
      />
    </form>
  );
};

export default GreenAreaForm;
