import React, { FC, useEffect, useState } from "react";
import { Box, Typography, IconButton, Divider } from "@mui/material";
import { useTranslation } from "react-i18next";
// ** MUI Imports
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import CardHeader from "@mui/material/CardHeader";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import SentimentSatisfiedAltIcon from "@mui/icons-material/SentimentSatisfiedAlt";
import CalendarTodayIcon from "@mui/icons-material/CalendarToday";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import { useRouter } from "next/router";
import getConfig from "next/config";
import {
  calculateTreeAge,
  checkIfImageExists,
  useDeviceSize,
} from "@lib/utils";

export interface CardInfosProps {
  handleCloseCardInfos: () => void;
  infos: {
    id?: string;
    status_name?: string;
    location_id?: string;
    tree_id?: string;
    tree_scientific_name?: string;
    tree_vernacular_name?: string;
    tree_serial_number?: string;
    plantation_date?: string;
    tree_height?: string;
    boundary_id?: string;
    boundary_name?: string;
    boundary_type?: string;
  };
}

const CardInfos: FC<CardInfosProps> = ({ handleCloseCardInfos, infos }) => {
  const { publicRuntimeConfig } = getConfig();
  const { t } = useTranslation(["common", "components"]);
  const router = useRouter();
  const { isMobile } = useDeviceSize();
  const [imageUrl, setImageUrl] = useState<string>("");
  const [hasImage, setHasImage] = useState<boolean>(false);
  const isTree =
    infos.tree_id && !["stump", "empty"].includes(infos.status_name as string);
  const isBoundary = infos.boundary_id !== undefined;

  useEffect(() => {
    if (isTree) {
      setImageUrl(
        `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/${publicRuntimeConfig.DEFAULT_STORAGE_BUCKET_NAME}/trees/${infos.tree_id}/latest.png`
      );
    }
  }, [infos]);

  useEffect(() => {
    checkIfImageExists(imageUrl, (exists) => {
      if (exists) {
        setHasImage(true);
      } else {
        setHasImage(false);
      }
    });
  }, [imageUrl]);

  return (
    <Card
      sx={[
        styles.cardModal,
        {
          width: isMobile ? "100%" : "370px",
          bottom: isMobile ? 0 : 10,
          left: isMobile ? 0 : 10,
        },
      ]}
      data-cy="map-card-infos"
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            flex: "1 0 auto",
          }}
        >
          <CardMedia
            sx={{
              height: "110px",
              width: "140px",
              m: 1,
              backgroundSize: "contain",
            }}
            image={
              imageUrl !== "" && hasImage
                ? imageUrl
                : isTree
                  ? "/components/map/location/tree.png"
                  : isBoundary
                    ? "/components/map/polygon/polygon.png"
                    : infos.status_name === "stump"
                      ? "/components/map/location/stump.png"
                      : "/components/map/location/empty.png"
            }
            data-cy="map-card-media"
          >
            <IconButton
              color="default"
              aria-label="Close"
              sx={{
                position: "absolute",
                top: 0,
                p: 0,
              }}
              onClick={() => handleCloseCardInfos()}
              data-cy="map-card-infos-close"
            >
              <HighlightOffIcon />
            </IconButton>{" "}
          </CardMedia>
        </Box>
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "flex-end",
          flex: "1 0 auto",
        }}
        data-cy="map-card-infos-box"
      >
        <CardHeader
          title={
            <div
              style={{
                overflow: "hidden",
                textOverflow: "ellipsis",
                whiteSpace: "pre-wrap",
                maxWidth: "130px",
                fontSize: "14px",
              }}
            >
              {isTree
                ? infos.tree_scientific_name
                : infos.boundary_name || t(`common.${infos.status_name}`)}
            </div>
          }
          subheader={
            <div
              style={{
                overflow: "hidden",
                textOverflow: "ellipsis",
                whiteSpace: "pre-wrap",
                maxWidth: "130px",
              }}
            >
              {isTree
                ? infos.tree_serial_number
                : infos.status_name
                  ? ""
                  : t(
                      `components.PolygonForm.polygonTypes.${infos.boundary_type}`
                    )}
            </div>
          }
          action={
            <IconButton
              aria-label="go to tree card"
              sx={{
                overflow: "hidden",
                p: 0,
                display: isBoundary ? "none" : "block",
              }}
              onClick={() =>
                isTree
                  ? router.push(`/tree/${infos.tree_id}`)
                  : router.push(`/location/${infos.location_id}`)
              }
            >
              <ArrowForwardIosIcon />
            </IconButton>
          }
          sx={{ paddingBottom: "0px", overflow: "hidden" }}
        />

        <CardContent sx={{ paddingBottom: "16px !important" }}>
          <Divider light />
          {isTree && (
            <Box
              sx={{
                paddingTop: "16px",
                display: "flex",
                alignItems: "center",
                color: "text.secondary",
                gap: "24px",
              }}
              data-cy="map-card-infos-tree"
            >
              <Box
                sx={{ display: "flex", gap: "8px", alignItems: "center" }}
                data-cy="map-card-infos-tree-age"
              >
                <CalendarTodayIcon sx={{ fontSize: "1rem" }} />
                {isTree && infos.plantation_date && (
                  <Typography sx={{ fontSize: "0.875rem" }}>
                    {calculateTreeAge(infos.plantation_date)?.years}
                    {t("age.years")},{" "}
                    {calculateTreeAge(infos.plantation_date)?.months}
                    {t("age.months")}
                  </Typography>
                )}
              </Box>
              <Box
                sx={{ display: "flex", gap: "8px", alignItems: "center" }}
                data-cy="map-card-infos-tree-height"
              >
                <ArrowUpwardIcon sx={{ fontSize: "1rem" }} />
                <Typography sx={{ fontSize: "0.875rem" }}>
                  {isTree && infos.tree_height} m
                </Typography>
              </Box>
            </Box>
          )}
        </CardContent>
      </Box>
    </Card>
  );
};

export default CardInfos;

const styles = {
  cardModal: {
    position: "absolute",

    height: "180px",
    bgcolor: "background.paper",
    borderRadius: "10px",
    display: "flex",
  },
  text: {
    top: "30%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    textTransform: "uppercase",
    position: "absolute",
    color: "#696B6D",
    opacity: "0.8",
  },
};
