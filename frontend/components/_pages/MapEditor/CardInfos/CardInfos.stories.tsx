import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import CardInfos, { CardInfosProps } from "./CardInfos";

const meta = {
  title: "Pages/MapEditor/CardInfos",
  component: CardInfos,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Card tree infos",
      },
    },
  },
} satisfies Meta<typeof CardInfos>;

export default meta;
type Story = StoryObj<typeof CardInfos>;

const treeInfosFixtures: CardInfosProps["infos"] = {
  id: "Id",
  location_id: "Id",
  tree_id: "Id",
  tree_serial_number: "serial number",
  tree_height: "5",
  tree_scientific_name: "Scientific name",
  tree_vernacular_name: "Vernacular name",
  plantation_date: "2020-04-25",
  status_name: "alive",
};

export const Test: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <CardInfos
        infos={treeInfosFixtures}
        handleCloseCardInfos={() => console.log("handleCloseCardInfos")}
      />
    </Box>
  ),
};
