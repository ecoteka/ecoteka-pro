import React, { FC } from "react";
import BasemapSelector from "@blocks/map/BasemapSelector";

interface BasemapManagerProps {}

const BasemapManager: React.FC<BasemapManagerProps> = (props) => {
  return (
    <BasemapSelector
      handleOnBaseMapChange={() => console.log("handle on base map change")}
    />
  );
};

export default BasemapManager;
