import React from 'react';
import { Box, Paper, Button, Typography } from '@mui/material';
import { useTranslation } from "react-i18next";
import { useRouter } from "next/router";
import { useDeviceSize } from "@lib/utils";

export const DashboardWidgetItem = ({ icon: Icon, title, description, dataCy }) => {
  const { t } = useTranslation(["components"]);
  const { locale, replace } = useRouter();
  const { isDesktop, isTablet, isMobile } = useDeviceSize();

  return (
    <Paper elevation={1} data-cy={dataCy} sx={{ width: isMobile ? "98%" : isTablet ? "48%" : "23%", padding: "25px 20px", display: "flex", flexDirection: "column", mb: isDesktop ? "" : 4 }}>
      <Box sx={{ display: "flex", borderBottom: "1px solid rgba(0,0,0,.1)", pb: 2}}>
        <Box sx={{ width: '50px' }}>
          <Icon />
        </Box>
        <Box sx={{ flexGrow: 1, flexBasis: `calc(100% - 75px)`, display: "flex", flexDirection: "column", justifyContent: "space-around", ml: 2, overflow: "hidden", textOverflow: "ellipsis" }}>
          <Typography sx={{ fontWeight: 500 }}>{title}</Typography>
          <Typography
            sx={{
              fontSize: "12px",
              width: "92%",
              whiteSpace: "nowrap",
              overflow: "hidden",
              textOverflow: "ellipsis",
            }}
          >
            {description}
          </Typography>
        </Box>
      </Box>
      <Button sx={{ alignSelf: "center", mt: 2, px: 2 }} onClick={() => replace(`/${locale}`)}>{t("Account.widget.goDashboard")}</Button>
    </Paper>
  );
}
