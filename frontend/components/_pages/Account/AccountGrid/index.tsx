import React, { useMemo, useState, useEffect } from "react";
import {
  MRT_ColumnDef,
  useMaterialReactTable,
  MaterialReactTable,
} from "material-react-table";
import {
  Box,
  Typography,
  IconButton,
  Button,
  Tooltip,
  TextField,
  Container,
  Grid,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import { format } from "date-fns";
import { useTranslation } from "react-i18next";
import { useDeviceSize } from "@lib/utils";
import {
  getKeycloackUsers,
  deleteUser,
  updateUser,
  createUser,
  getKeycloackRoles,
} from "@services/AccountService";
import DeleteIcon from "@mui/icons-material/Delete";
import { ModalComponent } from "@components/forms/modal/ModalComponent";
import { GlobalLoader } from "@components/layout/GlobalLoader";
import EditIcon from "@mui/icons-material/Edit";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import {
  UserData,
  KeycloakUser,
  KeycloakRole,
} from "types/Account/Account.types";
import useTableLocale from "@lib/useTableLocale";

const AccountGrid = () => {
  const { t } = useTranslation("components");
  const { isMobile } = useDeviceSize();
  const [usersData, setUsersData] = useState<UserData[]>([]);
  const localization = useTableLocale();
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [actionType, setActionType] = useState<
    "edit" | "create" | "delete" | ""
  >("");
  const [selectedUser, setSelectedUser] = useState<UserData>({} as UserData);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [reloadCounter, setReloadCounter] = useState<number>(0);
  const [formErrors, setFormErrors] = useState({});
  const [roles, setRoles] = useState<KeycloakRole[]>([]);
  const [newUser, setNewUser] = useState<UserData>({
    id: "",
    first_name: "",
    last_name: "",
    email: "",
    role: "",
    created_timestamp: undefined,
  } as UserData);
  const excludedKeys: string[] = ["id", "role", "created_timestamp"];

  const columns = useMemo(() => {
    if (usersData && usersData.length > 0) {
      const columnName: string[] = Object.keys(usersData[0]).filter(
        (key) => key !== "__typename"
      );

      return columnName.map((key) => ({
        id: key,
        accessorKey: key,
        header: t(`Account.grid.${key}`),
        Cell:
          key === "created_timestamp"
            ? ({ cell }) =>
                cell.getValue()
                  ? format(
                      new Date(cell.getValue() as string | number),
                      "dd/MM/yyyy"
                    )
                  : ""
            : undefined,
        size: 5,
        grow: true,
        enableEditing: key !== "id" && key !== "created_timestamp",
      })) as MRT_ColumnDef<UserData>[];
    }
    return [];
  }, [usersData, isMobile, t]);

  const validateField = (id, value) => {
    let newFormErrors = {};
    if (id === "email") {
      const isValidEmail =
        /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value);
      if (!isValidEmail) {
        newFormErrors[id] = t("Account.error.emailInvalid");
      } else {
        delete newFormErrors[id];
      }
    } else if (value === "") {
      newFormErrors[id] = value ? "" : t("Account.error.required");
    }

    setFormErrors(newFormErrors);
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setNewUser((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));

    validateField(name, value);
  };

  const handleOpenModal = (type: "edit" | "create" | "delete") => {
    setModalOpen(true);
    setActionType(type);
    setFormErrors({});
  };

  const handleCloseModal = () => {
    setModalOpen(false);
    setActionType("");
    setNewUser({
      id: "",
      first_name: "",
      last_name: "",
      email: "",
      team: "",
      role: "",
      created_timestamp: undefined,
    });
  };

  const handleDelete = async () => {
    setIsLoading(true);
    try {
      await deleteUser(selectedUser.id);
      setReloadCounter((prev) => prev + 1);
    } finally {
      setIsLoading(false);
      setModalOpen(false);
      setActionType("");
    }
  };

  useEffect(() => {
    const getUsers = async () => {
      try {
        const users: KeycloakUser[] = await getKeycloackUsers();
        const filteredUsers = users
          .filter((user) => user.createdTimestamp)
          .map((u) => {
            return {
              id: u.id,
              first_name: u.firstName,
              last_name: u.lastName,
              email: u.email,
              role: u.attributes.role,
              created_timestamp: u.createdTimestamp,
            };
          });
        setUsersData(filteredUsers);
      } catch (error) {
        console.error(
          "Erreur lors de la récupération des utilisateurs Keycloak",
          error
        );
      }
    };
    const getRoles = async () => {
      try {
        const allRoles: KeycloakRole[] = await getKeycloackRoles();
        setRoles(allRoles);
      } catch (error) {
        console.error(
          "Erreur lors de la récupération des roles Keycloak",
          error
        );
      }
    };
    getUsers();
    getRoles();
  }, [reloadCounter]);

  const handleSaveEditedUser = async ({ values, table }) => {
    setIsLoading(true);
    try {
      const updateData = {
        firstName: values.first_name,
        lastName: values.last_name,
        email: values.email,
        attributes: {
          role: values.role,
        },
      };
      await updateUser(values.id, updateData);
      table.setEditingRow(null);
      setReloadCounter((prev) => prev + 1);
    } finally {
      setIsLoading(false);
    }
  };

  const areAllFieldsFilled = () => {
    const allFieldsFilled = Object.keys(newUser)
      .filter((key) => !excludedKeys.includes(key))
      .every((key) => newUser[key]);
    return allFieldsFilled && Object.keys(formErrors).length === 0;
  };

  const handleCreateUser = async () => {
    setIsLoading(true);
    try {
      const updateData = {
        firstName: newUser.first_name,
        lastName: newUser.last_name,
        email: newUser.email,
        attributes: {
          team: newUser.team,
          role: newUser.role,
        },
      };
      const new_user = await createUser(updateData);
      if (typeof new_user !== "string") {
        const badMail = /same email/i;
        setModalOpen(true);
        if (badMail.test(new_user.detail)) {
          setFormErrors({
            email: t("Account.error.sameEmail"),
          });
        } else {
          setFormErrors({
            first_name: t("Account.error.sameUsername"),
            last_name: t("Account.error.sameUsername"),
          });
        }
      } else {
        setFormErrors({});
        handleCloseModal();
        setReloadCounter((prev) => prev + 1);
      }
    } finally {
      setIsLoading(false);
    }
  };

  const table = useMaterialReactTable({
    columns: columns as any,
    data: usersData || [],
    localization: localization,
    enableGlobalFilter: false,
    enableTopToolbar: true,
    enablePagination: true,
    enableRowActions: true,
    enableColumnResizing: true,
    initialState: {
      columnVisibility: { id: false },
      pagination: { pageIndex: 0, pageSize: 30 },
    },
    onEditingRowSave: handleSaveEditedUser,
    renderRowActions: ({ row, table }) => (
      <Box sx={{ display: "flex", width: 10 }}>
        <Tooltip title={t("Account.grid.edit")}>
          <IconButton
            onClick={() => {
              table.setEditingRow(row);
            }}
          >
            <EditIcon
              sx={{
                fontSize: 18,
                "&:hover": { color: "rgb(47, 163, 124)" },
              }}
            />
          </IconButton>
        </Tooltip>
        <Tooltip title={t("Account.grid.delete")}>
          <IconButton
            sx={{ color: 10 }}
            onClick={() => {
              setSelectedUser(row.original);
              handleOpenModal("delete");
            }}
          >
            <DeleteIcon
              sx={{
                fontSize: 18,
                color: "rgba(240,128,128,.8)",
                "&:hover": { color: "rgb(220,20,60)" },
              }}
            />
          </IconButton>
        </Tooltip>
      </Box>
    ),
    renderBottomToolbarCustomActions: ({ table }) => (
      <Tooltip title={t("Account.grid.create")}>
        <AddCircleIcon
          sx={{
            fontSize: 36,
            color: "primary.main",
            opacity: 0.7,
            cursor: "pointer",
            marginLeft: "50%",
            "&:hover": { opacity: 1 },
          }}
          onClick={() => {
            handleOpenModal("create");
          }}
        />
      </Tooltip>
    ),
  });

  return (
    <Box sx={{ mt: 2, mb: 4, padding: "10px 20px" }}>
      <Typography variant="h6" sx={{ my: 3, pl: 1 }}>
        {t("Organization.Tabs.members")}
      </Typography>
      <MaterialReactTable table={table} />
      {isLoading ? (
        <GlobalLoader sx={{ backgroundColor: "rgba(0,0,0,.4)" }} />
      ) : (
        <ModalComponent open={modalOpen} handleClose={handleCloseModal}>
          <Box sx={{ textAlign: "center" }}>
            {actionType === "create" ? (
              <Container sx={{ width: "30vw" }}>
                <Typography sx={{ mb: 5, fontSize: 20, fontWeight: 500 }}>
                  {t("Account.grid.create")}
                </Typography>
                <Grid container spacing={2} direction="column">
                  {Object.keys(newUser)
                    .filter((key) => !excludedKeys.includes(key))
                    .map((key) => {
                      return (
                        <Grid item key={key} sx={{ pb: 1 }}>
                          <TextField
                            key={key}
                            name={key}
                            fullWidth
                            label={t(`Account.grid.${key}`)}
                            value={newUser[key] || ""}
                            onChange={handleChange}
                            error={!!formErrors[key]}
                            helperText={formErrors[key] || ""}
                            FormHelperTextProps={{
                              style: {
                                fontSize: "0.6rem",
                                position: "absolute",
                                bottom: -17,
                              },
                            }}
                            InputProps={{
                              sx: {
                                borderColor: formErrors[key] ? "red" : "",
                                "&:hover fieldset": {
                                  borderColor: formErrors[key] ? "red" : "",
                                },
                                "&.Mui-focused fieldset": {
                                  borderColor: formErrors[key] ? "red" : "",
                                },
                              },
                            }}
                            InputLabelProps={{
                              sx: {
                                color: formErrors[key] ? "red" : "",
                                ".Mui-focused": {
                                  color: formErrors[key] ? "red" : "",
                                },
                              },
                            }}
                          />
                        </Grid>
                      );
                    })}
                  <FormControl
                    fullWidth
                    variant="outlined"
                    sx={{ my: 2, ml: 2 }}
                  >
                    <InputLabel id="role-label">
                      {t("Account.grid.role")}
                    </InputLabel>
                    <Select
                      labelId="role-label"
                      name="role"
                      value={newUser.role || ""}
                      onChange={handleChange}
                      label={t("Account.grid.role")}
                    >
                      {roles.map((role) => (
                        <MenuItem key={role.id} value={role.name}>
                          {role.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>
              </Container>
            ) : actionType === "delete" ? (
              <Typography sx={{ m: 3, fontSize: 20, fontWeight: 500 }}>{`${t(
                "Account.modal.deleteMessage"
              )} ${selectedUser.first_name} ${
                selectedUser.last_name
              } ?`}</Typography>
            ) : null}

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-around",
                mt: 6,
                mb: 3,
              }}
            >
              <Button
                sx={{ width: "35%" }}
                variant="outlined"
                onClick={handleCloseModal}
              >
                {t("Account.modal.cancel")}
              </Button>
              {actionType === "delete" ? (
                <Button
                  sx={{ width: "35%" }}
                  variant="contained"
                  onClick={() => {
                    handleDelete();
                  }}
                >
                  {t("Account.modal.confirm")}
                </Button>
              ) : (
                <Button
                  disabled={!areAllFieldsFilled()}
                  sx={{ width: "35%" }}
                  variant="contained"
                  onClick={() => {
                    handleCreateUser();
                  }}
                >
                  {t("Account.save")}
                </Button>
              )}
            </Box>
          </Box>
        </ModalComponent>
      )}
    </Box>
  );
};

export default AccountGrid;
