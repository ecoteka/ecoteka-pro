import { useMemo } from "react";
import { useTranslation } from "react-i18next";
import {
  MRT_ColumnDef,
  useMaterialReactTable,
  MaterialReactTable,
} from "material-react-table";
import useTableLocale from "@lib/useTableLocale";
import { IWorksitesGridProps } from "./WorksitesGrid.types";
import { useRouter } from "next/router";

import { Box, IconButton } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";

const WorksitesGrid = ({ worksites }: IWorksitesGridProps) => {
  const { t } = useTranslation(["components"]);
  const router = useRouter();
  const localization = useTableLocale();

  const columns = useMemo<MRT_ColumnDef<any>[]>(
    () => [
      {
        accessorFn: (originalRow) => originalRow.name,
        header: t("WorksiteForm.name"),
      },
      {
        accessorFn: (originalRow) => {
          return <span>{t(`common.${originalRow.location_status}`)}</span>;
        },
        header: t("WorksiteForm.location_status"),
      },
      {
        accessorFn: (originalRow) =>
          t(
            `components.PanelTree.interventionsList.${originalRow.interventions[0].intervention.intervention_type.slug}`
          ),
        header: t(`components.Intervention.properties.interventionType`),
      },
      {
        accessorFn: (originalRow) =>
          originalRow.interventions[0].intervention.intervention_partner?.name,
        header: t(`components.Intervention.properties.interventionPartner`),
      },
      {
        accessorFn: (originalRow) =>
          new Date(originalRow.interventions[0].intervention.scheduled_date),
        header: t(`components.Intervention.properties.scheduledDate`),
        filterVariant: "date-range",
        filterFn: "dateRangeBetweenInclusive",
        size: 350,
        Cell: ({ cell }) => {
          const dateValue = cell.getValue<Date>();

          return dateValue.toLocaleDateString(); // Format date for display
        },
      },
    ],
    []
  );

  const table = useMaterialReactTable({
    columns: columns as any,
    data: worksites,
    localization: localization,
    enableGlobalFilter: false,
    enableTopToolbar: true,
    enablePagination: true,
    enableRowActions: true,
    enableColumnResizing: true,
    getRowId: (originalRow) => originalRow.id,
    filterFns: {
      dateRangeBetweenInclusive: (row, id, filterValues) => {
        const rowDate = new Date(
          row.original?.interventions[0].intervention.scheduled_date
        );

        const filterStartDate = filterValues[0] ? filterValues[0] : null;
        const filterEndDate = filterValues[1] ? filterValues[1] : null;

        if (filterStartDate || filterEndDate) {
          rowDate.setTime(
            rowDate.getTime() + rowDate.getTimezoneOffset() * 60 * 1000
          );
        }
        return (
          (!filterStartDate || rowDate >= filterStartDate) &&
          (!filterEndDate || rowDate <= filterEndDate)
        );
      },
    },
    initialState: {
      density: "compact",
      pagination: { pageIndex: 0, pageSize: 30 },
    },
    renderRowActions: ({ row }) => (
      <Box>
        <IconButton
          onClick={() => router.push(`/worksites/${row.original.id}`)}
        >
          <EditIcon />
        </IconButton>
      </Box>
    ),

    positionActionsColumn: "first",
  });

  return <MaterialReactTable table={table} />;
};

export default WorksitesGrid;
