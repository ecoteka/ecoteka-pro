import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Home from "@pages/index";
// !! : Test failed, see: https://react.i18next.com/misc/testing
describe("Home", () => {
  it("renders a stack link button to map page", () => {
    render(<Home />);
    expect(screen.getByTestId("map-button-stack")).toBeInTheDocument();
  });
});
