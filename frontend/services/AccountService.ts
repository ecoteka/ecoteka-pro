import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();

export const getKeycloackUsers = async (): Promise<any> => {
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/keycloack/users`, {
        method: 'GET',
    });

    const data = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};


export const deleteUser = async (userId) => {
  try {
    const response = await fetch(`${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/keycloack/users/delete`, {
      method: 'DELETE',
      headers: {
        'user-id': userId
      },
    });
    return await response.json();
  } catch (error) {
    console.error("Error deleting user: ", error);
    throw error;
  }
};


export const updateUser = async (userId, userData) => {
  try {
    const response = await fetch(`${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/keycloack/users/update`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'user-id': userId
      },
      body: JSON.stringify(userData),
    });

    return await response.json();
  } catch (error) {
    console.error("Error updating user: ", error);
    throw error;
  }
};


export const createUser = async (userData) => {
  try {
    const response = await fetch(`${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/keycloack/users/create`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(userData),
    });
    return await response.json();
  } catch (error) {
    console.error("Error creating user: ", error);
    throw error;
  }
};


export const setPassword = async (userId, newPassword) => {
  try {
    const response = await fetch(`${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/keycloack/users/password`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'user-id': userId
      },
      body: JSON.stringify(newPassword),
    });
    return await response.json();
  } catch (error) {
    console.error("Error resetting password: ", error);
    throw error;
  }
};

export const getKeycloackRoles = async (): Promise<any> => {
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/keycloack/roles`, {
        method: 'GET',
    });

    const data = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};
