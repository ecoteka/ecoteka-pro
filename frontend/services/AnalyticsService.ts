import getConfig from "next/config";
import {
  OrganizationData,
  LateIntervention,
  BoundariesData,
  NotableTree,
} from "types/analytics/analytics.types";

export interface LateInterventionsByFamilyId {
  [familyId: string]: LateIntervention[];
}

const { publicRuntimeConfig } = getConfig();

export const getLateInterventionByFamily = async (
  boundaryIds
): Promise<any> => {
  const params = {
    boundary_ids: boundaryIds,
  };
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/widget/get_late_interventions_by_family`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      }
    );
    if (!response.ok) {
      throw new Error(`Error: ${response.status}`);
    }

    const data: LateInterventionsByFamilyId[] = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};

export const getAllFamiliesInterventionType = async (): Promise<any> => {
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/widget/get_all_families_intervention_type`
    );
    if (!response.ok) {
      throw new Error(`Error: ${response.status}`);
    }

    const data: LateInterventionsByFamilyId[] = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};

export const getDefaultOrganization = async (): Promise<any> => {
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/organization/default`
    );
    if (!response.ok) {
      throw new Error(`Error: ${response.status}`);
    }

    const data: OrganizationData = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};

export const getBoundariesExceptStations = async (): Promise<any> => {
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/boundary/except/stations`
    );
    if (!response.ok) {
      throw new Error(`Error: ${response.status}`);
    }

    const data: BoundariesData = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};

export const getDangerousTree = async (boundaryIds): Promise<any> => {
  const params = {
    boundary_ids: boundaryIds,
  };
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/diagnosis/dangerous`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      }
    );
    if (!response.ok) {
      throw new Error(`Error: ${response.status}`);
    }

    const data: BoundariesData = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};

export const getHealthScore = async (boundaryIds): Promise<any> => {
  const params = {
    boundary_ids: boundaryIds,
  };
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/metrics/health`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      }
    );
    if (!response.ok) {
      throw new Error(`Error: ${response.status}`);
    }

    const data = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};

export const getUrbasenseMetrics = async (params): Promise<any> => {
  const section = {
    id: null,
    ids: params.ids,
    provider: "urbasense",
    type: "site",
  };
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/urbasense/metrics/by_group`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(section),
      }
    );
    if (!response.ok) {
      throw new Error(`Error: ${response.status}`);
    }

    const data = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};

export const urbasenseGetMetricsByGroup = async (boundaryIds): Promise<any> => {
  const params = {
    boundary_ids: boundaryIds,
  };
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/urbasense/tree/subject`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      }
    );
    if (!response.ok) {
      throw new Error(`Error: ${response.status}`);
    }

    const data = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};

export const getAllEcosystemMetrics = async (boundaryIds): Promise<any> => {
  const params = {
    boundary_ids: boundaryIds,
  };
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/metrics/get_all_ecosystem_scores`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      }
    );
    if (!response.ok) {
      throw new Error(`Error: ${response.status}`);
    }

    const data = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};

export const getResilienceScore = async (boundaryIds): Promise<any> => {
  const params = {
    boundary_ids: boundaryIds,
  };
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/metrics/resilience`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      }
    );
    if (!response.ok) {
      throw new Error(`Error: ${response.status}`);
    }

    const data = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};

export const getTaxonomy = async (boundaryIds): Promise<any> => {
  const params = {
    boundary_ids: boundaryIds,
  };
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/metrics/get_taxonomy_aggregates`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      }
    );
    if (!response.ok) {
      throw new Error(`Error: ${response.status}`);
    }

    const data = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};

export const getNotableTrees = async (boundaryIds): Promise<any> => {
  const params = {
    boundary_ids: boundaryIds,
  };
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/metrics/tree/notable`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      }
    );
    if (!response.ok) {
      throw new Error(`Error: ${response.status}`);
    }

    const data: NotableTree[] = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};
