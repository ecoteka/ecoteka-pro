import getConfig from "next/config";
export const getFilterWithMartinDataService = async (): Promise<any> => {
  const { publicRuntimeConfig } = getConfig();
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/filters/filter_with_martin`
    );
    if (!response.ok) {
      throw new Error(`Error: ${response.status}`);
    }

    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};
