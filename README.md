<img src="frontend/public/logo.svg" alt="logo" />

# An application for inventoring, mapping and monitoring tree heritage. 🌳

## Developed by [Natural Solutions](https://www.natural-solutions.eu/)

🔍 More infos [here](https://www.natural-solutions.eu/ecoteka)

English version: https://www.natural-solutions.world/

## Requirements

- Docker 19.03.6+
- Compose 1.28.0+
- 2 CPU Cores
- 4 GB RAM
- 20 GB Free Disk Space

## Install environment for development

You need to login to the gitlab registry the first time in order to download the images. For that, you have to execute the following command:

```console
docker login registry.gitlab.com
```

## Dev environmemt with docker

```bash
./scripts/docker.sh pull
./scripts/docker.sh build
./scripts/docker.sh up -d (or npm run d:up)
```

Then, go to api/rest/docs, authenticate and call admin endpoints:

```
/setup : create your organization with a wikidata code (for page value)
/setup_storage : create a default bucket on minio for file uploads
/seed_taxa : seed 'taxon' table on database
/index_taxa : create taxa index on meilisearch service
```

If you need to remove all your containers and volumes :

```bash
npm run d:down:all
```

## Run integration tests with cypress

- You need to install modules:

        yarn
        or
        npm install

- In WSL2, you need to have dependencies and a x-server installed. Check [the installation docs](https://docs.cypress.io/guides/getting-started/installing-cypress)
- Once x-server is launched run
  npx cypress open
  or
  yarn run cypress open

The front page should be visible at

    http://localhost:8888/

## Running Docker and accessing the worker container

To run the Docker container and access the worker container, execute the following command:

```bash
./scripts/docker.sh exec worker bash
```

This command will start the worker container and open up a Bash terminal session inside it. From here, you can execute commands inside the container.

## Running a Celery task

To run a Celery task, execute the following command:

```bash
celery -A workers.run call workers.imports.tasks.toto
```

This command will call the toto task defined in the workers.imports.tasks module using the Celery application defined in the workers.run module. The task will be executed by the Celery worker running inside the container.
